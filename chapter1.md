<html>
<head><meta http-equiv=Content-Type content="text/html; charset=UTF-8">
<style type="text/css">
<!--
span.cls_002{font-family:Tahoma,serif;font-size:32.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_002{font-family:Tahoma,serif;font-size:32.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_003{font-family:Arial,serif;font-size:32.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_003{font-family:Arial,serif;font-size:32.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_004{font-family:Arial,serif;font-size:21.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_004{font-family:Arial,serif;font-size:21.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_005{font-family:Arial,serif;font-size:28.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_005{font-family:Arial,serif;font-size:28.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_006{font-family:Arial,serif;font-size:12.1px;color:rgb(137,136,137);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_006{font-family:Arial,serif;font-size:12.1px;color:rgb(137,136,137);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_007{font-family:Tahoma,serif;font-size:40.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_007{font-family:Tahoma,serif;font-size:40.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_008{font-family:Arial,serif;font-size:15.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_008{font-family:Arial,serif;font-size:15.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_009{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_009{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_012{font-family:Arial,serif;font-size:14.0px;color:rgb(0,0,0);font-weight:bold;font-style:italic;text-decoration: none}
div.cls_012{font-family:Arial,serif;font-size:14.0px;color:rgb(0,0,0);font-weight:bold;font-style:italic;text-decoration: none}
span.cls_010{font-family:Arial,serif;font-size:18.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_010{font-family:Arial,serif;font-size:18.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_011{font-family:Arial,serif;font-size:18.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_011{font-family:Arial,serif;font-size:18.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_013{font-family:Arial,serif;font-size:12.1px;color:rgb(0,0,0);font-weight:bold;font-style:italic;text-decoration: none}
div.cls_013{font-family:Arial,serif;font-size:12.1px;color:rgb(0,0,0);font-weight:bold;font-style:italic;text-decoration: none}
span.cls_014{font-family:"Monotype Corsiva",serif;font-size:18.1px;color:rgb(11,122,156);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_014{font-family:"Monotype Corsiva",serif;font-size:18.1px;color:rgb(11,122,156);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_015{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_015{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_016{font-family:Arial,serif;font-size:20.1px;color:rgb(11,122,156);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_016{font-family:Arial,serif;font-size:20.1px;color:rgb(11,122,156);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_017{font-family:Arial,serif;font-size:13.0px;color:rgb(11,122,156);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_017{font-family:Arial,serif;font-size:13.0px;color:rgb(11,122,156);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_018{font-family:"Monotype Corsiva",serif;font-size:15.1px;color:rgb(11,122,156);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_018{font-family:"Monotype Corsiva",serif;font-size:15.1px;color:rgb(11,122,156);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_019{font-family:Arial,serif;font-size:11.0px;color:rgb(11,122,156);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_019{font-family:Arial,serif;font-size:11.0px;color:rgb(11,122,156);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_020{font-family:Arial,serif;font-size:16.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_020{font-family:Arial,serif;font-size:16.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_021{font-family:Arial,serif;font-size:14.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_021{font-family:Arial,serif;font-size:14.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_022{font-family:Tahoma,serif;font-size:22.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_022{font-family:Tahoma,serif;font-size:22.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_023{font-family:Tahoma,serif;font-size:24.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_023{font-family:Tahoma,serif;font-size:24.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_024{font-family:Arial,serif;font-size:12.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_024{font-family:Arial,serif;font-size:12.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_025{font-family:"Monotype Corsiva",serif;font-size:24.1px;color:rgb(11,122,156);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_025{font-family:"Monotype Corsiva",serif;font-size:24.1px;color:rgb(11,122,156);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_026{font-family:Arial,serif;font-size:24.1px;color:rgb(11,122,156);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_026{font-family:Arial,serif;font-size:24.1px;color:rgb(11,122,156);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_027{font-family:Arial,serif;font-size:22.0px;color:rgb(11,122,156);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_027{font-family:Arial,serif;font-size:22.0px;color:rgb(11,122,156);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_028{font-family:Arial,serif;font-size:22.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_028{font-family:Arial,serif;font-size:22.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_029{font-family:"Monotype Corsiva",serif;font-size:17.1px;color:rgb(11,122,156);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_029{font-family:"Monotype Corsiva",serif;font-size:17.1px;color:rgb(11,122,156);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_030{font-family:"Monotype Corsiva",serif;font-size:21.1px;color:rgb(11,122,156);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_030{font-family:"Monotype Corsiva",serif;font-size:21.1px;color:rgb(11,122,156);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_031{font-family:Arial,serif;font-size:28.1px;color:rgb(11,122,156);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_031{font-family:Arial,serif;font-size:28.1px;color:rgb(11,122,156);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_032{font-family:Times,serif;font-size:12.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_032{font-family:Times,serif;font-size:12.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_035{font-family:Arial,serif;font-size:28.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_035{font-family:Arial,serif;font-size:28.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_033{font-family:Arial,serif;font-size:5.7px;color:rgb(255,255,255);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_033{font-family:Arial,serif;font-size:5.7px;color:rgb(255,255,255);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_034{font-family:Arial,serif;font-size:5.7px;color:rgb(255,255,255);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_034{font-family:Arial,serif;font-size:5.7px;color:rgb(255,255,255);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_036{font-family:Arial,serif;font-size:5.7px;color:rgb(1,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_036{font-family:Arial,serif;font-size:5.7px;color:rgb(1,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_037{font-family:Arial,serif;font-size:5.7px;color:rgb(255,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_037{font-family:Arial,serif;font-size:5.7px;color:rgb(255,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_039{font-family:Tahoma,serif;font-size:18.1px;color:rgb(204,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_039{font-family:Tahoma,serif;font-size:18.1px;color:rgb(204,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_040{font-family:Arial,serif;font-size:18.1px;color:rgb(255,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_040{font-family:Arial,serif;font-size:18.1px;color:rgb(255,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_042{font-family:Arial,serif;font-size:14.0px;color:rgb(255,255,255);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_042{font-family:Arial,serif;font-size:14.0px;color:rgb(255,255,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_041{font-family:Arial,serif;font-size:20.1px;color:rgb(41,131,135);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_041{font-family:Arial,serif;font-size:20.1px;color:rgb(41,131,135);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_043{font-family:Arial,serif;font-size:13.1px;color:rgb(255,255,255);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_043{font-family:Arial,serif;font-size:13.1px;color:rgb(255,255,255);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_047{font-family:Arial,serif;font-size:14.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_047{font-family:Arial,serif;font-size:14.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_044{font-family:Arial,serif;font-size:13.1px;color:rgb(255,255,255);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_044{font-family:Arial,serif;font-size:13.1px;color:rgb(255,255,255);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_045{font-family:Arial,serif;font-size:13.1px;color:rgb(1,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_045{font-family:Arial,serif;font-size:13.1px;color:rgb(1,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_046{font-family:Arial,serif;font-size:13.1px;color:rgb(255,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_046{font-family:Arial,serif;font-size:13.1px;color:rgb(255,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_050{font-family:Arial,serif;font-size:12.8px;color:rgb(255,255,255);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_050{font-family:Arial,serif;font-size:12.8px;color:rgb(255,255,255);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_049{font-family:Arial,serif;font-size:12.8px;color:rgb(255,255,255);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_049{font-family:Arial,serif;font-size:12.8px;color:rgb(255,255,255);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_051{font-family:Arial,serif;font-size:12.8px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_051{font-family:Arial,serif;font-size:12.8px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_052{font-family:Arial,serif;font-size:12.8px;color:rgb(255,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_052{font-family:Arial,serif;font-size:12.8px;color:rgb(255,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_054{font-family:Arial,serif;font-size:14.0px;color:rgb(0,0,204);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_054{font-family:Arial,serif;font-size:14.0px;color:rgb(0,0,204);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_055{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_055{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_056{font-family:Arial,serif;font-size:16.0px;color:rgb(0,106,97);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_056{font-family:Arial,serif;font-size:16.0px;color:rgb(0,106,97);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_057{font-family:Arial,serif;font-size:20.1px;color:rgb(204,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_057{font-family:Arial,serif;font-size:20.1px;color:rgb(204,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_058{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_058{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_059{font-family:Arial,serif;font-size:17.1px;color:rgb(11,122,156);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_059{font-family:Arial,serif;font-size:17.1px;color:rgb(11,122,156);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_060{font-family:Times,serif;font-size:24.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_060{font-family:Times,serif;font-size:24.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_061{font-family:Times,serif;font-size:22.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_061{font-family:Times,serif;font-size:22.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_062{font-family:Times,serif;font-size:16.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_062{font-family:Times,serif;font-size:16.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_063{font-family:Arial,serif;font-size:18.1px;color:rgb(0,0,0);font-weight:bold;font-style:italic;text-decoration: none}
div.cls_063{font-family:Arial,serif;font-size:18.1px;color:rgb(0,0,0);font-weight:bold;font-style:italic;text-decoration: none}
span.cls_066{font-family:Tahoma,serif;font-size:18.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_066{font-family:Tahoma,serif;font-size:18.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_064{font-family:Tahoma,serif;font-size:16.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_064{font-family:Tahoma,serif;font-size:16.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_065{font-family:Tahoma,serif;font-size:16.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_065{font-family:Tahoma,serif;font-size:16.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_067{font-family:Tahoma,serif;font-size:20.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_067{font-family:Tahoma,serif;font-size:20.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_068{font-family:Arial,serif;font-size:16.0px;color:rgb(11,122,156);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_068{font-family:Arial,serif;font-size:16.0px;color:rgb(11,122,156);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_069{font-family:Arial,serif;font-size:10.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_069{font-family:Arial,serif;font-size:10.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_070{font-family:Arial,serif;font-size:7.3px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_070{font-family:Arial,serif;font-size:7.3px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_071{font-family:Arial,serif;font-size:3.3px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_071{font-family:Arial,serif;font-size:3.3px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_072{font-family:Arial,serif;font-size:4.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_072{font-family:Arial,serif;font-size:4.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_073{font-family:Arial,serif;font-size:5.5px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_073{font-family:Arial,serif;font-size:5.5px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_074{font-family:Times,serif;font-size:15.4px;color:rgb(255,255,255);font-weight:bold;font-style:italic;text-decoration: none}
div.cls_074{font-family:Times,serif;font-size:15.4px;color:rgb(255,255,255);font-weight:bold;font-style:italic;text-decoration: none}
span.cls_075{font-family:Times,serif;font-size:17.5px;color:rgb(0,0,128);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_075{font-family:Times,serif;font-size:17.5px;color:rgb(0,0,128);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_076{font-family:Times,serif;font-size:20.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_076{font-family:Times,serif;font-size:20.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_077{font-family:Arial,serif;font-size:10.0px;color:rgb(0,111,192);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_077{font-family:Arial,serif;font-size:10.0px;color:rgb(0,111,192);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_078{font-family:Arial,serif;font-size:11.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_078{font-family:Arial,serif;font-size:11.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_079{font-family:Arial,serif;font-size:18.1px;color:rgb(11,122,156);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_079{font-family:Arial,serif;font-size:18.1px;color:rgb(11,122,156);font-weight:normal;font-style:normal;text-decoration: none}
-->
</style>
<script type="text/javascript" src="chapter1/wz_jsgraphics.js"></script>
</head>
<body>
<div style="position:absolute;left:50%;margin-left:-360px;top:0px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter1/background01.jpg" width=720 height=540></div>
<div style="position:absolute;left:153.69px;top:11.44px" class="cls_002"><span class="cls_002">Data Mining: Introduction</span></div>
<div style="position:absolute;left:153.88px;top:129.76px" class="cls_003"><span class="cls_003">Lecture Notes for Chapter 1</span></div>
<div style="position:absolute;left:77.14px;top:221.92px" class="cls_003"><span class="cls_003">Introduction to Data Mining, 2</span><span class="cls_004"><sup>nd</sup></span><span class="cls_003"> Edition</span></div>
<div style="position:absolute;left:336.14px;top:267.92px" class="cls_005"><span class="cls_005">by</span></div>
<div style="position:absolute;left:144.44px;top:307.76px" class="cls_005"><span class="cls_005">Tan, Steinbach, Karpatne, Kumar</span></div>
<div style="position:absolute;left:248.56px;top:500.40px" class="cls_006"><span class="cls_006">Introduction to Data Mining, 2nd Edition</span></div>
<div style="position:absolute;left:43.20px;top:507.60px" class="cls_006"><span class="cls_006">09/09/2020</span></div>
<div style="position:absolute;left:670.08px;top:507.60px" class="cls_006"><span class="cls_006">1</span></div>
<div style="position:absolute;left:269.07px;top:514.56px" class="cls_006"><span class="cls_006">Tan, Steinbach, Karpatne, Kumar</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:550px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter1/background02.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:2.48px" class="cls_007"><span class="cls_007">Large-scale Data is Everywhere!</span></div>
<div style="position:absolute;left:39.50px;top:90.40px" class="cls_008"><span class="cls_008">§</span><span class="cls_009">  There has been enormous data</span></div>
<div style="position:absolute;left:62.50px;top:112.48px" class="cls_009"><span class="cls_009">growth in both commercial and</span></div>
<div style="position:absolute;left:62.50px;top:133.60px" class="cls_009"><span class="cls_009">scientific databases due to</span></div>
<div style="position:absolute;left:62.50px;top:155.44px" class="cls_009"><span class="cls_009">advances in data generation</span></div>
<div style="position:absolute;left:62.50px;top:176.56px" class="cls_009"><span class="cls_009">and collection technologies</span></div>
<div style="position:absolute;left:548.06px;top:188.08px" class="cls_012"><span class="cls_012">E-Commerce</span></div>
<div style="position:absolute;left:403.81px;top:200.56px" class="cls_012"><span class="cls_012">Cyber Security</span></div>
<div style="position:absolute;left:39.50px;top:204.40px" class="cls_008"><span class="cls_008">§</span><span class="cls_009">  New mantra</span></div>
<div style="position:absolute;left:75.50px;top:233.52px" class="cls_010"><span class="cls_010">§</span><span class="cls_011">  Gather whatever data you can</span></div>
<div style="position:absolute;left:102.50px;top:252.48px" class="cls_011"><span class="cls_011">whenever and wherever</span></div>
<div style="position:absolute;left:102.50px;top:271.44px" class="cls_011"><span class="cls_011">possible.</span></div>
<div style="position:absolute;left:39.50px;top:297.52px" class="cls_008"><span class="cls_008">§</span><span class="cls_009">  Expectations</span></div>
<div style="position:absolute;left:75.50px;top:325.44px" class="cls_010"><span class="cls_010">§</span><span class="cls_011">  Gathered data will have value</span></div>
<div style="position:absolute;left:379.20px;top:338.56px" class="cls_012"><span class="cls_012">Traffic Patterns</span></div>
<div style="position:absolute;left:551.88px;top:338.64px" class="cls_013"><span class="cls_013">Social Networking: Twitter</span></div>
<div style="position:absolute;left:102.50px;top:344.40px" class="cls_011"><span class="cls_011">either for the purpose</span></div>
<div style="position:absolute;left:102.50px;top:364.56px" class="cls_011"><span class="cls_011">collected or for a purpose not</span></div>
<div style="position:absolute;left:102.50px;top:383.52px" class="cls_011"><span class="cls_011">envisioned.</span></div>
<div style="position:absolute;left:383.45px;top:476.56px" class="cls_012"><span class="cls_012">Sensor Networks</span></div>
<div style="position:absolute;left:532.08px;top:476.56px" class="cls_012"><span class="cls_012">Computational Simulations</span></div>
<div style="position:absolute;left:248.56px;top:500.40px" class="cls_006"><span class="cls_006">Introduction to Data Mining, 2nd Edition</span></div>
<div style="position:absolute;left:43.20px;top:507.60px" class="cls_006"><span class="cls_006">09/09/2020</span></div>
<div style="position:absolute;left:670.08px;top:507.60px" class="cls_006"><span class="cls_006">2</span></div>
<div style="position:absolute;left:269.07px;top:514.56px" class="cls_006"><span class="cls_006">Tan, Steinbach, Karpatne, Kumar</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:1100px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter1/background03.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.13px;top:23.44px" class="cls_002"><span class="cls_002">Why Data Mining? Commercial Viewpoint</span></div>
<div style="position:absolute;left:30.13px;top:119.52px" class="cls_014"><span class="cls_014">●</span><span class="cls_015"> Lots of data is being collected</span></div>
<div style="position:absolute;left:53.13px;top:145.44px" class="cls_015"><span class="cls_015">and warehoused</span></div>
<div style="position:absolute;left:66.13px;top:178.48px" class="cls_016"><span class="cls_016">-</span><span class="cls_009"> Web data</span></div>
<div style="position:absolute;left:102.13px;top:206.40px" class="cls_017"><span class="cls_017">u</span><span class="cls_011">Google has Peta Bytes of web data</span></div>
<div style="position:absolute;left:102.13px;top:232.56px" class="cls_017"><span class="cls_017">u</span><span class="cls_011">Facebook has billions of active users</span></div>
<div style="position:absolute;left:66.13px;top:257.44px" class="cls_016"><span class="cls_016">-</span><span class="cls_009"> purchases at department/</span></div>
<div style="position:absolute;left:93.13px;top:279.52px" class="cls_009"><span class="cls_009">grocery stores, e-commerce</span></div>
<div style="position:absolute;left:102.13px;top:307.44px" class="cls_017"><span class="cls_017">u</span><span class="cls_011"> Amazon handles millions of visits/day</span></div>
<div style="position:absolute;left:66.13px;top:333.52px" class="cls_016"><span class="cls_016">-</span><span class="cls_009"> Bank/Credit Card transactions</span></div>
<div style="position:absolute;left:30.13px;top:361.44px" class="cls_014"><span class="cls_014">●</span><span class="cls_015"> Computers have become cheaper and more powerful</span></div>
<div style="position:absolute;left:30.13px;top:393.60px" class="cls_014"><span class="cls_014">●</span><span class="cls_015"> Competitive Pressure is Strong</span></div>
<div style="position:absolute;left:66.13px;top:426.40px" class="cls_016"><span class="cls_016">-</span><span class="cls_009"> Provide better, customized services for an edge (e.g. in</span></div>
<div style="position:absolute;left:93.13px;top:448.48px" class="cls_009"><span class="cls_009">Customer Relationship Management)</span></div>
<div style="position:absolute;left:248.56px;top:500.40px" class="cls_006"><span class="cls_006">Introduction to Data Mining, 2nd Edition</span></div>
<div style="position:absolute;left:43.20px;top:507.60px" class="cls_006"><span class="cls_006">09/09/2020</span></div>
<div style="position:absolute;left:670.08px;top:507.60px" class="cls_006"><span class="cls_006">3</span></div>
<div style="position:absolute;left:269.07px;top:514.56px" class="cls_006"><span class="cls_006">Tan, Steinbach, Karpatne, Kumar</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:1650px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter1/background04.jpg" width=720 height=540></div>
<div style="position:absolute;left:12.00px;top:23.44px" class="cls_002"><span class="cls_002">Why Data Mining? Scientific Viewpoint</span></div>
<div style="position:absolute;left:19.13px;top:108.40px" class="cls_018"><span class="cls_018">●</span><span class="cls_009"> Data collected and stored at</span></div>
<div style="position:absolute;left:42.13px;top:130.48px" class="cls_009"><span class="cls_009">enormous speeds</span></div>
<div style="position:absolute;left:55.13px;top:165.52px" class="cls_016"><span class="cls_016">-</span><span class="cls_009"> remote sensors on a satellite</span></div>
<div style="position:absolute;left:91.13px;top:193.52px" class="cls_019"><span class="cls_019">u</span><span class="cls_020"> NASA EOSDIS archives over</span></div>
<div style="position:absolute;left:91.13px;top:210.56px" class="cls_020"><span class="cls_020">petabytes of earth science data / year</span></div>
<div style="position:absolute;left:557.56px;top:216.16px" class="cls_021"><span class="cls_021">Sky Survey Data</span></div>
<div style="position:absolute;left:374.50px;top:219.52px" class="cls_021"><span class="cls_021">fMRI Data from Brain</span></div>
<div style="position:absolute;left:55.13px;top:241.60px" class="cls_016"><span class="cls_016">-</span><span class="cls_009"> telescopes scanning the skies</span></div>
<div style="position:absolute;left:91.13px;top:267.44px" class="cls_019"><span class="cls_019">u</span><span class="cls_020"> Sky survey data</span></div>
<div style="position:absolute;left:55.13px;top:295.60px" class="cls_016"><span class="cls_016">-</span><span class="cls_009"> High-throughput biological data</span></div>
<div style="position:absolute;left:55.13px;top:334.48px" class="cls_016"><span class="cls_016">-</span><span class="cls_009"> scientific simulations</span></div>
<div style="position:absolute;left:91.13px;top:365.60px" class="cls_017"><span class="cls_017">u</span><span class="cls_020"> terabytes of data generated in a few hours</span></div>
<div style="position:absolute;left:553.13px;top:369.52px" class="cls_021"><span class="cls_021">Gene Expression Data</span></div>
<div style="position:absolute;left:19.13px;top:409.60px" class="cls_018"><span class="cls_018">●</span><span class="cls_009"> Data mining helps scientists</span></div>
<div style="position:absolute;left:55.13px;top:437.44px" class="cls_016"><span class="cls_016">-</span><span class="cls_009"> in automated analysis of massive datasets</span></div>
<div style="position:absolute;left:55.13px;top:465.52px" class="cls_016"><span class="cls_016">-</span><span class="cls_009"> In hypothesis formation</span></div>
<div style="position:absolute;left:248.56px;top:500.40px" class="cls_006"><span class="cls_006">Introduction to Data Mining, 2nd Edition</span></div>
<div style="position:absolute;left:516.46px;top:496.72px" class="cls_021"><span class="cls_021">Surface Temperature of Earth</span></div>
<div style="position:absolute;left:43.20px;top:507.60px" class="cls_006"><span class="cls_006">09/09/2020</span></div>
<div style="position:absolute;left:670.08px;top:507.60px" class="cls_006"><span class="cls_006">4</span></div>
<div style="position:absolute;left:269.07px;top:514.56px" class="cls_006"><span class="cls_006">Tan, Steinbach, Karpatne, Kumar</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:2200px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter1/background05.jpg" width=720 height=540></div>
<div style="position:absolute;left:7.13px;top:24.08px" class="cls_022"><span class="cls_022">Great opportunities to improve productivity in all walks of life</span></div>
<div style="position:absolute;left:248.56px;top:500.40px" class="cls_006"><span class="cls_006">Introduction to Data Mining, 2nd Edition</span></div>
<div style="position:absolute;left:43.20px;top:507.60px" class="cls_006"><span class="cls_006">09/09/2020</span></div>
<div style="position:absolute;left:670.08px;top:507.60px" class="cls_006"><span class="cls_006">5</span></div>
<div style="position:absolute;left:269.07px;top:514.56px" class="cls_006"><span class="cls_006">Tan, Steinbach, Karpatne, Kumar</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:2750px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter1/background06.jpg" width=720 height=540></div>
<div style="position:absolute;left:31.13px;top:21.60px" class="cls_023"><span class="cls_023">Great Opportunities to Solve Society’s Major Problems</span></div>
<div style="position:absolute;left:399.57px;top:281.76px" class="cls_024"><span class="cls_024">Predicting the impact of climate change</span></div>
<div style="position:absolute;left:71.82px;top:285.60px" class="cls_024"><span class="cls_024">Improving health care and reducing costs</span></div>
<div style="position:absolute;left:417.20px;top:471.60px" class="cls_024"><span class="cls_024">Reducing hunger and poverty by</span></div>
<div style="position:absolute;left:70.45px;top:477.60px" class="cls_024"><span class="cls_024">Finding alternative/ green energy sources</span></div>
<div style="position:absolute;left:417.20px;top:485.52px" class="cls_024"><span class="cls_024">increasing agriculture production</span></div>
<div style="position:absolute;left:248.56px;top:500.40px" class="cls_006"><span class="cls_006">Introduction to Data Mining, 2nd Edition</span></div>
<div style="position:absolute;left:43.20px;top:507.60px" class="cls_006"><span class="cls_006">09/09/2020</span></div>
<div style="position:absolute;left:670.08px;top:507.60px" class="cls_006"><span class="cls_006">6</span></div>
<div style="position:absolute;left:269.07px;top:514.56px" class="cls_006"><span class="cls_006">Tan, Steinbach, Karpatne, Kumar</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:3300px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter1/background07.jpg" width=720 height=540></div>
<div style="position:absolute;left:23.13px;top:17.44px" class="cls_002"><span class="cls_002">What is Data Mining?</span></div>
<div style="position:absolute;left:18.63px;top:78.40px" class="cls_025"><span class="cls_025">●</span><span class="cls_003"> Many Definitions</span></div>
<div style="position:absolute;left:54.63px;top:125.52px" class="cls_026"><span class="cls_026">-</span><span class="cls_015"> Non-trivial extraction of implicit, previously unknown</span></div>
<div style="position:absolute;left:81.63px;top:153.60px" class="cls_015"><span class="cls_015">and potentially useful information from data</span></div>
<div style="position:absolute;left:54.63px;top:190.56px" class="cls_026"><span class="cls_026">-</span><span class="cls_015"> Exploration & analysis, by automatic or semi-automatic</span></div>
<div style="position:absolute;left:81.63px;top:217.44px" class="cls_015"><span class="cls_015">means, of large quantities of data in order to discover</span></div>
<div style="position:absolute;left:81.63px;top:244.56px" class="cls_015"><span class="cls_015">meaningful patterns</span></div>
<div style="position:absolute;left:248.56px;top:500.40px" class="cls_006"><span class="cls_006">Introduction to Data Mining, 2nd Edition</span></div>
<div style="position:absolute;left:43.20px;top:507.60px" class="cls_006"><span class="cls_006">09/09/2020</span></div>
<div style="position:absolute;left:670.08px;top:507.60px" class="cls_006"><span class="cls_006">7</span></div>
<div style="position:absolute;left:269.07px;top:514.56px" class="cls_006"><span class="cls_006">Tan, Steinbach, Karpatne, Kumar</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:3850px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter1/background08.jpg" width=720 height=540></div>
<div style="position:absolute;left:24.00px;top:17.44px" class="cls_002"><span class="cls_002">Origins of Data Mining</span></div>
<div style="position:absolute;left:19.13px;top:98.40px" class="cls_014"><span class="cls_014">●</span></div>
<div style="position:absolute;left:42.13px;top:92.40px" class="cls_015"><span class="cls_015">Draws ideas from machine learning/AI, pattern recognition,</span></div>
<div style="position:absolute;left:42.13px;top:121.44px" class="cls_015"><span class="cls_015">statistics, and database systems</span></div>
<div style="position:absolute;left:19.13px;top:193.44px" class="cls_014"><span class="cls_014">●</span><span class="cls_015"> Traditional techniques may be unsuitable due to data that is</span></div>
<div style="position:absolute;left:55.13px;top:228.56px" class="cls_027"><span class="cls_027">-</span><span class="cls_028"> Large-scale</span></div>
<div style="position:absolute;left:55.13px;top:261.44px" class="cls_027"><span class="cls_027">-</span><span class="cls_028"> High dimensional</span></div>
<div style="position:absolute;left:55.13px;top:294.56px" class="cls_027"><span class="cls_027">-</span><span class="cls_028"> Heterogeneous</span></div>
<div style="position:absolute;left:55.13px;top:327.44px" class="cls_027"><span class="cls_027">-</span><span class="cls_028"> Complex</span></div>
<div style="position:absolute;left:55.13px;top:360.56px" class="cls_027"><span class="cls_027">-</span><span class="cls_028"> Distributed</span></div>
<div style="position:absolute;left:19.13px;top:426.56px" class="cls_029"><span class="cls_029">●</span><span class="cls_028"> A key component of the emerging field of data science and data-</span></div>
<div style="position:absolute;left:42.13px;top:453.44px" class="cls_028"><span class="cls_028">driven discovery</span></div>
<div style="position:absolute;left:248.56px;top:500.40px" class="cls_006"><span class="cls_006">Introduction to Data Mining, 2nd Edition</span></div>
<div style="position:absolute;left:43.20px;top:507.60px" class="cls_006"><span class="cls_006">09/09/2020</span></div>
<div style="position:absolute;left:670.08px;top:507.60px" class="cls_006"><span class="cls_006">8</span></div>
<div style="position:absolute;left:269.07px;top:514.56px" class="cls_006"><span class="cls_006">Tan, Steinbach, Karpatne, Kumar</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:4400px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter1/background09.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:11.44px" class="cls_002"><span class="cls_002">Data Mining Tasks</span></div>
<div style="position:absolute;left:43.13px;top:104.48px" class="cls_030"><span class="cls_030">●</span><span class="cls_005"> Prediction Methods</span></div>
<div style="position:absolute;left:79.13px;top:145.52px" class="cls_031"><span class="cls_031">-</span><span class="cls_005"> Use some variables to predict unknown or</span></div>
<div style="position:absolute;left:106.13px;top:179.60px" class="cls_005"><span class="cls_005">future values of other variables.</span></div>
<div style="position:absolute;left:43.13px;top:255.44px" class="cls_030"><span class="cls_030">●</span><span class="cls_005"> Description Methods</span></div>
<div style="position:absolute;left:79.13px;top:296.48px" class="cls_031"><span class="cls_031">-</span><span class="cls_005"> Find human-interpretable patterns that</span></div>
<div style="position:absolute;left:106.13px;top:330.56px" class="cls_005"><span class="cls_005">describe the data.</span></div>
<div style="position:absolute;left:307.20px;top:474.00px" class="cls_032"><span class="cls_032">From [Fayyad, et.al.] Advances in Knowledge Discovery and Data Mining, 1996</span></div>
<div style="position:absolute;left:248.56px;top:500.40px" class="cls_006"><span class="cls_006">Introduction to Data Mining, 2nd Edition</span></div>
<div style="position:absolute;left:43.20px;top:507.60px" class="cls_006"><span class="cls_006">09/09/2020</span></div>
<div style="position:absolute;left:670.08px;top:507.60px" class="cls_006"><span class="cls_006">9</span></div>
<div style="position:absolute;left:269.07px;top:514.56px" class="cls_006"><span class="cls_006">Tan, Steinbach, Karpatne, Kumar</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:4950px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter1/background10.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:11.44px" class="cls_002"><span class="cls_002">Data Mining Tasks …</span></div>
<div style="position:absolute;left:271.20px;top:176.48px" class="cls_035"><span class="cls_035">Data</span></div>
<div style="position:absolute;left:244.68px;top:209.79px" class="cls_033"><span class="cls_033">Tid</span></div>
<div style="position:absolute;left:257.61px;top:209.66px" class="cls_034"><span class="cls_034">Refund</span></div>
<div style="position:absolute;left:282.35px;top:209.66px" class="cls_034"><span class="cls_034">Marital</span></div>
<div style="position:absolute;left:309.78px;top:209.66px" class="cls_034"><span class="cls_034">Taxable</span></div>
<div style="position:absolute;left:282.35px;top:216.10px" class="cls_034"><span class="cls_034">Status</span></div>
<div style="position:absolute;left:309.78px;top:216.10px" class="cls_034"><span class="cls_034">Income</span></div>
<div style="position:absolute;left:336.02px;top:215.13px" class="cls_034"><span class="cls_034">Cheat</span></div>
<div style="position:absolute;left:245.43px;top:229.22px" class="cls_036"><span class="cls_036">1</span></div>
<div style="position:absolute;left:258.36px;top:229.22px" class="cls_036"><span class="cls_036">Yes</span></div>
<div style="position:absolute;left:282.35px;top:229.22px" class="cls_036"><span class="cls_036">Single</span></div>
<div style="position:absolute;left:309.78px;top:229.22px" class="cls_036"><span class="cls_036">125K</span></div>
<div style="position:absolute;left:336.02px;top:229.11px" class="cls_037"><span class="cls_037">No</span></div>
<div style="position:absolute;left:245.43px;top:239.78px" class="cls_036"><span class="cls_036">2</span></div>
<div style="position:absolute;left:258.36px;top:239.78px" class="cls_036"><span class="cls_036">No</span></div>
<div style="position:absolute;left:282.35px;top:239.78px" class="cls_036"><span class="cls_036">Married</span></div>
<div style="position:absolute;left:309.78px;top:239.78px" class="cls_036"><span class="cls_036">100K</span></div>
<div style="position:absolute;left:336.02px;top:239.68px" class="cls_037"><span class="cls_037">No</span></div>
<div style="position:absolute;left:245.43px;top:250.38px" class="cls_036"><span class="cls_036">3</span></div>
<div style="position:absolute;left:258.36px;top:250.38px" class="cls_036"><span class="cls_036">No</span></div>
<div style="position:absolute;left:282.35px;top:250.38px" class="cls_036"><span class="cls_036">Single</span></div>
<div style="position:absolute;left:309.78px;top:250.38px" class="cls_036"><span class="cls_036">70K</span></div>
<div style="position:absolute;left:336.02px;top:250.28px" class="cls_037"><span class="cls_037">No</span></div>
<div style="position:absolute;left:245.43px;top:260.95px" class="cls_036"><span class="cls_036">4</span></div>
<div style="position:absolute;left:258.36px;top:260.95px" class="cls_036"><span class="cls_036">Yes</span></div>
<div style="position:absolute;left:282.35px;top:260.95px" class="cls_036"><span class="cls_036">Married</span></div>
<div style="position:absolute;left:309.78px;top:260.95px" class="cls_036"><span class="cls_036">120K</span></div>
<div style="position:absolute;left:336.02px;top:260.84px" class="cls_037"><span class="cls_037">No</span></div>
<div style="position:absolute;left:245.43px;top:271.58px" class="cls_036"><span class="cls_036">5</span></div>
<div style="position:absolute;left:258.36px;top:271.58px" class="cls_036"><span class="cls_036">No</span></div>
<div style="position:absolute;left:282.35px;top:271.58px" class="cls_036"><span class="cls_036">Divorced</span></div>
<div style="position:absolute;left:309.78px;top:271.58px" class="cls_036"><span class="cls_036">95K</span></div>
<div style="position:absolute;left:336.02px;top:271.48px" class="cls_037"><span class="cls_037">Yes</span></div>
<div style="position:absolute;left:245.43px;top:282.14px" class="cls_036"><span class="cls_036">6</span></div>
<div style="position:absolute;left:258.36px;top:282.14px" class="cls_036"><span class="cls_036">No</span></div>
<div style="position:absolute;left:282.35px;top:282.14px" class="cls_036"><span class="cls_036">Married</span></div>
<div style="position:absolute;left:309.78px;top:282.14px" class="cls_036"><span class="cls_036">60K</span></div>
<div style="position:absolute;left:336.02px;top:282.04px" class="cls_037"><span class="cls_037">No</span></div>
<div style="position:absolute;left:245.43px;top:292.71px" class="cls_036"><span class="cls_036">7</span></div>
<div style="position:absolute;left:258.36px;top:292.71px" class="cls_036"><span class="cls_036">Yes</span></div>
<div style="position:absolute;left:282.35px;top:292.71px" class="cls_036"><span class="cls_036">Divorced</span></div>
<div style="position:absolute;left:309.78px;top:292.71px" class="cls_036"><span class="cls_036">220K</span></div>
<div style="position:absolute;left:336.02px;top:292.61px" class="cls_037"><span class="cls_037">No</span></div>
<div style="position:absolute;left:245.43px;top:303.27px" class="cls_036"><span class="cls_036">8</span></div>
<div style="position:absolute;left:258.36px;top:303.27px" class="cls_036"><span class="cls_036">No</span></div>
<div style="position:absolute;left:282.35px;top:303.27px" class="cls_036"><span class="cls_036">Single</span></div>
<div style="position:absolute;left:309.78px;top:303.27px" class="cls_036"><span class="cls_036">85K</span></div>
<div style="position:absolute;left:336.02px;top:303.17px" class="cls_037"><span class="cls_037">Yes</span></div>
<div style="position:absolute;left:245.43px;top:313.87px" class="cls_036"><span class="cls_036">9</span></div>
<div style="position:absolute;left:258.36px;top:313.87px" class="cls_036"><span class="cls_036">No</span></div>
<div style="position:absolute;left:282.35px;top:313.87px" class="cls_036"><span class="cls_036">Married</span></div>
<div style="position:absolute;left:309.78px;top:313.87px" class="cls_036"><span class="cls_036">75K</span></div>
<div style="position:absolute;left:336.02px;top:313.77px" class="cls_037"><span class="cls_037">No</span></div>
<div style="position:absolute;left:245.43px;top:324.47px" class="cls_036"><span class="cls_036">10</span></div>
<div style="position:absolute;left:258.36px;top:324.47px" class="cls_036"><span class="cls_036">No</span></div>
<div style="position:absolute;left:282.35px;top:324.47px" class="cls_036"><span class="cls_036">Single</span></div>
<div style="position:absolute;left:309.78px;top:324.47px" class="cls_036"><span class="cls_036">90K</span></div>
<div style="position:absolute;left:336.02px;top:324.37px" class="cls_037"><span class="cls_037">Yes</span></div>
<div style="position:absolute;left:245.43px;top:335.07px" class="cls_036"><span class="cls_036">11</span></div>
<div style="position:absolute;left:258.36px;top:335.07px" class="cls_036"><span class="cls_036">No</span></div>
<div style="position:absolute;left:282.35px;top:335.07px" class="cls_036"><span class="cls_036">Married</span></div>
<div style="position:absolute;left:309.78px;top:335.07px" class="cls_036"><span class="cls_036">60K</span></div>
<div style="position:absolute;left:336.02px;top:334.97px" class="cls_037"><span class="cls_037">No</span></div>
<div style="position:absolute;left:245.43px;top:345.67px" class="cls_036"><span class="cls_036">12</span></div>
<div style="position:absolute;left:258.36px;top:345.67px" class="cls_036"><span class="cls_036">Yes</span></div>
<div style="position:absolute;left:282.35px;top:345.67px" class="cls_036"><span class="cls_036">Divorced</span></div>
<div style="position:absolute;left:309.78px;top:345.67px" class="cls_036"><span class="cls_036">220K</span></div>
<div style="position:absolute;left:336.02px;top:345.57px" class="cls_037"><span class="cls_037">No</span></div>
<div style="position:absolute;left:245.43px;top:356.27px" class="cls_036"><span class="cls_036">13</span></div>
<div style="position:absolute;left:258.36px;top:356.27px" class="cls_036"><span class="cls_036">No</span></div>
<div style="position:absolute;left:282.35px;top:356.27px" class="cls_036"><span class="cls_036">Single</span></div>
<div style="position:absolute;left:309.78px;top:356.27px" class="cls_036"><span class="cls_036">85K</span></div>
<div style="position:absolute;left:336.02px;top:356.16px" class="cls_037"><span class="cls_037">Yes</span></div>
<div style="position:absolute;left:245.43px;top:366.87px" class="cls_036"><span class="cls_036">14</span></div>
<div style="position:absolute;left:258.36px;top:366.87px" class="cls_036"><span class="cls_036">No</span></div>
<div style="position:absolute;left:282.35px;top:366.87px" class="cls_036"><span class="cls_036">Married</span></div>
<div style="position:absolute;left:309.78px;top:366.87px" class="cls_036"><span class="cls_036">75K</span></div>
<div style="position:absolute;left:336.02px;top:366.76px" class="cls_037"><span class="cls_037">No</span></div>
<div style="position:absolute;left:245.43px;top:377.43px" class="cls_036"><span class="cls_036">15</span></div>
<div style="position:absolute;left:258.36px;top:377.43px" class="cls_036"><span class="cls_036">No</span></div>
<div style="position:absolute;left:282.35px;top:377.43px" class="cls_036"><span class="cls_036">Single</span></div>
<div style="position:absolute;left:309.78px;top:377.43px" class="cls_036"><span class="cls_036">90K</span></div>
<div style="position:absolute;left:336.02px;top:377.33px" class="cls_037"><span class="cls_037">Yes</span></div>
<div style="position:absolute;left:24.00px;top:449.76px" class="cls_039"><span class="cls_039">Milk</span></div>
<div style="position:absolute;left:248.56px;top:500.40px" class="cls_006"><span class="cls_006">Introduction to Data Mining, 2nd Edition</span></div>
<div style="position:absolute;left:43.20px;top:507.60px" class="cls_006"><span class="cls_006">09/09/2020</span></div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_006"><span class="cls_006">10</span></div>
<div style="position:absolute;left:269.07px;top:514.56px" class="cls_006"><span class="cls_006">Tan, Steinbach, Karpatne, Kumar</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:5500px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter1/background11.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:11.44px" class="cls_002"><span class="cls_002">Predictive Modeling: Classification</span></div>
<div style="position:absolute;left:39.50px;top:84.60px" class="cls_030"><span class="cls_030">●</span></div>
<div style="position:absolute;left:66.50px;top:77.60px" class="cls_005"><span class="cls_005">Find a model  for class attribute as a function of</span></div>
<div style="position:absolute;left:66.50px;top:107.60px" class="cls_005"><span class="cls_005">the values of other attributes</span></div>
<div style="position:absolute;left:478.10px;top:129.60px" class="cls_040"><span class="cls_040">Model for predicting credit</span></div>
<div style="position:absolute;left:544.10px;top:150.48px" class="cls_040"><span class="cls_040">worthiness</span></div>
<div style="position:absolute;left:460.49px;top:192.99px" class="cls_042"><span class="cls_042">Employed</span></div>
<div style="position:absolute;left:301.20px;top:190.00px" class="cls_041"><span class="cls_041">Class</span></div>
<div style="position:absolute;left:227.49px;top:216.70px" class="cls_043"><span class="cls_043"># years at</span></div>
<div style="position:absolute;left:143.99px;top:224.18px" class="cls_043"><span class="cls_043">Level of</span></div>
<div style="position:absolute;left:313.00px;top:224.18px" class="cls_043"><span class="cls_043">Credit</span></div>
<div style="position:absolute;left:52.11px;top:231.65px" class="cls_043"><span class="cls_043">Employed</span></div>
<div style="position:absolute;left:233.99px;top:231.65px" class="cls_043"><span class="cls_043">present</span></div>
<div style="position:absolute;left:541.52px;top:229.59px" class="cls_047"><span class="cls_047">Yes</span></div>
<div style="position:absolute;left:24.69px;top:232.04px" class="cls_044"><span class="cls_044">Tid</span></div>
<div style="position:absolute;left:427.65px;top:232.24px" class="cls_047"><span class="cls_047">No</span></div>
<div style="position:absolute;left:137.11px;top:239.13px" class="cls_043"><span class="cls_043">Education</span></div>
<div style="position:absolute;left:309.43px;top:239.13px" class="cls_043"><span class="cls_043">Worthy</span></div>
<div style="position:absolute;left:232.55px;top:246.60px" class="cls_043"><span class="cls_043">address</span></div>
<div style="position:absolute;left:30.80px;top:264.30px" class="cls_045"><span class="cls_045">1</span></div>
<div style="position:absolute;left:72.39px;top:264.30px" class="cls_045"><span class="cls_045">Yes</span></div>
<div style="position:absolute;left:140.02px;top:264.30px" class="cls_045"><span class="cls_045">Graduate</span></div>
<div style="position:absolute;left:253.88px;top:264.30px" class="cls_045"><span class="cls_045">5</span></div>
<div style="position:absolute;left:320.21px;top:264.30px" class="cls_046"><span class="cls_046">Yes</span></div>
<div style="position:absolute;left:30.80px;top:284.83px" class="cls_045"><span class="cls_045">2</span></div>
<div style="position:absolute;left:72.39px;top:284.83px" class="cls_045"><span class="cls_045">Yes</span></div>
<div style="position:absolute;left:130.60px;top:284.83px" class="cls_045"><span class="cls_045">High School</span></div>
<div style="position:absolute;left:253.88px;top:284.83px" class="cls_045"><span class="cls_045">2</span></div>
<div style="position:absolute;left:323.14px;top:284.83px" class="cls_046"><span class="cls_046">No</span></div>
<div style="position:absolute;left:408.06px;top:281.86px" class="cls_047"><span class="cls_047">No</span></div>
<div style="position:absolute;left:533.72px;top:287.08px" class="cls_042"><span class="cls_042">Education</span></div>
<div style="position:absolute;left:30.80px;top:305.31px" class="cls_045"><span class="cls_045">3</span></div>
<div style="position:absolute;left:75.25px;top:305.31px" class="cls_045"><span class="cls_045">No</span></div>
<div style="position:absolute;left:135.67px;top:305.31px" class="cls_045"><span class="cls_045">Undergrad</span></div>
<div style="position:absolute;left:253.88px;top:305.31px" class="cls_045"><span class="cls_045">1</span></div>
<div style="position:absolute;left:323.14px;top:305.31px" class="cls_046"><span class="cls_046">No</span></div>
<div style="position:absolute;left:597.86px;top:314.83px" class="cls_047"><span class="cls_047">{ High school,</span></div>
<div style="position:absolute;left:30.80px;top:325.79px" class="cls_045"><span class="cls_045">4</span></div>
<div style="position:absolute;left:72.39px;top:325.79px" class="cls_045"><span class="cls_045">Yes</span></div>
<div style="position:absolute;left:130.60px;top:325.79px" class="cls_045"><span class="cls_045">High School</span></div>
<div style="position:absolute;left:250.23px;top:325.79px" class="cls_045"><span class="cls_045">10</span></div>
<div style="position:absolute;left:320.21px;top:325.79px" class="cls_046"><span class="cls_046">Yes</span></div>
<div style="position:absolute;left:466.14px;top:324.23px" class="cls_047"><span class="cls_047">Graduate</span></div>
<div style="position:absolute;left:607.19px;top:331.54px" class="cls_047"><span class="cls_047">Undergrad }</span></div>
<div style="position:absolute;left:27.94px;top:346.41px" class="cls_045"><span class="cls_045">…</span></div>
<div style="position:absolute;left:77.38px;top:346.41px" class="cls_045"><span class="cls_045">…</span></div>
<div style="position:absolute;left:162.06px;top:346.41px" class="cls_045"><span class="cls_045">…</span></div>
<div style="position:absolute;left:250.95px;top:346.41px" class="cls_045"><span class="cls_045">…</span></div>
<div style="position:absolute;left:325.28px;top:346.41px" class="cls_046"><span class="cls_046">…</span></div>
<div style="position:absolute;left:447.39px;top:373.08px" class="cls_042"><span class="cls_042">Number of</span></div>
<div style="position:absolute;left:607.97px;top:372.88px" class="cls_042"><span class="cls_042">Number of</span></div>
<div style="position:absolute;left:462.87px;top:389.81px" class="cls_042"><span class="cls_042">years</span></div>
<div style="position:absolute;left:623.45px;top:389.61px" class="cls_042"><span class="cls_042">years</span></div>
<div style="position:absolute;left:582.00px;top:417.27px" class="cls_047"><span class="cls_047">> 7 yrs</span></div>
<div style="position:absolute;left:661.18px;top:417.27px" class="cls_047"><span class="cls_047">&lt; 7 yrs</span></div>
<div style="position:absolute;left:416.74px;top:420.41px" class="cls_047"><span class="cls_047">> 3 yr</span></div>
<div style="position:absolute;left:503.73px;top:420.41px" class="cls_047"><span class="cls_047">&lt; 3 yr</span></div>
<div style="position:absolute;left:421.93px;top:469.06px" class="cls_047"><span class="cls_047">Yes</span></div>
<div style="position:absolute;left:511.32px;top:469.06px" class="cls_047"><span class="cls_047">No</span></div>
<div style="position:absolute;left:591.87px;top:470.09px" class="cls_047"><span class="cls_047">Yes</span></div>
<div style="position:absolute;left:673.45px;top:470.09px" class="cls_047"><span class="cls_047">No</span></div>
<div style="position:absolute;left:248.56px;top:500.40px" class="cls_006"><span class="cls_006">Introduction to Data Mining, 2nd Edition</span></div>
<div style="position:absolute;left:43.20px;top:507.60px" class="cls_006"><span class="cls_006">09/09/2020</span></div>
<div style="position:absolute;left:664.11px;top:507.60px" class="cls_006"><span class="cls_006">11</span></div>
<div style="position:absolute;left:269.07px;top:514.56px" class="cls_006"><span class="cls_006">Tan, Steinbach, Karpatne, Kumar</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:6050px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter1/background12.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:11.44px" class="cls_002"><span class="cls_002">Classification Example</span></div>
<div style="position:absolute;left:575.94px;top:125.48px" class="cls_050"><span class="cls_050"># years at</span></div>
<div style="position:absolute;left:493.85px;top:132.81px" class="cls_050"><span class="cls_050">Level of</span></div>
<div style="position:absolute;left:660.00px;top:132.81px" class="cls_050"><span class="cls_050">Credit</span></div>
<div style="position:absolute;left:376.58px;top:140.15px" class="cls_049"><span class="cls_049">Tid</span></div>
<div style="position:absolute;left:403.53px;top:140.15px" class="cls_050"><span class="cls_050">Employed</span></div>
<div style="position:absolute;left:582.33px;top:140.15px" class="cls_050"><span class="cls_050">present</span></div>
<div style="position:absolute;left:487.08px;top:147.50px" class="cls_050"><span class="cls_050">Education</span></div>
<div style="position:absolute;left:656.49px;top:147.50px" class="cls_050"><span class="cls_050">Worthy</span></div>
<div style="position:absolute;left:580.92px;top:154.84px" class="cls_050"><span class="cls_050">address</span></div>
<div style="position:absolute;left:382.58px;top:172.20px" class="cls_051"><span class="cls_051">1</span></div>
<div style="position:absolute;left:423.47px;top:172.20px" class="cls_051"><span class="cls_051">Yes</span></div>
<div style="position:absolute;left:485.68px;top:172.20px" class="cls_051"><span class="cls_051">Undergrad</span></div>
<div style="position:absolute;left:601.88px;top:172.20px" class="cls_051"><span class="cls_051">7</span></div>
<div style="position:absolute;left:674.57px;top:172.20px" class="cls_052"><span class="cls_052">?</span></div>
<div style="position:absolute;left:209.49px;top:191.20px" class="cls_043"><span class="cls_043"># years at</span></div>
<div style="position:absolute;left:382.58px;top:192.37px" class="cls_051"><span class="cls_051">2</span></div>
<div style="position:absolute;left:426.28px;top:192.37px" class="cls_051"><span class="cls_051">No</span></div>
<div style="position:absolute;left:489.96px;top:192.37px" class="cls_051"><span class="cls_051">Graduate</span></div>
<div style="position:absolute;left:601.88px;top:192.37px" class="cls_051"><span class="cls_051">3</span></div>
<div style="position:absolute;left:674.57px;top:192.37px" class="cls_052"><span class="cls_052">?</span></div>
<div style="position:absolute;left:125.99px;top:198.68px" class="cls_043"><span class="cls_043">Level of</span></div>
<div style="position:absolute;left:295.00px;top:198.68px" class="cls_043"><span class="cls_043">Credit</span></div>
<div style="position:absolute;left:6.69px;top:206.54px" class="cls_044"><span class="cls_044">Tid</span></div>
<div style="position:absolute;left:34.11px;top:206.15px" class="cls_043"><span class="cls_043">Employed</span></div>
<div style="position:absolute;left:215.99px;top:206.15px" class="cls_043"><span class="cls_043">present</span></div>
<div style="position:absolute;left:119.11px;top:213.63px" class="cls_043"><span class="cls_043">Education</span></div>
<div style="position:absolute;left:291.43px;top:213.63px" class="cls_043"><span class="cls_043">Worthy</span></div>
<div style="position:absolute;left:382.58px;top:212.47px" class="cls_051"><span class="cls_051">3</span></div>
<div style="position:absolute;left:423.47px;top:212.47px" class="cls_051"><span class="cls_051">Yes</span></div>
<div style="position:absolute;left:480.70px;top:212.47px" class="cls_051"><span class="cls_051">High School</span></div>
<div style="position:absolute;left:601.88px;top:212.47px" class="cls_051"><span class="cls_051">2</span></div>
<div style="position:absolute;left:674.57px;top:212.47px" class="cls_052"><span class="cls_052">?</span></div>
<div style="position:absolute;left:214.55px;top:221.10px" class="cls_043"><span class="cls_043">address</span></div>
<div style="position:absolute;left:379.77px;top:232.58px" class="cls_051"><span class="cls_051">…</span></div>
<div style="position:absolute;left:428.39px;top:232.58px" class="cls_051"><span class="cls_051">…</span></div>
<div style="position:absolute;left:511.62px;top:232.58px" class="cls_051"><span class="cls_051">…</span></div>
<div style="position:absolute;left:599.01px;top:232.58px" class="cls_051"><span class="cls_051">…</span></div>
<div style="position:absolute;left:672.08px;top:232.58px" class="cls_052"><span class="cls_052">…</span></div>
<div style="position:absolute;left:12.80px;top:238.80px" class="cls_045"><span class="cls_045">1</span></div>
<div style="position:absolute;left:54.39px;top:238.80px" class="cls_045"><span class="cls_045">Yes</span></div>
<div style="position:absolute;left:122.02px;top:238.80px" class="cls_045"><span class="cls_045">Graduate</span></div>
<div style="position:absolute;left:235.88px;top:238.80px" class="cls_045"><span class="cls_045">5</span></div>
<div style="position:absolute;left:302.21px;top:238.80px" class="cls_046"><span class="cls_046">Yes</span></div>
<div style="position:absolute;left:12.80px;top:259.33px" class="cls_045"><span class="cls_045">2</span></div>
<div style="position:absolute;left:54.39px;top:259.33px" class="cls_045"><span class="cls_045">Yes</span></div>
<div style="position:absolute;left:112.60px;top:259.33px" class="cls_045"><span class="cls_045">High School</span></div>
<div style="position:absolute;left:235.88px;top:259.33px" class="cls_045"><span class="cls_045">2</span></div>
<div style="position:absolute;left:305.14px;top:259.33px" class="cls_046"><span class="cls_046">No</span></div>
<div style="position:absolute;left:12.80px;top:279.81px" class="cls_045"><span class="cls_045">3</span></div>
<div style="position:absolute;left:57.25px;top:279.81px" class="cls_045"><span class="cls_045">No</span></div>
<div style="position:absolute;left:117.67px;top:279.81px" class="cls_045"><span class="cls_045">Undergrad</span></div>
<div style="position:absolute;left:235.88px;top:279.81px" class="cls_045"><span class="cls_045">1</span></div>
<div style="position:absolute;left:305.14px;top:279.81px" class="cls_046"><span class="cls_046">No</span></div>
<div style="position:absolute;left:12.80px;top:300.29px" class="cls_045"><span class="cls_045">4</span></div>
<div style="position:absolute;left:54.39px;top:300.29px" class="cls_045"><span class="cls_045">Yes</span></div>
<div style="position:absolute;left:112.60px;top:300.29px" class="cls_045"><span class="cls_045">High School</span></div>
<div style="position:absolute;left:232.23px;top:300.29px" class="cls_045"><span class="cls_045">10</span></div>
<div style="position:absolute;left:302.21px;top:300.29px" class="cls_046"><span class="cls_046">Yes</span></div>
<div style="position:absolute;left:9.94px;top:320.91px" class="cls_045"><span class="cls_045">…</span></div>
<div style="position:absolute;left:59.38px;top:320.91px" class="cls_045"><span class="cls_045">…</span></div>
<div style="position:absolute;left:144.06px;top:320.91px" class="cls_045"><span class="cls_045">…</span></div>
<div style="position:absolute;left:232.95px;top:320.91px" class="cls_045"><span class="cls_045">…</span></div>
<div style="position:absolute;left:307.28px;top:320.91px" class="cls_046"><span class="cls_046">…</span></div>
<div style="position:absolute;left:631.52px;top:325.36px" class="cls_054"><span class="cls_054">Test</span></div>
<div style="position:absolute;left:634.43px;top:342.40px" class="cls_054"><span class="cls_054">Set</span></div>
<div style="position:absolute;left:460.18px;top:397.36px" class="cls_055"><span class="cls_055">Learn</span></div>
<div style="position:absolute;left:313.69px;top:412.16px" class="cls_056"><span class="cls_056">Training</span></div>
<div style="position:absolute;left:612.67px;top:415.84px" class="cls_057"><span class="cls_057">Model</span></div>
<div style="position:absolute;left:334.63px;top:431.12px" class="cls_056"><span class="cls_056">Set</span></div>
<div style="position:absolute;left:445.25px;top:426.40px" class="cls_055"><span class="cls_055">Classifier</span></div>
<div style="position:absolute;left:248.56px;top:500.40px" class="cls_006"><span class="cls_006">Introduction to Data Mining, 2nd Edition</span></div>
<div style="position:absolute;left:43.20px;top:507.60px" class="cls_006"><span class="cls_006">09/09/2020</span></div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_006"><span class="cls_006">12</span></div>
<div style="position:absolute;left:269.07px;top:514.56px" class="cls_006"><span class="cls_006">Tan, Steinbach, Karpatne, Kumar</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:6600px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter1/background13.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:11.44px" class="cls_002"><span class="cls_002">Examples of Classification Task</span></div>
<div style="position:absolute;left:37.13px;top:108.60px" class="cls_018"><span class="cls_018">●</span></div>
<div style="position:absolute;left:64.13px;top:103.60px" class="cls_009"><span class="cls_009">Classifying credit card transactions</span></div>
<div style="position:absolute;left:64.13px;top:122.56px" class="cls_009"><span class="cls_009">as legitimate or fraudulent</span></div>
<div style="position:absolute;left:37.13px;top:172.48px" class="cls_018"><span class="cls_018">●</span><span class="cls_009">  Classifying land covers (water bodies, urban areas,</span></div>
<div style="position:absolute;left:64.13px;top:191.44px" class="cls_009"><span class="cls_009">forests, etc.) using satellite data</span></div>
<div style="position:absolute;left:37.13px;top:241.60px" class="cls_018"><span class="cls_018">●</span><span class="cls_009">  Categorizing news stories as finance,</span></div>
<div style="position:absolute;left:64.13px;top:260.56px" class="cls_009"><span class="cls_009">weather, entertainment, sports, etc</span></div>
<div style="position:absolute;left:37.13px;top:311.44px" class="cls_018"><span class="cls_018">●</span><span class="cls_009">  Identifying intruders in the cyberspace</span></div>
<div style="position:absolute;left:37.13px;top:362.56px" class="cls_018"><span class="cls_018">●</span><span class="cls_009">  Predicting tumor cells as benign or malignant</span></div>
<div style="position:absolute;left:37.13px;top:414.40px" class="cls_018"><span class="cls_018">●</span><span class="cls_009">  Classifying secondary structures of protein</span></div>
<div style="position:absolute;left:64.13px;top:433.60px" class="cls_009"><span class="cls_009">as alpha-helix, beta-sheet, or random  coil</span></div>
<div style="position:absolute;left:248.56px;top:500.40px" class="cls_006"><span class="cls_006">Introduction to Data Mining, 2nd Edition</span></div>
<div style="position:absolute;left:43.20px;top:507.60px" class="cls_006"><span class="cls_006">09/09/2020</span></div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_006"><span class="cls_006">13</span></div>
<div style="position:absolute;left:269.07px;top:514.56px" class="cls_006"><span class="cls_006">Tan, Steinbach, Karpatne, Kumar</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:7150px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter1/background14.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:11.44px" class="cls_002"><span class="cls_002">Classification: Application 1</span></div>
<div style="position:absolute;left:43.13px;top:101.60px" class="cls_030"><span class="cls_030">●</span><span class="cls_005"> Fraud Detection</span></div>
<div style="position:absolute;left:79.13px;top:139.44px" class="cls_026"><span class="cls_026">-</span><span class="cls_058"> Goal:</span><span class="cls_015"> Predict fraudulent cases in credit card</span></div>
<div style="position:absolute;left:101.63px;top:165.60px" class="cls_015"><span class="cls_015">transactions.</span></div>
<div style="position:absolute;left:79.13px;top:197.52px" class="cls_026"><span class="cls_026">-</span><span class="cls_058"> Approach:</span></div>
<div style="position:absolute;left:115.13px;top:230.40px" class="cls_059"><span class="cls_059">u</span><span class="cls_015"> Use credit card transactions and the information</span></div>
<div style="position:absolute;left:138.13px;top:256.56px" class="cls_015"><span class="cls_015">on its account-holder as attributes.</span></div>
<div style="position:absolute;left:151.13px;top:292.56px" class="cls_060"><span class="cls_060">- When does a customer buy, what does he buy, how</span></div>
<div style="position:absolute;left:169.13px;top:317.52px" class="cls_060"><span class="cls_060">often he pays on time, etc</span></div>
<div style="position:absolute;left:115.13px;top:346.56px" class="cls_059"><span class="cls_059">u</span><span class="cls_015"> Label past transactions as fraud or fair</span></div>
<div style="position:absolute;left:138.13px;top:372.48px" class="cls_015"><span class="cls_015">transactions. This forms the class attribute.</span></div>
<div style="position:absolute;left:115.13px;top:405.60px" class="cls_059"><span class="cls_059">u</span><span class="cls_015"> Learn a model for the class of the transactions.</span></div>
<div style="position:absolute;left:115.13px;top:438.48px" class="cls_059"><span class="cls_059">u</span><span class="cls_015"> Use this model to detect fraud by observing credit</span></div>
<div style="position:absolute;left:138.13px;top:464.40px" class="cls_015"><span class="cls_015">card transactions on an account.</span></div>
<div style="position:absolute;left:248.56px;top:500.40px" class="cls_006"><span class="cls_006">Introduction to Data Mining, 2nd Edition</span></div>
<div style="position:absolute;left:43.20px;top:507.60px" class="cls_006"><span class="cls_006">09/09/2020</span></div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_006"><span class="cls_006">14</span></div>
<div style="position:absolute;left:269.07px;top:514.56px" class="cls_006"><span class="cls_006">Tan, Steinbach, Karpatne, Kumar</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:7700px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter1/background15.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:11.44px" class="cls_002"><span class="cls_002">Classification: Application 2</span></div>
<div style="position:absolute;left:39.50px;top:92.48px" class="cls_030"><span class="cls_030">●</span><span class="cls_005"> Churn prediction for telephone customers</span></div>
<div style="position:absolute;left:75.50px;top:133.52px" class="cls_031"><span class="cls_031">-</span><span class="cls_035"> Goal:</span><span class="cls_005"> To predict whether a customer is likely</span></div>
<div style="position:absolute;left:98.00px;top:167.60px" class="cls_005"><span class="cls_005">to be lost to a competitor.</span></div>
<div style="position:absolute;left:75.50px;top:208.40px" class="cls_031"><span class="cls_031">-</span><span class="cls_035"> Approach:</span></div>
<div style="position:absolute;left:107.00px;top:248.40px" class="cls_059"><span class="cls_059">u</span><span class="cls_015"> Use detailed record of transactions with each of the</span></div>
<div style="position:absolute;left:129.50px;top:277.44px" class="cls_015"><span class="cls_015">past and present customers, to find attributes.</span></div>
<div style="position:absolute;left:138.50px;top:315.44px" class="cls_061"><span class="cls_061">- How often the customer calls, where he calls, what time-</span></div>
<div style="position:absolute;left:165.50px;top:342.56px" class="cls_061"><span class="cls_061">of-the day he calls most, his financial status, marital</span></div>
<div style="position:absolute;left:165.50px;top:368.48px" class="cls_061"><span class="cls_061">status, etc.</span></div>
<div style="position:absolute;left:107.00px;top:397.44px" class="cls_059"><span class="cls_059">u</span><span class="cls_015"> Label the customers as loyal or disloyal.</span></div>
<div style="position:absolute;left:107.00px;top:433.44px" class="cls_059"><span class="cls_059">u</span><span class="cls_015"> Find a model for loyalty.</span></div>
<div style="position:absolute;left:403.20px;top:483.60px" class="cls_032"><span class="cls_032">From [Berry & Linoff] Data Mining Techniques, 1997</span></div>
<div style="position:absolute;left:248.56px;top:500.40px" class="cls_006"><span class="cls_006">Introduction to Data Mining, 2nd Edition</span></div>
<div style="position:absolute;left:43.20px;top:507.60px" class="cls_006"><span class="cls_006">09/09/2020</span></div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_006"><span class="cls_006">15</span></div>
<div style="position:absolute;left:269.07px;top:514.56px" class="cls_006"><span class="cls_006">Tan, Steinbach, Karpatne, Kumar</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:8250px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter1/background16.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:11.44px" class="cls_002"><span class="cls_002">Classification: Application 3</span></div>
<div style="position:absolute;left:39.50px;top:77.60px" class="cls_030"><span class="cls_030">●</span><span class="cls_005"> Sky Survey Cataloging</span></div>
<div style="position:absolute;left:75.50px;top:115.44px" class="cls_026"><span class="cls_026">-</span><span class="cls_058"> Goal:</span><span class="cls_015"> To predict class (star or galaxy) of sky objects,</span></div>
<div style="position:absolute;left:98.00px;top:141.60px" class="cls_015"><span class="cls_015">especially visually faint ones, based on the telescopic</span></div>
<div style="position:absolute;left:98.00px;top:166.56px" class="cls_015"><span class="cls_015">survey images (from Palomar Observatory).</span></div>
<div style="position:absolute;left:147.50px;top:202.40px" class="cls_061"><span class="cls_061">- 3000 images with 23,040 x 23,040 pixels per image.</span></div>
<div style="position:absolute;left:75.50px;top:228.48px" class="cls_026"><span class="cls_026">-</span><span class="cls_058"> Approach:</span></div>
<div style="position:absolute;left:107.00px;top:261.60px" class="cls_059"><span class="cls_059">u</span><span class="cls_015"> Segment the image.</span></div>
<div style="position:absolute;left:107.00px;top:293.52px" class="cls_059"><span class="cls_059">u</span><span class="cls_015"> Measure image attributes (features) - 40 of them per</span></div>
<div style="position:absolute;left:129.50px;top:319.44px" class="cls_015"><span class="cls_015">object.</span></div>
<div style="position:absolute;left:107.00px;top:351.60px" class="cls_059"><span class="cls_059">u</span><span class="cls_015"> Model the class based on these features.</span></div>
<div style="position:absolute;left:107.00px;top:383.52px" class="cls_059"><span class="cls_059">u</span><span class="cls_015"> Success Story: Could find 16 new high red-shift</span></div>
<div style="position:absolute;left:129.50px;top:409.44px" class="cls_015"><span class="cls_015">quasars, some of the farthest objects that are difficult</span></div>
<div style="position:absolute;left:129.50px;top:435.60px" class="cls_015"><span class="cls_015">to find!</span></div>
<div style="position:absolute;left:247.20px;top:453.60px" class="cls_032"><span class="cls_032">From [Fayyad, et.al.] Advances in Knowledge Discovery and Data Mining, 1996</span></div>
<div style="position:absolute;left:248.56px;top:500.40px" class="cls_006"><span class="cls_006">Introduction to Data Mining, 2nd Edition</span></div>
<div style="position:absolute;left:43.20px;top:507.60px" class="cls_006"><span class="cls_006">09/09/2020</span></div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_006"><span class="cls_006">16</span></div>
<div style="position:absolute;left:269.07px;top:514.56px" class="cls_006"><span class="cls_006">Tan, Steinbach, Karpatne, Kumar</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:8800px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter1/background17.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:11.44px" class="cls_002"><span class="cls_002">Classifying Galaxies</span></div>
<div style="position:absolute;left:499.20px;top:74.48px" class="cls_062"><span class="cls_062">Courtesy: </span><A HREF="http://aps.umn.edu/">http://aps.umn.edu</A> </div>
<div style="position:absolute;left:121.20px;top:135.60px" class="cls_063"><span class="cls_063">Early</span></div>
<div style="position:absolute;left:289.20px;top:135.60px" class="cls_066"><span class="cls_066">Class:</span></div>
<div style="position:absolute;left:499.58px;top:135.12px" class="cls_066"><span class="cls_066">Attributes:</span></div>
<div style="position:absolute;left:289.20px;top:157.52px" class="cls_064"><span class="cls_064">•</span><span class="cls_065"> Stages of Formation</span></div>
<div style="position:absolute;left:499.58px;top:157.28px" class="cls_064"><span class="cls_064">•</span><span class="cls_065"> Image features,</span></div>
<div style="position:absolute;left:499.58px;top:176.24px" class="cls_064"><span class="cls_064">•</span><span class="cls_065"> Characteristics of light</span></div>
<div style="position:absolute;left:513.22px;top:195.20px" class="cls_065"><span class="cls_065">waves received, etc.</span></div>
<div style="position:absolute;left:313.20px;top:219.60px" class="cls_063"><span class="cls_063">Intermediate</span></div>
<div style="position:absolute;left:571.20px;top:297.60px" class="cls_063"><span class="cls_063">Late</span></div>
<div style="position:absolute;left:37.20px;top:399.60px" class="cls_066"><span class="cls_066">Data Size:</span></div>
<div style="position:absolute;left:37.20px;top:421.52px" class="cls_064"><span class="cls_064">•</span><span class="cls_065"> 72 million stars, 20 million galaxies</span></div>
<div style="position:absolute;left:37.20px;top:440.48px" class="cls_064"><span class="cls_064">•</span><span class="cls_065"> Object Catalog: 9 GB</span></div>
<div style="position:absolute;left:37.20px;top:461.60px" class="cls_064"><span class="cls_064">•</span><span class="cls_065"> Image Database: 150 GB</span></div>
<div style="position:absolute;left:248.56px;top:500.40px" class="cls_006"><span class="cls_006">Introduction to Data Mining, 2nd Edition</span></div>
<div style="position:absolute;left:43.20px;top:507.60px" class="cls_006"><span class="cls_006">09/09/2020</span></div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_006"><span class="cls_006">17</span></div>
<div style="position:absolute;left:269.07px;top:514.56px" class="cls_006"><span class="cls_006">Tan, Steinbach, Karpatne, Kumar</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:9350px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter1/background18.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:11.44px" class="cls_002"><span class="cls_002">Regression</span></div>
<div style="position:absolute;left:37.13px;top:98.40px" class="cls_014"><span class="cls_014">●</span><span class="cls_015"> Predict a value of a given continuous valued variable</span></div>
<div style="position:absolute;left:64.13px;top:127.44px" class="cls_015"><span class="cls_015">based on the values of other variables, assuming a</span></div>
<div style="position:absolute;left:64.13px;top:156.48px" class="cls_015"><span class="cls_015">linear or nonlinear model of dependency.</span></div>
<div style="position:absolute;left:37.13px;top:192.48px" class="cls_014"><span class="cls_014">●</span><span class="cls_015"> Extensively studied in statistics, neural network fields.</span></div>
<div style="position:absolute;left:37.13px;top:227.52px" class="cls_014"><span class="cls_014">●</span><span class="cls_015"> Examples:</span></div>
<div style="position:absolute;left:73.13px;top:263.52px" class="cls_026"><span class="cls_026">-</span><span class="cls_015"> Predicting sales amounts of new product based on</span></div>
<div style="position:absolute;left:95.63px;top:292.56px" class="cls_015"><span class="cls_015">advetising expenditure.</span></div>
<div style="position:absolute;left:73.13px;top:327.60px" class="cls_026"><span class="cls_026">-</span><span class="cls_015"> Predicting wind velocities as a function of</span></div>
<div style="position:absolute;left:95.63px;top:356.40px" class="cls_015"><span class="cls_015">temperature, humidity, air pressure, etc.</span></div>
<div style="position:absolute;left:73.13px;top:392.40px" class="cls_026"><span class="cls_026">-</span><span class="cls_015"> Time series prediction of stock market indices.</span></div>
<div style="position:absolute;left:248.56px;top:500.40px" class="cls_006"><span class="cls_006">Introduction to Data Mining, 2nd Edition</span></div>
<div style="position:absolute;left:43.20px;top:507.60px" class="cls_006"><span class="cls_006">09/09/2020</span></div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_006"><span class="cls_006">18</span></div>
<div style="position:absolute;left:269.07px;top:514.56px" class="cls_006"><span class="cls_006">Tan, Steinbach, Karpatne, Kumar</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:9900px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter1/background19.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:11.44px" class="cls_002"><span class="cls_002">Clustering</span></div>
<div style="position:absolute;left:61.13px;top:104.40px" class="cls_014"><span class="cls_014">●</span></div>
<div style="position:absolute;left:88.13px;top:98.40px" class="cls_015"><span class="cls_015">Finding groups of objects such that the objects in a</span></div>
<div style="position:absolute;left:88.13px;top:127.44px" class="cls_015"><span class="cls_015">group will be similar (or related) to one another and</span></div>
<div style="position:absolute;left:88.13px;top:156.48px" class="cls_015"><span class="cls_015">different from (or unrelated to) the objects in other</span></div>
<div style="position:absolute;left:88.13px;top:185.52px" class="cls_015"><span class="cls_015">groups</span></div>
<div style="position:absolute;left:521.79px;top:213.52px" class="cls_067"><span class="cls_067">Inter-cluster</span></div>
<div style="position:absolute;left:125.85px;top:237.52px" class="cls_067"><span class="cls_067">Intra-cluster</span></div>
<div style="position:absolute;left:518.05px;top:237.52px" class="cls_067"><span class="cls_067">distances are</span></div>
<div style="position:absolute;left:122.05px;top:261.52px" class="cls_067"><span class="cls_067">distances are</span></div>
<div style="position:absolute;left:529.30px;top:261.52px" class="cls_067"><span class="cls_067">maximized</span></div>
<div style="position:absolute;left:135.61px;top:285.52px" class="cls_067"><span class="cls_067">minimized</span></div>
<div style="position:absolute;left:248.56px;top:500.40px" class="cls_006"><span class="cls_006">Introduction to Data Mining, 2nd Edition</span></div>
<div style="position:absolute;left:43.20px;top:507.60px" class="cls_006"><span class="cls_006">09/09/2020</span></div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_006"><span class="cls_006">19</span></div>
<div style="position:absolute;left:269.07px;top:514.56px" class="cls_006"><span class="cls_006">Tan, Steinbach, Karpatne, Kumar</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:10450px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter1/background20.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:5.44px" class="cls_002"><span class="cls_002">Applications of Cluster Analysis</span></div>
<div style="position:absolute;left:31.20px;top:79.56px" class="cls_018"><span class="cls_018">●</span></div>
<div style="position:absolute;left:58.20px;top:74.56px" class="cls_055"><span class="cls_055">Understanding</span></div>
<div style="position:absolute;left:67.20px;top:104.48px" class="cls_068"><span class="cls_068">-</span><span class="cls_020">  Custom profiling for targeted</span></div>
<div style="position:absolute;left:89.70px;top:124.64px" class="cls_020"><span class="cls_020">marketing</span></div>
<div style="position:absolute;left:67.20px;top:149.60px" class="cls_068"><span class="cls_068">-</span><span class="cls_020">  Group related documents for</span></div>
<div style="position:absolute;left:89.70px;top:168.56px" class="cls_020"><span class="cls_020">browsing</span></div>
<div style="position:absolute;left:67.20px;top:193.52px" class="cls_068"><span class="cls_068">-</span><span class="cls_020">  Group genes and proteins that</span></div>
<div style="position:absolute;left:89.70px;top:212.48px" class="cls_020"><span class="cls_020">have similar functionality</span></div>
<div style="position:absolute;left:67.20px;top:237.68px" class="cls_068"><span class="cls_068">-</span><span class="cls_020">  Group stocks with similar price</span></div>
<div style="position:absolute;left:89.70px;top:257.60px" class="cls_020"><span class="cls_020">fluctuations</span></div>
<div style="position:absolute;left:31.20px;top:282.64px" class="cls_018"><span class="cls_018">●</span><span class="cls_055">  Summarization</span></div>
<div style="position:absolute;left:67.20px;top:312.56px" class="cls_068"><span class="cls_068">-</span><span class="cls_020">  Reduce the size of large data</span></div>
<div style="position:absolute;left:89.70px;top:331.52px" class="cls_020"><span class="cls_020">sets</span></div>
<div style="position:absolute;left:439.20px;top:344.24px" class="cls_069"><span class="cls_069">Courtesy: Michael Eisen</span></div>
<div style="position:absolute;left:40.87px;top:372.68px" class="cls_070"><span class="cls_070">Clusters for Raw SST and Raw NPP</span></div>
<div style="position:absolute;left:18.04px;top:381.69px" class="cls_071"><span class="cls_071">9 0</span></div>
<div style="position:absolute;left:241.20px;top:387.60px" class="cls_024"><span class="cls_024">Use of K-means to</span></div>
<div style="position:absolute;left:18.04px;top:403.55px" class="cls_071"><span class="cls_071">6 0</span></div>
<div style="position:absolute;left:241.20px;top:401.52px" class="cls_024"><span class="cls_024">partition Sea Surface</span></div>
<div style="position:absolute;left:202.37px;top:414.24px" class="cls_072"><span class="cls_072">Land Clus</span></div>
<div style="position:absolute;left:18.04px;top:425.88px" class="cls_071"><span class="cls_071">3 0</span></div>
<div style="position:absolute;left:241.20px;top:415.68px" class="cls_024"><span class="cls_024">Temperature (SST) and</span></div>
<div style="position:absolute;left:202.37px;top:436.80px" class="cls_072"><span class="cls_072">Land Clus</span></div>
<div style="position:absolute;left:241.20px;top:430.56px" class="cls_024"><span class="cls_024">Net Primary Production</span></div>
<div style="position:absolute;left:19.01px;top:447.97px" class="cls_071"><span class="cls_071">0</span></div>
<div style="position:absolute;left:241.20px;top:444.48px" class="cls_024"><span class="cls_024">(NPP) into clusters that</span></div>
<div style="position:absolute;left:202.37px;top:458.89px" class="cls_072"><span class="cls_072">Ice or No</span></div>
<div style="position:absolute;left:17.80px;top:470.53px" class="cls_071"><span class="cls_071">-30</span></div>
<div style="position:absolute;left:241.20px;top:459.60px" class="cls_024"><span class="cls_024">reflect the Northern</span></div>
<div style="position:absolute;left:202.37px;top:481.21px" class="cls_072"><span class="cls_072">Sea Clust</span></div>
<div style="position:absolute;left:241.20px;top:473.52px" class="cls_024"><span class="cls_024">and Southern</span></div>
<div style="position:absolute;left:17.80px;top:492.62px" class="cls_071"><span class="cls_071">-60</span></div>
<div style="position:absolute;left:241.20px;top:487.68px" class="cls_024"><span class="cls_024">Hemispheres.</span></div>
<div style="position:absolute;left:202.37px;top:503.31px" class="cls_072"><span class="cls_072">Sea Clust</span></div>
<div style="position:absolute;left:248.56px;top:500.40px" class="cls_006"><span class="cls_006">Introduction to Data Mining, 2nd Edition</span></div>
<div style="position:absolute;left:17.80px;top:514.94px" class="cls_071"><span class="cls_071">-90</span></div>
<div style="position:absolute;left:43.20px;top:507.60px" class="cls_006"><span class="cls_006">09/</span></div>
<div style="position:absolute;left:59.82px;top:507.60px" class="cls_006"><span class="cls_006">09/20</span></div>
<div style="position:absolute;left:89.70px;top:507.60px" class="cls_006"><span class="cls_006">20</span></div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_006"><span class="cls_006">20</span></div>
<div style="position:absolute;left:20.47px;top:517.90px" class="cls_071"><span class="cls_071">-180</span></div>
<div style="position:absolute;left:32.86px;top:517.90px" class="cls_071"><span class="cls_071">-150</span></div>
<div style="position:absolute;left:45.97px;top:517.90px" class="cls_071"><span class="cls_071">-120</span></div>
<div style="position:absolute;left:59.33px;top:517.90px" class="cls_071"><span class="cls_071">-90</span></div>
<div style="position:absolute;left:72.20px;top:517.90px" class="cls_071"><span class="cls_071">-60</span></div>
<div style="position:absolute;left:84.83px;top:517.90px" class="cls_071"><span class="cls_071">-30</span></div>
<div style="position:absolute;left:98.91px;top:517.90px" class="cls_071"><span class="cls_071">0</span></div>
<div style="position:absolute;left:111.30px;top:517.90px" class="cls_071"><span class="cls_071">30</span></div>
<div style="position:absolute;left:124.17px;top:517.90px" class="cls_071"><span class="cls_071">60</span></div>
<div style="position:absolute;left:137.04px;top:517.90px" class="cls_071"><span class="cls_071">90</span></div>
<div style="position:absolute;left:149.43px;top:517.90px" class="cls_071"><span class="cls_071">1 20</span></div>
<div style="position:absolute;left:162.30px;top:517.90px" class="cls_071"><span class="cls_071">150</span></div>
<div style="position:absolute;left:175.17px;top:517.90px" class="cls_071"><span class="cls_071">180</span></div>
<div style="position:absolute;left:185.61px;top:519.20px" class="cls_073"><span class="cls_073">Cluster</span></div>
<div style="position:absolute;left:269.07px;top:514.56px" class="cls_006"><span class="cls_006">Tan, Steinbach, Karpatne, Kumar</span></div>
<div style="position:absolute;left:90.17px;top:523.31px" class="cls_073"><span class="cls_073">longitude</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:11000px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter1/background21.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:11.44px" class="cls_002"><span class="cls_002">Clustering: Application 1</span></div>
<div style="position:absolute;left:43.13px;top:95.60px" class="cls_030"><span class="cls_030">●</span><span class="cls_005"> Market Segmentation:</span></div>
<div style="position:absolute;left:79.13px;top:146.40px" class="cls_026"><span class="cls_026">-</span><span class="cls_058"> Goal:</span><span class="cls_015"> subdivide a market into distinct subsets of</span></div>
<div style="position:absolute;left:101.63px;top:172.56px" class="cls_015"><span class="cls_015">customers where any subset may conceivably be</span></div>
<div style="position:absolute;left:101.63px;top:198.48px" class="cls_015"><span class="cls_015">selected as a market target to be reached with a</span></div>
<div style="position:absolute;left:101.63px;top:224.40px" class="cls_015"><span class="cls_015">distinct marketing mix.</span></div>
<div style="position:absolute;left:79.13px;top:257.52px" class="cls_026"><span class="cls_026">-</span><span class="cls_058"> Approach:</span></div>
<div style="position:absolute;left:111.13px;top:290.40px" class="cls_059"><span class="cls_059">u</span><span class="cls_015"> Collect different attributes of customers based on</span></div>
<div style="position:absolute;left:133.13px;top:315.60px" class="cls_015"><span class="cls_015">their geographical and lifestyle related information.</span></div>
<div style="position:absolute;left:111.13px;top:348.48px" class="cls_059"><span class="cls_059">u</span><span class="cls_015"> Find clusters of similar customers.</span></div>
<div style="position:absolute;left:111.13px;top:381.60px" class="cls_059"><span class="cls_059">u</span><span class="cls_015"> Measure the clustering quality by observing buying</span></div>
<div style="position:absolute;left:133.13px;top:407.52px" class="cls_015"><span class="cls_015">patterns of customers in same cluster vs. those</span></div>
<div style="position:absolute;left:133.13px;top:433.44px" class="cls_015"><span class="cls_015">from different clusters.</span></div>
<div style="position:absolute;left:248.56px;top:500.40px" class="cls_006"><span class="cls_006">Introduction to Data Mining, 2nd Edition</span></div>
<div style="position:absolute;left:43.20px;top:507.60px" class="cls_006"><span class="cls_006">09/09/2020</span></div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_006"><span class="cls_006">21</span></div>
<div style="position:absolute;left:269.07px;top:514.56px" class="cls_006"><span class="cls_006">Tan, Steinbach, Karpatne, Kumar</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:11550px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter1/background22.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:11.44px" class="cls_002"><span class="cls_002">Clustering: Application 2</span></div>
<div style="position:absolute;left:39.50px;top:92.40px" class="cls_014"><span class="cls_014">●</span><span class="cls_015"> Document Clustering:</span></div>
<div style="position:absolute;left:75.50px;top:141.60px" class="cls_026"><span class="cls_026">-</span><span class="cls_058"> Goal:</span><span class="cls_015"> To find groups of documents that are similar to</span></div>
<div style="position:absolute;left:98.00px;top:170.40px" class="cls_015"><span class="cls_015">each other based on the important terms appearing in</span></div>
<div style="position:absolute;left:98.00px;top:199.44px" class="cls_015"><span class="cls_015">them.</span></div>
<div style="position:absolute;left:75.50px;top:253.44px" class="cls_026"><span class="cls_026">-</span><span class="cls_058"> Approach:</span><span class="cls_015"> To identify frequently occurring terms in</span></div>
<div style="position:absolute;left:98.00px;top:282.48px" class="cls_015"><span class="cls_015">each document. Form a similarity measure based on</span></div>
<div style="position:absolute;left:98.00px;top:311.52px" class="cls_015"><span class="cls_015">the frequencies of different terms. Use it to cluster.</span></div>
<div style="position:absolute;left:111.57px;top:414.64px" class="cls_021"><span class="cls_021">Enron email dataset</span></div>
<div style="position:absolute;left:248.56px;top:500.40px" class="cls_006"><span class="cls_006">Introduction to Data Mining, 2nd Edition</span></div>
<div style="position:absolute;left:43.20px;top:507.60px" class="cls_006"><span class="cls_006">09/09/2020</span></div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_006"><span class="cls_006">22</span></div>
<div style="position:absolute;left:269.07px;top:514.56px" class="cls_006"><span class="cls_006">Tan, Steinbach, Karpatne, Kumar</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:12100px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter1/background23.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:11.44px" class="cls_002"><span class="cls_002">Association Rule Discovery: Definition</span></div>
<div style="position:absolute;left:39.50px;top:92.48px" class="cls_030"><span class="cls_030">●</span><span class="cls_005"> Given a set of records each of which contain</span></div>
<div style="position:absolute;left:62.50px;top:126.56px" class="cls_005"><span class="cls_005">some number of items from a given collection</span></div>
<div style="position:absolute;left:75.50px;top:166.56px" class="cls_026"><span class="cls_026">-</span><span class="cls_015"> Produce dependency rules which will predict</span></div>
<div style="position:absolute;left:102.50px;top:195.60px" class="cls_015"><span class="cls_015">occurrence of an item based on occurrences of other</span></div>
<div style="position:absolute;left:102.50px;top:224.40px" class="cls_015"><span class="cls_015">items.</span></div>
<div style="position:absolute;left:44.21px;top:292.87px" class="cls_074"><span class="cls_074">TID</span></div>
<div style="position:absolute;left:101.79px;top:292.87px" class="cls_074"><span class="cls_074">Items</span></div>
<div style="position:absolute;left:44.21px;top:318.58px" class="cls_075"><span class="cls_075">1</span></div>
<div style="position:absolute;left:101.79px;top:318.58px" class="cls_075"><span class="cls_075">Bread, Coke, Milk</span></div>
<div style="position:absolute;left:391.20px;top:325.12px" class="cls_076"><span class="cls_076">Rules Discovered:</span></div>
<div style="position:absolute;left:44.21px;top:344.44px" class="cls_075"><span class="cls_075">2</span></div>
<div style="position:absolute;left:101.79px;top:344.44px" class="cls_075"><span class="cls_075">Beer, Bread</span></div>
<div style="position:absolute;left:411.20px;top:351.12px" class="cls_039"><span class="cls_039">{Milk} --> {Coke}</span></div>
<div style="position:absolute;left:44.21px;top:370.31px" class="cls_075"><span class="cls_075">3</span></div>
<div style="position:absolute;left:101.79px;top:370.31px" class="cls_075"><span class="cls_075">Beer, Coke, Diaper, Milk</span></div>
<div style="position:absolute;left:412.20px;top:373.20px" class="cls_039"><span class="cls_039">{Diaper, Milk} --> {Beer}</span></div>
<div style="position:absolute;left:44.21px;top:396.17px" class="cls_075"><span class="cls_075">4</span></div>
<div style="position:absolute;left:101.79px;top:396.17px" class="cls_075"><span class="cls_075">Beer, Bread, Diaper, Milk</span></div>
<div style="position:absolute;left:44.21px;top:422.03px" class="cls_075"><span class="cls_075">5</span></div>
<div style="position:absolute;left:101.79px;top:422.03px" class="cls_075"><span class="cls_075">Coke, Diaper, Milk</span></div>
<div style="position:absolute;left:248.56px;top:500.40px" class="cls_006"><span class="cls_006">Introduction to Data Mining, 2nd Edition</span></div>
<div style="position:absolute;left:43.20px;top:507.60px" class="cls_006"><span class="cls_006">09/09/2020</span></div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_006"><span class="cls_006">23</span></div>
<div style="position:absolute;left:269.07px;top:514.56px" class="cls_006"><span class="cls_006">Tan, Steinbach, Karpatne, Kumar</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:12650px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter1/background24.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:11.44px" class="cls_002"><span class="cls_002">Association Analysis: Applications</span></div>
<div style="position:absolute;left:39.50px;top:92.48px" class="cls_030"><span class="cls_030">●</span><span class="cls_005"> Market-basket analysis</span></div>
<div style="position:absolute;left:75.50px;top:133.44px" class="cls_026"><span class="cls_026">-</span><span class="cls_015"> Rules are used for sales promotion, shelf</span></div>
<div style="position:absolute;left:102.50px;top:162.48px" class="cls_015"><span class="cls_015">management, and inventory management</span></div>
<div style="position:absolute;left:39.50px;top:232.40px" class="cls_030"><span class="cls_030">●</span><span class="cls_005"> Telecommunication alarm diagnosis</span></div>
<div style="position:absolute;left:75.50px;top:273.60px" class="cls_026"><span class="cls_026">-</span><span class="cls_015"> Rules are used to find combination of alarms that</span></div>
<div style="position:absolute;left:102.50px;top:302.40px" class="cls_015"><span class="cls_015">occur together frequently in the same time period</span></div>
<div style="position:absolute;left:39.50px;top:372.56px" class="cls_030"><span class="cls_030">●</span><span class="cls_005"> Medical Informatics</span></div>
<div style="position:absolute;left:75.50px;top:413.52px" class="cls_026"><span class="cls_026">-</span><span class="cls_015"> Rules are used to find combination of patient</span></div>
<div style="position:absolute;left:102.50px;top:442.56px" class="cls_015"><span class="cls_015">symptoms and test results associated with certain</span></div>
<div style="position:absolute;left:102.50px;top:470.40px" class="cls_015"><span class="cls_015">diseases</span></div>
<div style="position:absolute;left:248.56px;top:500.40px" class="cls_006"><span class="cls_006">Introduction to Data Mining, 2nd Edition</span></div>
<div style="position:absolute;left:43.20px;top:507.60px" class="cls_006"><span class="cls_006">09/09/2020</span></div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_006"><span class="cls_006">24</span></div>
<div style="position:absolute;left:269.07px;top:514.56px" class="cls_006"><span class="cls_006">Tan, Steinbach, Karpatne, Kumar</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:13200px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter1/background25.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.13px;top:27.12px" class="cls_058"><span class="cls_058">Association Analysis: Applications</span></div>
<div style="position:absolute;left:55.13px;top:116.40px" class="cls_014"><span class="cls_014">●</span></div>
<div style="position:absolute;left:78.13px;top:110.40px" class="cls_015"><span class="cls_015">An Example Subspace Differential Coexpression Pattern</span></div>
<div style="position:absolute;left:499.20px;top:147.68px" class="cls_077"><span class="cls_077">Three lung cancer datasets [Bhattacharjee et a</span></div>
<div style="position:absolute;left:78.13px;top:139.44px" class="cls_015"><span class="cls_015">from lung cancer dataset</span></div>
<div style="position:absolute;left:499.20px;top:159.68px" class="cls_077"><span class="cls_077">2001], [Stearman et al. 2005], [Su et al. 2007]</span></div>
<div style="position:absolute;left:55.13px;top:397.52px" class="cls_020"><span class="cls_020">Enriched with the TNF/NFB signaling pathway</span></div>
<div style="position:absolute;left:55.13px;top:423.44px" class="cls_020"><span class="cls_020">which is well-known to be related to lung cancer</span></div>
<div style="position:absolute;left:55.13px;top:448.40px" class="cls_020"><span class="cls_020">P-value: 1.4*10</span><span class="cls_078"><sup>-5</sup></span><span class="cls_020"> (6/10 overlap with the pathway)</span></div>
<div style="position:absolute;left:31.20px;top:483.28px" class="cls_021"><span class="cls_021">[Fang et al PSB 2010]</span></div>
<div style="position:absolute;left:248.56px;top:500.40px" class="cls_006"><span class="cls_006">Introduction to Data Mining, 2nd Edition</span></div>
<div style="position:absolute;left:43.20px;top:507.60px" class="cls_006"><span class="cls_006">09/09/2020</span></div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_006"><span class="cls_006">25</span></div>
<div style="position:absolute;left:269.07px;top:514.56px" class="cls_006"><span class="cls_006">Tan, Steinbach, Karpatne, Kumar</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:13750px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter1/background26.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.13px;top:5.44px" class="cls_002"><span class="cls_002">Deviation/Anomaly/Change Detection</span></div>
<div style="position:absolute;left:25.13px;top:80.56px" class="cls_018"><span class="cls_018">●</span><span class="cls_009"> Detect significant deviations from</span></div>
<div style="position:absolute;left:48.13px;top:104.56px" class="cls_009"><span class="cls_009">normal behavior</span></div>
<div style="position:absolute;left:25.13px;top:135.52px" class="cls_018"><span class="cls_018">●</span><span class="cls_009"> Applications:</span></div>
<div style="position:absolute;left:61.13px;top:165.60px" class="cls_079"><span class="cls_079">-</span><span class="cls_011">  Credit Card Fraud Detection</span></div>
<div style="position:absolute;left:61.13px;top:193.44px" class="cls_079"><span class="cls_079">-</span><span class="cls_011">  Network Intrusion</span></div>
<div style="position:absolute;left:88.13px;top:214.56px" class="cls_011"><span class="cls_011">Detection</span></div>
<div style="position:absolute;left:61.13px;top:242.40px" class="cls_079"><span class="cls_079">-</span><span class="cls_011">  Identify anomalous behavior from</span></div>
<div style="position:absolute;left:88.13px;top:264.48px" class="cls_011"><span class="cls_011">sensor networks for monitoring and</span></div>
<div style="position:absolute;left:88.13px;top:285.60px" class="cls_011"><span class="cls_011">surveillance.</span></div>
<div style="position:absolute;left:61.13px;top:313.44px" class="cls_079"><span class="cls_079">-</span><span class="cls_011">  Detecting changes in the global</span></div>
<div style="position:absolute;left:88.13px;top:335.52px" class="cls_011"><span class="cls_011">forest cover.</span></div>
<div style="position:absolute;left:248.56px;top:500.40px" class="cls_006"><span class="cls_006">Introduction to Data Mining, 2nd Edition</span></div>
<div style="position:absolute;left:43.20px;top:507.60px" class="cls_006"><span class="cls_006">09/09/2020</span></div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_006"><span class="cls_006">26</span></div>
<div style="position:absolute;left:269.07px;top:514.56px" class="cls_006"><span class="cls_006">Tan, Steinbach, Karpatne, Kumar</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:14300px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter1/background27.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:11.44px" class="cls_002"><span class="cls_002">Motivating Challenges</span></div>
<div style="position:absolute;left:39.50px;top:92.48px" class="cls_030"><span class="cls_030">●</span><span class="cls_005"> Scalability</span></div>
<div style="position:absolute;left:39.50px;top:174.56px" class="cls_030"><span class="cls_030">●</span><span class="cls_005"> High Dimensionality</span></div>
<div style="position:absolute;left:39.50px;top:256.40px" class="cls_030"><span class="cls_030">●</span><span class="cls_005"> Heterogeneous and Complex Data</span></div>
<div style="position:absolute;left:39.50px;top:338.48px" class="cls_030"><span class="cls_030">●</span><span class="cls_005"> Data Ownership and Distribution</span></div>
<div style="position:absolute;left:39.50px;top:420.56px" class="cls_030"><span class="cls_030">●</span><span class="cls_005"> Non-traditional Analysis</span></div>
<div style="position:absolute;left:248.56px;top:500.40px" class="cls_006"><span class="cls_006">Introduction to Data Mining, 2nd Edition</span></div>
<div style="position:absolute;left:43.20px;top:507.60px" class="cls_006"><span class="cls_006">09/09/2020</span></div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_006"><span class="cls_006">27</span></div>
<div style="position:absolute;left:269.07px;top:514.56px" class="cls_006"><span class="cls_006">Tan, Steinbach, Karpatne, Kumar</span></div>
</div>

</body>
</html>
