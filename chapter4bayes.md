<html>
<head><meta http-equiv=Content-Type content="text/html; charset=UTF-8">
<style type="text/css">
<!--
span.cls_003{font-family:Tahoma,serif;font-size:32.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_003{font-family:Tahoma,serif;font-size:32.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_002{font-family:Arial,serif;font-size:32.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_002{font-family:Arial,serif;font-size:32.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_004{font-family:Arial,serif;font-size:21.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_004{font-family:Arial,serif;font-size:21.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_007{font-family:Arial,serif;font-size:21.1px;color:rgb(11,122,156);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_007{font-family:Arial,serif;font-size:21.1px;color:rgb(11,122,156);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_008{font-family:Arial,serif;font-size:28.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_008{font-family:Arial,serif;font-size:28.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_009{font-family:Times,serif;font-size:29.2px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_009{font-family:Times,serif;font-size:29.2px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_010{font-family:Times,serif;font-size:29.2px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_010{font-family:Times,serif;font-size:29.2px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_011{font-family:Arial,serif;font-size:29.2px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_011{font-family:Arial,serif;font-size:29.2px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_013{font-family:Times,serif;font-size:32.8px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_013{font-family:Times,serif;font-size:32.8px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_012{font-family:Times,serif;font-size:32.8px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_012{font-family:Times,serif;font-size:32.8px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_014{font-family:Arial,serif;font-size:32.8px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_014{font-family:Arial,serif;font-size:32.8px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_005{font-family:Arial,serif;font-size:14.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_005{font-family:Arial,serif;font-size:14.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_006{font-family:Arial,serif;font-size:9.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_006{font-family:Arial,serif;font-size:9.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_015{font-family:Arial,serif;font-size:22.0px;color:rgb(11,122,156);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_015{font-family:Arial,serif;font-size:22.0px;color:rgb(11,122,156);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_016{font-family:Arial,serif;font-size:22.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_016{font-family:Arial,serif;font-size:22.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_017{font-family:Times,serif;font-size:29.4px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_017{font-family:Times,serif;font-size:29.4px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_019{font-family:Times,serif;font-size:29.4px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_019{font-family:Times,serif;font-size:29.4px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_018{font-family:Arial,serif;font-size:29.4px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_018{font-family:Arial,serif;font-size:29.4px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_020{font-family:Arial,serif;font-size:19.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_020{font-family:Arial,serif;font-size:19.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_021{font-family:Arial,serif;font-size:28.1px;color:rgb(11,122,156);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_021{font-family:Arial,serif;font-size:28.1px;color:rgb(11,122,156);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_025{font-family:Arial,serif;font-size:18.1px;color:rgb(255,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_025{font-family:Arial,serif;font-size:18.1px;color:rgb(255,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_022{font-family:Times,serif;font-size:22.2px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_022{font-family:Times,serif;font-size:22.2px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_023{font-family:Arial,serif;font-size:22.2px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_023{font-family:Arial,serif;font-size:22.2px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_024{font-family:Times,serif;font-size:22.2px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_024{font-family:Times,serif;font-size:22.2px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_026{font-family:Arial,serif;font-size:10.8px;color:rgb(255,255,255);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_026{font-family:Arial,serif;font-size:10.8px;color:rgb(255,255,255);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_027{font-family:Arial,serif;font-size:10.8px;color:rgb(255,255,255);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_027{font-family:Arial,serif;font-size:10.8px;color:rgb(255,255,255);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_030{font-family:Arial,serif;font-size:18.1px;color:rgb(11,122,156);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_030{font-family:Arial,serif;font-size:18.1px;color:rgb(11,122,156);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_031{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_031{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_028{font-family:Arial,serif;font-size:10.8px;color:rgb(1,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_028{font-family:Arial,serif;font-size:10.8px;color:rgb(1,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_029{font-family:Arial,serif;font-size:10.8px;color:rgb(255,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_029{font-family:Arial,serif;font-size:10.8px;color:rgb(255,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_033{font-family:Arial,serif;font-size:24.1px;color:rgb(11,122,156);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_033{font-family:Arial,serif;font-size:24.1px;color:rgb(11,122,156);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_034{font-family:Arial,serif;font-size:16.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_034{font-family:Arial,serif;font-size:16.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_038{font-family:Times,serif;font-size:25.0px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_038{font-family:Times,serif;font-size:25.0px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_035{font-family:Times,serif;font-size:25.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_035{font-family:Times,serif;font-size:25.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_039{font-family:Arial,serif;font-size:25.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_039{font-family:Arial,serif;font-size:25.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_036{font-family:Times,serif;font-size:14.6px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_036{font-family:Times,serif;font-size:14.6px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_037{font-family:Times,serif;font-size:14.6px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_037{font-family:Times,serif;font-size:14.6px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_040{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_040{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_041{font-family:Arial,serif;font-size:28.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_041{font-family:Arial,serif;font-size:28.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_042{font-family:Arial,serif;font-size:15.1px;color:rgb(11,122,156);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_042{font-family:Arial,serif;font-size:15.1px;color:rgb(11,122,156);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_043{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_043{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_048{font-family:Arial,serif;font-size:14.5px;color:rgb(255,255,255);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_048{font-family:Arial,serif;font-size:14.5px;color:rgb(255,255,255);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_045{font-family:Arial,serif;font-size:14.5px;color:rgb(255,255,255);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_045{font-family:Arial,serif;font-size:14.5px;color:rgb(255,255,255);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_044{font-family:Arial,serif;font-size:20.1px;color:rgb(11,122,156);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_044{font-family:Arial,serif;font-size:20.1px;color:rgb(11,122,156);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_046{font-family:Arial,serif;font-size:14.5px;color:rgb(1,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_046{font-family:Arial,serif;font-size:14.5px;color:rgb(1,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_047{font-family:Arial,serif;font-size:14.5px;color:rgb(255,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_047{font-family:Arial,serif;font-size:14.5px;color:rgb(255,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_050{font-family:Arial,serif;font-size:28.1px;color:rgb(255,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_050{font-family:Arial,serif;font-size:28.1px;color:rgb(255,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_051{font-family:Arial,serif;font-size:17.1px;color:rgb(11,122,156);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_051{font-family:Arial,serif;font-size:17.1px;color:rgb(11,122,156);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_052{font-family:Times,serif;font-size:20.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_052{font-family:Times,serif;font-size:20.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_053{font-family:Arial,serif;font-size:13.8px;color:rgb(255,255,255);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_053{font-family:Arial,serif;font-size:13.8px;color:rgb(255,255,255);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_054{font-family:Arial,serif;font-size:13.8px;color:rgb(255,255,255);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_054{font-family:Arial,serif;font-size:13.8px;color:rgb(255,255,255);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_055{font-family:Times,serif;font-size:9.4px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_055{font-family:Times,serif;font-size:9.4px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_060{font-family:Times,serif;font-size:13.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_060{font-family:Times,serif;font-size:13.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_061{font-family:Times,serif;font-size:13.1px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_061{font-family:Times,serif;font-size:13.1px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_063{font-family:Arial,serif;font-size:13.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_063{font-family:Arial,serif;font-size:13.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_058{font-family:Times,serif;font-size:9.4px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_058{font-family:Times,serif;font-size:9.4px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_056{font-family:Arial,serif;font-size:13.8px;color:rgb(1,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_056{font-family:Arial,serif;font-size:13.8px;color:rgb(1,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_057{font-family:Arial,serif;font-size:13.8px;color:rgb(255,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_057{font-family:Arial,serif;font-size:13.8px;color:rgb(255,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_062{font-family:Times,serif;font-size:22.4px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_062{font-family:Times,serif;font-size:22.4px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_064{font-family:Times,serif;font-size:22.4px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_064{font-family:Times,serif;font-size:22.4px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_065{font-family:Arial,serif;font-size:22.4px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_065{font-family:Arial,serif;font-size:22.4px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_059{font-family:Arial,serif;font-size:14.0px;color:rgb(11,122,156);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_059{font-family:Arial,serif;font-size:14.0px;color:rgb(11,122,156);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_068{font-family:Times,serif;font-size:6.8px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_068{font-family:Times,serif;font-size:6.8px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_067{font-family:Times,serif;font-size:10.8px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_067{font-family:Times,serif;font-size:10.8px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_070{font-family:Arial,serif;font-size:10.8px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_070{font-family:Arial,serif;font-size:10.8px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_072{font-family:Times,serif;font-size:32.3px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_072{font-family:Times,serif;font-size:32.3px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_071{font-family:Times,serif;font-size:32.3px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_071{font-family:Times,serif;font-size:32.3px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_069{font-family:Arial,serif;font-size:32.3px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_069{font-family:Arial,serif;font-size:32.3px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_073{font-family:Times,serif;font-size:31.3px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_073{font-family:Times,serif;font-size:31.3px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_074{font-family:Arial,serif;font-size:31.3px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_074{font-family:Arial,serif;font-size:31.3px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_075{font-family:Times,serif;font-size:31.3px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_075{font-family:Times,serif;font-size:31.3px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_076{font-family:Arial,serif;font-size:16.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_076{font-family:Arial,serif;font-size:16.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_077{font-family:Arial,serif;font-size:14.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_077{font-family:Arial,serif;font-size:14.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_078{font-family:Arial,serif;font-size:12.1px;color:rgb(11,122,156);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_078{font-family:Arial,serif;font-size:12.1px;color:rgb(11,122,156);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_079{font-family:Arial,serif;font-size:11.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_079{font-family:Arial,serif;font-size:11.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_080{font-family:Arial,serif;font-size:18.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_080{font-family:Arial,serif;font-size:18.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_081{font-family:Arial,serif;font-size:12.1px;color:rgb(255,255,255);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_081{font-family:Arial,serif;font-size:12.1px;color:rgb(255,255,255);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_082{font-family:Arial,serif;font-size:12.1px;color:rgb(255,255,255);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_082{font-family:Arial,serif;font-size:12.1px;color:rgb(255,255,255);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_083{font-family:Arial,serif;font-size:12.1px;color:rgb(1,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_083{font-family:Arial,serif;font-size:12.1px;color:rgb(1,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_084{font-family:Arial,serif;font-size:12.1px;color:rgb(255,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_084{font-family:Arial,serif;font-size:12.1px;color:rgb(255,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_086{font-family:Arial,serif;font-size:13.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_086{font-family:Arial,serif;font-size:13.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_087{font-family:Times,serif;font-size:24.6px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_087{font-family:Times,serif;font-size:24.6px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_088{font-family:Times,serif;font-size:14.4px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_088{font-family:Times,serif;font-size:14.4px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_090{font-family:Times,serif;font-size:24.6px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_090{font-family:Times,serif;font-size:24.6px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_089{font-family:Arial,serif;font-size:24.6px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_089{font-family:Arial,serif;font-size:24.6px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_091{font-family:Times,serif;font-size:20.1px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_091{font-family:Times,serif;font-size:20.1px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_092{font-family:Times,serif;font-size:13.0px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_092{font-family:Times,serif;font-size:13.0px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_093{font-family:Arial,serif;font-size:9.9px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_093{font-family:Arial,serif;font-size:9.9px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_094{font-family:Arial,serif;font-size:11.8px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_094{font-family:Arial,serif;font-size:11.8px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_096{font-family:Times,serif;font-size:19.7px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_096{font-family:Times,serif;font-size:19.7px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_098{font-family:Times,serif;font-size:19.7px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_098{font-family:Times,serif;font-size:19.7px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_097{font-family:Arial,serif;font-size:19.7px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_097{font-family:Arial,serif;font-size:19.7px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_095{font-family:Arial,serif;font-size:12.6px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_095{font-family:Arial,serif;font-size:12.6px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_099{font-family:Arial,serif;font-size:14.9px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_099{font-family:Arial,serif;font-size:14.9px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_100{font-family:Arial,serif;font-size:23.4px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_100{font-family:Arial,serif;font-size:23.4px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_101{font-family:Arial,serif;font-size:28.2px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_101{font-family:Arial,serif;font-size:28.2px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_102{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_102{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_103{font-family:Arial,serif;font-size:29.3px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_103{font-family:Arial,serif;font-size:29.3px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_105{font-family:Arial,serif;font-size:19.6px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_105{font-family:Arial,serif;font-size:19.6px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_106{font-family:Arial,serif;font-size:14.7px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_106{font-family:Arial,serif;font-size:14.7px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_107{font-family:Arial,serif;font-size:23.2px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_107{font-family:Arial,serif;font-size:23.2px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_108{font-family:Arial,serif;font-size:15.6px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_108{font-family:Arial,serif;font-size:15.6px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_109{font-family:Arial,serif;font-size:15.6px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_109{font-family:Arial,serif;font-size:15.6px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_110{font-family:Arial,serif;font-size:19.6px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_110{font-family:Arial,serif;font-size:19.6px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_111{font-family:Arial,serif;font-size:12.9px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_111{font-family:Arial,serif;font-size:12.9px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_112{font-family:Arial,serif;font-size:12.9px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_112{font-family:Arial,serif;font-size:12.9px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
-->
</style>
<script type="text/javascript" src="chapter4bayes/wz_jsgraphics.js"></script>
</head>
<body>
<div style="position:absolute;left:50%;margin-left:-360px;top:0px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter4bayes/background01.jpg" width=720 height=540></div>
<div style="position:absolute;left:265.94px;top:0.00px" class="cls_003"><span class="cls_003">Data Mining</span></div>
<div style="position:absolute;left:61.00px;top:35.44px" class="cls_003"><span class="cls_003">Classification: Alternative Techniques</span></div>
<div style="position:absolute;left:210.01px;top:150.40px" class="cls_002"><span class="cls_002">Bayesian Classifiers</span></div>
<div style="position:absolute;left:80.14px;top:242.32px" class="cls_002"><span class="cls_002">Introduction to Data Mining, 2</span><span class="cls_004"><sup>nd</sup></span><span class="cls_002"> Edition</span></div>
<div style="position:absolute;left:337.08px;top:288.40px" class="cls_002"><span class="cls_002">by</span></div>
<div style="position:absolute;left:118.66px;top:334.48px" class="cls_002"><span class="cls_002">Tan, Steinbach, Karpatne, Kumar</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:550px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter4bayes/background02.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:11.44px" class="cls_003"><span class="cls_003">Bayes Classifier</span></div>
<div style="position:absolute;left:39.50px;top:92.48px" class="cls_007"><span class="cls_007">●</span><span class="cls_008"> A probabilistic framework for solving classification</span></div>
<div style="position:absolute;left:62.50px;top:126.56px" class="cls_008"><span class="cls_008">problems</span></div>
<div style="position:absolute;left:527.92px;top:161.09px" class="cls_009"><span class="cls_009">P</span><span class="cls_010">(</span><span class="cls_009">X</span><span class="cls_010">,</span><span class="cls_009">Y</span><span class="cls_010">)</span></div>
<div style="position:absolute;left:39.50px;top:167.60px" class="cls_007"><span class="cls_007">●</span><span class="cls_008"> Conditional Probability:</span></div>
<div style="position:absolute;left:395.23px;top:179.29px" class="cls_009"><span class="cls_009">P</span><span class="cls_010">(</span><span class="cls_009">Y</span></div>
<div style="position:absolute;left:448.81px;top:179.29px" class="cls_010"><span class="cls_010">|</span></div>
<div style="position:absolute;left:461.84px;top:179.29px" class="cls_009"><span class="cls_009">X</span><span class="cls_010">)</span></div>
<div style="position:absolute;left:501.47px;top:179.29px" class="cls_011"><span class="cls_011">=</span></div>
<div style="position:absolute;left:542.47px;top:201.87px" class="cls_009"><span class="cls_009">P</span></div>
<div style="position:absolute;left:561.11px;top:201.87px" class="cls_010"><span class="cls_010">(</span></div>
<div style="position:absolute;left:574.30px;top:201.87px" class="cls_009"><span class="cls_009">X</span></div>
<div style="position:absolute;left:597.26px;top:201.87px" class="cls_010"><span class="cls_010">)</span></div>
<div style="position:absolute;left:527.92px;top:244.86px" class="cls_009"><span class="cls_009">P</span><span class="cls_010">(</span><span class="cls_009">X</span><span class="cls_010">,</span><span class="cls_009">Y</span><span class="cls_010">)</span></div>
<div style="position:absolute;left:395.23px;top:263.05px" class="cls_009"><span class="cls_009">P</span><span class="cls_010">(</span><span class="cls_009">X</span></div>
<div style="position:absolute;left:455.02px;top:263.05px" class="cls_010"><span class="cls_010">|</span><span class="cls_009">Y </span><span class="cls_010">)</span></div>
<div style="position:absolute;left:501.47px;top:263.05px" class="cls_011"><span class="cls_011">=</span></div>
<div style="position:absolute;left:545.58px;top:285.64px" class="cls_009"><span class="cls_009">P</span><span class="cls_010">(</span><span class="cls_009">Y</span><span class="cls_010">)</span></div>
<div style="position:absolute;left:39.50px;top:331.52px" class="cls_007"><span class="cls_007">●</span><span class="cls_008"> Bayes theorem:</span></div>
<div style="position:absolute;left:363.45px;top:374.28px" class="cls_013"><span class="cls_013">P</span><span class="cls_012">(</span><span class="cls_013">X</span></div>
<div style="position:absolute;left:430.73px;top:374.28px" class="cls_012"><span class="cls_012">|</span><span class="cls_013">Y </span><span class="cls_012">)</span><span class="cls_013">P</span><span class="cls_012">(</span><span class="cls_013">Y </span><span class="cls_012">)</span></div>
<div style="position:absolute;left:214.13px;top:394.78px" class="cls_013"><span class="cls_013">P</span><span class="cls_012">(</span><span class="cls_013">Y</span></div>
<div style="position:absolute;left:274.42px;top:394.78px" class="cls_012"><span class="cls_012">|</span></div>
<div style="position:absolute;left:289.09px;top:394.78px" class="cls_013"><span class="cls_013">X</span><span class="cls_012">)</span></div>
<div style="position:absolute;left:333.69px;top:394.78px" class="cls_014"><span class="cls_014">=</span></div>
<div style="position:absolute;left:416.75px;top:420.24px" class="cls_013"><span class="cls_013">P</span></div>
<div style="position:absolute;left:437.73px;top:420.24px" class="cls_012"><span class="cls_012">(</span><span class="cls_013">X</span></div>
<div style="position:absolute;left:478.40px;top:420.24px" class="cls_012"><span class="cls_012">)</span></div>
<div style="position:absolute;left:43.20px;top:507.52px" class="cls_005"><span class="cls_005">02/14/2018</span></div>
<div style="position:absolute;left:210.45px;top:507.52px" class="cls_005"><span class="cls_005">Introduction to Data Mining, 2</span><span class="cls_006"><sup>nd</sup></span><span class="cls_005"> Edition</span></div>
<div style="position:absolute;left:673.45px;top:507.52px" class="cls_005"><span class="cls_005">2</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:1100px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter4bayes/background03.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:11.44px" class="cls_003"><span class="cls_003">Example of Bayes Theorem</span></div>
<div style="position:absolute;left:39.50px;top:92.48px" class="cls_007"><span class="cls_007">●</span><span class="cls_008"> Given:</span></div>
<div style="position:absolute;left:75.50px;top:133.52px" class="cls_015"><span class="cls_015">-</span><span class="cls_016"> A doctor knows that meningitis causes stiff neck 50% of the</span></div>
<div style="position:absolute;left:102.50px;top:159.44px" class="cls_016"><span class="cls_016">time</span></div>
<div style="position:absolute;left:75.50px;top:192.56px" class="cls_015"><span class="cls_015">-</span><span class="cls_016"> Prior probability of any patient having meningitis is 1/50,000</span></div>
<div style="position:absolute;left:75.50px;top:225.44px" class="cls_015"><span class="cls_015">-</span><span class="cls_016"> Prior probability of any patient having stiff neck is 1/20</span></div>
<div style="position:absolute;left:39.50px;top:292.40px" class="cls_007"><span class="cls_007">●</span><span class="cls_008"> If a patient has stiff neck, what’s the probability</span></div>
<div style="position:absolute;left:62.50px;top:325.52px" class="cls_008"><span class="cls_008">he/she has meningitis?</span></div>
<div style="position:absolute;left:204.37px;top:374.64px" class="cls_017"><span class="cls_017">(</span><span class="cls_019">S</span></div>
<div style="position:absolute;left:237.39px;top:374.64px" class="cls_017"><span class="cls_017">|</span></div>
<div style="position:absolute;left:249.30px;top:374.64px" class="cls_019"><span class="cls_019">M</span><span class="cls_017">)</span><span class="cls_019">P</span><span class="cls_017">(</span><span class="cls_019">M</span><span class="cls_017">)</span></div>
<div style="position:absolute;left:390.74px;top:374.64px" class="cls_017"><span class="cls_017">0.5</span><span class="cls_018">´</span><span class="cls_017">1/50000</span></div>
<div style="position:absolute;left:50.90px;top:392.67px" class="cls_019"><span class="cls_019">P</span><span class="cls_017">(</span><span class="cls_019">M</span></div>
<div style="position:absolute;left:114.66px;top:392.67px" class="cls_017"><span class="cls_017">|</span></div>
<div style="position:absolute;left:126.57px;top:392.67px" class="cls_019"><span class="cls_019">S</span><span class="cls_017">)</span></div>
<div style="position:absolute;left:160.50px;top:374.64px" class="cls_018"><span class="cls_018">=</span><span class="cls_019"><sup>P</sup></span></div>
<div style="position:absolute;left:367.08px;top:392.67px" class="cls_018"><span class="cls_018">=</span></div>
<div style="position:absolute;left:555.51px;top:392.67px" class="cls_018"><span class="cls_018">=</span></div>
<div style="position:absolute;left:577.96px;top:392.67px" class="cls_017"><span class="cls_017">0.0002</span></div>
<div style="position:absolute;left:243.80px;top:415.13px" class="cls_019"><span class="cls_019">P</span><span class="cls_017">(</span><span class="cls_019">S</span><span class="cls_017">)</span></div>
<div style="position:absolute;left:438.85px;top:415.13px" class="cls_017"><span class="cls_017">1/20</span></div>
<div style="position:absolute;left:43.20px;top:507.52px" class="cls_005"><span class="cls_005">02/14/2018</span></div>
<div style="position:absolute;left:210.45px;top:507.52px" class="cls_005"><span class="cls_005">Introduction to Data Mining, 2</span><span class="cls_006"><sup>nd</sup></span><span class="cls_005"> Edition</span></div>
<div style="position:absolute;left:673.45px;top:507.52px" class="cls_005"><span class="cls_005">3</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:1650px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter4bayes/background04.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:11.44px" class="cls_003"><span class="cls_003">Using Bayes Theorem for Classification</span></div>
<div style="position:absolute;left:25.13px;top:86.48px" class="cls_007"><span class="cls_007">●</span><span class="cls_008"> Consider each attribute and class label as random</span></div>
<div style="position:absolute;left:48.13px;top:120.56px" class="cls_008"><span class="cls_008">variables</span></div>
<div style="position:absolute;left:25.13px;top:202.40px" class="cls_007"><span class="cls_007">●</span><span class="cls_008"> Given a record with attributes (X</span><span class="cls_020"><sub>1</sub></span><span class="cls_008">, X</span><span class="cls_020"><sub>2</sub></span><span class="cls_008">,…, X</span><span class="cls_020"><sub>d</sub></span><span class="cls_008">)</span></div>
<div style="position:absolute;left:61.13px;top:243.44px" class="cls_021"><span class="cls_021">-</span><span class="cls_008"> Goal is to predict class Y</span></div>
<div style="position:absolute;left:61.13px;top:284.48px" class="cls_021"><span class="cls_021">-</span><span class="cls_008"> Specifically, we want to find the value of Y that</span></div>
<div style="position:absolute;left:88.13px;top:317.60px" class="cls_008"><span class="cls_008">maximizes P(Y| X</span><span class="cls_020"><sub>1</sub></span><span class="cls_008">, X</span><span class="cls_020"><sub>2</sub></span><span class="cls_008">,…, X</span><span class="cls_020"><sub>d </sub></span><span class="cls_008">)</span></div>
<div style="position:absolute;left:25.13px;top:399.44px" class="cls_007"><span class="cls_007">●</span><span class="cls_008"> Can we estimate P(Y| X</span><span class="cls_020"><sub>1</sub></span><span class="cls_008">, X</span><span class="cls_020"><sub>2</sub></span><span class="cls_008">,…, X</span><span class="cls_020"><sub>d </sub></span><span class="cls_008">) directly from</span></div>
<div style="position:absolute;left:48.13px;top:433.52px" class="cls_008"><span class="cls_008">data?</span></div>
<div style="position:absolute;left:43.20px;top:507.52px" class="cls_005"><span class="cls_005">02/14/2018</span></div>
<div style="position:absolute;left:210.45px;top:507.52px" class="cls_005"><span class="cls_005">Introduction to Data Mining, 2</span><span class="cls_006"><sup>nd</sup></span><span class="cls_005"> Edition</span></div>
<div style="position:absolute;left:673.45px;top:507.52px" class="cls_005"><span class="cls_005">4</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:2200px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter4bayes/background05.jpg" width=720 height=540></div>
<div style="position:absolute;left:31.13px;top:11.44px" class="cls_003"><span class="cls_003">Example Data</span></div>
<div style="position:absolute;left:25.20px;top:81.60px" class="cls_025"><span class="cls_025">Given a Test Record:</span></div>
<div style="position:absolute;left:161.08px;top:112.14px" class="cls_022"><span class="cls_022">X</span><span class="cls_023"> = </span><span class="cls_024">(Refund </span><span class="cls_023">= </span><span class="cls_024">No,Divorced,Income</span><span class="cls_023">=</span><span class="cls_024">120K)</span></div>
<div style="position:absolute;left:24.74px;top:168.34px" class="cls_026"><span class="cls_026">Tid</span></div>
<div style="position:absolute;left:49.19px;top:168.04px" class="cls_027"><span class="cls_027">Refund</span></div>
<div style="position:absolute;left:95.97px;top:168.04px" class="cls_027"><span class="cls_027">Marital</span></div>
<div style="position:absolute;left:147.79px;top:168.04px" class="cls_027"><span class="cls_027">Taxable</span></div>
<div style="position:absolute;left:197.34px;top:179.68px" class="cls_027"><span class="cls_027">Evade</span></div>
<div style="position:absolute;left:241.13px;top:164.40px" class="cls_030"><span class="cls_030">●</span><span class="cls_031"> Can we estimate</span></div>
<div style="position:absolute;left:95.97px;top:181.77px" class="cls_027"><span class="cls_027">Status</span></div>
<div style="position:absolute;left:147.79px;top:181.77px" class="cls_027"><span class="cls_027">Income</span></div>
<div style="position:absolute;left:26.20px;top:209.62px" class="cls_028"><span class="cls_028">1</span></div>
<div style="position:absolute;left:50.57px;top:209.62px" class="cls_028"><span class="cls_028">Yes</span></div>
<div style="position:absolute;left:95.97px;top:209.62px" class="cls_028"><span class="cls_028">Single</span></div>
<div style="position:absolute;left:147.79px;top:209.62px" class="cls_028"><span class="cls_028">125K</span></div>
<div style="position:absolute;left:197.34px;top:209.42px" class="cls_029"><span class="cls_029">No</span></div>
<div style="position:absolute;left:277.13px;top:200.48px" class="cls_016"><span class="cls_016">P(Evade = Yes | X) and P(Evade = No | X)?</span></div>
<div style="position:absolute;left:26.20px;top:232.20px" class="cls_028"><span class="cls_028">2</span></div>
<div style="position:absolute;left:50.57px;top:232.20px" class="cls_028"><span class="cls_028">No</span></div>
<div style="position:absolute;left:95.97px;top:232.20px" class="cls_028"><span class="cls_028">Married</span></div>
<div style="position:absolute;left:147.79px;top:232.20px" class="cls_028"><span class="cls_028">100K</span></div>
<div style="position:absolute;left:197.34px;top:231.90px" class="cls_029"><span class="cls_029">No</span></div>
<div style="position:absolute;left:26.20px;top:254.67px" class="cls_028"><span class="cls_028">3</span></div>
<div style="position:absolute;left:50.57px;top:254.67px" class="cls_028"><span class="cls_028">No</span></div>
<div style="position:absolute;left:95.97px;top:254.67px" class="cls_028"><span class="cls_028">Single</span></div>
<div style="position:absolute;left:147.79px;top:254.67px" class="cls_028"><span class="cls_028">70K</span></div>
<div style="position:absolute;left:197.34px;top:254.47px" class="cls_029"><span class="cls_029">No</span></div>
<div style="position:absolute;left:26.20px;top:277.25px" class="cls_028"><span class="cls_028">4</span></div>
<div style="position:absolute;left:50.57px;top:277.25px" class="cls_028"><span class="cls_028">Yes</span></div>
<div style="position:absolute;left:95.97px;top:277.25px" class="cls_028"><span class="cls_028">Married</span></div>
<div style="position:absolute;left:147.79px;top:277.25px" class="cls_028"><span class="cls_028">120K</span></div>
<div style="position:absolute;left:197.34px;top:276.95px" class="cls_029"><span class="cls_029">No</span></div>
<div style="position:absolute;left:277.13px;top:266.48px" class="cls_016"><span class="cls_016">In the following we will replace</span></div>
<div style="position:absolute;left:26.20px;top:299.83px" class="cls_028"><span class="cls_028">5</span></div>
<div style="position:absolute;left:50.57px;top:299.83px" class="cls_028"><span class="cls_028">No</span></div>
<div style="position:absolute;left:95.97px;top:299.83px" class="cls_028"><span class="cls_028">Divorced</span></div>
<div style="position:absolute;left:147.79px;top:299.83px" class="cls_028"><span class="cls_028">95K</span></div>
<div style="position:absolute;left:197.34px;top:299.63px" class="cls_029"><span class="cls_029">Yes</span></div>
<div style="position:absolute;left:304.13px;top:299.60px" class="cls_016"><span class="cls_016">Evade = Yes by Yes, and</span></div>
<div style="position:absolute;left:26.20px;top:322.30px" class="cls_028"><span class="cls_028">6</span></div>
<div style="position:absolute;left:50.57px;top:322.30px" class="cls_028"><span class="cls_028">No</span></div>
<div style="position:absolute;left:95.97px;top:322.30px" class="cls_028"><span class="cls_028">Married</span></div>
<div style="position:absolute;left:147.79px;top:322.30px" class="cls_028"><span class="cls_028">60K</span></div>
<div style="position:absolute;left:197.34px;top:322.10px" class="cls_029"><span class="cls_029">No</span></div>
<div style="position:absolute;left:26.20px;top:344.88px" class="cls_028"><span class="cls_028">7</span></div>
<div style="position:absolute;left:50.57px;top:344.88px" class="cls_028"><span class="cls_028">Yes</span></div>
<div style="position:absolute;left:95.97px;top:344.88px" class="cls_028"><span class="cls_028">Divorced</span></div>
<div style="position:absolute;left:147.79px;top:344.88px" class="cls_028"><span class="cls_028">220K</span></div>
<div style="position:absolute;left:197.34px;top:344.58px" class="cls_029"><span class="cls_029">No</span></div>
<div style="position:absolute;left:304.13px;top:332.48px" class="cls_016"><span class="cls_016">Evade = No by No</span></div>
<div style="position:absolute;left:26.20px;top:367.36px" class="cls_028"><span class="cls_028">8</span></div>
<div style="position:absolute;left:50.57px;top:367.36px" class="cls_028"><span class="cls_028">No</span></div>
<div style="position:absolute;left:95.97px;top:367.36px" class="cls_028"><span class="cls_028">Single</span></div>
<div style="position:absolute;left:147.79px;top:367.36px" class="cls_028"><span class="cls_028">85K</span></div>
<div style="position:absolute;left:197.34px;top:367.16px" class="cls_029"><span class="cls_029">Yes</span></div>
<div style="position:absolute;left:26.20px;top:389.93px" class="cls_028"><span class="cls_028">9</span></div>
<div style="position:absolute;left:50.57px;top:389.93px" class="cls_028"><span class="cls_028">No</span></div>
<div style="position:absolute;left:95.97px;top:389.93px" class="cls_028"><span class="cls_028">Married</span></div>
<div style="position:absolute;left:147.79px;top:389.93px" class="cls_028"><span class="cls_028">75K</span></div>
<div style="position:absolute;left:197.34px;top:389.63px" class="cls_029"><span class="cls_029">No</span></div>
<div style="position:absolute;left:26.20px;top:412.41px" class="cls_028"><span class="cls_028">10</span></div>
<div style="position:absolute;left:50.57px;top:412.41px" class="cls_028"><span class="cls_028">No</span></div>
<div style="position:absolute;left:95.97px;top:412.41px" class="cls_028"><span class="cls_028">Single</span></div>
<div style="position:absolute;left:147.79px;top:412.41px" class="cls_028"><span class="cls_028">90K</span></div>
<div style="position:absolute;left:197.34px;top:412.21px" class="cls_029"><span class="cls_029">Yes</span></div>
<div style="position:absolute;left:43.20px;top:507.52px" class="cls_005"><span class="cls_005">02/14/2018</span></div>
<div style="position:absolute;left:210.45px;top:507.52px" class="cls_005"><span class="cls_005">Introduction to Data Mining, 2</span><span class="cls_006"><sup>nd</sup></span><span class="cls_005"> Edition</span></div>
<div style="position:absolute;left:673.45px;top:507.52px" class="cls_005"><span class="cls_005">5</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:2750px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter4bayes/background06.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:11.44px" class="cls_003"><span class="cls_003">Using Bayes Theorem for Classification</span></div>
<div style="position:absolute;left:39.50px;top:89.52px" class="cls_030"><span class="cls_030">●</span><span class="cls_031"> Approach:</span></div>
<div style="position:absolute;left:75.50px;top:122.40px" class="cls_033"><span class="cls_033">-</span><span class="cls_031"> compute posterior probability P(Y | X</span><span class="cls_034"><sub>1</sub></span><span class="cls_031">, X</span><span class="cls_034"><sub>2</sub></span><span class="cls_031">, …, X</span><span class="cls_034"><sub>d</sub></span><span class="cls_031">) using</span></div>
<div style="position:absolute;left:102.50px;top:148.56px" class="cls_031"><span class="cls_031">the Bayes theorem</span></div>
<div style="position:absolute;left:390.70px;top:195.55px" class="cls_038"><span class="cls_038">P</span><span class="cls_035">(</span><span class="cls_038">X</span></div>
<div style="position:absolute;left:448.69px;top:195.55px" class="cls_038"><span class="cls_038">X</span></div>
<div style="position:absolute;left:479.71px;top:195.55px" class="cls_039"><span class="cls_039">!</span></div>
<div style="position:absolute;left:510.58px;top:195.55px" class="cls_038"><span class="cls_038">X</span></div>
<div style="position:absolute;left:547.56px;top:195.55px" class="cls_035"><span class="cls_035">| </span><span class="cls_038">Y</span></div>
<div style="position:absolute;left:575.46px;top:195.55px" class="cls_035"><span class="cls_035">)</span><span class="cls_038">P</span><span class="cls_035">(</span><span class="cls_038">Y</span></div>
<div style="position:absolute;left:631.60px;top:195.55px" class="cls_035"><span class="cls_035">)</span></div>
<div style="position:absolute;left:438.75px;top:211.66px" class="cls_036"><span class="cls_036">1</span></div>
<div style="position:absolute;left:468.35px;top:211.66px" class="cls_036"><span class="cls_036">2</span></div>
<div style="position:absolute;left:530.24px;top:211.66px" class="cls_037"><span class="cls_037">d</span></div>
<div style="position:absolute;left:166.90px;top:209.82px" class="cls_038"><span class="cls_038">P</span><span class="cls_035">(</span><span class="cls_038">Y</span></div>
<div style="position:absolute;left:217.08px;top:209.82px" class="cls_035"><span class="cls_035">|</span></div>
<div style="position:absolute;left:229.29px;top:209.82px" class="cls_038"><span class="cls_038">X</span></div>
<div style="position:absolute;left:257.47px;top:209.82px" class="cls_038"><span class="cls_038">X</span></div>
<div style="position:absolute;left:288.49px;top:209.82px" class="cls_039"><span class="cls_039">!</span></div>
<div style="position:absolute;left:319.36px;top:209.82px" class="cls_038"><span class="cls_038">X</span></div>
<div style="position:absolute;left:350.31px;top:209.82px" class="cls_035"><span class="cls_035">)</span></div>
<div style="position:absolute;left:365.93px;top:209.82px" class="cls_039"><span class="cls_039">=</span></div>
<div style="position:absolute;left:247.53px;top:225.92px" class="cls_036"><span class="cls_036">1</span></div>
<div style="position:absolute;left:277.13px;top:225.92px" class="cls_036"><span class="cls_036">2</span></div>
<div style="position:absolute;left:339.03px;top:225.92px" class="cls_037"><span class="cls_037">n</span></div>
<div style="position:absolute;left:435.06px;top:227.54px" class="cls_038"><span class="cls_038">P</span><span class="cls_035">(</span><span class="cls_038">X</span></div>
<div style="position:absolute;left:493.05px;top:227.54px" class="cls_038"><span class="cls_038">X</span></div>
<div style="position:absolute;left:524.07px;top:227.54px" class="cls_039"><span class="cls_039">!</span></div>
<div style="position:absolute;left:554.95px;top:227.54px" class="cls_038"><span class="cls_038">X</span></div>
<div style="position:absolute;left:587.24px;top:227.54px" class="cls_035"><span class="cls_035">)</span></div>
<div style="position:absolute;left:483.11px;top:243.64px" class="cls_036"><span class="cls_036">1</span></div>
<div style="position:absolute;left:512.71px;top:243.64px" class="cls_036"><span class="cls_036">2</span></div>
<div style="position:absolute;left:574.61px;top:243.64px" class="cls_037"><span class="cls_037">d</span></div>
<div style="position:absolute;left:75.50px;top:279.60px" class="cls_033"><span class="cls_033">–</span><span class="cls_040"> Maximum a-posteriori</span><span class="cls_031">: Choose Y that maximizes</span></div>
<div style="position:absolute;left:183.50px;top:305.52px" class="cls_031"><span class="cls_031">P(Y | X</span><span class="cls_034"><sub>1</sub></span><span class="cls_031">, X</span><span class="cls_034"><sub>2</sub></span><span class="cls_031">, …, X</span><span class="cls_034"><sub>d</sub></span><span class="cls_031">)</span></div>
<div style="position:absolute;left:75.50px;top:364.56px" class="cls_033"><span class="cls_033">-</span><span class="cls_031"> Equivalent to choosing value of Y that maximizes</span></div>
<div style="position:absolute;left:183.50px;top:390.48px" class="cls_031"><span class="cls_031">P(X</span><span class="cls_034"><sub>1</sub></span><span class="cls_031">, X</span><span class="cls_034"><sub>2</sub></span><span class="cls_031">, …, X</span><span class="cls_034"><sub>d</sub></span><span class="cls_031">|Y) P(Y)</span></div>
<div style="position:absolute;left:39.50px;top:456.48px" class="cls_030"><span class="cls_030">●</span><span class="cls_031"> How to estimate P(X</span><span class="cls_034"><sub>1</sub></span><span class="cls_031">, X</span><span class="cls_034"><sub>2</sub></span><span class="cls_031">, …, X</span><span class="cls_034"><sub>d </sub></span><span class="cls_031">| Y )?</span></div>
<div style="position:absolute;left:43.20px;top:507.52px" class="cls_005"><span class="cls_005">02/14/2018</span></div>
<div style="position:absolute;left:210.45px;top:507.52px" class="cls_005"><span class="cls_005">Introduction to Data Mining, 2</span><span class="cls_006"><sup>nd</sup></span><span class="cls_005"> Edition</span></div>
<div style="position:absolute;left:673.45px;top:507.52px" class="cls_005"><span class="cls_005">6</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:3300px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter4bayes/background07.jpg" width=720 height=540></div>
<div style="position:absolute;left:31.13px;top:11.44px" class="cls_003"><span class="cls_003">Example Data</span></div>
<div style="position:absolute;left:25.20px;top:81.60px" class="cls_025"><span class="cls_025">Given a Test Record:</span></div>
<div style="position:absolute;left:167.08px;top:112.14px" class="cls_022"><span class="cls_022">X</span><span class="cls_023"> = </span><span class="cls_024">(Refund </span><span class="cls_023">= </span><span class="cls_024">No,Divorced,Income</span><span class="cls_023">=</span><span class="cls_024">120K)</span></div>
<div style="position:absolute;left:24.74px;top:168.34px" class="cls_026"><span class="cls_026">Tid</span></div>
<div style="position:absolute;left:49.19px;top:168.04px" class="cls_027"><span class="cls_027">Refund</span></div>
<div style="position:absolute;left:95.97px;top:168.04px" class="cls_027"><span class="cls_027">Marital</span></div>
<div style="position:absolute;left:147.79px;top:168.04px" class="cls_027"><span class="cls_027">Taxable</span></div>
<div style="position:absolute;left:95.97px;top:181.77px" class="cls_027"><span class="cls_027">Status</span></div>
<div style="position:absolute;left:147.79px;top:181.77px" class="cls_027"><span class="cls_027">Income</span></div>
<div style="position:absolute;left:197.34px;top:179.68px" class="cls_027"><span class="cls_027">Evade</span></div>
<div style="position:absolute;left:26.20px;top:209.62px" class="cls_028"><span class="cls_028">1</span></div>
<div style="position:absolute;left:50.57px;top:209.62px" class="cls_028"><span class="cls_028">Yes</span></div>
<div style="position:absolute;left:95.97px;top:209.62px" class="cls_028"><span class="cls_028">Single</span></div>
<div style="position:absolute;left:147.79px;top:209.62px" class="cls_028"><span class="cls_028">125K</span></div>
<div style="position:absolute;left:197.34px;top:209.42px" class="cls_029"><span class="cls_029">No</span></div>
<div style="position:absolute;left:26.20px;top:232.20px" class="cls_028"><span class="cls_028">2</span></div>
<div style="position:absolute;left:50.57px;top:232.20px" class="cls_028"><span class="cls_028">No</span></div>
<div style="position:absolute;left:95.97px;top:232.20px" class="cls_028"><span class="cls_028">Married</span></div>
<div style="position:absolute;left:147.79px;top:232.20px" class="cls_028"><span class="cls_028">100K</span></div>
<div style="position:absolute;left:197.34px;top:231.90px" class="cls_029"><span class="cls_029">No</span></div>
<div style="position:absolute;left:26.20px;top:254.67px" class="cls_028"><span class="cls_028">3</span></div>
<div style="position:absolute;left:50.57px;top:254.67px" class="cls_028"><span class="cls_028">No</span></div>
<div style="position:absolute;left:95.97px;top:254.67px" class="cls_028"><span class="cls_028">Single</span></div>
<div style="position:absolute;left:147.79px;top:254.67px" class="cls_028"><span class="cls_028">70K</span></div>
<div style="position:absolute;left:197.34px;top:254.47px" class="cls_029"><span class="cls_029">No</span></div>
<div style="position:absolute;left:26.20px;top:277.25px" class="cls_028"><span class="cls_028">4</span></div>
<div style="position:absolute;left:50.57px;top:277.25px" class="cls_028"><span class="cls_028">Yes</span></div>
<div style="position:absolute;left:95.97px;top:277.25px" class="cls_028"><span class="cls_028">Married</span></div>
<div style="position:absolute;left:147.79px;top:277.25px" class="cls_028"><span class="cls_028">120K</span></div>
<div style="position:absolute;left:197.34px;top:276.95px" class="cls_029"><span class="cls_029">No</span></div>
<div style="position:absolute;left:26.20px;top:299.83px" class="cls_028"><span class="cls_028">5</span></div>
<div style="position:absolute;left:50.57px;top:299.83px" class="cls_028"><span class="cls_028">No</span></div>
<div style="position:absolute;left:95.97px;top:299.83px" class="cls_028"><span class="cls_028">Divorced</span></div>
<div style="position:absolute;left:147.79px;top:299.83px" class="cls_028"><span class="cls_028">95K</span></div>
<div style="position:absolute;left:197.34px;top:299.63px" class="cls_029"><span class="cls_029">Yes</span></div>
<div style="position:absolute;left:26.20px;top:322.30px" class="cls_028"><span class="cls_028">6</span></div>
<div style="position:absolute;left:50.57px;top:322.30px" class="cls_028"><span class="cls_028">No</span></div>
<div style="position:absolute;left:95.97px;top:322.30px" class="cls_028"><span class="cls_028">Married</span></div>
<div style="position:absolute;left:147.79px;top:322.30px" class="cls_028"><span class="cls_028">60K</span></div>
<div style="position:absolute;left:197.34px;top:322.10px" class="cls_029"><span class="cls_029">No</span></div>
<div style="position:absolute;left:26.20px;top:344.88px" class="cls_028"><span class="cls_028">7</span></div>
<div style="position:absolute;left:50.57px;top:344.88px" class="cls_028"><span class="cls_028">Yes</span></div>
<div style="position:absolute;left:95.97px;top:344.88px" class="cls_028"><span class="cls_028">Divorced</span></div>
<div style="position:absolute;left:147.79px;top:344.88px" class="cls_028"><span class="cls_028">220K</span></div>
<div style="position:absolute;left:197.34px;top:344.58px" class="cls_029"><span class="cls_029">No</span></div>
<div style="position:absolute;left:26.20px;top:367.36px" class="cls_028"><span class="cls_028">8</span></div>
<div style="position:absolute;left:50.57px;top:367.36px" class="cls_028"><span class="cls_028">No</span></div>
<div style="position:absolute;left:95.97px;top:367.36px" class="cls_028"><span class="cls_028">Single</span></div>
<div style="position:absolute;left:147.79px;top:367.36px" class="cls_028"><span class="cls_028">85K</span></div>
<div style="position:absolute;left:197.34px;top:367.16px" class="cls_029"><span class="cls_029">Yes</span></div>
<div style="position:absolute;left:26.20px;top:389.93px" class="cls_028"><span class="cls_028">9</span></div>
<div style="position:absolute;left:50.57px;top:389.93px" class="cls_028"><span class="cls_028">No</span></div>
<div style="position:absolute;left:95.97px;top:389.93px" class="cls_028"><span class="cls_028">Married</span></div>
<div style="position:absolute;left:147.79px;top:389.93px" class="cls_028"><span class="cls_028">75K</span></div>
<div style="position:absolute;left:197.34px;top:389.63px" class="cls_029"><span class="cls_029">No</span></div>
<div style="position:absolute;left:26.20px;top:412.41px" class="cls_028"><span class="cls_028">10</span></div>
<div style="position:absolute;left:50.57px;top:412.41px" class="cls_028"><span class="cls_028">No</span></div>
<div style="position:absolute;left:95.97px;top:412.41px" class="cls_028"><span class="cls_028">Single</span></div>
<div style="position:absolute;left:147.79px;top:412.41px" class="cls_028"><span class="cls_028">90K</span></div>
<div style="position:absolute;left:197.34px;top:412.21px" class="cls_029"><span class="cls_029">Yes</span></div>
<div style="position:absolute;left:43.20px;top:507.52px" class="cls_005"><span class="cls_005">02/14/2018</span></div>
<div style="position:absolute;left:210.45px;top:507.52px" class="cls_005"><span class="cls_005">Intr</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:3850px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter4bayes/background08.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:11.44px" class="cls_003"><span class="cls_003">Naïve Bayes Classifier</span></div>
<div style="position:absolute;left:39.50px;top:92.40px" class="cls_030"><span class="cls_030">●</span><span class="cls_031"> Assume independence among attributes X</span><span class="cls_020"><sub>i</sub></span><span class="cls_031"> when class is</span></div>
<div style="position:absolute;left:62.50px;top:121.44px" class="cls_031"><span class="cls_031">given:</span></div>
<div style="position:absolute;left:75.50px;top:157.44px" class="cls_033"><span class="cls_033">-</span><span class="cls_031"> P(X</span><span class="cls_034"><sub>1</sub></span><span class="cls_031">, X</span><span class="cls_034"><sub>2</sub></span><span class="cls_031">, …, X</span><span class="cls_034"><sub>d</sub></span><span class="cls_031"> |Y</span><span class="cls_034"><sub>j</sub></span><span class="cls_031">) = P(X</span><span class="cls_034"><sub>1</sub></span><span class="cls_031">| Y</span><span class="cls_034"><sub>j</sub></span><span class="cls_031">) P(X</span><span class="cls_034"><sub>2</sub></span><span class="cls_031">| Y</span><span class="cls_034"><sub>j</sub></span><span class="cls_031">)… P(X</span><span class="cls_034"><sub>d</sub></span><span class="cls_031">| Y</span><span class="cls_034"><sub>j</sub></span><span class="cls_031">)</span></div>
<div style="position:absolute;left:75.50px;top:228.48px" class="cls_033"><span class="cls_033">-</span><span class="cls_031"> Now we can estimate P(X</span><span class="cls_020"><sub>i</sub></span><span class="cls_031">| Y</span><span class="cls_020"><sub>j</sub></span><span class="cls_031">) for all X</span><span class="cls_020"><sub>i</sub></span><span class="cls_031"> and Y</span><span class="cls_020"><sub>j</sub></span></div>
<div style="position:absolute;left:102.50px;top:257.52px" class="cls_031"><span class="cls_031">combinations from the training data</span></div>
<div style="position:absolute;left:75.50px;top:328.56px" class="cls_033"><span class="cls_033">-</span><span class="cls_031"> New point is classified to Y</span><span class="cls_034"><sub>j</sub></span><span class="cls_031"> if  P(Y</span><span class="cls_034"><sub>j</sub></span><span class="cls_031">) P P(X</span><span class="cls_034"><sub>i</sub></span><span class="cls_031">| Y</span><span class="cls_034"><sub>j</sub></span><span class="cls_031">)  is</span></div>
<div style="position:absolute;left:102.50px;top:357.60px" class="cls_031"><span class="cls_031">maximal.</span></div>
<div style="position:absolute;left:43.20px;top:507.52px" class="cls_005"><span class="cls_005">02/14/2018</span></div>
<div style="position:absolute;left:210.45px;top:507.52px" class="cls_005"><span class="cls_005">Introduction to Data Mining, 2</span><span class="cls_006"><sup>nd</sup></span><span class="cls_005"> Edition</span></div>
<div style="position:absolute;left:673.45px;top:507.52px" class="cls_005"><span class="cls_005">8</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:4400px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter4bayes/background09.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:11.44px" class="cls_003"><span class="cls_003">Conditional Independence</span></div>
<div style="position:absolute;left:39.50px;top:92.48px" class="cls_007"><span class="cls_007">● </span><span class="cls_041">X </span><span class="cls_008">and </span><span class="cls_041">Y </span><span class="cls_008">are conditionally independent given </span><span class="cls_041">Z </span><span class="cls_008">if</span></div>
<div style="position:absolute;left:62.50px;top:126.56px" class="cls_008"><span class="cls_008">P(</span><span class="cls_041">X</span><span class="cls_008">|</span><span class="cls_041">YZ</span><span class="cls_008">) = P(</span><span class="cls_041">X</span><span class="cls_008">|</span><span class="cls_041">Z</span><span class="cls_008">)</span></div>
<div style="position:absolute;left:39.50px;top:208.40px" class="cls_007"><span class="cls_007">●</span><span class="cls_008"> Example: Arm length and reading skills</span></div>
<div style="position:absolute;left:75.50px;top:249.44px" class="cls_021"><span class="cls_021">-</span><span class="cls_008"> Young child has shorter arm length and</span></div>
<div style="position:absolute;left:102.50px;top:282.56px" class="cls_008"><span class="cls_008">limited reading skills, compared to adults</span></div>
<div style="position:absolute;left:75.50px;top:323.60px" class="cls_021"><span class="cls_021">-</span><span class="cls_008"> If age is fixed, no apparent relationship</span></div>
<div style="position:absolute;left:102.50px;top:357.44px" class="cls_008"><span class="cls_008">between arm length and reading skills</span></div>
<div style="position:absolute;left:75.50px;top:398.48px" class="cls_021"><span class="cls_021">-</span><span class="cls_008"> Arm length and reading skills are conditionally</span></div>
<div style="position:absolute;left:102.50px;top:431.60px" class="cls_008"><span class="cls_008">independent given age</span></div>
<div style="position:absolute;left:43.20px;top:507.52px" class="cls_005"><span class="cls_005">02/14/2018</span></div>
<div style="position:absolute;left:210.45px;top:507.52px" class="cls_005"><span class="cls_005">Introduction to Data Mining, 2</span><span class="cls_006"><sup>nd</sup></span><span class="cls_005"> Edition</span></div>
<div style="position:absolute;left:673.45px;top:507.52px" class="cls_005"><span class="cls_005">9</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:4950px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter4bayes/background10.jpg" width=720 height=540></div>
<div style="position:absolute;left:31.13px;top:11.44px" class="cls_003"><span class="cls_003">Naïve Bayes on Example Data</span></div>
<div style="position:absolute;left:25.20px;top:81.60px" class="cls_025"><span class="cls_025">Given a Test Record:</span></div>
<div style="position:absolute;left:161.08px;top:112.14px" class="cls_022"><span class="cls_022">X</span><span class="cls_023"> = </span><span class="cls_024">(Refund </span><span class="cls_023">= </span><span class="cls_024">No,Divorced,Income</span><span class="cls_023">=</span><span class="cls_024">120K)</span></div>
<div style="position:absolute;left:24.74px;top:168.34px" class="cls_026"><span class="cls_026">Tid</span></div>
<div style="position:absolute;left:49.19px;top:168.04px" class="cls_027"><span class="cls_027">Refund</span></div>
<div style="position:absolute;left:95.97px;top:168.04px" class="cls_027"><span class="cls_027">Marital</span></div>
<div style="position:absolute;left:147.79px;top:168.04px" class="cls_027"><span class="cls_027">Taxable</span></div>
<div style="position:absolute;left:95.97px;top:181.77px" class="cls_027"><span class="cls_027">Status</span></div>
<div style="position:absolute;left:147.79px;top:181.77px" class="cls_027"><span class="cls_027">Income</span></div>
<div style="position:absolute;left:197.34px;top:179.68px" class="cls_027"><span class="cls_027">Evade</span></div>
<div style="position:absolute;left:241.13px;top:176.56px" class="cls_042"><span class="cls_042">●</span><span class="cls_043"> P(X | Yes) =</span></div>
<div style="position:absolute;left:26.20px;top:209.62px" class="cls_028"><span class="cls_028">1</span></div>
<div style="position:absolute;left:50.57px;top:209.62px" class="cls_028"><span class="cls_028">Yes</span></div>
<div style="position:absolute;left:95.97px;top:209.62px" class="cls_028"><span class="cls_028">Single</span></div>
<div style="position:absolute;left:147.79px;top:209.62px" class="cls_028"><span class="cls_028">125K</span></div>
<div style="position:absolute;left:197.34px;top:209.42px" class="cls_029"><span class="cls_029">No</span></div>
<div style="position:absolute;left:313.13px;top:207.52px" class="cls_043"><span class="cls_043">P(Refund = No | Yes) x</span></div>
<div style="position:absolute;left:26.20px;top:232.20px" class="cls_028"><span class="cls_028">2</span></div>
<div style="position:absolute;left:50.57px;top:232.20px" class="cls_028"><span class="cls_028">No</span></div>
<div style="position:absolute;left:95.97px;top:232.20px" class="cls_028"><span class="cls_028">Married</span></div>
<div style="position:absolute;left:147.79px;top:232.20px" class="cls_028"><span class="cls_028">100K</span></div>
<div style="position:absolute;left:197.34px;top:231.90px" class="cls_029"><span class="cls_029">No</span></div>
<div style="position:absolute;left:313.13px;top:237.52px" class="cls_043"><span class="cls_043">P(Divorced | Yes) x</span></div>
<div style="position:absolute;left:26.20px;top:254.67px" class="cls_028"><span class="cls_028">3</span></div>
<div style="position:absolute;left:50.57px;top:254.67px" class="cls_028"><span class="cls_028">No</span></div>
<div style="position:absolute;left:95.97px;top:254.67px" class="cls_028"><span class="cls_028">Single</span></div>
<div style="position:absolute;left:147.79px;top:254.67px" class="cls_028"><span class="cls_028">70K</span></div>
<div style="position:absolute;left:197.34px;top:254.47px" class="cls_029"><span class="cls_029">No</span></div>
<div style="position:absolute;left:26.20px;top:277.25px" class="cls_028"><span class="cls_028">4</span></div>
<div style="position:absolute;left:50.57px;top:277.25px" class="cls_028"><span class="cls_028">Yes</span></div>
<div style="position:absolute;left:95.97px;top:277.25px" class="cls_028"><span class="cls_028">Married</span></div>
<div style="position:absolute;left:147.79px;top:277.25px" class="cls_028"><span class="cls_028">120K</span></div>
<div style="position:absolute;left:197.34px;top:276.95px" class="cls_029"><span class="cls_029">No</span></div>
<div style="position:absolute;left:313.13px;top:268.48px" class="cls_043"><span class="cls_043">P(Income = 120K | Yes)</span></div>
<div style="position:absolute;left:26.20px;top:299.83px" class="cls_028"><span class="cls_028">5</span></div>
<div style="position:absolute;left:50.57px;top:299.83px" class="cls_028"><span class="cls_028">No</span></div>
<div style="position:absolute;left:95.97px;top:299.83px" class="cls_028"><span class="cls_028">Divorced</span></div>
<div style="position:absolute;left:147.79px;top:299.83px" class="cls_028"><span class="cls_028">95K</span></div>
<div style="position:absolute;left:197.34px;top:299.63px" class="cls_029"><span class="cls_029">Yes</span></div>
<div style="position:absolute;left:26.20px;top:322.30px" class="cls_028"><span class="cls_028">6</span></div>
<div style="position:absolute;left:50.57px;top:322.30px" class="cls_028"><span class="cls_028">No</span></div>
<div style="position:absolute;left:95.97px;top:322.30px" class="cls_028"><span class="cls_028">Married</span></div>
<div style="position:absolute;left:147.79px;top:322.30px" class="cls_028"><span class="cls_028">60K</span></div>
<div style="position:absolute;left:197.34px;top:322.10px" class="cls_029"><span class="cls_029">No</span></div>
<div style="position:absolute;left:241.13px;top:328.48px" class="cls_042"><span class="cls_042">●</span><span class="cls_043"> P(X | No) =</span></div>
<div style="position:absolute;left:26.20px;top:344.88px" class="cls_028"><span class="cls_028">7</span></div>
<div style="position:absolute;left:50.57px;top:344.88px" class="cls_028"><span class="cls_028">Yes</span></div>
<div style="position:absolute;left:95.97px;top:344.88px" class="cls_028"><span class="cls_028">Divorced</span></div>
<div style="position:absolute;left:147.79px;top:344.88px" class="cls_028"><span class="cls_028">220K</span></div>
<div style="position:absolute;left:197.34px;top:344.58px" class="cls_029"><span class="cls_029">No</span></div>
<div style="position:absolute;left:26.20px;top:367.36px" class="cls_028"><span class="cls_028">8</span></div>
<div style="position:absolute;left:50.57px;top:367.36px" class="cls_028"><span class="cls_028">No</span></div>
<div style="position:absolute;left:95.97px;top:367.36px" class="cls_028"><span class="cls_028">Single</span></div>
<div style="position:absolute;left:147.79px;top:367.36px" class="cls_028"><span class="cls_028">85K</span></div>
<div style="position:absolute;left:197.34px;top:367.16px" class="cls_029"><span class="cls_029">Yes</span></div>
<div style="position:absolute;left:313.13px;top:359.44px" class="cls_043"><span class="cls_043">P(Refund = No | No) x</span></div>
<div style="position:absolute;left:26.20px;top:389.93px" class="cls_028"><span class="cls_028">9</span></div>
<div style="position:absolute;left:50.57px;top:389.93px" class="cls_028"><span class="cls_028">No</span></div>
<div style="position:absolute;left:95.97px;top:389.93px" class="cls_028"><span class="cls_028">Married</span></div>
<div style="position:absolute;left:147.79px;top:389.93px" class="cls_028"><span class="cls_028">75K</span></div>
<div style="position:absolute;left:197.34px;top:389.63px" class="cls_029"><span class="cls_029">No</span></div>
<div style="position:absolute;left:313.13px;top:389.44px" class="cls_043"><span class="cls_043">P(Divorced | No) x</span></div>
<div style="position:absolute;left:26.20px;top:412.41px" class="cls_028"><span class="cls_028">10</span></div>
<div style="position:absolute;left:50.57px;top:412.41px" class="cls_028"><span class="cls_028">No</span></div>
<div style="position:absolute;left:95.97px;top:412.41px" class="cls_028"><span class="cls_028">Single</span></div>
<div style="position:absolute;left:147.79px;top:412.41px" class="cls_028"><span class="cls_028">90K</span></div>
<div style="position:absolute;left:197.34px;top:412.21px" class="cls_029"><span class="cls_029">Yes</span></div>
<div style="position:absolute;left:313.13px;top:420.40px" class="cls_043"><span class="cls_043">P(Income = 120K | No)</span></div>
<div style="position:absolute;left:43.20px;top:507.52px" class="cls_005"><span class="cls_005">02/14/2018</span></div>
<div style="position:absolute;left:210.45px;top:507.52px" class="cls_005"><span class="cls_005">Introduction to Data Mining, 2</span><span class="cls_006"><sup>nd</sup></span><span class="cls_005"> Edition</span></div>
<div style="position:absolute;left:673.45px;top:507.52px" class="cls_005"><span class="cls_005">10</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:5500px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter4bayes/background11.jpg" width=720 height=540></div>
<div style="position:absolute;left:31.13px;top:11.44px" class="cls_003"><span class="cls_003">Estimate Probabilities from Data</span></div>
<div style="position:absolute;left:349.13px;top:83.60px" class="cls_007"><span class="cls_007">●</span><span class="cls_008"> Class:  P(Y) = N</span><span class="cls_020"><sub>c</sub></span><span class="cls_008">/N</span></div>
<div style="position:absolute;left:21.96px;top:126.29px" class="cls_048"><span class="cls_048">Tid</span></div>
<div style="position:absolute;left:58.07px;top:125.93px" class="cls_045"><span class="cls_045">Refund</span></div>
<div style="position:absolute;left:127.17px;top:125.93px" class="cls_045"><span class="cls_045">Marital</span></div>
<div style="position:absolute;left:203.71px;top:125.93px" class="cls_045"><span class="cls_045">Taxable</span></div>
<div style="position:absolute;left:385.13px;top:121.60px" class="cls_044"><span class="cls_044">-</span><span class="cls_043"> e.g.,  P(No) = 7/10,</span></div>
<div style="position:absolute;left:127.17px;top:142.52px" class="cls_045"><span class="cls_045">Status</span></div>
<div style="position:absolute;left:203.71px;top:142.52px" class="cls_045"><span class="cls_045">Income</span></div>
<div style="position:absolute;left:276.89px;top:139.99px" class="cls_045"><span class="cls_045">Evade</span></div>
<div style="position:absolute;left:465.13px;top:142.48px" class="cls_043"><span class="cls_043">P(Yes) = 3/10</span></div>
<div style="position:absolute;left:24.12px;top:176.18px" class="cls_046"><span class="cls_046">1</span></div>
<div style="position:absolute;left:60.11px;top:176.18px" class="cls_046"><span class="cls_046">Yes</span></div>
<div style="position:absolute;left:127.17px;top:176.18px" class="cls_046"><span class="cls_046">Single</span></div>
<div style="position:absolute;left:203.71px;top:176.18px" class="cls_046"><span class="cls_046">125K</span></div>
<div style="position:absolute;left:276.89px;top:175.94px" class="cls_047"><span class="cls_047">No</span></div>
<div style="position:absolute;left:24.12px;top:203.47px" class="cls_046"><span class="cls_046">2</span></div>
<div style="position:absolute;left:60.11px;top:203.47px" class="cls_046"><span class="cls_046">No</span></div>
<div style="position:absolute;left:127.17px;top:203.47px" class="cls_046"><span class="cls_046">Married</span></div>
<div style="position:absolute;left:203.71px;top:203.47px" class="cls_046"><span class="cls_046">100K</span></div>
<div style="position:absolute;left:276.89px;top:203.11px" class="cls_047"><span class="cls_047">No</span></div>
<div style="position:absolute;left:349.13px;top:198.56px" class="cls_007"><span class="cls_007">●</span><span class="cls_008"> For categorical attributes:</span></div>
<div style="position:absolute;left:24.12px;top:230.64px" class="cls_046"><span class="cls_046">3</span></div>
<div style="position:absolute;left:60.11px;top:230.64px" class="cls_046"><span class="cls_046">No</span></div>
<div style="position:absolute;left:127.17px;top:230.64px" class="cls_046"><span class="cls_046">Single</span></div>
<div style="position:absolute;left:203.71px;top:230.64px" class="cls_046"><span class="cls_046">70K</span></div>
<div style="position:absolute;left:276.89px;top:230.40px" class="cls_047"><span class="cls_047">No</span></div>
<div style="position:absolute;left:410.88px;top:238.40px" class="cls_008"><span class="cls_008">P(X</span><span class="cls_020"><sub>i</sub></span><span class="cls_008"> | Y</span><span class="cls_020"><sub>k</sub></span><span class="cls_008">) = |X</span><span class="cls_020"><sub>ik</sub></span><span class="cls_008">|/ N</span><span class="cls_020"><sub>c</sub></span></div>
<div style="position:absolute;left:24.12px;top:257.93px" class="cls_046"><span class="cls_046">4</span></div>
<div style="position:absolute;left:60.11px;top:257.93px" class="cls_046"><span class="cls_046">Yes</span></div>
<div style="position:absolute;left:127.17px;top:257.93px" class="cls_046"><span class="cls_046">Married</span></div>
<div style="position:absolute;left:203.71px;top:257.93px" class="cls_046"><span class="cls_046">120K</span></div>
<div style="position:absolute;left:276.89px;top:257.57px" class="cls_047"><span class="cls_047">No</span></div>
<div style="position:absolute;left:649.20px;top:261.52px" class="cls_005"><span class="cls_005">k</span></div>
<div style="position:absolute;left:24.12px;top:285.22px" class="cls_046"><span class="cls_046">5</span></div>
<div style="position:absolute;left:60.11px;top:285.22px" class="cls_046"><span class="cls_046">No</span></div>
<div style="position:absolute;left:127.17px;top:285.22px" class="cls_046"><span class="cls_046">Divorced</span></div>
<div style="position:absolute;left:203.71px;top:285.22px" class="cls_046"><span class="cls_046">95K</span></div>
<div style="position:absolute;left:276.89px;top:284.98px" class="cls_047"><span class="cls_047">Yes</span></div>
<div style="position:absolute;left:385.13px;top:289.44px" class="cls_033"><span class="cls_033">-</span></div>
<div style="position:absolute;left:412.13px;top:289.44px" class="cls_031"><span class="cls_031">where |X</span><span class="cls_034"><sub>ik</sub></span><span class="cls_031">| is number of</span></div>
<div style="position:absolute;left:24.12px;top:312.38px" class="cls_046"><span class="cls_046">6</span></div>
<div style="position:absolute;left:60.11px;top:312.38px" class="cls_046"><span class="cls_046">No</span></div>
<div style="position:absolute;left:127.17px;top:312.38px" class="cls_046"><span class="cls_046">Married</span></div>
<div style="position:absolute;left:203.71px;top:312.38px" class="cls_046"><span class="cls_046">60K</span></div>
<div style="position:absolute;left:276.89px;top:312.14px" class="cls_047"><span class="cls_047">No</span></div>
<div style="position:absolute;left:412.13px;top:315.60px" class="cls_031"><span class="cls_031">instances having attribute</span></div>
<div style="position:absolute;left:24.12px;top:339.67px" class="cls_046"><span class="cls_046">7</span></div>
<div style="position:absolute;left:60.11px;top:339.67px" class="cls_046"><span class="cls_046">Yes</span></div>
<div style="position:absolute;left:127.17px;top:339.67px" class="cls_046"><span class="cls_046">Divorced</span></div>
<div style="position:absolute;left:203.71px;top:339.67px" class="cls_046"><span class="cls_046">220K</span></div>
<div style="position:absolute;left:276.89px;top:339.31px" class="cls_047"><span class="cls_047">No</span></div>
<div style="position:absolute;left:412.13px;top:341.52px" class="cls_031"><span class="cls_031">value X</span><span class="cls_034"><sub>i</sub></span><span class="cls_031"> and belonging to</span></div>
<div style="position:absolute;left:24.12px;top:366.84px" class="cls_046"><span class="cls_046">8</span></div>
<div style="position:absolute;left:60.11px;top:366.84px" class="cls_046"><span class="cls_046">No</span></div>
<div style="position:absolute;left:127.17px;top:366.84px" class="cls_046"><span class="cls_046">Single</span></div>
<div style="position:absolute;left:203.71px;top:366.84px" class="cls_046"><span class="cls_046">85K</span></div>
<div style="position:absolute;left:276.89px;top:366.60px" class="cls_047"><span class="cls_047">Yes</span></div>
<div style="position:absolute;left:412.13px;top:367.44px" class="cls_031"><span class="cls_031">class Y</span><span class="cls_034"><sub>k</sub></span></div>
<div style="position:absolute;left:24.12px;top:394.13px" class="cls_046"><span class="cls_046">9</span></div>
<div style="position:absolute;left:60.11px;top:394.13px" class="cls_046"><span class="cls_046">No</span></div>
<div style="position:absolute;left:127.17px;top:394.13px" class="cls_046"><span class="cls_046">Married</span></div>
<div style="position:absolute;left:203.71px;top:394.13px" class="cls_046"><span class="cls_046">75K</span></div>
<div style="position:absolute;left:276.89px;top:393.77px" class="cls_047"><span class="cls_047">No</span></div>
<div style="position:absolute;left:385.13px;top:400.56px" class="cls_033"><span class="cls_033">-</span></div>
<div style="position:absolute;left:412.13px;top:400.56px" class="cls_031"><span class="cls_031">Examples:</span></div>
<div style="position:absolute;left:24.12px;top:421.30px" class="cls_046"><span class="cls_046">10</span></div>
<div style="position:absolute;left:60.11px;top:421.30px" class="cls_046"><span class="cls_046">No</span></div>
<div style="position:absolute;left:127.17px;top:421.30px" class="cls_046"><span class="cls_046">Single</span></div>
<div style="position:absolute;left:203.71px;top:421.30px" class="cls_046"><span class="cls_046">90K</span></div>
<div style="position:absolute;left:276.89px;top:421.06px" class="cls_047"><span class="cls_047">Yes</span></div>
<div style="position:absolute;left:412.13px;top:441.52px" class="cls_043"><span class="cls_043">P(Status=Married|No) = 4/7</span></div>
<div style="position:absolute;left:412.13px;top:463.60px" class="cls_043"><span class="cls_043">P(Refund=Yes|Yes)=0</span></div>
<div style="position:absolute;left:43.20px;top:507.52px" class="cls_005"><span class="cls_005">02/14/2018</span></div>
<div style="position:absolute;left:210.45px;top:507.52px" class="cls_005"><span class="cls_005">Introduction to Data Mining, 2</span><span class="cls_006"><sup>nd</sup></span><span class="cls_005"> Edition</span></div>
<div style="position:absolute;left:673.45px;top:507.52px" class="cls_005"><span class="cls_005">11</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:6050px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter4bayes/background12.jpg" width=720 height=540></div>
<div style="position:absolute;left:31.13px;top:11.44px" class="cls_003"><span class="cls_003">Estimate Probabilities from Data</span></div>
<div style="position:absolute;left:39.50px;top:92.48px" class="cls_007"><span class="cls_007">●</span><span class="cls_008">  For continuous attributes:</span></div>
<div style="position:absolute;left:75.50px;top:133.52px" class="cls_021"><span class="cls_021">-</span><span class="cls_050">  Discretization:</span><span class="cls_008"> Partition the range into bins:</span></div>
<div style="position:absolute;left:111.50px;top:174.48px" class="cls_051"><span class="cls_051">u</span><span class="cls_031">  Replace continuous value with bin value</span></div>
<div style="position:absolute;left:541.20px;top:201.52px" class="cls_005"><span class="cls_005">k</span></div>
<div style="position:absolute;left:147.50px;top:211.60px" class="cls_052"><span class="cls_052">-  Attribute changed from continuous to ordinal</span></div>
<div style="position:absolute;left:75.50px;top:267.44px" class="cls_021"><span class="cls_021">-</span><span class="cls_050">  Probability density estimation:</span></div>
<div style="position:absolute;left:111.50px;top:308.40px" class="cls_051"><span class="cls_051">u</span><span class="cls_031">  Assume attribute follows a normal distribution</span></div>
<div style="position:absolute;left:111.50px;top:344.40px" class="cls_051"><span class="cls_051">u</span><span class="cls_031">  Use data to estimate parameters of distribution</span></div>
<div style="position:absolute;left:167.38px;top:373.44px" class="cls_031"><span class="cls_031">(e.g., mean and standard deviation)</span></div>
<div style="position:absolute;left:111.50px;top:408.48px" class="cls_051"><span class="cls_051">u</span><span class="cls_031">  Once probability distribution is known, use it to</span></div>
<div style="position:absolute;left:147.50px;top:437.52px" class="cls_031"><span class="cls_031">estimate the conditional probability P(X</span><span class="cls_034"><sub>i</sub></span><span class="cls_031">|Y)</span></div>
<div style="position:absolute;left:43.20px;top:507.52px" class="cls_005"><span class="cls_005">02/14/2018</span></div>
<div style="position:absolute;left:210.45px;top:507.52px" class="cls_005"><span class="cls_005">Introduction to Data Mining, 2</span><span class="cls_006"><sup>nd</sup></span><span class="cls_005"> Edition</span></div>
<div style="position:absolute;left:673.45px;top:507.52px" class="cls_005"><span class="cls_005">12</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:6600px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter4bayes/background13.jpg" width=720 height=540></div>
<div style="position:absolute;left:31.13px;top:11.44px" class="cls_003"><span class="cls_003">Estimate Probabilities from Data</span></div>
<div style="position:absolute;left:33.52px;top:92.30px" class="cls_053"><span class="cls_053">Tid</span></div>
<div style="position:absolute;left:68.03px;top:91.95px" class="cls_054"><span class="cls_054">Refund</span></div>
<div style="position:absolute;left:134.09px;top:91.95px" class="cls_054"><span class="cls_054">Marital</span></div>
<div style="position:absolute;left:207.25px;top:91.95px" class="cls_054"><span class="cls_054">Taxable</span></div>
<div style="position:absolute;left:361.13px;top:86.40px" class="cls_030"><span class="cls_030">●</span><span class="cls_031"> Normal distribution:</span></div>
<div style="position:absolute;left:134.09px;top:107.81px" class="cls_054"><span class="cls_054">Status</span></div>
<div style="position:absolute;left:207.25px;top:107.81px" class="cls_054"><span class="cls_054">Income</span></div>
<div style="position:absolute;left:277.20px;top:105.39px" class="cls_054"><span class="cls_054">Evade</span></div>
<div style="position:absolute;left:660.03px;top:121.04px" class="cls_055"><span class="cls_055">2</span></div>
<div style="position:absolute;left:611.33px;top:123.14px" class="cls_060"><span class="cls_060">( </span><span class="cls_061">X</span></div>
<div style="position:absolute;left:631.96px;top:123.14px" class="cls_063"><span class="cls_063">-µ</span></div>
<div style="position:absolute;left:655.21px;top:123.14px" class="cls_060"><span class="cls_060">)</span></div>
<div style="position:absolute;left:627.20px;top:130.11px" class="cls_058"><span class="cls_058">i</span></div>
<div style="position:absolute;left:647.48px;top:130.11px" class="cls_058"><span class="cls_058">ij</span></div>
<div style="position:absolute;left:602.79px;top:133.07px" class="cls_063"><span class="cls_063">-</span></div>
<div style="position:absolute;left:35.58px;top:139.98px" class="cls_056"><span class="cls_056">1</span></div>
<div style="position:absolute;left:69.98px;top:139.98px" class="cls_056"><span class="cls_056">Yes</span></div>
<div style="position:absolute;left:134.09px;top:139.98px" class="cls_056"><span class="cls_056">Single</span></div>
<div style="position:absolute;left:207.25px;top:139.98px" class="cls_056"><span class="cls_056">125K</span></div>
<div style="position:absolute;left:277.20px;top:139.75px" class="cls_057"><span class="cls_057">No</span></div>
<div style="position:absolute;left:644.16px;top:142.08px" class="cls_055"><span class="cls_055">2</span></div>
<div style="position:absolute;left:549.09px;top:133.07px" class="cls_062"><span class="cls_062">1</span></div>
<div style="position:absolute;left:627.37px;top:144.18px" class="cls_060"><span class="cls_060">2</span><span class="cls_063">s</span></div>
<div style="position:absolute;left:643.00px;top:151.15px" class="cls_058"><span class="cls_058">ij</span></div>
<div style="position:absolute;left:412.01px;top:147.02px" class="cls_064"><span class="cls_064">P</span><span class="cls_062">(</span><span class="cls_064">X</span></div>
<div style="position:absolute;left:462.45px;top:147.02px" class="cls_062"><span class="cls_062">|</span><span class="cls_064">Y</span></div>
<div style="position:absolute;left:490.00px;top:147.02px" class="cls_062"><span class="cls_062">)</span></div>
<div style="position:absolute;left:502.78px;top:147.02px" class="cls_065"><span class="cls_065">=</span></div>
<div style="position:absolute;left:591.98px;top:147.02px" class="cls_064"><span class="cls_064">e</span></div>
<div style="position:absolute;left:452.17px;top:161.90px" class="cls_061"><span class="cls_061">i</span></div>
<div style="position:absolute;left:483.54px;top:161.90px" class="cls_061"><span class="cls_061">j</span></div>
<div style="position:absolute;left:35.58px;top:166.06px" class="cls_056"><span class="cls_056">2</span></div>
<div style="position:absolute;left:69.98px;top:166.06px" class="cls_056"><span class="cls_056">No</span></div>
<div style="position:absolute;left:134.09px;top:166.06px" class="cls_056"><span class="cls_056">Married</span></div>
<div style="position:absolute;left:207.25px;top:166.06px" class="cls_056"><span class="cls_056">100K</span></div>
<div style="position:absolute;left:277.20px;top:165.71px" class="cls_057"><span class="cls_057">No</span></div>
<div style="position:absolute;left:577.05px;top:167.60px" class="cls_060"><span class="cls_060">2</span></div>
<div style="position:absolute;left:537.30px;top:168.30px" class="cls_062"><span class="cls_062">2</span><span class="cls_065">ps</span></div>
<div style="position:absolute;left:575.01px;top:183.18px" class="cls_061"><span class="cls_061">ij</span></div>
<div style="position:absolute;left:35.58px;top:192.02px" class="cls_056"><span class="cls_056">3</span></div>
<div style="position:absolute;left:69.98px;top:192.02px" class="cls_056"><span class="cls_056">No</span></div>
<div style="position:absolute;left:134.09px;top:192.02px" class="cls_056"><span class="cls_056">Single</span></div>
<div style="position:absolute;left:207.25px;top:192.02px" class="cls_056"><span class="cls_056">70K</span></div>
<div style="position:absolute;left:277.20px;top:191.79px" class="cls_057"><span class="cls_057">No</span></div>
<div style="position:absolute;left:35.58px;top:218.10px" class="cls_056"><span class="cls_056">4</span></div>
<div style="position:absolute;left:69.98px;top:218.10px" class="cls_056"><span class="cls_056">Yes</span></div>
<div style="position:absolute;left:134.09px;top:218.10px" class="cls_056"><span class="cls_056">Married</span></div>
<div style="position:absolute;left:207.25px;top:218.10px" class="cls_056"><span class="cls_056">120K</span></div>
<div style="position:absolute;left:277.20px;top:217.76px" class="cls_057"><span class="cls_057">No</span></div>
<div style="position:absolute;left:397.13px;top:211.44px" class="cls_033"><span class="cls_033">-</span><span class="cls_031"> One for each (X</span><span class="cls_034"><sub>i</sub></span><span class="cls_031">,Y</span><span class="cls_034"><sub>i</sub></span><span class="cls_031">) pair</span></div>
<div style="position:absolute;left:35.58px;top:244.18px" class="cls_056"><span class="cls_056">5</span></div>
<div style="position:absolute;left:69.98px;top:244.18px" class="cls_056"><span class="cls_056">No</span></div>
<div style="position:absolute;left:134.09px;top:244.18px" class="cls_056"><span class="cls_056">Divorced</span></div>
<div style="position:absolute;left:207.25px;top:244.18px" class="cls_056"><span class="cls_056">95K</span></div>
<div style="position:absolute;left:277.20px;top:243.95px" class="cls_057"><span class="cls_057">Yes</span></div>
<div style="position:absolute;left:35.58px;top:270.15px" class="cls_056"><span class="cls_056">6</span></div>
<div style="position:absolute;left:69.98px;top:270.15px" class="cls_056"><span class="cls_056">No</span></div>
<div style="position:absolute;left:134.09px;top:270.15px" class="cls_056"><span class="cls_056">Married</span></div>
<div style="position:absolute;left:207.25px;top:270.15px" class="cls_056"><span class="cls_056">60K</span></div>
<div style="position:absolute;left:277.20px;top:269.92px" class="cls_057"><span class="cls_057">No</span></div>
<div style="position:absolute;left:361.13px;top:261.60px" class="cls_030"><span class="cls_030">●</span><span class="cls_031"> For (Income, Class=No):</span></div>
<div style="position:absolute;left:35.58px;top:296.23px" class="cls_056"><span class="cls_056">7</span></div>
<div style="position:absolute;left:69.98px;top:296.23px" class="cls_056"><span class="cls_056">Yes</span></div>
<div style="position:absolute;left:134.09px;top:296.23px" class="cls_056"><span class="cls_056">Divorced</span></div>
<div style="position:absolute;left:207.25px;top:296.23px" class="cls_056"><span class="cls_056">220K</span></div>
<div style="position:absolute;left:277.20px;top:295.88px" class="cls_057"><span class="cls_057">No</span></div>
<div style="position:absolute;left:397.13px;top:297.60px" class="cls_033"><span class="cls_033">-</span><span class="cls_031"> If Class=No</span></div>
<div style="position:absolute;left:35.58px;top:322.19px" class="cls_056"><span class="cls_056">8</span></div>
<div style="position:absolute;left:69.98px;top:322.19px" class="cls_056"><span class="cls_056">No</span></div>
<div style="position:absolute;left:134.09px;top:322.19px" class="cls_056"><span class="cls_056">Single</span></div>
<div style="position:absolute;left:207.25px;top:322.19px" class="cls_056"><span class="cls_056">85K</span></div>
<div style="position:absolute;left:277.20px;top:321.96px" class="cls_057"><span class="cls_057">Yes</span></div>
<div style="position:absolute;left:433.13px;top:332.56px" class="cls_059"><span class="cls_059">u</span><span class="cls_043"> sample mean = 110</span></div>
<div style="position:absolute;left:35.58px;top:348.27px" class="cls_056"><span class="cls_056">9</span></div>
<div style="position:absolute;left:69.98px;top:348.27px" class="cls_056"><span class="cls_056">No</span></div>
<div style="position:absolute;left:134.09px;top:348.27px" class="cls_056"><span class="cls_056">Married</span></div>
<div style="position:absolute;left:207.25px;top:348.27px" class="cls_056"><span class="cls_056">75K</span></div>
<div style="position:absolute;left:277.20px;top:347.93px" class="cls_057"><span class="cls_057">No</span></div>
<div style="position:absolute;left:433.13px;top:362.56px" class="cls_059"><span class="cls_059">u</span><span class="cls_043"> sample variance = 2975</span></div>
<div style="position:absolute;left:35.58px;top:374.24px" class="cls_056"><span class="cls_056">10</span></div>
<div style="position:absolute;left:69.98px;top:374.24px" class="cls_056"><span class="cls_056">No</span></div>
<div style="position:absolute;left:134.09px;top:374.24px" class="cls_056"><span class="cls_056">Single</span></div>
<div style="position:absolute;left:207.25px;top:374.24px" class="cls_056"><span class="cls_056">90K</span></div>
<div style="position:absolute;left:277.20px;top:374.01px" class="cls_057"><span class="cls_057">Yes</span></div>
<div style="position:absolute;left:558.29px;top:421.50px" class="cls_068"><span class="cls_068">2</span></div>
<div style="position:absolute;left:510.32px;top:422.30px" class="cls_067"><span class="cls_067">(120</span></div>
<div style="position:absolute;left:536.36px;top:422.30px" class="cls_067"><span class="cls_067">110</span></div>
<div style="position:absolute;left:554.18px;top:422.30px" class="cls_067"><span class="cls_067">)</span></div>
<div style="position:absolute;left:531.16px;top:422.30px" class="cls_070"><span class="cls_070">-</span></div>
<div style="position:absolute;left:503.70px;top:428.96px" class="cls_070"><span class="cls_070">-</span></div>
<div style="position:absolute;left:397.48px;top:410.30px" class="cls_072"><span class="cls_072">1</span></div>
<div style="position:absolute;left:516.99px;top:437.26px" class="cls_067"><span class="cls_067">2</span></div>
<div style="position:absolute;left:524.08px;top:437.26px" class="cls_067"><span class="cls_067">(</span></div>
<div style="position:absolute;left:529.65px;top:437.26px" class="cls_067"><span class="cls_067">2975</span></div>
<div style="position:absolute;left:552.68px;top:437.26px" class="cls_067"><span class="cls_067">)</span></div>
<div style="position:absolute;left:21.81px;top:430.09px" class="cls_071"><span class="cls_071">P</span><span class="cls_072">(</span><span class="cls_071">Income</span></div>
<div style="position:absolute;left:156.75px;top:430.09px" class="cls_069"><span class="cls_069">=</span><span class="cls_072">120</span></div>
<div style="position:absolute;left:231.72px;top:430.09px" class="cls_072"><span class="cls_072">|</span></div>
<div style="position:absolute;left:245.81px;top:430.09px" class="cls_071"><span class="cls_071">No</span><span class="cls_072">)</span></div>
<div style="position:absolute;left:302.21px;top:430.09px" class="cls_069"><span class="cls_069">=</span></div>
<div style="position:absolute;left:487.76px;top:430.09px" class="cls_071"><span class="cls_071">e</span></div>
<div style="position:absolute;left:574.31px;top:430.09px" class="cls_069"><span class="cls_069">=</span></div>
<div style="position:absolute;left:598.97px;top:430.09px" class="cls_072"><span class="cls_072">0.0072</span></div>
<div style="position:absolute;left:350.56px;top:454.73px" class="cls_072"><span class="cls_072">2</span><span class="cls_069">p</span></div>
<div style="position:absolute;left:389.22px;top:454.73px" class="cls_072"><span class="cls_072">(54.54</span></div>
<div style="position:absolute;left:472.33px;top:454.73px" class="cls_072"><span class="cls_072">)</span></div>
<div style="position:absolute;left:43.20px;top:507.52px" class="cls_005"><span class="cls_005">02/14/2018</span></div>
<div style="position:absolute;left:210.45px;top:507.52px" class="cls_005"><span class="cls_005">Introduction to Data Mining, 2</span><span class="cls_006"><sup>nd</sup></span><span class="cls_005"> Edition</span></div>
<div style="position:absolute;left:673.45px;top:507.52px" class="cls_005"><span class="cls_005">13</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:7150px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter4bayes/background14.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:11.44px" class="cls_003"><span class="cls_003">Example of Naïve Bayes Classifier</span></div>
<div style="position:absolute;left:25.20px;top:81.60px" class="cls_025"><span class="cls_025">Given a Test Record:</span></div>
<div style="position:absolute;left:97.14px;top:104.26px" class="cls_073"><span class="cls_073">X</span><span class="cls_074"> = </span><span class="cls_075">(Refund </span><span class="cls_074">= </span><span class="cls_075">No,Divorced,Income</span><span class="cls_074">=</span><span class="cls_075">120K)</span></div>
<div style="position:absolute;left:25.20px;top:159.68px" class="cls_076"><span class="cls_076">Naïve  Bayes Classifier:</span></div>
<div style="position:absolute;left:25.20px;top:195.52px" class="cls_077"><span class="cls_077">P(Refund = Yes | No) = 3/7</span></div>
<div style="position:absolute;left:25.20px;top:211.60px" class="cls_077"><span class="cls_077">P(Refund = No | No) = 4/7</span></div>
<div style="position:absolute;left:301.13px;top:207.44px" class="cls_078"><span class="cls_078">●</span><span class="cls_034">  P(X | No) = P(Refund=No | No)</span></div>
<div style="position:absolute;left:25.20px;top:228.64px" class="cls_077"><span class="cls_077">P(Refund = Yes | Yes) = 0</span></div>
<div style="position:absolute;left:449.63px;top:226.40px" class="cls_034"><span class="cls_034">´ P(Divorced | No)</span></div>
<div style="position:absolute;left:25.20px;top:245.68px" class="cls_077"><span class="cls_077">P(Refund = No | Yes) = 1</span></div>
<div style="position:absolute;left:449.63px;top:245.60px" class="cls_034"><span class="cls_034">´ P(Income=120K | No)</span></div>
<div style="position:absolute;left:25.20px;top:262.48px" class="cls_077"><span class="cls_077">P(Marital Status = Single | No) = 2/7</span></div>
<div style="position:absolute;left:436.13px;top:264.56px" class="cls_034"><span class="cls_034">= 4/7 ´ 1/7 ´ 0.0072 = 0.0006</span></div>
<div style="position:absolute;left:25.20px;top:279.52px" class="cls_077"><span class="cls_077">P(Marital Status = Divorced | No) = 1/7</span></div>
<div style="position:absolute;left:25.20px;top:295.60px" class="cls_077"><span class="cls_077">P(Marital Status = Married | No) = 4/7</span></div>
<div style="position:absolute;left:301.13px;top:304.40px" class="cls_078"><span class="cls_078">●</span><span class="cls_034">  P(X | Yes) = P(Refund=No | Yes)</span></div>
<div style="position:absolute;left:25.20px;top:312.64px" class="cls_077"><span class="cls_077">P(Marital Status = Single | Yes) = 2/3</span></div>
<div style="position:absolute;left:454.13px;top:323.60px" class="cls_034"><span class="cls_034">´ P(Divorced | Yes)</span></div>
<div style="position:absolute;left:25.20px;top:329.68px" class="cls_077"><span class="cls_077">P(Marital Status = Divorced | Yes) = 1/3</span></div>
<div style="position:absolute;left:25.20px;top:346.48px" class="cls_077"><span class="cls_077">P(Marital Status = Married | Yes) = 0</span></div>
<div style="position:absolute;left:454.13px;top:342.56px" class="cls_034"><span class="cls_034">´ P(Income=120K | Yes)</span></div>
<div style="position:absolute;left:440.63px;top:361.48px" class="cls_034"><span class="cls_034">= 1 ´ 1/3 ´ 1.2 ´ 10</span><span class="cls_079"><sup>-9</sup></span><span class="cls_034"> = 4 ´ 10</span><span class="cls_079"><sup>-10</sup></span></div>
<div style="position:absolute;left:25.20px;top:379.60px" class="cls_077"><span class="cls_077">For Taxable Income:</span></div>
<div style="position:absolute;left:25.20px;top:396.64px" class="cls_077"><span class="cls_077">If class = No: sample mean = 110</span></div>
<div style="position:absolute;left:301.13px;top:401.52px" class="cls_080"><span class="cls_080">Since P(X|No)P(No) > P(X|Yes)P(Yes)</span></div>
<div style="position:absolute;left:108.82px;top:413.68px" class="cls_077"><span class="cls_077">sample variance = 2975</span></div>
<div style="position:absolute;left:25.20px;top:430.48px" class="cls_077"><span class="cls_077">If class = Yes: sample mean = 90</span></div>
<div style="position:absolute;left:301.13px;top:429.60px" class="cls_080"><span class="cls_080">Therefore P(No|X) > P(Yes|X)</span></div>
<div style="position:absolute;left:108.82px;top:447.52px" class="cls_077"><span class="cls_077">sample variance = 25</span></div>
<div style="position:absolute;left:354.13px;top:451.60px" class="cls_043"><span class="cls_043">=> Class = No</span></div>
<div style="position:absolute;left:43.20px;top:507.52px" class="cls_005"><span class="cls_005">02/14/2018</span></div>
<div style="position:absolute;left:210.45px;top:507.52px" class="cls_005"><span class="cls_005">Introduction to Data Mining, 2</span><span class="cls_006"><sup>nd</sup></span><span class="cls_005"> Edition</span></div>
<div style="position:absolute;left:673.45px;top:507.52px" class="cls_005"><span class="cls_005">14</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:7700px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter4bayes/background15.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:11.44px" class="cls_003"><span class="cls_003">Example of Naïve Bayes Classifier</span></div>
<div style="position:absolute;left:25.20px;top:81.60px" class="cls_025"><span class="cls_025">Given a Test Record:</span></div>
<div style="position:absolute;left:97.14px;top:104.26px" class="cls_073"><span class="cls_073">X</span><span class="cls_074"> = </span><span class="cls_075">(Refund </span><span class="cls_074">= </span><span class="cls_075">No,Divorced,Income</span><span class="cls_074">=</span><span class="cls_075">120K)</span></div>
<div style="position:absolute;left:25.20px;top:159.68px" class="cls_076"><span class="cls_076">Naïve  Bayes Classifier:</span></div>
<div style="position:absolute;left:301.13px;top:181.44px" class="cls_078"><span class="cls_078">●</span></div>
<div style="position:absolute;left:324.13px;top:177.44px" class="cls_034"><span class="cls_034">P(Yes) = 3/10</span></div>
<div style="position:absolute;left:25.20px;top:195.52px" class="cls_077"><span class="cls_077">P(Refund = Yes | No) = 3/7</span></div>
<div style="position:absolute;left:323.63px;top:202.40px" class="cls_034"><span class="cls_034">P(No) = 7/10</span></div>
<div style="position:absolute;left:25.20px;top:211.60px" class="cls_077"><span class="cls_077">P(Refund = No | No) = 4/7</span></div>
<div style="position:absolute;left:25.20px;top:228.64px" class="cls_077"><span class="cls_077">P(Refund = Yes | Yes) = 0</span></div>
<div style="position:absolute;left:25.20px;top:245.68px" class="cls_077"><span class="cls_077">P(Refund = No | Yes) = 1</span></div>
<div style="position:absolute;left:301.13px;top:256.56px" class="cls_078"><span class="cls_078">●</span></div>
<div style="position:absolute;left:324.13px;top:252.56px" class="cls_034"><span class="cls_034">P(Yes | Divorced) = 1/3 x 3/10 / P(Divorced)</span></div>
<div style="position:absolute;left:25.20px;top:262.48px" class="cls_077"><span class="cls_077">P(Marital Status = Single | No) = 2/7</span></div>
<div style="position:absolute;left:323.63px;top:277.52px" class="cls_034"><span class="cls_034">P(No | Divorced) = 1/7 x 7/10 / P(Divorced)</span></div>
<div style="position:absolute;left:25.20px;top:279.52px" class="cls_077"><span class="cls_077">P(Marital Status = Divorced | No) = 1/7</span></div>
<div style="position:absolute;left:25.20px;top:295.60px" class="cls_077"><span class="cls_077">P(Marital Status = Married | No) = 4/7</span></div>
<div style="position:absolute;left:25.20px;top:312.64px" class="cls_077"><span class="cls_077">P(Marital Status = Single | Yes) = 2/3</span></div>
<div style="position:absolute;left:25.20px;top:329.68px" class="cls_077"><span class="cls_077">P(Marital Status = Divorced | Yes) = 1/3</span></div>
<div style="position:absolute;left:301.13px;top:335.52px" class="cls_078"><span class="cls_078">●</span></div>
<div style="position:absolute;left:324.13px;top:331.52px" class="cls_034"><span class="cls_034">P(Yes | Refund = No, Divorced) = 1 x 1/3 x 3/10 /</span></div>
<div style="position:absolute;left:25.20px;top:346.48px" class="cls_077"><span class="cls_077">P(Marital Status = Married | Yes) = 0</span></div>
<div style="position:absolute;left:490.13px;top:350.48px" class="cls_034"><span class="cls_034">P(Divorced, Refund = No)</span></div>
<div style="position:absolute;left:25.20px;top:379.60px" class="cls_077"><span class="cls_077">For Taxable Income:</span></div>
<div style="position:absolute;left:323.63px;top:376.40px" class="cls_034"><span class="cls_034">P(No | Refund = No, Divorced) = 4/7 x 1/7 x 7/10 /</span></div>
<div style="position:absolute;left:25.20px;top:396.64px" class="cls_077"><span class="cls_077">If class = No: sample mean = 110</span></div>
<div style="position:absolute;left:490.13px;top:395.60px" class="cls_034"><span class="cls_034">P(Divorced, Refund = No)</span></div>
<div style="position:absolute;left:108.82px;top:413.68px" class="cls_077"><span class="cls_077">sample variance = 2975</span></div>
<div style="position:absolute;left:25.20px;top:430.48px" class="cls_077"><span class="cls_077">If class = Yes: sample mean = 90</span></div>
<div style="position:absolute;left:108.82px;top:447.52px" class="cls_077"><span class="cls_077">sample variance = 25</span></div>
<div style="position:absolute;left:43.20px;top:507.52px" class="cls_005"><span class="cls_005">02/14/2018</span></div>
<div style="position:absolute;left:210.45px;top:507.52px" class="cls_005"><span class="cls_005">Introduction to Data Mining, 2</span><span class="cls_006"><sup>nd</sup></span><span class="cls_005"> Edition</span></div>
<div style="position:absolute;left:673.45px;top:507.52px" class="cls_005"><span class="cls_005">15</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:8250px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter4bayes/background16.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:11.44px" class="cls_003"><span class="cls_003">Issues with Naïve Bayes Classifier</span></div>
<div style="position:absolute;left:25.20px;top:128.48px" class="cls_076"><span class="cls_076">Naïve  Bayes Classifier:</span></div>
<div style="position:absolute;left:313.13px;top:135.60px" class="cls_059"><span class="cls_059">●</span><span class="cls_080">  P(Yes) = 3/10</span></div>
<div style="position:absolute;left:25.20px;top:164.56px" class="cls_077"><span class="cls_077">P(Refund = Yes | No) = 3/7</span></div>
<div style="position:absolute;left:338.13px;top:162.48px" class="cls_080"><span class="cls_080">P(No) = 7/10</span></div>
<div style="position:absolute;left:25.20px;top:180.64px" class="cls_077"><span class="cls_077">P(Refund = No | No) = 4/7</span></div>
<div style="position:absolute;left:25.20px;top:197.68px" class="cls_077"><span class="cls_077">P(Refund = Yes | Yes) = 0</span></div>
<div style="position:absolute;left:25.20px;top:214.48px" class="cls_077"><span class="cls_077">P(Refund = No | Yes) = 1</span></div>
<div style="position:absolute;left:313.13px;top:218.40px" class="cls_059"><span class="cls_059">●</span><span class="cls_080">  P(Yes | Married) = 0 x 3/10 / P(Married)</span></div>
<div style="position:absolute;left:25.20px;top:231.52px" class="cls_077"><span class="cls_077">P(Marital Status = Single | No) = 2/7</span></div>
<div style="position:absolute;left:25.20px;top:248.56px" class="cls_077"><span class="cls_077">P(Marital Status = Divorced | No) = 1/7</span></div>
<div style="position:absolute;left:338.13px;top:246.48px" class="cls_080"><span class="cls_080">P(No | Married) = 4/7 x 7/10 / P(Married)</span></div>
<div style="position:absolute;left:25.20px;top:264.64px" class="cls_077"><span class="cls_077">P(Marital Status = Married | No) = 4/7</span></div>
<div style="position:absolute;left:25.20px;top:281.68px" class="cls_077"><span class="cls_077">P(Marital Status = Single | Yes) = 2/3</span></div>
<div style="position:absolute;left:25.20px;top:298.48px" class="cls_077"><span class="cls_077">P(Marital Status = Divorced | Yes) = 1/3</span></div>
<div style="position:absolute;left:25.20px;top:315.52px" class="cls_077"><span class="cls_077">P(Marital Status = Married | Yes) = 0</span></div>
<div style="position:absolute;left:25.20px;top:348.64px" class="cls_077"><span class="cls_077">For Taxable Income:</span></div>
<div style="position:absolute;left:25.20px;top:365.68px" class="cls_077"><span class="cls_077">If class = No: sample mean = 110</span></div>
<div style="position:absolute;left:108.82px;top:382.48px" class="cls_077"><span class="cls_077">sample variance = 2975</span></div>
<div style="position:absolute;left:25.20px;top:399.52px" class="cls_077"><span class="cls_077">If class = Yes: sample mean = 90</span></div>
<div style="position:absolute;left:108.82px;top:416.56px" class="cls_077"><span class="cls_077">sample variance = 25</span></div>
<div style="position:absolute;left:43.20px;top:507.52px" class="cls_005"><span class="cls_005">02/14/2018</span></div>
<div style="position:absolute;left:210.45px;top:507.52px" class="cls_005"><span class="cls_005">Introduction to Data Mining, 2</span><span class="cls_006"><sup>nd</sup></span><span class="cls_005"> Edition</span></div>
<div style="position:absolute;left:673.45px;top:507.52px" class="cls_005"><span class="cls_005">16</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:8800px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter4bayes/background17.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:11.44px" class="cls_003"><span class="cls_003">Issues with Naïve Bayes Classifier</span></div>
<div style="position:absolute;left:25.75px;top:89.36px" class="cls_034"><span class="cls_034">Consider the table with Tid = 7 deleted</span></div>
<div style="position:absolute;left:355.20px;top:87.20px" class="cls_076"><span class="cls_076">Naïve  Bayes Classifier:</span></div>
<div style="position:absolute;left:40.60px;top:115.51px" class="cls_081"><span class="cls_081">Tid</span></div>
<div style="position:absolute;left:70.89px;top:115.21px" class="cls_082"><span class="cls_082">Refund</span></div>
<div style="position:absolute;left:128.84px;top:115.21px" class="cls_082"><span class="cls_082">Marital</span></div>
<div style="position:absolute;left:193.03px;top:115.21px" class="cls_082"><span class="cls_082">Taxable</span></div>
<div style="position:absolute;left:254.41px;top:127.00px" class="cls_082"><span class="cls_082">Evade</span></div>
<div style="position:absolute;left:355.20px;top:123.28px" class="cls_077"><span class="cls_077">P(Refund = Yes | No) = 2/6</span></div>
<div style="position:absolute;left:128.84px;top:129.12px" class="cls_082"><span class="cls_082">Status</span></div>
<div style="position:absolute;left:193.03px;top:129.12px" class="cls_082"><span class="cls_082">Income</span></div>
<div style="position:absolute;left:355.20px;top:139.12px" class="cls_077"><span class="cls_077">P(Refund = No | No) = 4/6</span></div>
<div style="position:absolute;left:42.41px;top:157.35px" class="cls_083"><span class="cls_083">1</span></div>
<div style="position:absolute;left:72.60px;top:157.35px" class="cls_083"><span class="cls_083">Yes</span></div>
<div style="position:absolute;left:128.84px;top:157.35px" class="cls_083"><span class="cls_083">Single</span></div>
<div style="position:absolute;left:193.03px;top:157.35px" class="cls_083"><span class="cls_083">125K</span></div>
<div style="position:absolute;left:254.41px;top:157.15px" class="cls_084"><span class="cls_084">No</span></div>
<div style="position:absolute;left:355.20px;top:156.16px" class="cls_077"><span class="cls_077">P(Refund = Yes | Yes) = 0</span></div>
<div style="position:absolute;left:355.20px;top:173.20px" class="cls_077"><span class="cls_077">P(Refund = No | Yes) = 1</span></div>
<div style="position:absolute;left:42.41px;top:180.23px" class="cls_083"><span class="cls_083">2</span></div>
<div style="position:absolute;left:72.60px;top:180.23px" class="cls_083"><span class="cls_083">No</span></div>
<div style="position:absolute;left:128.84px;top:180.23px" class="cls_083"><span class="cls_083">Married</span></div>
<div style="position:absolute;left:193.03px;top:180.23px" class="cls_083"><span class="cls_083">100K</span></div>
<div style="position:absolute;left:254.41px;top:179.93px" class="cls_084"><span class="cls_084">No</span></div>
<div style="position:absolute;left:355.20px;top:190.24px" class="cls_077"><span class="cls_077">P(Marital Status = Single | No) = 2/6</span></div>
<div style="position:absolute;left:42.41px;top:203.01px" class="cls_083"><span class="cls_083">3</span></div>
<div style="position:absolute;left:72.60px;top:203.01px" class="cls_083"><span class="cls_083">No</span></div>
<div style="position:absolute;left:128.84px;top:203.01px" class="cls_083"><span class="cls_083">Single</span></div>
<div style="position:absolute;left:193.03px;top:203.01px" class="cls_083"><span class="cls_083">70K</span></div>
<div style="position:absolute;left:254.41px;top:202.81px" class="cls_084"><span class="cls_084">No</span></div>
<div style="position:absolute;left:355.20px;top:207.28px" class="cls_077"><span class="cls_077">P(Marital Status = Divorced | No) = 0</span></div>
<div style="position:absolute;left:42.41px;top:225.89px" class="cls_083"><span class="cls_083">4</span></div>
<div style="position:absolute;left:72.60px;top:225.89px" class="cls_083"><span class="cls_083">Yes</span></div>
<div style="position:absolute;left:128.84px;top:225.89px" class="cls_083"><span class="cls_083">Married</span></div>
<div style="position:absolute;left:193.03px;top:225.89px" class="cls_083"><span class="cls_083">120K</span></div>
<div style="position:absolute;left:254.41px;top:225.59px" class="cls_084"><span class="cls_084">No</span></div>
<div style="position:absolute;left:355.20px;top:223.12px" class="cls_077"><span class="cls_077">P(Marital Status = Married | No) = 4/6</span></div>
<div style="position:absolute;left:355.20px;top:240.16px" class="cls_077"><span class="cls_077">P(Marital Status = Single | Yes) = 2/3</span></div>
<div style="position:absolute;left:42.41px;top:248.77px" class="cls_083"><span class="cls_083">5</span></div>
<div style="position:absolute;left:72.60px;top:248.77px" class="cls_083"><span class="cls_083">No</span></div>
<div style="position:absolute;left:128.84px;top:248.77px" class="cls_083"><span class="cls_083">Divorced</span></div>
<div style="position:absolute;left:193.03px;top:248.77px" class="cls_083"><span class="cls_083">95K</span></div>
<div style="position:absolute;left:254.41px;top:248.57px" class="cls_084"><span class="cls_084">Yes</span></div>
<div style="position:absolute;left:355.20px;top:257.20px" class="cls_077"><span class="cls_077">P(Marital Status = Divorced | Yes) = 1/3</span></div>
<div style="position:absolute;left:42.41px;top:271.55px" class="cls_083"><span class="cls_083">6</span></div>
<div style="position:absolute;left:72.60px;top:271.55px" class="cls_083"><span class="cls_083">No</span></div>
<div style="position:absolute;left:128.84px;top:271.55px" class="cls_083"><span class="cls_083">Married</span></div>
<div style="position:absolute;left:193.03px;top:271.55px" class="cls_083"><span class="cls_083">60K</span></div>
<div style="position:absolute;left:254.41px;top:271.35px" class="cls_084"><span class="cls_084">No</span></div>
<div style="position:absolute;left:355.20px;top:274.24px" class="cls_077"><span class="cls_077">P(Marital Status = Married | Yes) = 0/3</span></div>
<div style="position:absolute;left:355.20px;top:291.28px" class="cls_077"><span class="cls_077">For Taxable Income:</span></div>
<div style="position:absolute;left:355.20px;top:307.12px" class="cls_077"><span class="cls_077">If class = No: sample mean = 91</span></div>
<div style="position:absolute;left:42.41px;top:317.21px" class="cls_083"><span class="cls_083">8</span></div>
<div style="position:absolute;left:72.60px;top:317.21px" class="cls_083"><span class="cls_083">No</span></div>
<div style="position:absolute;left:128.84px;top:317.21px" class="cls_083"><span class="cls_083">Single</span></div>
<div style="position:absolute;left:193.03px;top:317.21px" class="cls_083"><span class="cls_083">85K</span></div>
<div style="position:absolute;left:254.41px;top:317.01px" class="cls_084"><span class="cls_084">Yes</span></div>
<div style="position:absolute;left:438.83px;top:324.16px" class="cls_077"><span class="cls_077">sample variance = 685</span></div>
<div style="position:absolute;left:42.41px;top:340.10px" class="cls_083"><span class="cls_083">9</span></div>
<div style="position:absolute;left:72.60px;top:340.10px" class="cls_083"><span class="cls_083">No</span></div>
<div style="position:absolute;left:128.84px;top:340.10px" class="cls_083"><span class="cls_083">Married</span></div>
<div style="position:absolute;left:193.03px;top:340.10px" class="cls_083"><span class="cls_083">75K</span></div>
<div style="position:absolute;left:254.41px;top:339.79px" class="cls_084"><span class="cls_084">No</span></div>
<div style="position:absolute;left:355.20px;top:341.20px" class="cls_077"><span class="cls_077">If class = No: sample mean = 90</span></div>
<div style="position:absolute;left:42.41px;top:362.88px" class="cls_083"><span class="cls_083">10</span></div>
<div style="position:absolute;left:72.60px;top:362.88px" class="cls_083"><span class="cls_083">No</span></div>
<div style="position:absolute;left:128.84px;top:362.88px" class="cls_083"><span class="cls_083">Single</span></div>
<div style="position:absolute;left:193.03px;top:362.88px" class="cls_083"><span class="cls_083">90K</span></div>
<div style="position:absolute;left:254.41px;top:362.68px" class="cls_084"><span class="cls_084">Yes</span></div>
<div style="position:absolute;left:438.83px;top:358.24px" class="cls_077"><span class="cls_077">sample variance = 25</span></div>
<div style="position:absolute;left:40.20px;top:402.24px" class="cls_080"><span class="cls_080">Given X = (Refund = Yes, Divorced, 120K)</span></div>
<div style="position:absolute;left:409.88px;top:414.48px" class="cls_025"><span class="cls_025">Naïve Bayes will not be able to</span></div>
<div style="position:absolute;left:40.20px;top:431.44px" class="cls_043"><span class="cls_043">P(X | No) = 2/6 X 0 X 0.0083 = 0</span></div>
<div style="position:absolute;left:440.04px;top:442.56px" class="cls_025"><span class="cls_025">classify X as Yes or No!</span></div>
<div style="position:absolute;left:40.20px;top:459.28px" class="cls_043"><span class="cls_043">P(X | Yes) = 0 X 1/3 X 1.2 X 10</span><span class="cls_086"><sup>-9</sup></span><span class="cls_043"> = 0</span></div>
<div style="position:absolute;left:43.20px;top:507.52px" class="cls_005"><span class="cls_005">02/14/2018</span></div>
<div style="position:absolute;left:210.45px;top:507.52px" class="cls_005"><span class="cls_005">Introduction to Data Mining, 2</span><span class="cls_006"><sup>nd</sup></span><span class="cls_005"> Edition</span></div>
<div style="position:absolute;left:673.45px;top:507.52px" class="cls_005"><span class="cls_005">17</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:9350px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter4bayes/background18.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:11.44px" class="cls_003"><span class="cls_003">Issues with Naïve Bayes Classifier</span></div>
<div style="position:absolute;left:39.50px;top:92.48px" class="cls_007"><span class="cls_007">●</span><span class="cls_008"> If one of the conditional probabilities is zero, then</span></div>
<div style="position:absolute;left:62.50px;top:126.56px" class="cls_008"><span class="cls_008">the entire expression becomes zero</span></div>
<div style="position:absolute;left:39.50px;top:166.56px" class="cls_030"><span class="cls_030">●</span><span class="cls_031"> Need to use other estimates of conditional probabilities</span></div>
<div style="position:absolute;left:62.50px;top:195.60px" class="cls_031"><span class="cls_031">than simple fractions</span></div>
<div style="position:absolute;left:469.20px;top:208.48px" class="cls_052"><span class="cls_052">c: number of classes</span></div>
<div style="position:absolute;left:39.50px;top:231.60px" class="cls_030"><span class="cls_030">●</span><span class="cls_031"> Probability estimation:</span></div>
<div style="position:absolute;left:469.20px;top:244.48px" class="cls_052"><span class="cls_052">p: prior probability of</span></div>
<div style="position:absolute;left:469.20px;top:268.48px" class="cls_052"><span class="cls_052">the class</span></div>
<div style="position:absolute;left:273.00px;top:276.31px" class="cls_087"><span class="cls_087">N</span></div>
<div style="position:absolute;left:290.61px;top:292.69px" class="cls_088"><span class="cls_088">ic</span></div>
<div style="position:absolute;left:63.33px;top:291.86px" class="cls_090"><span class="cls_090">Original</span></div>
<div style="position:absolute;left:148.58px;top:291.86px" class="cls_090"><span class="cls_090">: </span><span class="cls_087">P</span><span class="cls_090">(</span><span class="cls_087">A </span><span class="cls_090">|</span><span class="cls_087">C</span><span class="cls_090">)</span></div>
<div style="position:absolute;left:250.60px;top:291.86px" class="cls_089"><span class="cls_089">=</span></div>
<div style="position:absolute;left:198.82px;top:308.24px" class="cls_088"><span class="cls_088">i</span></div>
<div style="position:absolute;left:469.20px;top:304.48px" class="cls_052"><span class="cls_052">m: parameter</span></div>
<div style="position:absolute;left:274.92px;top:311.05px" class="cls_087"><span class="cls_087">N</span></div>
<div style="position:absolute;left:292.72px;top:327.43px" class="cls_088"><span class="cls_088">c</span></div>
<div style="position:absolute;left:269.16px;top:348.47px" class="cls_087"><span class="cls_087">N</span></div>
<div style="position:absolute;left:304.11px;top:348.47px" class="cls_089"><span class="cls_089">+</span><span class="cls_090">1</span></div>
<div style="position:absolute;left:469.20px;top:352.48px" class="cls_091"><span class="cls_091">N</span><span class="cls_092"><sub>c</sub></span><span class="cls_052">: number of instances</span></div>
<div style="position:absolute;left:286.76px;top:364.85px" class="cls_088"><span class="cls_088">ic</span></div>
<div style="position:absolute;left:63.71px;top:364.02px" class="cls_090"><span class="cls_090">Laplace :</span><span class="cls_087">P</span><span class="cls_090">(</span><span class="cls_087">A </span><span class="cls_090">|</span><span class="cls_087">C</span><span class="cls_090">)</span></div>
<div style="position:absolute;left:246.76px;top:364.02px" class="cls_089"><span class="cls_089">=</span></div>
<div style="position:absolute;left:194.98px;top:380.39px" class="cls_088"><span class="cls_088">i</span></div>
<div style="position:absolute;left:469.20px;top:376.48px" class="cls_052"><span class="cls_052">in the class</span></div>
<div style="position:absolute;left:269.36px;top:383.21px" class="cls_087"><span class="cls_087">N </span><span class="cls_089">+</span><span class="cls_087">c</span></div>
<div style="position:absolute;left:287.15px;top:399.59px" class="cls_088"><span class="cls_088">c</span></div>
<div style="position:absolute;left:308.72px;top:420.63px" class="cls_087"><span class="cls_087">N  </span><span class="cls_089">+</span><span class="cls_087">mp</span></div>
<div style="position:absolute;left:326.32px;top:437.01px" class="cls_088"><span class="cls_088">ic</span></div>
<div style="position:absolute;left:469.20px;top:436.48px" class="cls_091"><span class="cls_091">N</span><span class="cls_092"><sub>ic</sub></span><span class="cls_052">: number of instances</span></div>
<div style="position:absolute;left:195.05px;top:436.17px" class="cls_087"><span class="cls_087">P</span><span class="cls_090">(</span><span class="cls_087">A </span><span class="cls_090">|</span><span class="cls_087">C</span></div>
<div style="position:absolute;left:63.71px;top:436.17px" class="cls_090"><span class="cls_090">m-estimate:</span></div>
<div style="position:absolute;left:272.49px;top:436.17px" class="cls_090"><span class="cls_090">)</span></div>
<div style="position:absolute;left:286.32px;top:436.17px" class="cls_089"><span class="cls_089">=</span></div>
<div style="position:absolute;left:234.54px;top:452.55px" class="cls_088"><span class="cls_088">i</span></div>
<div style="position:absolute;left:316.97px;top:455.36px" class="cls_087"><span class="cls_087">N</span><span class="cls_088"><sub>c</sub></span></div>
<div style="position:absolute;left:348.08px;top:455.36px" class="cls_089"><span class="cls_089">+</span><span class="cls_087">m</span></div>
<div style="position:absolute;left:469.20px;top:460.48px" class="cls_052"><span class="cls_052">having attribute value </span><span class="cls_091">A</span><span class="cls_092"><sub>i</sub></span></div>
<div style="position:absolute;left:469.20px;top:484.48px" class="cls_052"><span class="cls_052">in class </span><span class="cls_091">c</span></div>
<div style="position:absolute;left:43.20px;top:507.52px" class="cls_005"><span class="cls_005">02/14/2018</span></div>
<div style="position:absolute;left:210.45px;top:507.52px" class="cls_005"><span class="cls_005">Introduction to Data Mining, 2</span><span class="cls_006"><sup>nd</sup></span><span class="cls_005"> Edition</span></div>
<div style="position:absolute;left:673.45px;top:507.52px" class="cls_005"><span class="cls_005">18</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:9900px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter4bayes/background19.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:11.44px" class="cls_003"><span class="cls_003">Example of Naïve Bayes Classifier</span></div>
<div style="position:absolute;left:38.72px;top:101.51px" class="cls_093"><span class="cls_093">Name</span></div>
<div style="position:absolute;left:100.64px;top:101.51px" class="cls_093"><span class="cls_093">Give Birth</span></div>
<div style="position:absolute;left:168.03px;top:101.51px" class="cls_093"><span class="cls_093">Can Fly</span></div>
<div style="position:absolute;left:218.43px;top:101.51px" class="cls_093"><span class="cls_093">Live in Water</span></div>
<div style="position:absolute;left:286.44px;top:101.51px" class="cls_093"><span class="cls_093">Have Legs</span></div>
<div style="position:absolute;left:367.78px;top:101.51px" class="cls_093"><span class="cls_093">Class</span></div>
<div style="position:absolute;left:469.20px;top:105.52px" class="cls_005"><span class="cls_005">A: attributes</span></div>
<div style="position:absolute;left:14.42px;top:113.72px" class="cls_094"><span class="cls_094">human</span></div>
<div style="position:absolute;left:93.97px;top:113.72px" class="cls_094"><span class="cls_094">yes</span></div>
<div style="position:absolute;left:156.50px;top:113.72px" class="cls_094"><span class="cls_094">no</span></div>
<div style="position:absolute;left:219.04px;top:113.72px" class="cls_094"><span class="cls_094">no</span></div>
<div style="position:absolute;left:281.57px;top:113.72px" class="cls_094"><span class="cls_094">yes</span></div>
<div style="position:absolute;left:344.11px;top:113.72px" class="cls_094"><span class="cls_094">mammals</span></div>
<div style="position:absolute;left:14.42px;top:127.78px" class="cls_094"><span class="cls_094">python</span></div>
<div style="position:absolute;left:93.97px;top:127.78px" class="cls_094"><span class="cls_094">no</span></div>
<div style="position:absolute;left:156.50px;top:127.78px" class="cls_094"><span class="cls_094">no</span></div>
<div style="position:absolute;left:219.04px;top:127.78px" class="cls_094"><span class="cls_094">no</span></div>
<div style="position:absolute;left:281.57px;top:127.78px" class="cls_094"><span class="cls_094">no</span></div>
<div style="position:absolute;left:344.11px;top:127.78px" class="cls_094"><span class="cls_094">non-mammals</span></div>
<div style="position:absolute;left:469.20px;top:130.48px" class="cls_005"><span class="cls_005">M: mammals</span></div>
<div style="position:absolute;left:14.42px;top:141.83px" class="cls_094"><span class="cls_094">salmon</span></div>
<div style="position:absolute;left:93.97px;top:141.83px" class="cls_094"><span class="cls_094">no</span></div>
<div style="position:absolute;left:156.50px;top:141.83px" class="cls_094"><span class="cls_094">no</span></div>
<div style="position:absolute;left:219.04px;top:141.83px" class="cls_094"><span class="cls_094">yes</span></div>
<div style="position:absolute;left:281.57px;top:141.83px" class="cls_094"><span class="cls_094">no</span></div>
<div style="position:absolute;left:344.11px;top:141.83px" class="cls_094"><span class="cls_094">non-mammals</span></div>
<div style="position:absolute;left:14.42px;top:155.88px" class="cls_094"><span class="cls_094">whale</span></div>
<div style="position:absolute;left:93.97px;top:155.88px" class="cls_094"><span class="cls_094">yes</span></div>
<div style="position:absolute;left:156.50px;top:155.88px" class="cls_094"><span class="cls_094">no</span></div>
<div style="position:absolute;left:219.04px;top:155.88px" class="cls_094"><span class="cls_094">yes</span></div>
<div style="position:absolute;left:281.57px;top:155.88px" class="cls_094"><span class="cls_094">no</span></div>
<div style="position:absolute;left:344.11px;top:155.88px" class="cls_094"><span class="cls_094">mammals</span></div>
<div style="position:absolute;left:469.20px;top:155.68px" class="cls_005"><span class="cls_005">N: non-mammals</span></div>
<div style="position:absolute;left:14.42px;top:169.95px" class="cls_094"><span class="cls_094">frog</span></div>
<div style="position:absolute;left:93.97px;top:169.95px" class="cls_094"><span class="cls_094">no</span></div>
<div style="position:absolute;left:156.50px;top:169.95px" class="cls_094"><span class="cls_094">no</span></div>
<div style="position:absolute;left:219.04px;top:169.95px" class="cls_094"><span class="cls_094">sometimes</span></div>
<div style="position:absolute;left:281.57px;top:169.95px" class="cls_094"><span class="cls_094">yes</span></div>
<div style="position:absolute;left:344.11px;top:169.95px" class="cls_094"><span class="cls_094">non-mammals</span></div>
<div style="position:absolute;left:14.42px;top:184.00px" class="cls_094"><span class="cls_094">komodo</span></div>
<div style="position:absolute;left:93.97px;top:184.00px" class="cls_094"><span class="cls_094">no</span></div>
<div style="position:absolute;left:156.50px;top:184.00px" class="cls_094"><span class="cls_094">no</span></div>
<div style="position:absolute;left:219.04px;top:184.00px" class="cls_094"><span class="cls_094">no</span></div>
<div style="position:absolute;left:281.57px;top:184.00px" class="cls_094"><span class="cls_094">yes</span></div>
<div style="position:absolute;left:344.11px;top:184.00px" class="cls_094"><span class="cls_094">non-mammals</span></div>
<div style="position:absolute;left:524.94px;top:183.23px" class="cls_096"><span class="cls_096">6</span></div>
<div style="position:absolute;left:551.80px;top:183.23px" class="cls_096"><span class="cls_096">6</span></div>
<div style="position:absolute;left:578.82px;top:183.23px" class="cls_096"><span class="cls_096">2</span></div>
<div style="position:absolute;left:605.68px;top:183.23px" class="cls_096"><span class="cls_096">2</span></div>
<div style="position:absolute;left:14.42px;top:198.05px" class="cls_094"><span class="cls_094">bat</span></div>
<div style="position:absolute;left:93.97px;top:198.05px" class="cls_094"><span class="cls_094">yes</span></div>
<div style="position:absolute;left:156.50px;top:198.05px" class="cls_094"><span class="cls_094">yes</span></div>
<div style="position:absolute;left:219.04px;top:198.05px" class="cls_094"><span class="cls_094">no</span></div>
<div style="position:absolute;left:281.57px;top:198.05px" class="cls_094"><span class="cls_094">yes</span></div>
<div style="position:absolute;left:344.11px;top:198.05px" class="cls_094"><span class="cls_094">mammals</span></div>
<div style="position:absolute;left:434.07px;top:195.34px" class="cls_098"><span class="cls_098">P</span><span class="cls_096">(</span><span class="cls_098">A</span><span class="cls_096">|</span></div>
<div style="position:absolute;left:478.11px;top:195.34px" class="cls_098"><span class="cls_098">M</span><span class="cls_096">)</span></div>
<div style="position:absolute;left:508.89px;top:195.34px" class="cls_097"><span class="cls_097">=</span></div>
<div style="position:absolute;left:537.60px;top:195.34px" class="cls_097"><span class="cls_097">´</span></div>
<div style="position:absolute;left:564.46px;top:195.34px" class="cls_097"><span class="cls_097">´</span></div>
<div style="position:absolute;left:591.33px;top:195.34px" class="cls_097"><span class="cls_097">´</span></div>
<div style="position:absolute;left:620.65px;top:195.34px" class="cls_097"><span class="cls_097">=</span></div>
<div style="position:absolute;left:635.72px;top:195.34px" class="cls_096"><span class="cls_096">0.06</span></div>
<div style="position:absolute;left:14.42px;top:212.12px" class="cls_094"><span class="cls_094">pigeon</span></div>
<div style="position:absolute;left:93.97px;top:212.12px" class="cls_094"><span class="cls_094">no</span></div>
<div style="position:absolute;left:156.50px;top:212.12px" class="cls_094"><span class="cls_094">yes</span></div>
<div style="position:absolute;left:219.04px;top:212.12px" class="cls_094"><span class="cls_094">no</span></div>
<div style="position:absolute;left:281.57px;top:212.12px" class="cls_094"><span class="cls_094">yes</span></div>
<div style="position:absolute;left:344.11px;top:212.12px" class="cls_094"><span class="cls_094">non-mammals</span></div>
<div style="position:absolute;left:524.79px;top:210.42px" class="cls_096"><span class="cls_096">7</span></div>
<div style="position:absolute;left:551.65px;top:210.42px" class="cls_096"><span class="cls_096">7</span></div>
<div style="position:absolute;left:578.51px;top:210.42px" class="cls_096"><span class="cls_096">7</span></div>
<div style="position:absolute;left:605.37px;top:210.42px" class="cls_096"><span class="cls_096">7</span></div>
<div style="position:absolute;left:14.42px;top:226.16px" class="cls_094"><span class="cls_094">cat</span></div>
<div style="position:absolute;left:93.97px;top:226.16px" class="cls_094"><span class="cls_094">yes</span></div>
<div style="position:absolute;left:156.50px;top:226.16px" class="cls_094"><span class="cls_094">no</span></div>
<div style="position:absolute;left:219.04px;top:226.16px" class="cls_094"><span class="cls_094">no</span></div>
<div style="position:absolute;left:281.57px;top:226.16px" class="cls_094"><span class="cls_094">yes</span></div>
<div style="position:absolute;left:344.11px;top:226.16px" class="cls_094"><span class="cls_094">mammals</span></div>
<div style="position:absolute;left:14.42px;top:240.23px" class="cls_094"><span class="cls_094">leopard shark</span></div>
<div style="position:absolute;left:93.97px;top:240.23px" class="cls_094"><span class="cls_094">yes</span></div>
<div style="position:absolute;left:156.50px;top:240.23px" class="cls_094"><span class="cls_094">no</span></div>
<div style="position:absolute;left:219.04px;top:240.23px" class="cls_094"><span class="cls_094">yes</span></div>
<div style="position:absolute;left:281.57px;top:240.23px" class="cls_094"><span class="cls_094">no</span></div>
<div style="position:absolute;left:344.11px;top:240.23px" class="cls_094"><span class="cls_094">non-mammals</span></div>
<div style="position:absolute;left:524.63px;top:235.66px" class="cls_096"><span class="cls_096">1</span></div>
<div style="position:absolute;left:553.19px;top:235.66px" class="cls_096"><span class="cls_096">10</span></div>
<div style="position:absolute;left:593.43px;top:235.66px" class="cls_096"><span class="cls_096">3</span></div>
<div style="position:absolute;left:627.36px;top:235.66px" class="cls_096"><span class="cls_096">4</span></div>
<div style="position:absolute;left:14.42px;top:254.28px" class="cls_094"><span class="cls_094">turtle</span></div>
<div style="position:absolute;left:93.97px;top:254.28px" class="cls_094"><span class="cls_094">no</span></div>
<div style="position:absolute;left:156.50px;top:254.28px" class="cls_094"><span class="cls_094">no</span></div>
<div style="position:absolute;left:219.04px;top:254.28px" class="cls_094"><span class="cls_094">sometimes</span></div>
<div style="position:absolute;left:281.57px;top:254.28px" class="cls_094"><span class="cls_094">yes</span></div>
<div style="position:absolute;left:344.11px;top:254.28px" class="cls_094"><span class="cls_094">non-mammals</span></div>
<div style="position:absolute;left:434.07px;top:247.76px" class="cls_098"><span class="cls_098">P</span><span class="cls_096">(</span><span class="cls_098">A</span><span class="cls_096">|</span></div>
<div style="position:absolute;left:478.72px;top:247.76px" class="cls_098"><span class="cls_098">N</span><span class="cls_096">)</span></div>
<div style="position:absolute;left:505.20px;top:247.76px" class="cls_097"><span class="cls_097">=</span></div>
<div style="position:absolute;left:540.98px;top:247.76px" class="cls_097"><span class="cls_097">´</span></div>
<div style="position:absolute;left:575.54px;top:247.76px" class="cls_097"><span class="cls_097">´</span></div>
<div style="position:absolute;left:609.47px;top:247.76px" class="cls_097"><span class="cls_097">´</span></div>
<div style="position:absolute;left:645.87px;top:247.76px" class="cls_097"><span class="cls_097">=</span></div>
<div style="position:absolute;left:660.94px;top:247.76px" class="cls_096"><span class="cls_096">0.0042</span></div>
<div style="position:absolute;left:14.42px;top:268.33px" class="cls_094"><span class="cls_094">penguin</span></div>
<div style="position:absolute;left:93.97px;top:268.33px" class="cls_094"><span class="cls_094">no</span></div>
<div style="position:absolute;left:156.50px;top:268.33px" class="cls_094"><span class="cls_094">no</span></div>
<div style="position:absolute;left:219.04px;top:268.33px" class="cls_094"><span class="cls_094">sometimes</span></div>
<div style="position:absolute;left:281.57px;top:268.33px" class="cls_094"><span class="cls_094">yes</span></div>
<div style="position:absolute;left:344.11px;top:268.33px" class="cls_094"><span class="cls_094">non-mammals</span></div>
<div style="position:absolute;left:519.25px;top:262.84px" class="cls_096"><span class="cls_096">13</span></div>
<div style="position:absolute;left:553.49px;top:262.84px" class="cls_096"><span class="cls_096">13</span></div>
<div style="position:absolute;left:587.74px;top:262.84px" class="cls_096"><span class="cls_096">13</span></div>
<div style="position:absolute;left:621.67px;top:262.84px" class="cls_096"><span class="cls_096">13</span></div>
<div style="position:absolute;left:14.42px;top:282.39px" class="cls_094"><span class="cls_094">porcupine</span></div>
<div style="position:absolute;left:93.97px;top:282.39px" class="cls_094"><span class="cls_094">yes</span></div>
<div style="position:absolute;left:156.50px;top:282.39px" class="cls_094"><span class="cls_094">no</span></div>
<div style="position:absolute;left:219.04px;top:282.39px" class="cls_094"><span class="cls_094">no</span></div>
<div style="position:absolute;left:281.57px;top:282.39px" class="cls_094"><span class="cls_094">yes</span></div>
<div style="position:absolute;left:344.11px;top:282.39px" class="cls_094"><span class="cls_094">mammals</span></div>
<div style="position:absolute;left:14.42px;top:296.44px" class="cls_094"><span class="cls_094">eel</span></div>
<div style="position:absolute;left:93.97px;top:296.44px" class="cls_094"><span class="cls_094">no</span></div>
<div style="position:absolute;left:156.50px;top:296.44px" class="cls_094"><span class="cls_094">no</span></div>
<div style="position:absolute;left:219.04px;top:296.44px" class="cls_094"><span class="cls_094">yes</span></div>
<div style="position:absolute;left:281.57px;top:296.44px" class="cls_094"><span class="cls_094">no</span></div>
<div style="position:absolute;left:344.11px;top:296.44px" class="cls_094"><span class="cls_094">non-mammals</span></div>
<div style="position:absolute;left:626.70px;top:288.07px" class="cls_096"><span class="cls_096">7</span></div>
<div style="position:absolute;left:434.07px;top:300.18px" class="cls_098"><span class="cls_098">P</span><span class="cls_096">(</span><span class="cls_098">A</span><span class="cls_096">|</span></div>
<div style="position:absolute;left:478.11px;top:300.18px" class="cls_098"><span class="cls_098">M</span><span class="cls_096">)</span><span class="cls_098">P</span><span class="cls_096">(</span><span class="cls_098">M</span><span class="cls_096">)</span></div>
<div style="position:absolute;left:556.36px;top:300.18px" class="cls_097"><span class="cls_097">=</span></div>
<div style="position:absolute;left:571.44px;top:300.18px" class="cls_096"><span class="cls_096">0.06</span><span class="cls_097">´</span></div>
<div style="position:absolute;left:646.90px;top:300.18px" class="cls_097"><span class="cls_097">=</span></div>
<div style="position:absolute;left:661.97px;top:300.18px" class="cls_096"><span class="cls_096">0.021</span></div>
<div style="position:absolute;left:14.42px;top:310.49px" class="cls_094"><span class="cls_094">salamander</span></div>
<div style="position:absolute;left:93.97px;top:310.49px" class="cls_094"><span class="cls_094">no</span></div>
<div style="position:absolute;left:156.50px;top:310.49px" class="cls_094"><span class="cls_094">no</span></div>
<div style="position:absolute;left:219.04px;top:310.49px" class="cls_094"><span class="cls_094">sometimes</span></div>
<div style="position:absolute;left:281.57px;top:310.49px" class="cls_094"><span class="cls_094">yes</span></div>
<div style="position:absolute;left:344.11px;top:310.49px" class="cls_094"><span class="cls_094">non-mammals</span></div>
<div style="position:absolute;left:14.42px;top:324.56px" class="cls_094"><span class="cls_094">gila monster</span></div>
<div style="position:absolute;left:93.97px;top:324.56px" class="cls_094"><span class="cls_094">no</span></div>
<div style="position:absolute;left:156.50px;top:324.56px" class="cls_094"><span class="cls_094">no</span></div>
<div style="position:absolute;left:219.04px;top:324.56px" class="cls_094"><span class="cls_094">no</span></div>
<div style="position:absolute;left:281.57px;top:324.56px" class="cls_094"><span class="cls_094">yes</span></div>
<div style="position:absolute;left:344.11px;top:324.56px" class="cls_094"><span class="cls_094">non-mammals</span></div>
<div style="position:absolute;left:622.08px;top:315.26px" class="cls_096"><span class="cls_096">20</span></div>
<div style="position:absolute;left:14.42px;top:338.61px" class="cls_094"><span class="cls_094">platypus</span></div>
<div style="position:absolute;left:93.97px;top:338.61px" class="cls_094"><span class="cls_094">no</span></div>
<div style="position:absolute;left:156.50px;top:338.61px" class="cls_094"><span class="cls_094">no</span></div>
<div style="position:absolute;left:219.04px;top:338.61px" class="cls_094"><span class="cls_094">no</span></div>
<div style="position:absolute;left:281.57px;top:338.61px" class="cls_094"><span class="cls_094">yes</span></div>
<div style="position:absolute;left:344.11px;top:338.61px" class="cls_094"><span class="cls_094">mammals</span></div>
<div style="position:absolute;left:623.78px;top:340.50px" class="cls_096"><span class="cls_096">13</span></div>
<div style="position:absolute;left:14.42px;top:352.65px" class="cls_094"><span class="cls_094">owl</span></div>
<div style="position:absolute;left:93.97px;top:352.65px" class="cls_094"><span class="cls_094">no</span></div>
<div style="position:absolute;left:156.50px;top:352.65px" class="cls_094"><span class="cls_094">yes</span></div>
<div style="position:absolute;left:219.04px;top:352.65px" class="cls_094"><span class="cls_094">no</span></div>
<div style="position:absolute;left:281.57px;top:352.65px" class="cls_094"><span class="cls_094">yes</span></div>
<div style="position:absolute;left:344.11px;top:352.65px" class="cls_094"><span class="cls_094">non-mammals</span></div>
<div style="position:absolute;left:434.07px;top:352.60px" class="cls_098"><span class="cls_098">P</span></div>
<div style="position:absolute;left:446.71px;top:352.60px" class="cls_096"><span class="cls_096">(</span></div>
<div style="position:absolute;left:455.35px;top:352.60px" class="cls_098"><span class="cls_098">A</span></div>
<div style="position:absolute;left:470.11px;top:352.60px" class="cls_096"><span class="cls_096">|</span></div>
<div style="position:absolute;left:478.72px;top:352.60px" class="cls_098"><span class="cls_098">N</span></div>
<div style="position:absolute;left:494.13px;top:352.60px" class="cls_096"><span class="cls_096">)</span></div>
<div style="position:absolute;left:501.84px;top:352.60px" class="cls_098"><span class="cls_098">P</span></div>
<div style="position:absolute;left:514.48px;top:352.60px" class="cls_096"><span class="cls_096">(</span></div>
<div style="position:absolute;left:522.50px;top:352.60px" class="cls_098"><span class="cls_098">N</span></div>
<div style="position:absolute;left:537.91px;top:352.60px" class="cls_096"><span class="cls_096">)</span></div>
<div style="position:absolute;left:548.98px;top:352.60px" class="cls_097"><span class="cls_097">=</span></div>
<div style="position:absolute;left:564.05px;top:352.60px" class="cls_096"><span class="cls_096">0.004</span></div>
<div style="position:absolute;left:610.19px;top:352.60px" class="cls_097"><span class="cls_097">´</span></div>
<div style="position:absolute;left:649.36px;top:352.60px" class="cls_097"><span class="cls_097">=</span></div>
<div style="position:absolute;left:664.43px;top:352.60px" class="cls_096"><span class="cls_096">0.0027</span></div>
<div style="position:absolute;left:14.42px;top:366.72px" class="cls_094"><span class="cls_094">dolphin</span></div>
<div style="position:absolute;left:93.97px;top:366.72px" class="cls_094"><span class="cls_094">yes</span></div>
<div style="position:absolute;left:156.50px;top:366.72px" class="cls_094"><span class="cls_094">no</span></div>
<div style="position:absolute;left:219.04px;top:366.72px" class="cls_094"><span class="cls_094">yes</span></div>
<div style="position:absolute;left:281.57px;top:366.72px" class="cls_094"><span class="cls_094">no</span></div>
<div style="position:absolute;left:344.11px;top:366.72px" class="cls_094"><span class="cls_094">mammals</span></div>
<div style="position:absolute;left:624.55px;top:367.68px" class="cls_096"><span class="cls_096">20</span></div>
<div style="position:absolute;left:14.42px;top:380.77px" class="cls_094"><span class="cls_094">eagle</span></div>
<div style="position:absolute;left:93.97px;top:380.77px" class="cls_094"><span class="cls_094">no</span></div>
<div style="position:absolute;left:156.50px;top:380.77px" class="cls_094"><span class="cls_094">yes</span></div>
<div style="position:absolute;left:219.04px;top:380.77px" class="cls_094"><span class="cls_094">no</span></div>
<div style="position:absolute;left:281.57px;top:380.77px" class="cls_094"><span class="cls_094">yes</span></div>
<div style="position:absolute;left:344.11px;top:380.77px" class="cls_094"><span class="cls_094">non-mammals</span></div>
<div style="position:absolute;left:463.20px;top:417.52px" class="cls_005"><span class="cls_005">P(A|M)P(M) > P(A|N)P(N)</span></div>
<div style="position:absolute;left:35.25px;top:425.48px" class="cls_095"><span class="cls_095">Give Birth</span></div>
<div style="position:absolute;left:118.51px;top:425.48px" class="cls_095"><span class="cls_095">Can Fly</span></div>
<div style="position:absolute;left:180.75px;top:425.48px" class="cls_095"><span class="cls_095">Live in Water</span></div>
<div style="position:absolute;left:264.75px;top:425.48px" class="cls_095"><span class="cls_095">Have Legs</span></div>
<div style="position:absolute;left:365.25px;top:425.48px" class="cls_095"><span class="cls_095">Class</span></div>
<div style="position:absolute;left:26.99px;top:441.13px" class="cls_099"><span class="cls_099">yes</span></div>
<div style="position:absolute;left:104.26px;top:441.13px" class="cls_099"><span class="cls_099">no</span></div>
<div style="position:absolute;left:181.50px;top:441.13px" class="cls_099"><span class="cls_099">yes</span></div>
<div style="position:absolute;left:258.76px;top:441.13px" class="cls_099"><span class="cls_099">no</span></div>
<div style="position:absolute;left:336.00px;top:441.13px" class="cls_099"><span class="cls_099">?</span></div>
<div style="position:absolute;left:463.20px;top:442.48px" class="cls_005"><span class="cls_005">=> Mammals</span></div>
<div style="position:absolute;left:43.20px;top:507.52px" class="cls_005"><span class="cls_005">02/14/2018</span></div>
<div style="position:absolute;left:210.45px;top:507.52px" class="cls_005"><span class="cls_005">Introduction to Data Mining, 2</span><span class="cls_006"><sup>nd</sup></span><span class="cls_005"> Edition</span></div>
<div style="position:absolute;left:673.45px;top:507.52px" class="cls_005"><span class="cls_005">19</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:10450px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter4bayes/background20.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:11.44px" class="cls_003"><span class="cls_003">Naïve Bayes (Summary)</span></div>
<div style="position:absolute;left:39.50px;top:89.60px" class="cls_007"><span class="cls_007">●</span><span class="cls_008"> Robust to isolated noise points</span></div>
<div style="position:absolute;left:39.50px;top:164.48px" class="cls_007"><span class="cls_007">●</span><span class="cls_008"> Handle missing values by ignoring the instance</span></div>
<div style="position:absolute;left:62.50px;top:194.48px" class="cls_008"><span class="cls_008">during probability estimate calculations</span></div>
<div style="position:absolute;left:39.50px;top:270.56px" class="cls_007"><span class="cls_007">●</span><span class="cls_008"> Robust to irrelevant attributes</span></div>
<div style="position:absolute;left:39.50px;top:345.44px" class="cls_007"><span class="cls_007">●</span><span class="cls_008"> Independence assumption may not hold for some</span></div>
<div style="position:absolute;left:62.50px;top:375.44px" class="cls_008"><span class="cls_008">attributes</span></div>
<div style="position:absolute;left:75.50px;top:413.60px" class="cls_021"><span class="cls_021">-</span><span class="cls_008"> Use other techniques such as Bayesian Belief</span></div>
<div style="position:absolute;left:102.50px;top:443.60px" class="cls_008"><span class="cls_008">Networks (BBN)</span></div>
<div style="position:absolute;left:43.20px;top:507.52px" class="cls_005"><span class="cls_005">02/14/2018</span></div>
<div style="position:absolute;left:210.45px;top:507.52px" class="cls_005"><span class="cls_005">Introduction to Data Mining, 2</span><span class="cls_006"><sup>nd</sup></span><span class="cls_005"> Edition</span></div>
<div style="position:absolute;left:673.45px;top:507.52px" class="cls_005"><span class="cls_005">20</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:11000px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter4bayes/background21.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:11.44px" class="cls_003"><span class="cls_003">Naïve Bayes</span></div>
<div style="position:absolute;left:39.50px;top:92.56px" class="cls_042"><span class="cls_042">●</span><span class="cls_043"> How does Naïve Bayes perform on the following dataset?</span></div>
<div style="position:absolute;left:153.57px;top:462.00px" class="cls_080"><span class="cls_080">Conditional independence of attributes is violated</span></div>
<div style="position:absolute;left:43.20px;top:507.52px" class="cls_005"><span class="cls_005">02/14/2018</span></div>
<div style="position:absolute;left:210.45px;top:507.52px" class="cls_005"><span class="cls_005">Introduction to Data Mining, 2</span><span class="cls_006"><sup>nd</sup></span><span class="cls_005"> Edition</span></div>
<div style="position:absolute;left:673.45px;top:507.52px" class="cls_005"><span class="cls_005">21</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:11550px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter4bayes/background22.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:11.44px" class="cls_003"><span class="cls_003">Naïve Bayes</span></div>
<div style="position:absolute;left:39.50px;top:92.56px" class="cls_042"><span class="cls_042">●</span><span class="cls_043"> How does Naïve Bayes perform on the following dataset?</span></div>
<div style="position:absolute;left:136.32px;top:458.64px" class="cls_080"><span class="cls_080">Naïve Bayes can construct oblique decision boundaries</span></div>
<div style="position:absolute;left:43.20px;top:507.52px" class="cls_005"><span class="cls_005">02/14/2018</span></div>
<div style="position:absolute;left:210.45px;top:507.52px" class="cls_005"><span class="cls_005">Introduction to Data Mining, 2</span><span class="cls_006"><sup>nd</sup></span><span class="cls_005"> Edition</span></div>
<div style="position:absolute;left:673.45px;top:507.52px" class="cls_005"><span class="cls_005">22</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:12100px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter4bayes/background23.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:11.44px" class="cls_003"><span class="cls_003">Naïve Bayes</span></div>
<div style="position:absolute;left:39.50px;top:92.56px" class="cls_042"><span class="cls_042">●</span><span class="cls_043"> How does Naïve Bayes perform on the following dataset?</span></div>
<div style="position:absolute;left:157.20px;top:189.60px" class="cls_080"><span class="cls_080">Y = 1</span></div>
<div style="position:absolute;left:231.57px;top:189.60px" class="cls_080"><span class="cls_080">1</span></div>
<div style="position:absolute;left:305.95px;top:189.60px" class="cls_080"><span class="cls_080">1</span></div>
<div style="position:absolute;left:380.45px;top:189.60px" class="cls_080"><span class="cls_080">1</span></div>
<div style="position:absolute;left:454.83px;top:189.60px" class="cls_080"><span class="cls_080">0</span></div>
<div style="position:absolute;left:157.20px;top:228.72px" class="cls_080"><span class="cls_080">Y = 2</span></div>
<div style="position:absolute;left:231.57px;top:228.72px" class="cls_080"><span class="cls_080">0</span></div>
<div style="position:absolute;left:305.95px;top:228.72px" class="cls_080"><span class="cls_080">1</span></div>
<div style="position:absolute;left:380.45px;top:228.72px" class="cls_080"><span class="cls_080">0</span></div>
<div style="position:absolute;left:454.83px;top:228.72px" class="cls_080"><span class="cls_080">0</span></div>
<div style="position:absolute;left:157.20px;top:268.56px" class="cls_080"><span class="cls_080">Y = 3</span></div>
<div style="position:absolute;left:231.57px;top:268.56px" class="cls_080"><span class="cls_080">0</span></div>
<div style="position:absolute;left:305.95px;top:268.56px" class="cls_080"><span class="cls_080">0</span></div>
<div style="position:absolute;left:380.45px;top:268.56px" class="cls_080"><span class="cls_080">1</span></div>
<div style="position:absolute;left:454.83px;top:268.56px" class="cls_080"><span class="cls_080">1</span></div>
<div style="position:absolute;left:157.20px;top:308.16px" class="cls_080"><span class="cls_080">Y = 4</span></div>
<div style="position:absolute;left:231.57px;top:308.16px" class="cls_080"><span class="cls_080">0</span></div>
<div style="position:absolute;left:305.95px;top:308.16px" class="cls_080"><span class="cls_080">0</span></div>
<div style="position:absolute;left:380.45px;top:308.16px" class="cls_080"><span class="cls_080">1</span></div>
<div style="position:absolute;left:454.83px;top:308.16px" class="cls_080"><span class="cls_080">1</span></div>
<div style="position:absolute;left:231.57px;top:348.00px" class="cls_080"><span class="cls_080">X = 1</span></div>
<div style="position:absolute;left:305.95px;top:348.00px" class="cls_080"><span class="cls_080">X = 2</span></div>
<div style="position:absolute;left:380.45px;top:348.00px" class="cls_080"><span class="cls_080">X = 3</span></div>
<div style="position:absolute;left:454.83px;top:348.00px" class="cls_080"><span class="cls_080">X = 4</span></div>
<div style="position:absolute;left:153.57px;top:405.60px" class="cls_080"><span class="cls_080">Conditional independence of attributes is violated</span></div>
<div style="position:absolute;left:43.20px;top:507.52px" class="cls_005"><span class="cls_005">02/14/2018</span></div>
<div style="position:absolute;left:210.45px;top:507.52px" class="cls_005"><span class="cls_005">Introduction to Data Mining, 2</span><span class="cls_006"><sup>nd</sup></span><span class="cls_005"> Edition</span></div>
<div style="position:absolute;left:673.45px;top:507.52px" class="cls_005"><span class="cls_005">23</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:12650px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter4bayes/background24.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:11.44px" class="cls_003"><span class="cls_003">Bayesian Belief Networks</span></div>
<div style="position:absolute;left:39.50px;top:92.48px" class="cls_007"><span class="cls_007">●</span><span class="cls_008"> Provides graphical representation of probabilistic</span></div>
<div style="position:absolute;left:62.50px;top:126.56px" class="cls_008"><span class="cls_008">relationships among a set of random variables</span></div>
<div style="position:absolute;left:39.50px;top:167.60px" class="cls_007"><span class="cls_007">●</span><span class="cls_008"> Consists of:</span></div>
<div style="position:absolute;left:75.50px;top:208.40px" class="cls_021"><span class="cls_021">-</span><span class="cls_008"> A directed acyclic graph (dag)</span></div>
<div style="position:absolute;left:539.38px;top:212.99px" class="cls_100"><span class="cls_100">A</span></div>
<div style="position:absolute;left:670.19px;top:214.81px" class="cls_100"><span class="cls_100">B</span></div>
<div style="position:absolute;left:111.50px;top:248.40px" class="cls_051"><span class="cls_051">u</span><span class="cls_031"> Node corresponds to a variable</span></div>
<div style="position:absolute;left:111.50px;top:284.40px" class="cls_051"><span class="cls_051">u</span><span class="cls_031"> Arc corresponds to dependence</span></div>
<div style="position:absolute;left:606.65px;top:302.32px" class="cls_100"><span class="cls_100">C</span></div>
<div style="position:absolute;left:111.50px;top:313.44px" class="cls_031"><span class="cls_031">relationship between a pair of variables</span></div>
<div style="position:absolute;left:75.50px;top:385.52px" class="cls_021"><span class="cls_021">-</span><span class="cls_008"> A probability table associating each node to its</span></div>
<div style="position:absolute;left:102.50px;top:418.40px" class="cls_008"><span class="cls_008">immediate parent</span></div>
<div style="position:absolute;left:43.20px;top:507.52px" class="cls_005"><span class="cls_005">02/14/2018</span></div>
<div style="position:absolute;left:210.45px;top:507.52px" class="cls_005"><span class="cls_005">Introduction to Data Mining, 2</span><span class="cls_006"><sup>nd</sup></span><span class="cls_005"> Edition</span></div>
<div style="position:absolute;left:673.45px;top:507.52px" class="cls_005"><span class="cls_005">24</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:13200px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter4bayes/background25.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:11.44px" class="cls_003"><span class="cls_003">Conditional Independence</span></div>
<div style="position:absolute;left:263.77px;top:99.65px" class="cls_101"><span class="cls_101">D</span></div>
<div style="position:absolute;left:385.20px;top:140.64px" class="cls_102"><span class="cls_102">D is parent of C</span></div>
<div style="position:absolute;left:385.20px;top:184.56px" class="cls_102"><span class="cls_102">A is child of C</span></div>
<div style="position:absolute;left:184.56px;top:208.32px" class="cls_101"><span class="cls_101">C</span></div>
<div style="position:absolute;left:385.20px;top:227.52px" class="cls_102"><span class="cls_102">B is descendant of D</span></div>
<div style="position:absolute;left:385.20px;top:270.48px" class="cls_102"><span class="cls_102">D is ancestor of A</span></div>
<div style="position:absolute;left:260.95px;top:313.80px" class="cls_101"><span class="cls_101">B</span></div>
<div style="position:absolute;left:103.68px;top:316.00px" class="cls_101"><span class="cls_101">A</span></div>
<div style="position:absolute;left:39.50px;top:392.48px" class="cls_007"><span class="cls_007">●</span><span class="cls_008"> A node in a Bayesian network is conditionally</span></div>
<div style="position:absolute;left:62.50px;top:426.56px" class="cls_008"><span class="cls_008">independent of all of its nondescendants, if its</span></div>
<div style="position:absolute;left:62.50px;top:459.44px" class="cls_008"><span class="cls_008">parents are known</span></div>
<div style="position:absolute;left:43.20px;top:507.52px" class="cls_005"><span class="cls_005">02/14/2018</span></div>
<div style="position:absolute;left:210.45px;top:507.52px" class="cls_005"><span class="cls_005">Introduction to Data Mining, 2</span><span class="cls_006"><sup>nd</sup></span><span class="cls_005"> Edition</span></div>
<div style="position:absolute;left:673.45px;top:507.52px" class="cls_005"><span class="cls_005">25</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:13750px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter4bayes/background26.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:11.44px" class="cls_003"><span class="cls_003">Conditional Independence</span></div>
<div style="position:absolute;left:39.50px;top:92.48px" class="cls_007"><span class="cls_007">●</span><span class="cls_008"> Naïve Bayes assumption:</span></div>
<div style="position:absolute;left:343.80px;top:189.37px" class="cls_103"><span class="cls_103">y</span></div>
<div style="position:absolute;left:129.58px;top:308.86px" class="cls_103"><span class="cls_103">X</span><span class="cls_105"><sub>1</sub></span></div>
<div style="position:absolute;left:212.93px;top:308.86px" class="cls_103"><span class="cls_103">X</span><span class="cls_105"><sub>2</sub></span></div>
<div style="position:absolute;left:288.70px;top:308.86px" class="cls_103"><span class="cls_103">X</span><span class="cls_105"><sub>3</sub></span></div>
<div style="position:absolute;left:363.65px;top:308.86px" class="cls_103"><span class="cls_103">X</span><span class="cls_105"><sub>4</sub></span></div>
<div style="position:absolute;left:515.13px;top:308.48px" class="cls_103"><span class="cls_103">X</span></div>
<div style="position:absolute;left:534.90px;top:330.36px" class="cls_106"><span class="cls_106">d</span></div>
<div style="position:absolute;left:43.20px;top:507.52px" class="cls_005"><span class="cls_005">02/14/2018</span></div>
<div style="position:absolute;left:210.45px;top:507.52px" class="cls_005"><span class="cls_005">Introduction to Data Mining, 2</span><span class="cls_006"><sup>nd</sup></span><span class="cls_005"> Edition</span></div>
<div style="position:absolute;left:673.45px;top:507.52px" class="cls_005"><span class="cls_005">26</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:14300px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter4bayes/background27.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:11.44px" class="cls_003"><span class="cls_003">Probability Tables</span></div>
<div style="position:absolute;left:39.50px;top:92.48px" class="cls_007"><span class="cls_007">●</span><span class="cls_008"> If X does not have any parents, table contains</span></div>
<div style="position:absolute;left:62.50px;top:126.56px" class="cls_008"><span class="cls_008">prior probability P(X)</span></div>
<div style="position:absolute;left:624.65px;top:153.13px" class="cls_107"><span class="cls_107">Y</span></div>
<div style="position:absolute;left:39.50px;top:208.40px" class="cls_007"><span class="cls_007">●</span><span class="cls_008"> If X has only one parent (Y), table contains</span></div>
<div style="position:absolute;left:62.50px;top:241.52px" class="cls_008"><span class="cls_008">conditional probability P(X|Y)</span></div>
<div style="position:absolute;left:623.87px;top:248.20px" class="cls_107"><span class="cls_107">X</span></div>
<div style="position:absolute;left:39.50px;top:323.60px" class="cls_007"><span class="cls_007">●</span><span class="cls_008"> If X has multiple parents (Y</span><span class="cls_020"><sub>1</sub></span><span class="cls_008">, Y</span><span class="cls_020"><sub>2</sub></span><span class="cls_008">,…, Y</span><span class="cls_020"><sub>k</sub></span><span class="cls_008">), table</span></div>
<div style="position:absolute;left:62.50px;top:357.44px" class="cls_008"><span class="cls_008">contains conditional probability P(X|Y</span><span class="cls_020"><sub>1</sub></span><span class="cls_008">, Y</span><span class="cls_020"><sub>2</sub></span><span class="cls_008">,…, Y</span><span class="cls_020"><sub>k</sub></span><span class="cls_008">)</span></div>
<div style="position:absolute;left:43.20px;top:507.52px" class="cls_005"><span class="cls_005">02/14/2018</span></div>
<div style="position:absolute;left:210.45px;top:507.52px" class="cls_005"><span class="cls_005">Introduction to Data Mining, 2</span><span class="cls_006"><sup>nd</sup></span><span class="cls_005"> Edition</span></div>
<div style="position:absolute;left:673.45px;top:507.52px" class="cls_005"><span class="cls_005">27</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:14850px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter4bayes/background28.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:11.44px" class="cls_003"><span class="cls_003">Example of Bayesian Belief Network</span></div>
<div style="position:absolute;left:43.51px;top:93.68px" class="cls_108"><span class="cls_108">Exercise=Yes</span></div>
<div style="position:absolute;left:178.57px;top:93.68px" class="cls_108"><span class="cls_108">0.7</span></div>
<div style="position:absolute;left:282.24px;top:92.27px" class="cls_109"><span class="cls_109">Diet=Healthy</span></div>
<div style="position:absolute;left:418.30px;top:92.27px" class="cls_109"><span class="cls_109">0.25</span></div>
<div style="position:absolute;left:43.51px;top:114.09px" class="cls_108"><span class="cls_108">Exercise=No</span></div>
<div style="position:absolute;left:178.57px;top:114.09px" class="cls_108"><span class="cls_108">0.3</span></div>
<div style="position:absolute;left:282.24px;top:112.70px" class="cls_109"><span class="cls_109">Diet=Unhealthy</span></div>
<div style="position:absolute;left:418.30px;top:112.70px" class="cls_109"><span class="cls_109">0.75</span></div>
<div style="position:absolute;left:88.45px;top:165.08px" class="cls_110"><span class="cls_110">Exercise</span></div>
<div style="position:absolute;left:334.52px;top:165.08px" class="cls_110"><span class="cls_110">Diet</span></div>
<div style="position:absolute;left:370.57px;top:250.89px" class="cls_111"><span class="cls_111">E=Healthy</span></div>
<div style="position:absolute;left:442.10px;top:250.89px" class="cls_111"><span class="cls_111">E=Healthy</span></div>
<div style="position:absolute;left:512.66px;top:250.89px" class="cls_111"><span class="cls_111">E=Unhealthy</span></div>
<div style="position:absolute;left:598.01px;top:250.89px" class="cls_111"><span class="cls_111">E=Unhealthy</span></div>
<div style="position:absolute;left:215.05px;top:254.94px" class="cls_110"><span class="cls_110">Heart</span></div>
<div style="position:absolute;left:370.57px;top:267.24px" class="cls_111"><span class="cls_111">D=Yes</span></div>
<div style="position:absolute;left:442.10px;top:267.24px" class="cls_111"><span class="cls_111">D=No</span></div>
<div style="position:absolute;left:512.66px;top:267.24px" class="cls_111"><span class="cls_111">D=Yes</span></div>
<div style="position:absolute;left:598.01px;top:267.24px" class="cls_111"><span class="cls_111">D=No</span></div>
<div style="position:absolute;left:203.53px;top:278.13px" class="cls_110"><span class="cls_110">Disease</span></div>
<div style="position:absolute;left:316.79px;top:285.01px" class="cls_111"><span class="cls_111">HD=Yes</span></div>
<div style="position:absolute;left:411.52px;top:285.01px" class="cls_111"><span class="cls_111">0.25</span></div>
<div style="position:absolute;left:482.06px;top:285.01px" class="cls_111"><span class="cls_111">0.45</span></div>
<div style="position:absolute;left:567.42px;top:285.01px" class="cls_111"><span class="cls_111">0.55</span></div>
<div style="position:absolute;left:649.82px;top:285.01px" class="cls_111"><span class="cls_111">0.75</span></div>
<div style="position:absolute;left:316.79px;top:302.32px" class="cls_111"><span class="cls_111">HD=No</span></div>
<div style="position:absolute;left:411.52px;top:302.32px" class="cls_111"><span class="cls_111">0.75</span></div>
<div style="position:absolute;left:482.06px;top:302.32px" class="cls_111"><span class="cls_111">0.55</span></div>
<div style="position:absolute;left:567.42px;top:302.32px" class="cls_111"><span class="cls_111">0.45</span></div>
<div style="position:absolute;left:649.82px;top:302.32px" class="cls_111"><span class="cls_111">0.25</span></div>
<div style="position:absolute;left:317.57px;top:370.90px" class="cls_110"><span class="cls_110">Blood</span></div>
<div style="position:absolute;left:104.85px;top:382.95px" class="cls_110"><span class="cls_110">Chest Pain</span></div>
<div style="position:absolute;left:303.33px;top:394.09px" class="cls_110"><span class="cls_110">Pressure</span></div>
<div style="position:absolute;left:126.30px;top:443.03px" class="cls_112"><span class="cls_112">HD=Yes</span></div>
<div style="position:absolute;left:180.98px;top:443.03px" class="cls_112"><span class="cls_112">HD=No</span></div>
<div style="position:absolute;left:326.17px;top:441.58px" class="cls_112"><span class="cls_112">HD=Yes</span></div>
<div style="position:absolute;left:380.85px;top:441.58px" class="cls_112"><span class="cls_112">HD=No</span></div>
<div style="position:absolute;left:72.63px;top:460.77px" class="cls_112"><span class="cls_112">CP=Yes</span></div>
<div style="position:absolute;left:157.83px;top:460.77px" class="cls_112"><span class="cls_112">0.8</span></div>
<div style="position:absolute;left:205.12px;top:460.77px" class="cls_112"><span class="cls_112">0.01</span></div>
<div style="position:absolute;left:272.49px;top:459.32px" class="cls_112"><span class="cls_112">BP=High</span></div>
<div style="position:absolute;left:350.31px;top:459.32px" class="cls_112"><span class="cls_112">0.85</span></div>
<div style="position:absolute;left:412.37px;top:459.32px" class="cls_112"><span class="cls_112">0.2</span></div>
<div style="position:absolute;left:72.63px;top:478.04px" class="cls_112"><span class="cls_112">CP=No</span></div>
<div style="position:absolute;left:157.83px;top:478.04px" class="cls_112"><span class="cls_112">0.2</span></div>
<div style="position:absolute;left:205.12px;top:478.04px" class="cls_112"><span class="cls_112">0.99</span></div>
<div style="position:absolute;left:272.49px;top:476.59px" class="cls_112"><span class="cls_112">BP=Low</span></div>
<div style="position:absolute;left:350.31px;top:476.59px" class="cls_112"><span class="cls_112">0.15</span></div>
<div style="position:absolute;left:412.37px;top:476.59px" class="cls_112"><span class="cls_112">0.8</span></div>
<div style="position:absolute;left:43.20px;top:507.52px" class="cls_005"><span class="cls_005">02/14/2018</span></div>
<div style="position:absolute;left:210.45px;top:507.52px" class="cls_005"><span class="cls_005">Introduction to Data Mining, 2</span><span class="cls_006"><sup>nd</sup></span><span class="cls_005"> Edition</span></div>
<div style="position:absolute;left:673.45px;top:507.52px" class="cls_005"><span class="cls_005">28</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:15400px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter4bayes/background29.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:11.44px" class="cls_003"><span class="cls_003">Example of Inferencing using BBN</span></div>
<div style="position:absolute;left:39.50px;top:92.40px" class="cls_030"><span class="cls_030">●</span><span class="cls_031"> Given: X = (E=No, D=Yes, CP=Yes, BP=High)</span></div>
<div style="position:absolute;left:75.50px;top:128.40px" class="cls_033"><span class="cls_033">-</span><span class="cls_031"> Compute P(HD|E,D,CP,BP)?</span></div>
<div style="position:absolute;left:39.50px;top:174.48px" class="cls_030"><span class="cls_030">●</span><span class="cls_031"> P(HD=Yes| E=No,D=Yes) = 0.55</span></div>
<div style="position:absolute;left:62.50px;top:203.52px" class="cls_031"><span class="cls_031">P(CP=Yes| HD=Yes) = 0.8</span></div>
<div style="position:absolute;left:62.50px;top:231.60px" class="cls_031"><span class="cls_031">P(BP=High| HD=Yes) = 0.85</span></div>
<div style="position:absolute;left:75.50px;top:267.52px" class="cls_044"><span class="cls_044">-</span><span class="cls_043"> P(HD=Yes|E=No,D=Yes,CP=Yes,BP=High)</span></div>
<div style="position:absolute;left:102.50px;top:291.52px" class="cls_043"><span class="cls_043">µ 0.55 ´ 0.8 ´ 0.85 = 0.374</span></div>
<div style="position:absolute;left:565.20px;top:297.84px" class="cls_102"><span class="cls_102">Classify X</span></div>
<div style="position:absolute;left:565.20px;top:326.88px" class="cls_102"><span class="cls_102">as Yes</span></div>
<div style="position:absolute;left:39.50px;top:332.40px" class="cls_030"><span class="cls_030">●</span><span class="cls_031"> P(HD=No| E=No,D=Yes) = 0.45</span></div>
<div style="position:absolute;left:62.50px;top:360.48px" class="cls_031"><span class="cls_031">P(CP=Yes| HD=No) = 0.01</span></div>
<div style="position:absolute;left:62.50px;top:389.52px" class="cls_031"><span class="cls_031">P(BP=High| HD=No) = 0.2</span></div>
<div style="position:absolute;left:75.50px;top:424.48px" class="cls_044"><span class="cls_044">-</span><span class="cls_043"> P(HD=No|E=No,D=Yes,CP=Yes,BP=High)</span></div>
<div style="position:absolute;left:102.50px;top:448.48px" class="cls_043"><span class="cls_043">µ 0.45 ´ 0.01 ´ 0.2 = 0.0009</span></div>
<div style="position:absolute;left:43.20px;top:507.52px" class="cls_005"><span class="cls_005">02/14/2018</span></div>
<div style="position:absolute;left:210.45px;top:507.52px" class="cls_005"><span class="cls_005">Introduction to Data Mining, 2</span><span class="cls_006"><sup>nd</sup></span><span class="cls_005"> Edition</span></div>
<div style="position:absolute;left:673.45px;top:507.52px" class="cls_005"><span class="cls_005">29</span></div>
</div>

</body>
</html>
