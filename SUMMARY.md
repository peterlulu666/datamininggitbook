# Summary

* [Introduction](README.md)
* [Chapter 1](chapter1.md)
* [Lecture 1](lecture1.md)
* [Chapter 2](chapter2.md)
* [Proximity Measures](proximityMeasures.md)
* [Chapter 3](chapter3.md)
* [Gini](gini.md)
* [Entropy](entropy.md)
* [Gini Entropy Practice](GiniExtropyPractice.md)
* [Gini Index Practice](GiniIndexPractice.md)
* [Entropy Practice](entropypractice.md)
* [Chapter 4 Naive Bayes Classification](chapter4bayes.md)
* [Naive Bayes Classification](Bayes.md)
* [Naive Bayes Classification Practice](NaiveBayesClassificationPractice.md)
* [Chapter 4 KNN](chapter4knn.md)
* [KNN](knn.md)
* [KNN Practice](knnpractice.md)
* [Chapter 4 Rule Based](chapter4rulebased.md)
* [One Rule Practice](onerulepractice.md)
* [C45 Ripper Accurary](C45RipperAccurary.md)
* [Chapter 4 Imbalanced Classes](chap4_imbalanced_classes.md)
* [Chapter 4 Ensemble](chap4_ensemble.md)
* [Chapter 5 Association Analysis](chapter5associationanalysis.md)
* [Apriori](Apriori.md)
* [FP Growth Tree](FP_Growth_Tree.md)
* [Chapter 7 Cluster Analysis](ClusterAnalysis.md)
* [K Means](kmeans.md)
* [Hierarchical Clustering](HierarchicalClustering.md)
* [Chapter 9 Anomaly Detection](AnomalyDetection.md)
* [DATA MINING REVIEW](DATAMININGREVIEW.md)
* [Exam 2](Exam2.md)
* [Exam 2 Solution](exam2solution.md)































