## Quiz      

1. Predictive tasks 
    - The objective of these tasks is to **predict** the value of a particular attribute based on the values of other attributes. The attribute to be predicted is commonly known as the **target** or dependent variable 
        - The **classification** is used for **discrete** target variables 
            - The predicting whether a web user will make a purchase at an online bookstore 
        - The **regression** is used for **continuous** target variables 
            - The forecasting the future price of a stock 

2. Descriptive tasks 
    - Here, the objective is to derive **patterns** (correlations, trends, clusters, trajectories, and anomalies) that summarize the underlying **relationships** in data 


