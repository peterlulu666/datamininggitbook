<html>
<head><meta http-equiv=Content-Type content="text/html; charset=UTF-8">
<style type="text/css">
<!--
span.cls_002{font-family:Tahoma,serif;font-size:32.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_002{font-family:Tahoma,serif;font-size:32.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_003{font-family:Arial,serif;font-size:32.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_003{font-family:Arial,serif;font-size:32.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_004{font-family:Arial,serif;font-size:21.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_004{font-family:Arial,serif;font-size:21.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_005{font-family:Arial,serif;font-size:28.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_005{font-family:Arial,serif;font-size:28.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_006{font-family:Arial,serif;font-size:12.1px;color:rgb(137,136,137);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_006{font-family:Arial,serif;font-size:12.1px;color:rgb(137,136,137);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_007{font-family:Arial,serif;font-size:8.0px;color:rgb(137,136,137);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_007{font-family:Arial,serif;font-size:8.0px;color:rgb(137,136,137);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_008{font-family:Arial,serif;font-size:21.1px;color:rgb(11,122,156);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_008{font-family:Arial,serif;font-size:21.1px;color:rgb(11,122,156);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_009{font-family:Arial,serif;font-size:28.1px;color:rgb(11,122,156);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_009{font-family:Arial,serif;font-size:28.1px;color:rgb(11,122,156);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_010{font-family:Times,serif;font-size:28.1px;color:rgb(0,0,0);font-weight:bold;font-style:italic;text-decoration: none}
div.cls_010{font-family:Times,serif;font-size:28.1px;color:rgb(0,0,0);font-weight:bold;font-style:italic;text-decoration: none}
span.cls_011{font-family:Times,serif;font-size:28.1px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_011{font-family:Times,serif;font-size:28.1px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_012{font-family:Arial,serif;font-size:17.1px;color:rgb(11,122,156);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_012{font-family:Arial,serif;font-size:17.1px;color:rgb(11,122,156);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_013{font-family:Times,serif;font-size:24.1px;color:rgb(0,0,0);font-weight:bold;font-style:italic;text-decoration: none}
div.cls_013{font-family:Times,serif;font-size:24.1px;color:rgb(0,0,0);font-weight:bold;font-style:italic;text-decoration: none}
span.cls_014{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_014{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_015{font-family:Times,serif;font-size:24.1px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_015{font-family:Times,serif;font-size:24.1px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_016{font-family:Arial,serif;font-size:8.7px;color:rgb(255,255,255);font-weight:bold;font-style:italic;text-decoration: none}
div.cls_016{font-family:Arial,serif;font-size:8.7px;color:rgb(255,255,255);font-weight:bold;font-style:italic;text-decoration: none}
span.cls_017{font-family:Arial,serif;font-size:8.7px;color:rgb(255,255,255);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_017{font-family:Arial,serif;font-size:8.7px;color:rgb(255,255,255);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_020{font-family:Arial,serif;font-size:16.2px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_020{font-family:Arial,serif;font-size:16.2px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_018{font-family:Arial,serif;font-size:8.7px;color:rgb(1,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_018{font-family:Arial,serif;font-size:8.7px;color:rgb(1,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_019{font-family:Arial,serif;font-size:8.7px;color:rgb(255,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_019{font-family:Arial,serif;font-size:8.7px;color:rgb(255,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_021{font-family:Arial,serif;font-size:15.5px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_021{font-family:Arial,serif;font-size:15.5px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_022{font-family:Arial,serif;font-size:14.5px;color:rgb(204,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_022{font-family:Arial,serif;font-size:14.5px;color:rgb(204,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_024{font-family:Arial,serif;font-size:15.5px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_024{font-family:Arial,serif;font-size:15.5px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_025{font-family:Arial,serif;font-size:8.8px;color:rgb(255,255,255);font-weight:bold;font-style:italic;text-decoration: none}
div.cls_025{font-family:Arial,serif;font-size:8.8px;color:rgb(255,255,255);font-weight:bold;font-style:italic;text-decoration: none}
span.cls_026{font-family:Arial,serif;font-size:8.8px;color:rgb(255,255,255);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_026{font-family:Arial,serif;font-size:8.8px;color:rgb(255,255,255);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_027{font-family:Arial,serif;font-size:8.8px;color:rgb(1,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_027{font-family:Arial,serif;font-size:8.8px;color:rgb(1,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_028{font-family:Arial,serif;font-size:8.8px;color:rgb(255,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_028{font-family:Arial,serif;font-size:8.8px;color:rgb(255,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_030{font-family:Arial,serif;font-size:18.1px;color:rgb(255,0,0);font-weight:bold;font-style:italic;text-decoration: none}
div.cls_030{font-family:Arial,serif;font-size:18.1px;color:rgb(255,0,0);font-weight:bold;font-style:italic;text-decoration: none}
span.cls_031{font-family:Arial,serif;font-size:13.1px;color:rgb(255,255,255);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_031{font-family:Arial,serif;font-size:13.1px;color:rgb(255,255,255);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_032{font-family:Arial,serif;font-size:13.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_032{font-family:Arial,serif;font-size:13.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_033{font-family:Arial,serif;font-size:13.1px;color:rgb(255,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_033{font-family:Arial,serif;font-size:13.1px;color:rgb(255,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_035{font-family:Arial,serif;font-size:16.0px;color:rgb(45,24,147);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_035{font-family:Arial,serif;font-size:16.0px;color:rgb(45,24,147);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_034{font-family:Arial,serif;font-size:16.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_034{font-family:Arial,serif;font-size:16.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_036{font-family:Arial,serif;font-size:16.0px;color:rgb(128,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_036{font-family:Arial,serif;font-size:16.0px;color:rgb(128,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_038{font-family:Arial,serif;font-size:20.1px;color:rgb(0,106,97);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_038{font-family:Arial,serif;font-size:20.1px;color:rgb(0,106,97);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_039{font-family:Arial,serif;font-size:13.3px;color:rgb(255,255,255);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_039{font-family:Arial,serif;font-size:13.3px;color:rgb(255,255,255);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_040{font-family:Arial,serif;font-size:13.3px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_040{font-family:Arial,serif;font-size:13.3px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_041{font-family:Arial,serif;font-size:13.3px;color:rgb(255,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_041{font-family:Arial,serif;font-size:13.3px;color:rgb(255,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_042{font-family:Arial,serif;font-size:18.1px;color:rgb(204,51,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_042{font-family:Arial,serif;font-size:18.1px;color:rgb(204,51,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_044{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_044{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_045{font-family:Arial,serif;font-size:14.1px;color:rgb(255,255,255);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_045{font-family:Arial,serif;font-size:14.1px;color:rgb(255,255,255);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_046{font-family:Arial,serif;font-size:14.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_046{font-family:Arial,serif;font-size:14.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_047{font-family:Arial,serif;font-size:14.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_047{font-family:Arial,serif;font-size:14.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_049{font-family:Arial,serif;font-size:14.1px;color:rgb(255,255,255);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_049{font-family:Arial,serif;font-size:14.1px;color:rgb(255,255,255);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_050{font-family:Arial,serif;font-size:14.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_050{font-family:Arial,serif;font-size:14.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_051{font-family:Arial,serif;font-size:14.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_051{font-family:Arial,serif;font-size:14.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_053{font-family:Arial,serif;font-size:14.2px;color:rgb(255,255,255);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_053{font-family:Arial,serif;font-size:14.2px;color:rgb(255,255,255);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_054{font-family:Arial,serif;font-size:14.2px;color:rgb(255,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_054{font-family:Arial,serif;font-size:14.2px;color:rgb(255,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_055{font-family:Arial,serif;font-size:14.2px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_055{font-family:Arial,serif;font-size:14.2px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_056{font-family:Arial,serif;font-size:14.2px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_056{font-family:Arial,serif;font-size:14.2px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_058{font-family:Arial,serif;font-size:16.0px;color:rgb(255,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_058{font-family:Arial,serif;font-size:16.0px;color:rgb(255,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_059{font-family:Arial,serif;font-size:14.2px;color:rgb(255,255,255);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_059{font-family:Arial,serif;font-size:14.2px;color:rgb(255,255,255);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_060{font-family:Arial,serif;font-size:14.2px;color:rgb(255,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_060{font-family:Arial,serif;font-size:14.2px;color:rgb(255,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_061{font-family:Arial,serif;font-size:14.2px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_061{font-family:Arial,serif;font-size:14.2px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_062{font-family:Arial,serif;font-size:14.2px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_062{font-family:Arial,serif;font-size:14.2px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_064{font-family:Arial,serif;font-size:14.1px;color:rgb(255,255,255);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_064{font-family:Arial,serif;font-size:14.1px;color:rgb(255,255,255);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_065{font-family:Arial,serif;font-size:14.1px;color:rgb(255,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_065{font-family:Arial,serif;font-size:14.1px;color:rgb(255,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_066{font-family:Arial,serif;font-size:14.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_066{font-family:Arial,serif;font-size:14.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_067{font-family:Arial,serif;font-size:14.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_067{font-family:Arial,serif;font-size:14.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_069{font-family:Arial,serif;font-size:14.2px;color:rgb(255,255,255);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_069{font-family:Arial,serif;font-size:14.2px;color:rgb(255,255,255);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_070{font-family:Arial,serif;font-size:14.2px;color:rgb(255,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_070{font-family:Arial,serif;font-size:14.2px;color:rgb(255,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_071{font-family:Arial,serif;font-size:14.2px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_071{font-family:Arial,serif;font-size:14.2px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_072{font-family:Arial,serif;font-size:14.2px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_072{font-family:Arial,serif;font-size:14.2px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_074{font-family:Arial,serif;font-size:8.7px;color:rgb(255,255,255);font-weight:bold;font-style:italic;text-decoration: none}
div.cls_074{font-family:Arial,serif;font-size:8.7px;color:rgb(255,255,255);font-weight:bold;font-style:italic;text-decoration: none}
span.cls_075{font-family:Arial,serif;font-size:8.7px;color:rgb(255,255,255);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_075{font-family:Arial,serif;font-size:8.7px;color:rgb(255,255,255);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_076{font-family:Arial,serif;font-size:8.7px;color:rgb(1,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_076{font-family:Arial,serif;font-size:8.7px;color:rgb(1,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_077{font-family:Arial,serif;font-size:8.7px;color:rgb(255,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_077{font-family:Arial,serif;font-size:8.7px;color:rgb(255,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_079{font-family:Arial,serif;font-size:14.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_079{font-family:Arial,serif;font-size:14.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_087{font-family:Arial,serif;font-size:11.0px;color:rgb(255,255,255);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_087{font-family:Arial,serif;font-size:11.0px;color:rgb(255,255,255);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_080{font-family:Arial,serif;font-size:15.1px;color:rgb(11,122,156);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_080{font-family:Arial,serif;font-size:15.1px;color:rgb(11,122,156);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_081{font-family:Arial,serif;font-size:13.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_081{font-family:Arial,serif;font-size:13.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_082{font-family:Arial,serif;font-size:11.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_082{font-family:Arial,serif;font-size:11.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_083{font-family:Arial,serif;font-size:11.0px;color:rgb(255,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_083{font-family:Arial,serif;font-size:11.0px;color:rgb(255,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_084{font-family:Arial,serif;font-size:20.1px;color:rgb(11,122,156);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_084{font-family:Arial,serif;font-size:20.1px;color:rgb(11,122,156);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_085{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_085{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_086{font-family:Arial,serif;font-size:13.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_086{font-family:Arial,serif;font-size:13.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_089{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_089{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_091{font-family:Arial,serif;font-size:10.9px;color:rgb(255,255,255);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_091{font-family:Arial,serif;font-size:10.9px;color:rgb(255,255,255);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_092{font-family:Arial,serif;font-size:10.9px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_092{font-family:Arial,serif;font-size:10.9px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_093{font-family:Arial,serif;font-size:10.9px;color:rgb(255,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_093{font-family:Arial,serif;font-size:10.9px;color:rgb(255,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_094{font-family:Times,serif;font-size:12.6px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_094{font-family:Times,serif;font-size:12.6px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_098{font-family:Arial,serif;font-size:18.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_098{font-family:Arial,serif;font-size:18.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_095{font-family:Times,serif;font-size:11.7px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_095{font-family:Times,serif;font-size:11.7px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_096{font-family:Arial,serif;font-size:10.6px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_096{font-family:Arial,serif;font-size:10.6px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_097{font-family:Times,serif;font-size:10.6px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_097{font-family:Times,serif;font-size:10.6px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_101{font-family:Arial,serif;font-size:10.5px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_101{font-family:Arial,serif;font-size:10.5px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_102{font-family:Times,serif;font-size:11.6px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_102{font-family:Times,serif;font-size:11.6px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_099{font-family:Arial,serif;font-size:10.3px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_099{font-family:Arial,serif;font-size:10.3px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_100{font-family:Times,serif;font-size:11.4px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_100{font-family:Times,serif;font-size:11.4px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_103{font-family:Arial,serif;font-size:18.1px;color:rgb(11,122,156);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_103{font-family:Arial,serif;font-size:18.1px;color:rgb(11,122,156);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_104{font-family:Arial,serif;font-size:24.1px;color:rgb(255,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_104{font-family:Arial,serif;font-size:24.1px;color:rgb(255,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_106{font-family:Arial,serif;font-size:14.8px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_106{font-family:Arial,serif;font-size:14.8px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_105{font-family:Arial,serif;font-size:24.1px;color:rgb(11,122,156);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_105{font-family:Arial,serif;font-size:24.1px;color:rgb(11,122,156);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_107{font-family:Arial,serif;font-size:12.7px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_107{font-family:Arial,serif;font-size:12.7px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_110{font-family:Arial,serif;font-size:13.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_110{font-family:Arial,serif;font-size:13.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_108{font-family:Arial,serif;font-size:14.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_108{font-family:Arial,serif;font-size:14.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_109{font-family:Arial,serif;font-size:14.6px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_109{font-family:Arial,serif;font-size:14.6px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_111{font-family:Arial,serif;font-size:13.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_111{font-family:Arial,serif;font-size:13.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_112{font-family:Arial,serif;font-size:10.2px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_112{font-family:Arial,serif;font-size:10.2px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_114{font-family:Arial,serif;font-size:13.3px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_114{font-family:Arial,serif;font-size:13.3px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_113{font-family:Arial,serif;font-size:10.3px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_113{font-family:Arial,serif;font-size:10.3px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_115{font-family:Arial,serif;font-size:13.4px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_115{font-family:Arial,serif;font-size:13.4px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_116{font-family:Arial,serif;font-size:10.5px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_116{font-family:Arial,serif;font-size:10.5px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_117{font-family:Arial,serif;font-size:20.9px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_117{font-family:Arial,serif;font-size:20.9px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_118{font-family:Arial,serif;font-size:15.7px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_118{font-family:Arial,serif;font-size:15.7px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_119{font-family:Arial,serif;font-size:28.1px;color:rgb(204,51,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_119{font-family:Arial,serif;font-size:28.1px;color:rgb(204,51,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_120{font-family:Arial,serif;font-size:18.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_120{font-family:Arial,serif;font-size:18.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_121{font-family:Arial,serif;font-size:13.9px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_121{font-family:Arial,serif;font-size:13.9px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_122{font-family:Arial,serif;font-size:9.3px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_122{font-family:Arial,serif;font-size:9.3px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_123{font-family:Arial,serif;font-size:12.4px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_123{font-family:Arial,serif;font-size:12.4px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_125{font-family:Arial,serif;font-size:28.1px;color:rgb(255,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_125{font-family:Arial,serif;font-size:28.1px;color:rgb(255,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_126{font-family:Arial,serif;font-size:21.5px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_126{font-family:Arial,serif;font-size:21.5px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_131{font-family:Times,serif;font-size:16.3px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_131{font-family:Times,serif;font-size:16.3px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_130{font-family:Times,serif;font-size:27.9px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_130{font-family:Times,serif;font-size:27.9px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_132{font-family:Times,serif;font-size:27.9px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_132{font-family:Times,serif;font-size:27.9px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_128{font-family:Arial,serif;font-size:27.9px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_128{font-family:Arial,serif;font-size:27.9px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_127{font-family:Arial,serif;font-size:41.8px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_127{font-family:Arial,serif;font-size:41.8px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_129{font-family:Times,serif;font-size:16.3px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_129{font-family:Times,serif;font-size:16.3px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_133{font-family:Arial,serif;font-size:34.3px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_133{font-family:Arial,serif;font-size:34.3px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_134{font-family:Arial,serif;font-size:25.7px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_134{font-family:Arial,serif;font-size:25.7px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_135{font-family:Times,serif;font-size:34.3px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_135{font-family:Times,serif;font-size:34.3px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_136{font-family:Times,serif;font-size:34.3px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_136{font-family:Times,serif;font-size:34.3px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_137{font-family:Times,serif;font-size:11.5px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_137{font-family:Times,serif;font-size:11.5px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_138{font-family:Times,serif;font-size:35.1px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_138{font-family:Times,serif;font-size:35.1px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_139{font-family:Times,serif;font-size:35.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_139{font-family:Times,serif;font-size:35.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_140{font-family:Arial,serif;font-size:35.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_140{font-family:Arial,serif;font-size:35.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_141{font-family:Times,serif;font-size:11.7px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_141{font-family:Times,serif;font-size:11.7px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_142{font-family:Tahoma,serif;font-size:15.3px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_142{font-family:Tahoma,serif;font-size:15.3px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_143{font-family:Tahoma,serif;font-size:15.3px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_143{font-family:Tahoma,serif;font-size:15.3px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_145{font-family:Times,serif;font-size:20.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_145{font-family:Times,serif;font-size:20.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_146{font-family:Times,serif;font-size:18.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_146{font-family:Times,serif;font-size:18.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_147{font-family:Tahoma,serif;font-size:16.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_147{font-family:Tahoma,serif;font-size:16.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_148{font-family:Tahoma,serif;font-size:16.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_148{font-family:Tahoma,serif;font-size:16.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_150{font-family:Tahoma,serif;font-size:15.8px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_150{font-family:Tahoma,serif;font-size:15.8px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_151{font-family:Tahoma,serif;font-size:15.8px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_151{font-family:Tahoma,serif;font-size:15.8px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_153{font-family:Tahoma,serif;font-size:15.7px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_153{font-family:Tahoma,serif;font-size:15.7px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_154{font-family:Tahoma,serif;font-size:15.7px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_154{font-family:Tahoma,serif;font-size:15.7px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_156{font-family:Tahoma,serif;font-size:15.3px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_156{font-family:Tahoma,serif;font-size:15.3px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_157{font-family:Tahoma,serif;font-size:15.3px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_157{font-family:Tahoma,serif;font-size:15.3px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_164{font-family:Times,serif;font-size:14.6px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_164{font-family:Times,serif;font-size:14.6px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_163{font-family:Times,serif;font-size:25.0px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_163{font-family:Times,serif;font-size:25.0px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_165{font-family:Times,serif;font-size:25.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_165{font-family:Times,serif;font-size:25.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_160{font-family:Arial,serif;font-size:25.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_160{font-family:Arial,serif;font-size:25.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_159{font-family:Arial,serif;font-size:37.4px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_159{font-family:Arial,serif;font-size:37.4px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_161{font-family:Times,serif;font-size:14.6px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_161{font-family:Times,serif;font-size:14.6px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_162{font-family:Times,serif;font-size:20.1px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_162{font-family:Times,serif;font-size:20.1px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_166{font-family:Arial,serif;font-size:14.0px;color:rgb(11,122,156);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_166{font-family:Arial,serif;font-size:14.0px;color:rgb(11,122,156);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_167{font-family:Tahoma,serif;font-size:13.3px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_167{font-family:Tahoma,serif;font-size:13.3px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_168{font-family:Tahoma,serif;font-size:13.3px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_168{font-family:Tahoma,serif;font-size:13.3px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_169{font-family:Tahoma,serif;font-size:22.8px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_169{font-family:Tahoma,serif;font-size:22.8px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_170{font-family:Tahoma,serif;font-size:22.8px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_170{font-family:Tahoma,serif;font-size:22.8px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_172{font-family:Tahoma,serif;font-size:22.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_172{font-family:Tahoma,serif;font-size:22.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_173{font-family:Tahoma,serif;font-size:22.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_173{font-family:Tahoma,serif;font-size:22.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_175{font-family:Tahoma,serif;font-size:22.2px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_175{font-family:Tahoma,serif;font-size:22.2px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_176{font-family:Tahoma,serif;font-size:22.2px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_176{font-family:Tahoma,serif;font-size:22.2px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_178{font-family:Times,serif;font-size:18.0px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_178{font-family:Times,serif;font-size:18.0px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_183{font-family:Times,serif;font-size:30.9px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_183{font-family:Times,serif;font-size:30.9px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_181{font-family:Arial,serif;font-size:30.9px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_181{font-family:Arial,serif;font-size:30.9px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_184{font-family:Times,serif;font-size:30.9px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_184{font-family:Times,serif;font-size:30.9px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_179{font-family:Arial,serif;font-size:46.3px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_179{font-family:Arial,serif;font-size:46.3px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_182{font-family:Arial,serif;font-size:18.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_182{font-family:Arial,serif;font-size:18.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_180{font-family:Times,serif;font-size:18.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_180{font-family:Times,serif;font-size:18.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_185{font-family:Tahoma,serif;font-size:28.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_185{font-family:Tahoma,serif;font-size:28.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_187{font-family:Tahoma,serif;font-size:20.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_187{font-family:Tahoma,serif;font-size:20.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_186{font-family:Tahoma,serif;font-size:20.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_186{font-family:Tahoma,serif;font-size:20.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_190{font-family:Tahoma,serif;font-size:18.7px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_190{font-family:Tahoma,serif;font-size:18.7px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_189{font-family:Tahoma,serif;font-size:18.7px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_189{font-family:Tahoma,serif;font-size:18.7px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_192{font-family:Arial,serif;font-size:18.1px;color:rgb(255,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_192{font-family:Arial,serif;font-size:18.1px;color:rgb(255,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_205{font-family:Arial,serif;font-size:15.8px;color:rgb(255,255,255);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_205{font-family:Arial,serif;font-size:15.8px;color:rgb(255,255,255);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_194{font-family:Arial,serif;font-size:14.1px;color:rgb(255,255,255);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_194{font-family:Arial,serif;font-size:14.1px;color:rgb(255,255,255);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_202{font-family:Arial,serif;font-size:14.1px;color:rgb(255,255,255);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_202{font-family:Arial,serif;font-size:14.1px;color:rgb(255,255,255);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_195{font-family:Arial,serif;font-size:14.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_195{font-family:Arial,serif;font-size:14.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_203{font-family:Arial,serif;font-size:14.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_203{font-family:Arial,serif;font-size:14.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_196{font-family:Arial,serif;font-size:15.8px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_196{font-family:Arial,serif;font-size:15.8px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_197{font-family:Tahoma,serif;font-size:15.8px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_197{font-family:Tahoma,serif;font-size:15.8px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_193{font-family:Tahoma,serif;font-size:14.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_193{font-family:Tahoma,serif;font-size:14.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_201{font-family:Tahoma,serif;font-size:14.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_201{font-family:Tahoma,serif;font-size:14.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_198{font-family:Tahoma,serif;font-size:15.8px;color:rgb(255,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_198{font-family:Tahoma,serif;font-size:15.8px;color:rgb(255,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_199{font-family:Tahoma,serif;font-size:14.1px;color:rgb(255,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_199{font-family:Tahoma,serif;font-size:14.1px;color:rgb(255,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_204{font-family:Tahoma,serif;font-size:14.1px;color:rgb(255,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_204{font-family:Tahoma,serif;font-size:14.1px;color:rgb(255,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_208{font-family:Arial,serif;font-size:18.1px;color:rgb(0,175,80);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_208{font-family:Arial,serif;font-size:18.1px;color:rgb(0,175,80);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_213{font-family:Arial,serif;font-size:12.0px;color:rgb(255,255,255);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_213{font-family:Arial,serif;font-size:12.0px;color:rgb(255,255,255);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_212{font-family:Times,serif;font-size:12.0px;color:rgb(255,255,255);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_212{font-family:Times,serif;font-size:12.0px;color:rgb(255,255,255);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_209{font-family:Arial,serif;font-size:12.0px;color:rgb(137,137,137);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_209{font-family:Arial,serif;font-size:12.0px;color:rgb(137,137,137);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_210{font-family:Arial,serif;font-size:12.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_210{font-family:Arial,serif;font-size:12.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_211{font-family:Arial,serif;font-size:12.0px;color:rgb(255,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_211{font-family:Arial,serif;font-size:12.0px;color:rgb(255,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_215{font-family:Arial,serif;font-size:11.9px;color:rgb(0,0,128);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_215{font-family:Arial,serif;font-size:11.9px;color:rgb(0,0,128);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_216{font-family:Arial,serif;font-size:11.9px;color:rgb(255,255,255);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_216{font-family:Arial,serif;font-size:11.9px;color:rgb(255,255,255);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_218{font-family:Arial,serif;font-size:16.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_218{font-family:Arial,serif;font-size:16.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_220{font-family:Arial,serif;font-size:11.9px;color:rgb(0,0,128);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_220{font-family:Arial,serif;font-size:11.9px;color:rgb(0,0,128);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_222{font-family:Arial,serif;font-size:11.9px;color:rgb(255,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_222{font-family:Arial,serif;font-size:11.9px;color:rgb(255,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_221{font-family:Arial,serif;font-size:11.9px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_221{font-family:Arial,serif;font-size:11.9px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_290{font-family:Arial,serif;font-size:11.9px;color:rgb(255,0,0);font-weight:bold;font-style:italic;text-decoration: underline}
div.cls_290{font-family:Arial,serif;font-size:11.9px;color:rgb(255,0,0);font-weight:bold;font-style:italic;text-decoration: none}
span.cls_225{font-family:Arial,serif;font-size:33.4px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_225{font-family:Arial,serif;font-size:33.4px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_226{font-family:Arial,serif;font-size:25.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_226{font-family:Arial,serif;font-size:25.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_228{font-family:Times,serif;font-size:33.4px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_228{font-family:Times,serif;font-size:33.4px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_229{font-family:Times,serif;font-size:33.4px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_229{font-family:Times,serif;font-size:33.4px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_227{font-family:Times,serif;font-size:11.2px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_227{font-family:Times,serif;font-size:11.2px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_230{font-family:Arial,serif;font-size:33.4px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_230{font-family:Arial,serif;font-size:33.4px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_231{font-family:Arial,serif;font-size:25.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_231{font-family:Arial,serif;font-size:25.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_234{font-family:Times,serif;font-size:33.4px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_234{font-family:Times,serif;font-size:33.4px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_235{font-family:Times,serif;font-size:33.4px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_235{font-family:Times,serif;font-size:33.4px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_232{font-family:Times,serif;font-size:11.2px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_232{font-family:Times,serif;font-size:11.2px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_233{font-family:Times,serif;font-size:11.2px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_233{font-family:Times,serif;font-size:11.2px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_239{font-family:Times,serif;font-size:9.9px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_239{font-family:Times,serif;font-size:9.9px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_240{font-family:Times,serif;font-size:29.5px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_240{font-family:Times,serif;font-size:29.5px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_236{font-family:Arial,serif;font-size:29.5px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_236{font-family:Arial,serif;font-size:29.5px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_242{font-family:Times,serif;font-size:29.5px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_242{font-family:Times,serif;font-size:29.5px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_237{font-family:Arial,serif;font-size:22.2px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_237{font-family:Arial,serif;font-size:22.2px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_238{font-family:Arial,serif;font-size:9.9px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_238{font-family:Arial,serif;font-size:9.9px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_241{font-family:Times,serif;font-size:9.9px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_241{font-family:Times,serif;font-size:9.9px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_243{font-family:Tahoma,serif;font-size:30.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_243{font-family:Tahoma,serif;font-size:30.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_244{font-family:Arial,serif;font-size:14.6px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_244{font-family:Arial,serif;font-size:14.6px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_245{font-family:Arial,serif;font-size:9.8px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_245{font-family:Arial,serif;font-size:9.8px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_246{font-family:Arial,serif;font-size:13.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_246{font-family:Arial,serif;font-size:13.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_248{font-family:Times,serif;font-size:28.7px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_248{font-family:Times,serif;font-size:28.7px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_254{font-family:Times,serif;font-size:10.9px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_254{font-family:Times,serif;font-size:10.9px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_255{font-family:Times,serif;font-size:32.5px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_255{font-family:Times,serif;font-size:32.5px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_249{font-family:Times,serif;font-size:9.6px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_249{font-family:Times,serif;font-size:9.6px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_250{font-family:Arial,serif;font-size:28.7px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_250{font-family:Arial,serif;font-size:28.7px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_253{font-family:Arial,serif;font-size:32.5px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_253{font-family:Arial,serif;font-size:32.5px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_251{font-family:Arial,serif;font-size:24.4px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_251{font-family:Arial,serif;font-size:24.4px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_257{font-family:Times,serif;font-size:32.5px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_257{font-family:Times,serif;font-size:32.5px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_252{font-family:Arial,serif;font-size:10.9px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_252{font-family:Arial,serif;font-size:10.9px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_256{font-family:Times,serif;font-size:10.9px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_256{font-family:Times,serif;font-size:10.9px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_258{font-family:Arial,serif;font-size:18.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_258{font-family:Arial,serif;font-size:18.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_259{font-family:Arial,serif;font-size:12.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_259{font-family:Arial,serif;font-size:12.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_260{font-family:Arial,serif;font-size:13.0px;color:rgb(11,122,156);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_260{font-family:Arial,serif;font-size:13.0px;color:rgb(11,122,156);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_269{font-family:Arial,serif;font-size:15.8px;color:rgb(255,255,255);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_269{font-family:Arial,serif;font-size:15.8px;color:rgb(255,255,255);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_262{font-family:Arial,serif;font-size:14.1px;color:rgb(255,255,255);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_262{font-family:Arial,serif;font-size:14.1px;color:rgb(255,255,255);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_263{font-family:Arial,serif;font-size:14.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_263{font-family:Arial,serif;font-size:14.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_264{font-family:Arial,serif;font-size:15.8px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_264{font-family:Arial,serif;font-size:15.8px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_265{font-family:Tahoma,serif;font-size:15.8px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_265{font-family:Tahoma,serif;font-size:15.8px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_261{font-family:Tahoma,serif;font-size:14.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_261{font-family:Tahoma,serif;font-size:14.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_266{font-family:Tahoma,serif;font-size:15.8px;color:rgb(255,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_266{font-family:Tahoma,serif;font-size:15.8px;color:rgb(255,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_267{font-family:Tahoma,serif;font-size:14.1px;color:rgb(255,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_267{font-family:Tahoma,serif;font-size:14.1px;color:rgb(255,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_271{font-family:Times,serif;font-size:38.6px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_271{font-family:Times,serif;font-size:38.6px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_272{font-family:Times,serif;font-size:38.6px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_272{font-family:Times,serif;font-size:38.6px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_274{font-family:Arial,serif;font-size:38.6px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_274{font-family:Arial,serif;font-size:38.6px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_273{font-family:Times,serif;font-size:12.9px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_273{font-family:Times,serif;font-size:12.9px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_276{font-family:Tahoma,serif;font-size:19.9px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_276{font-family:Tahoma,serif;font-size:19.9px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_275{font-family:Tahoma,serif;font-size:19.9px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_275{font-family:Tahoma,serif;font-size:19.9px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_279{font-family:Tahoma,serif;font-size:18.6px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_279{font-family:Tahoma,serif;font-size:18.6px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_278{font-family:Tahoma,serif;font-size:18.6px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_278{font-family:Tahoma,serif;font-size:18.6px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_281{font-family:Arial,serif;font-size:20.1px;color:rgb(255,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_281{font-family:Arial,serif;font-size:20.1px;color:rgb(255,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_283{font-family:Tahoma,serif;font-size:18.3px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_283{font-family:Tahoma,serif;font-size:18.3px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_282{font-family:Tahoma,serif;font-size:18.3px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_282{font-family:Tahoma,serif;font-size:18.3px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_285{font-family:Arial,serif;font-size:22.0px;color:rgb(11,122,156);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_285{font-family:Arial,serif;font-size:22.0px;color:rgb(11,122,156);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_286{font-family:Arial,serif;font-size:22.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_286{font-family:Arial,serif;font-size:22.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_287{font-family:Arial,serif;font-size:18.1px;color:rgb(0,111,192);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_287{font-family:Arial,serif;font-size:18.1px;color:rgb(0,111,192);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_288{font-family:Tahoma,serif;font-size:23.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_288{font-family:Tahoma,serif;font-size:23.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_289{font-family:Arial,serif;font-size:20.1px;color:rgb(0,111,192);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_289{font-family:Arial,serif;font-size:20.1px;color:rgb(0,111,192);font-weight:bold;font-style:normal;text-decoration: none}
-->
</style>
<script type="text/javascript" src="chapter3/wz_jsgraphics.js"></script>
</head>
<body>
<div style="position:absolute;left:50%;margin-left:-360px;top:0px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background01.jpg" width=720 height=540></div>
<div style="position:absolute;left:265.94px;top:0.00px" class="cls_002"><span class="cls_002">Data Mining</span></div>
<div style="position:absolute;left:90.62px;top:35.44px" class="cls_002"><span class="cls_002">Classification: Basic Concepts and</span></div>
<div style="position:absolute;left:271.87px;top:71.44px" class="cls_002"><span class="cls_002">Techniques</span></div>
<div style="position:absolute;left:153.88px;top:156.16px" class="cls_003"><span class="cls_003">Lecture Notes for Chapter 3</span></div>
<div style="position:absolute;left:77.14px;top:248.08px" class="cls_003"><span class="cls_003">Introduction to Data Mining, 2</span><span class="cls_004"><sup>nd</sup></span><span class="cls_003"> Edition</span></div>
<div style="position:absolute;left:336.14px;top:294.08px" class="cls_005"><span class="cls_005">by</span></div>
<div style="position:absolute;left:144.44px;top:334.16px" class="cls_005"><span class="cls_005">Tan, Steinbach, Karpatne, Kumar</span></div>
<div style="position:absolute;left:56.70px;top:507.60px" class="cls_006"><span class="cls_006">10/23/21</span></div>
<div style="position:absolute;left:248.06px;top:507.60px" class="cls_006"><span class="cls_006">Introduction to Data Mining, 2</span><span class="cls_007"><sup>nd</sup></span><span class="cls_006"> Edition</span></div>
<div style="position:absolute;left:656.58px;top:507.60px" class="cls_006"><span class="cls_006">1</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:550px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background02.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:11.44px" class="cls_002"><span class="cls_002">Classification: Definition</span></div>
<div style="position:absolute;left:39.50px;top:92.48px" class="cls_008"><span class="cls_008">●</span><span class="cls_005"> Given a collection of records (training set )</span></div>
<div style="position:absolute;left:75.50px;top:133.52px" class="cls_009"><span class="cls_009">-</span><span class="cls_005"> Each record is by characterized by a tuple</span></div>
<div style="position:absolute;left:102.50px;top:167.60px" class="cls_005"><span class="cls_005">(</span><span class="cls_010">x</span><span class="cls_005">,</span><span class="cls_011">y</span><span class="cls_005">), where </span><span class="cls_010">x </span><span class="cls_005">is the attribute set and </span><span class="cls_011">y </span><span class="cls_005">is the</span></div>
<div style="position:absolute;left:102.50px;top:200.48px" class="cls_005"><span class="cls_005">class label</span></div>
<div style="position:absolute;left:111.50px;top:241.44px" class="cls_012"><span class="cls_012">u</span><span class="cls_013"> x</span><span class="cls_014">: attribute, predictor, independent variable, input</span></div>
<div style="position:absolute;left:111.50px;top:277.44px" class="cls_012"><span class="cls_012">u</span><span class="cls_015"> y</span><span class="cls_014">: class, response, dependent variable, output</span></div>
<div style="position:absolute;left:39.50px;top:342.56px" class="cls_008"><span class="cls_008">●</span><span class="cls_005"> Task:</span></div>
<div style="position:absolute;left:75.50px;top:382.40px" class="cls_009"><span class="cls_009">-</span><span class="cls_005"> Learn a model that maps each attribute set </span><span class="cls_010">x</span></div>
<div style="position:absolute;left:102.50px;top:416.48px" class="cls_005"><span class="cls_005">into one of the predefined class labels </span><span class="cls_011">y</span></div>
<div style="position:absolute;left:56.70px;top:507.60px" class="cls_006"><span class="cls_006">10/23/21</span></div>
<div style="position:absolute;left:248.06px;top:507.60px" class="cls_006"><span class="cls_006">Introduction to Data Mining, 2</span><span class="cls_007"><sup>nd</sup></span><span class="cls_006"> Edition</span></div>
<div style="position:absolute;left:656.58px;top:507.60px" class="cls_006"><span class="cls_006">2</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:1100px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background03.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:11.44px" class="cls_002"><span class="cls_002">Examples of Classification Task</span></div>
<div style="position:absolute;left:82.14px;top:110.64px" class="cls_014"><span class="cls_014">Task</span></div>
<div style="position:absolute;left:248.50px;top:110.64px" class="cls_014"><span class="cls_014">Attribute set, </span><span class="cls_013">x</span></div>
<div style="position:absolute;left:512.38px;top:110.64px" class="cls_014"><span class="cls_014">Class label, </span><span class="cls_015">y</span></div>
<div style="position:absolute;left:37.20px;top:170.64px" class="cls_014"><span class="cls_014">Categorizing</span></div>
<div style="position:absolute;left:190.82px;top:170.64px" class="cls_014"><span class="cls_014">Features extracted from</span></div>
<div style="position:absolute;left:473.83px;top:170.64px" class="cls_014"><span class="cls_014">spam or non-spam</span></div>
<div style="position:absolute;left:37.20px;top:199.68px" class="cls_014"><span class="cls_014">email</span></div>
<div style="position:absolute;left:190.82px;top:199.68px" class="cls_014"><span class="cls_014">email message header</span></div>
<div style="position:absolute;left:37.20px;top:228.48px" class="cls_014"><span class="cls_014">messages</span></div>
<div style="position:absolute;left:190.82px;top:228.48px" class="cls_014"><span class="cls_014">and content</span></div>
<div style="position:absolute;left:37.20px;top:272.64px" class="cls_014"><span class="cls_014">Identifying</span></div>
<div style="position:absolute;left:190.82px;top:272.64px" class="cls_014"><span class="cls_014">Features extracted from</span></div>
<div style="position:absolute;left:473.83px;top:272.64px" class="cls_014"><span class="cls_014">malignant or benign</span></div>
<div style="position:absolute;left:37.20px;top:301.68px" class="cls_014"><span class="cls_014">tumor cells</span></div>
<div style="position:absolute;left:190.82px;top:301.68px" class="cls_014"><span class="cls_014">MRI scans</span></div>
<div style="position:absolute;left:473.83px;top:301.68px" class="cls_014"><span class="cls_014">cells</span></div>
<div style="position:absolute;left:37.20px;top:374.64px" class="cls_014"><span class="cls_014">Cataloging</span></div>
<div style="position:absolute;left:190.82px;top:374.64px" class="cls_014"><span class="cls_014">Features extracted from</span></div>
<div style="position:absolute;left:473.83px;top:374.64px" class="cls_014"><span class="cls_014">Elliptical, spiral, or</span></div>
<div style="position:absolute;left:37.20px;top:403.68px" class="cls_014"><span class="cls_014">galaxies</span></div>
<div style="position:absolute;left:190.82px;top:403.68px" class="cls_014"><span class="cls_014">telescope images</span></div>
<div style="position:absolute;left:473.83px;top:403.68px" class="cls_014"><span class="cls_014">irregular-shaped</span></div>
<div style="position:absolute;left:473.83px;top:432.48px" class="cls_014"><span class="cls_014">galaxies</span></div>
<div style="position:absolute;left:56.70px;top:507.60px" class="cls_006"><span class="cls_006">10/23/21</span></div>
<div style="position:absolute;left:248.06px;top:507.60px" class="cls_006"><span class="cls_006">Introduction to Data Mining, 2</span><span class="cls_007"><sup>nd</sup></span><span class="cls_006"> Edition</span></div>
<div style="position:absolute;left:656.58px;top:507.60px" class="cls_006"><span class="cls_006">3</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:1650px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background04.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:0.00px" class="cls_002"><span class="cls_002">General Approach for Building</span></div>
<div style="position:absolute;left:37.13px;top:29.44px" class="cls_002"><span class="cls_002">Classification Model</span></div>
<div style="position:absolute;left:97.98px;top:115.28px" class="cls_016"><span class="cls_016">Tid</span></div>
<div style="position:absolute;left:122.61px;top:115.08px" class="cls_017"><span class="cls_017">Attrib1</span></div>
<div style="position:absolute;left:169.52px;top:115.08px" class="cls_017"><span class="cls_017">Attrib2</span></div>
<div style="position:absolute;left:217.10px;top:115.08px" class="cls_017"><span class="cls_017">Attrib3</span></div>
<div style="position:absolute;left:259.91px;top:115.08px" class="cls_017"><span class="cls_017">Class</span></div>
<div style="position:absolute;left:413.21px;top:112.04px" class="cls_020"><span class="cls_020">Learning</span></div>
<div style="position:absolute;left:98.18px;top:132.37px" class="cls_018"><span class="cls_018">1</span></div>
<div style="position:absolute;left:122.41px;top:132.37px" class="cls_018"><span class="cls_018">Yes</span></div>
<div style="position:absolute;left:162.68px;top:132.37px" class="cls_018"><span class="cls_018">Large</span></div>
<div style="position:absolute;left:213.99px;top:132.37px" class="cls_018"><span class="cls_018">125K</span></div>
<div style="position:absolute;left:257.96px;top:131.99px" class="cls_019"><span class="cls_019">No</span></div>
<div style="position:absolute;left:411.41px;top:131.16px" class="cls_020"><span class="cls_020">algorithm</span></div>
<div style="position:absolute;left:98.18px;top:149.57px" class="cls_018"><span class="cls_018">2</span></div>
<div style="position:absolute;left:122.41px;top:149.57px" class="cls_018"><span class="cls_018">No</span></div>
<div style="position:absolute;left:162.68px;top:149.57px" class="cls_018"><span class="cls_018">Medium</span></div>
<div style="position:absolute;left:213.99px;top:149.57px" class="cls_018"><span class="cls_018">100K</span></div>
<div style="position:absolute;left:257.96px;top:149.19px" class="cls_019"><span class="cls_019">No</span></div>
<div style="position:absolute;left:98.18px;top:166.77px" class="cls_018"><span class="cls_018">3</span></div>
<div style="position:absolute;left:122.41px;top:166.77px" class="cls_018"><span class="cls_018">No</span></div>
<div style="position:absolute;left:162.68px;top:166.77px" class="cls_018"><span class="cls_018">Small</span></div>
<div style="position:absolute;left:213.99px;top:166.77px" class="cls_018"><span class="cls_018">70K</span></div>
<div style="position:absolute;left:257.96px;top:166.39px" class="cls_019"><span class="cls_019">No</span></div>
<div style="position:absolute;left:98.18px;top:183.97px" class="cls_018"><span class="cls_018">4</span></div>
<div style="position:absolute;left:122.41px;top:183.97px" class="cls_018"><span class="cls_018">Yes</span></div>
<div style="position:absolute;left:162.68px;top:183.97px" class="cls_018"><span class="cls_018">Medium</span></div>
<div style="position:absolute;left:213.99px;top:183.97px" class="cls_018"><span class="cls_018">120K</span></div>
<div style="position:absolute;left:257.96px;top:183.59px" class="cls_019"><span class="cls_019">No</span></div>
<div style="position:absolute;left:345.92px;top:182.53px" class="cls_020"><span class="cls_020">Induction</span></div>
<div style="position:absolute;left:98.18px;top:201.17px" class="cls_018"><span class="cls_018">5</span></div>
<div style="position:absolute;left:122.41px;top:201.17px" class="cls_018"><span class="cls_018">No</span></div>
<div style="position:absolute;left:162.68px;top:201.17px" class="cls_018"><span class="cls_018">Large</span></div>
<div style="position:absolute;left:213.99px;top:201.17px" class="cls_018"><span class="cls_018">95K</span></div>
<div style="position:absolute;left:257.96px;top:200.78px" class="cls_019"><span class="cls_019">Yes</span></div>
<div style="position:absolute;left:98.18px;top:218.36px" class="cls_018"><span class="cls_018">6</span></div>
<div style="position:absolute;left:122.41px;top:218.36px" class="cls_018"><span class="cls_018">No</span></div>
<div style="position:absolute;left:162.68px;top:218.36px" class="cls_018"><span class="cls_018">Medium</span></div>
<div style="position:absolute;left:213.99px;top:218.36px" class="cls_018"><span class="cls_018">60K</span></div>
<div style="position:absolute;left:257.96px;top:217.98px" class="cls_019"><span class="cls_019">No</span></div>
<div style="position:absolute;left:98.18px;top:235.55px" class="cls_018"><span class="cls_018">7</span></div>
<div style="position:absolute;left:122.41px;top:235.55px" class="cls_018"><span class="cls_018">Yes</span></div>
<div style="position:absolute;left:162.68px;top:235.55px" class="cls_018"><span class="cls_018">Large</span></div>
<div style="position:absolute;left:213.99px;top:235.55px" class="cls_018"><span class="cls_018">220K</span></div>
<div style="position:absolute;left:257.96px;top:235.17px" class="cls_019"><span class="cls_019">No</span></div>
<div style="position:absolute;left:426.58px;top:227.15px" class="cls_021"><span class="cls_021">Learn</span></div>
<div style="position:absolute;left:98.18px;top:252.76px" class="cls_018"><span class="cls_018">8</span></div>
<div style="position:absolute;left:122.41px;top:252.76px" class="cls_018"><span class="cls_018">No</span></div>
<div style="position:absolute;left:162.68px;top:252.76px" class="cls_018"><span class="cls_018">Small</span></div>
<div style="position:absolute;left:213.99px;top:252.76px" class="cls_018"><span class="cls_018">85K</span></div>
<div style="position:absolute;left:257.96px;top:252.38px" class="cls_019"><span class="cls_019">Yes</span></div>
<div style="position:absolute;left:427.16px;top:247.75px" class="cls_021"><span class="cls_021">Model</span></div>
<div style="position:absolute;left:98.18px;top:269.96px" class="cls_018"><span class="cls_018">9</span></div>
<div style="position:absolute;left:122.41px;top:269.96px" class="cls_018"><span class="cls_018">No</span></div>
<div style="position:absolute;left:162.68px;top:269.96px" class="cls_018"><span class="cls_018">Medium</span></div>
<div style="position:absolute;left:213.99px;top:269.96px" class="cls_018"><span class="cls_018">75K</span></div>
<div style="position:absolute;left:257.96px;top:269.57px" class="cls_019"><span class="cls_019">No</span></div>
<div style="position:absolute;left:98.18px;top:287.15px" class="cls_018"><span class="cls_018">10</span></div>
<div style="position:absolute;left:122.41px;top:287.15px" class="cls_018"><span class="cls_018">No</span></div>
<div style="position:absolute;left:162.68px;top:287.15px" class="cls_018"><span class="cls_018">Small</span></div>
<div style="position:absolute;left:213.99px;top:287.15px" class="cls_018"><span class="cls_018">90K</span></div>
<div style="position:absolute;left:257.96px;top:286.77px" class="cls_019"><span class="cls_019">Yes</span></div>
<div style="position:absolute;left:565.83px;top:297.65px" class="cls_022"><span class="cls_022">Model</span></div>
<div style="position:absolute;left:142.06px;top:304.96px" class="cls_020"><span class="cls_020">Training Set</span></div>
<div style="position:absolute;left:426.01px;top:329.21px" class="cls_024"><span class="cls_024">Apply</span></div>
<div style="position:absolute;left:427.16px;top:349.28px" class="cls_024"><span class="cls_024">Model</span></div>
<div style="position:absolute;left:97.98px;top:359.16px" class="cls_025"><span class="cls_025">Tid</span></div>
<div style="position:absolute;left:122.61px;top:358.98px" class="cls_026"><span class="cls_026">Attrib1</span></div>
<div style="position:absolute;left:169.52px;top:358.98px" class="cls_026"><span class="cls_026">Attrib2</span></div>
<div style="position:absolute;left:217.10px;top:358.98px" class="cls_026"><span class="cls_026">Attrib3</span></div>
<div style="position:absolute;left:259.91px;top:358.98px" class="cls_026"><span class="cls_026">Class</span></div>
<div style="position:absolute;left:98.18px;top:376.29px" class="cls_027"><span class="cls_027">11</span></div>
<div style="position:absolute;left:122.41px;top:376.29px" class="cls_027"><span class="cls_027">No</span></div>
<div style="position:absolute;left:162.68px;top:376.29px" class="cls_027"><span class="cls_027">Small</span></div>
<div style="position:absolute;left:213.99px;top:376.29px" class="cls_027"><span class="cls_027">55K</span></div>
<div style="position:absolute;left:257.96px;top:375.91px" class="cls_028"><span class="cls_028">?</span></div>
<div style="position:absolute;left:98.18px;top:393.51px" class="cls_027"><span class="cls_027">12</span></div>
<div style="position:absolute;left:122.41px;top:393.51px" class="cls_027"><span class="cls_027">Yes</span></div>
<div style="position:absolute;left:162.68px;top:393.51px" class="cls_027"><span class="cls_027">Medium</span></div>
<div style="position:absolute;left:213.99px;top:393.51px" class="cls_027"><span class="cls_027">80K</span></div>
<div style="position:absolute;left:257.96px;top:393.13px" class="cls_028"><span class="cls_028">?</span></div>
<div style="position:absolute;left:98.18px;top:410.73px" class="cls_027"><span class="cls_027">13</span></div>
<div style="position:absolute;left:122.41px;top:410.73px" class="cls_027"><span class="cls_027">Yes</span></div>
<div style="position:absolute;left:162.68px;top:410.73px" class="cls_027"><span class="cls_027">Large</span></div>
<div style="position:absolute;left:213.99px;top:410.73px" class="cls_027"><span class="cls_027">110K</span></div>
<div style="position:absolute;left:257.96px;top:410.34px" class="cls_028"><span class="cls_028">?</span></div>
<div style="position:absolute;left:342.30px;top:401.16px" class="cls_020"><span class="cls_020">Deduction</span></div>
<div style="position:absolute;left:98.18px;top:427.94px" class="cls_027"><span class="cls_027">14</span></div>
<div style="position:absolute;left:122.41px;top:427.94px" class="cls_027"><span class="cls_027">No</span></div>
<div style="position:absolute;left:162.68px;top:427.94px" class="cls_027"><span class="cls_027">Small</span></div>
<div style="position:absolute;left:213.99px;top:427.94px" class="cls_027"><span class="cls_027">95K</span></div>
<div style="position:absolute;left:257.96px;top:427.56px" class="cls_028"><span class="cls_028">?</span></div>
<div style="position:absolute;left:98.18px;top:445.16px" class="cls_027"><span class="cls_027">15</span></div>
<div style="position:absolute;left:122.41px;top:445.16px" class="cls_027"><span class="cls_027">No</span></div>
<div style="position:absolute;left:162.68px;top:445.16px" class="cls_027"><span class="cls_027">Large</span></div>
<div style="position:absolute;left:213.99px;top:445.16px" class="cls_027"><span class="cls_027">67K</span></div>
<div style="position:absolute;left:257.96px;top:444.78px" class="cls_028"><span class="cls_028">?</span></div>
<div style="position:absolute;left:152.56px;top:465.68px" class="cls_020"><span class="cls_020">Test Set</span></div>
<div style="position:absolute;left:56.70px;top:507.60px" class="cls_006"><span class="cls_006">10/23/21</span></div>
<div style="position:absolute;left:248.06px;top:507.60px" class="cls_006"><span class="cls_006">Introduction to Data Mining, 2</span><span class="cls_007"><sup>nd</sup></span><span class="cls_006"> Edition</span></div>
<div style="position:absolute;left:656.58px;top:507.60px" class="cls_006"><span class="cls_006">4</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:2200px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background05.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:11.44px" class="cls_002"><span class="cls_002">Classification Techniques</span></div>
<div style="position:absolute;left:39.50px;top:89.60px" class="cls_008"><span class="cls_008">●</span><span class="cls_005"> Base Classifiers</span></div>
<div style="position:absolute;left:75.50px;top:126.56px" class="cls_009"><span class="cls_009">-</span><span class="cls_005"> Decision Tree based Methods</span></div>
<div style="position:absolute;left:75.50px;top:164.48px" class="cls_009"><span class="cls_009">-</span><span class="cls_005"> Rule-based Methods</span></div>
<div style="position:absolute;left:75.50px;top:202.40px" class="cls_009"><span class="cls_009">-</span><span class="cls_005"> Nearest-neighbor</span></div>
<div style="position:absolute;left:75.50px;top:239.60px" class="cls_009"><span class="cls_009">-</span><span class="cls_005"> Neural Networks</span></div>
<div style="position:absolute;left:75.50px;top:277.52px" class="cls_009"><span class="cls_009">-</span><span class="cls_005"> Deep Learning</span></div>
<div style="position:absolute;left:75.50px;top:314.48px" class="cls_009"><span class="cls_009">-</span><span class="cls_005"> Naïve Bayes and Bayesian Belief Networks</span></div>
<div style="position:absolute;left:75.50px;top:352.40px" class="cls_009"><span class="cls_009">-</span><span class="cls_005"> Support Vector Machines</span></div>
<div style="position:absolute;left:39.50px;top:427.52px" class="cls_008"><span class="cls_008">●</span><span class="cls_005"> Ensemble Classifiers</span></div>
<div style="position:absolute;left:75.50px;top:465.44px" class="cls_009"><span class="cls_009">-</span><span class="cls_005"> Boosting, Bagging, Random Forests</span></div>
<div style="position:absolute;left:56.70px;top:507.60px" class="cls_006"><span class="cls_006">10/23/21</span></div>
<div style="position:absolute;left:248.06px;top:507.60px" class="cls_006"><span class="cls_006">Introduction to Data Mining, 2</span><span class="cls_007"><sup>nd</sup></span><span class="cls_006"> Edition</span></div>
<div style="position:absolute;left:656.58px;top:507.60px" class="cls_006"><span class="cls_006">5</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:2750px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background06.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:11.44px" class="cls_002"><span class="cls_002">Example of a Decision Tree</span></div>
<div style="position:absolute;left:513.99px;top:142.80px" class="cls_030"><span class="cls_030">Splitting Attributes</span></div>
<div style="position:absolute;left:55.08px;top:161.63px" class="cls_031"><span class="cls_031">Home</span></div>
<div style="position:absolute;left:112.07px;top:161.63px" class="cls_031"><span class="cls_031">Marital</span></div>
<div style="position:absolute;left:172.49px;top:161.63px" class="cls_031"><span class="cls_031">Annual</span></div>
<div style="position:absolute;left:229.63px;top:161.63px" class="cls_031"><span class="cls_031">Defaulted</span></div>
<div style="position:absolute;left:24.33px;top:169.07px" class="cls_031"><span class="cls_031">ID</span></div>
<div style="position:absolute;left:53.75px;top:176.66px" class="cls_031"><span class="cls_031">Owner</span></div>
<div style="position:absolute;left:113.16px;top:176.66px" class="cls_031"><span class="cls_031">Status</span></div>
<div style="position:absolute;left:171.79px;top:176.66px" class="cls_031"><span class="cls_031">Income</span></div>
<div style="position:absolute;left:230.33px;top:176.66px" class="cls_031"><span class="cls_031">Borrower</span></div>
<div style="position:absolute;left:20.43px;top:201.66px" class="cls_032"><span class="cls_032">1</span></div>
<div style="position:absolute;left:50.09px;top:201.66px" class="cls_032"><span class="cls_032">Yes</span></div>
<div style="position:absolute;left:105.12px;top:201.66px" class="cls_032"><span class="cls_032">Single</span></div>
<div style="position:absolute;left:168.04px;top:201.66px" class="cls_032"><span class="cls_032">125K</span></div>
<div style="position:absolute;left:228.23px;top:201.74px" class="cls_033"><span class="cls_033">No</span></div>
<div style="position:absolute;left:470.44px;top:202.88px" class="cls_035"><span class="cls_035">Home</span></div>
<div style="position:absolute;left:20.43px;top:226.33px" class="cls_032"><span class="cls_032">2</span></div>
<div style="position:absolute;left:50.09px;top:226.33px" class="cls_032"><span class="cls_032">No</span></div>
<div style="position:absolute;left:105.12px;top:226.33px" class="cls_032"><span class="cls_032">Married</span></div>
<div style="position:absolute;left:168.04px;top:226.33px" class="cls_032"><span class="cls_032">100K</span></div>
<div style="position:absolute;left:228.23px;top:226.40px" class="cls_033"><span class="cls_033">No</span></div>
<div style="position:absolute;left:467.69px;top:221.84px" class="cls_035"><span class="cls_035">Owner</span></div>
<div style="position:absolute;left:407.17px;top:238.64px" class="cls_034"><span class="cls_034">Yes</span></div>
<div style="position:absolute;left:552.58px;top:238.64px" class="cls_034"><span class="cls_034">No</span></div>
<div style="position:absolute;left:20.43px;top:251.07px" class="cls_032"><span class="cls_032">3</span></div>
<div style="position:absolute;left:50.09px;top:251.07px" class="cls_032"><span class="cls_032">No</span></div>
<div style="position:absolute;left:105.12px;top:251.07px" class="cls_032"><span class="cls_032">Single</span></div>
<div style="position:absolute;left:168.04px;top:251.07px" class="cls_032"><span class="cls_032">70K</span></div>
<div style="position:absolute;left:228.23px;top:251.15px" class="cls_033"><span class="cls_033">No</span></div>
<div style="position:absolute;left:20.43px;top:275.74px" class="cls_032"><span class="cls_032">4</span></div>
<div style="position:absolute;left:50.09px;top:275.74px" class="cls_032"><span class="cls_032">Yes</span></div>
<div style="position:absolute;left:105.12px;top:275.74px" class="cls_032"><span class="cls_032">Married</span></div>
<div style="position:absolute;left:168.04px;top:275.74px" class="cls_032"><span class="cls_032">120K</span></div>
<div style="position:absolute;left:228.23px;top:275.82px" class="cls_033"><span class="cls_033">No</span></div>
<div style="position:absolute;left:404.38px;top:275.12px" class="cls_036"><span class="cls_036">NO</span></div>
<div style="position:absolute;left:550.31px;top:275.12px" class="cls_035"><span class="cls_035">MarSt</span></div>
<div style="position:absolute;left:20.43px;top:300.58px" class="cls_032"><span class="cls_032">5</span></div>
<div style="position:absolute;left:50.09px;top:300.58px" class="cls_032"><span class="cls_032">No</span></div>
<div style="position:absolute;left:105.12px;top:300.58px" class="cls_032"><span class="cls_032">Divorced</span></div>
<div style="position:absolute;left:168.04px;top:300.58px" class="cls_032"><span class="cls_032">95K</span></div>
<div style="position:absolute;left:228.23px;top:300.66px" class="cls_033"><span class="cls_033">Yes</span></div>
<div style="position:absolute;left:629.95px;top:298.88px" class="cls_034"><span class="cls_034">Married</span></div>
<div style="position:absolute;left:455.45px;top:301.04px" class="cls_034"><span class="cls_034">Single, Divorced</span></div>
<div style="position:absolute;left:20.43px;top:325.25px" class="cls_032"><span class="cls_032">6</span></div>
<div style="position:absolute;left:50.09px;top:325.25px" class="cls_032"><span class="cls_032">No</span></div>
<div style="position:absolute;left:105.12px;top:325.25px" class="cls_032"><span class="cls_032">Married</span></div>
<div style="position:absolute;left:168.04px;top:325.25px" class="cls_032"><span class="cls_032">60K</span></div>
<div style="position:absolute;left:228.23px;top:325.33px" class="cls_033"><span class="cls_033">No</span></div>
<div style="position:absolute;left:488.75px;top:337.52px" class="cls_035"><span class="cls_035">Income</span></div>
<div style="position:absolute;left:630.88px;top:339.68px" class="cls_036"><span class="cls_036">NO</span></div>
<div style="position:absolute;left:20.43px;top:349.92px" class="cls_032"><span class="cls_032">7</span></div>
<div style="position:absolute;left:50.09px;top:349.92px" class="cls_032"><span class="cls_032">Yes</span></div>
<div style="position:absolute;left:105.12px;top:349.92px" class="cls_032"><span class="cls_032">Divorced</span></div>
<div style="position:absolute;left:168.04px;top:349.92px" class="cls_032"><span class="cls_032">220K</span></div>
<div style="position:absolute;left:228.23px;top:350.00px" class="cls_033"><span class="cls_033">No</span></div>
<div style="position:absolute;left:425.58px;top:363.44px" class="cls_034"><span class="cls_034">&lt; 80K</span></div>
<div style="position:absolute;left:565.33px;top:363.44px" class="cls_034"><span class="cls_034">> 80K</span></div>
<div style="position:absolute;left:20.43px;top:374.59px" class="cls_032"><span class="cls_032">8</span></div>
<div style="position:absolute;left:50.09px;top:374.59px" class="cls_032"><span class="cls_032">No</span></div>
<div style="position:absolute;left:105.12px;top:374.59px" class="cls_032"><span class="cls_032">Single</span></div>
<div style="position:absolute;left:168.04px;top:374.59px" class="cls_032"><span class="cls_032">85K</span></div>
<div style="position:absolute;left:228.23px;top:374.66px" class="cls_033"><span class="cls_033">Yes</span></div>
<div style="position:absolute;left:20.43px;top:399.35px" class="cls_032"><span class="cls_032">9</span></div>
<div style="position:absolute;left:50.09px;top:399.35px" class="cls_032"><span class="cls_032">No</span></div>
<div style="position:absolute;left:105.12px;top:399.35px" class="cls_032"><span class="cls_032">Married</span></div>
<div style="position:absolute;left:168.04px;top:399.35px" class="cls_032"><span class="cls_032">75K</span></div>
<div style="position:absolute;left:228.23px;top:399.43px" class="cls_033"><span class="cls_033">No</span></div>
<div style="position:absolute;left:449.00px;top:399.92px" class="cls_036"><span class="cls_036">NO</span></div>
<div style="position:absolute;left:556.69px;top:399.68px" class="cls_036"><span class="cls_036">YES</span></div>
<div style="position:absolute;left:20.43px;top:424.09px" class="cls_032"><span class="cls_032">10</span></div>
<div style="position:absolute;left:50.09px;top:424.09px" class="cls_032"><span class="cls_032">No</span></div>
<div style="position:absolute;left:105.12px;top:424.09px" class="cls_032"><span class="cls_032">Single</span></div>
<div style="position:absolute;left:168.04px;top:424.09px" class="cls_032"><span class="cls_032">90K</span></div>
<div style="position:absolute;left:228.23px;top:424.17px" class="cls_033"><span class="cls_033">Yes</span></div>
<div style="position:absolute;left:95.68px;top:460.48px" class="cls_038"><span class="cls_038">Training Data</span></div>
<div style="position:absolute;left:416.30px;top:458.08px" class="cls_038"><span class="cls_038">Model:  Decision Tree</span></div>
<div style="position:absolute;left:56.70px;top:507.60px" class="cls_006"><span class="cls_006">10/23/21</span></div>
<div style="position:absolute;left:248.06px;top:507.60px" class="cls_006"><span class="cls_006">Introduction to Data Mining, 2</span><span class="cls_007"><sup>nd</sup></span><span class="cls_006"> Edition</span></div>
<div style="position:absolute;left:656.58px;top:507.60px" class="cls_006"><span class="cls_006">6</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:3300px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background07.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:11.44px" class="cls_002"><span class="cls_002">Another Example of Decision Tree</span></div>
<div style="position:absolute;left:423.12px;top:140.96px" class="cls_035"><span class="cls_035">MarSt</span></div>
<div style="position:absolute;left:506.58px;top:138.08px" class="cls_034"><span class="cls_034">Single,</span></div>
<div style="position:absolute;left:333.70px;top:156.08px" class="cls_034"><span class="cls_034">Married</span></div>
<div style="position:absolute;left:492.33px;top:157.04px" class="cls_034"><span class="cls_034">Divorced</span></div>
<div style="position:absolute;left:55.94px;top:168.06px" class="cls_039"><span class="cls_039">Home</span></div>
<div style="position:absolute;left:114.07px;top:168.06px" class="cls_039"><span class="cls_039">Marital</span></div>
<div style="position:absolute;left:175.70px;top:168.06px" class="cls_039"><span class="cls_039">Annual</span></div>
<div style="position:absolute;left:233.98px;top:168.06px" class="cls_039"><span class="cls_039">Defaulted</span></div>
<div style="position:absolute;left:24.58px;top:175.62px" class="cls_039"><span class="cls_039">ID</span></div>
<div style="position:absolute;left:54.59px;top:183.34px" class="cls_039"><span class="cls_039">Owner</span></div>
<div style="position:absolute;left:115.18px;top:183.34px" class="cls_039"><span class="cls_039">Status</span></div>
<div style="position:absolute;left:174.98px;top:183.34px" class="cls_039"><span class="cls_039">Income</span></div>
<div style="position:absolute;left:234.70px;top:183.34px" class="cls_039"><span class="cls_039">Borrower</span></div>
<div style="position:absolute;left:357.13px;top:198.08px" class="cls_036"><span class="cls_036">NO</span></div>
<div style="position:absolute;left:503.13px;top:198.08px" class="cls_035"><span class="cls_035">Home</span></div>
<div style="position:absolute;left:20.60px;top:208.75px" class="cls_040"><span class="cls_040">1</span></div>
<div style="position:absolute;left:50.85px;top:208.75px" class="cls_040"><span class="cls_040">Yes</span></div>
<div style="position:absolute;left:106.98px;top:208.75px" class="cls_040"><span class="cls_040">Single</span></div>
<div style="position:absolute;left:171.16px;top:208.75px" class="cls_040"><span class="cls_040">125K</span></div>
<div style="position:absolute;left:232.55px;top:208.83px" class="cls_041"><span class="cls_041">No</span></div>
<div style="position:absolute;left:500.38px;top:217.04px" class="cls_035"><span class="cls_035">Owner</span></div>
<div style="position:absolute;left:579.70px;top:216.08px" class="cls_034"><span class="cls_034">No</span></div>
<div style="position:absolute;left:443.17px;top:222.08px" class="cls_034"><span class="cls_034">Yes</span></div>
<div style="position:absolute;left:20.60px;top:233.82px" class="cls_040"><span class="cls_040">2</span></div>
<div style="position:absolute;left:50.85px;top:233.82px" class="cls_040"><span class="cls_040">No</span></div>
<div style="position:absolute;left:106.98px;top:233.82px" class="cls_040"><span class="cls_040">Married</span></div>
<div style="position:absolute;left:171.16px;top:233.82px" class="cls_040"><span class="cls_040">100K</span></div>
<div style="position:absolute;left:232.55px;top:233.90px" class="cls_041"><span class="cls_041">No</span></div>
<div style="position:absolute;left:20.60px;top:258.97px" class="cls_040"><span class="cls_040">3</span></div>
<div style="position:absolute;left:50.85px;top:258.97px" class="cls_040"><span class="cls_040">No</span></div>
<div style="position:absolute;left:106.98px;top:258.97px" class="cls_040"><span class="cls_040">Single</span></div>
<div style="position:absolute;left:171.16px;top:258.97px" class="cls_040"><span class="cls_040">70K</span></div>
<div style="position:absolute;left:232.55px;top:259.05px" class="cls_041"><span class="cls_041">No</span></div>
<div style="position:absolute;left:453.75px;top:258.08px" class="cls_036"><span class="cls_036">NO</span></div>
<div style="position:absolute;left:570.62px;top:258.08px" class="cls_035"><span class="cls_035">Income</span></div>
<div style="position:absolute;left:20.60px;top:284.04px" class="cls_040"><span class="cls_040">4</span></div>
<div style="position:absolute;left:50.85px;top:284.04px" class="cls_040"><span class="cls_040">Yes</span></div>
<div style="position:absolute;left:106.98px;top:284.04px" class="cls_040"><span class="cls_040">Married</span></div>
<div style="position:absolute;left:171.16px;top:284.04px" class="cls_040"><span class="cls_040">120K</span></div>
<div style="position:absolute;left:232.55px;top:284.12px" class="cls_041"><span class="cls_041">No</span></div>
<div style="position:absolute;left:507.45px;top:284.00px" class="cls_034"><span class="cls_034">&lt; 80K</span></div>
<div style="position:absolute;left:647.20px;top:284.00px" class="cls_034"><span class="cls_034">> 80K</span></div>
<div style="position:absolute;left:20.60px;top:309.30px" class="cls_040"><span class="cls_040">5</span></div>
<div style="position:absolute;left:50.85px;top:309.30px" class="cls_040"><span class="cls_040">No</span></div>
<div style="position:absolute;left:106.98px;top:309.30px" class="cls_040"><span class="cls_040">Divorced</span></div>
<div style="position:absolute;left:171.16px;top:309.30px" class="cls_040"><span class="cls_040">95K</span></div>
<div style="position:absolute;left:232.55px;top:309.38px" class="cls_041"><span class="cls_041">Yes</span></div>
<div style="position:absolute;left:530.88px;top:320.48px" class="cls_036"><span class="cls_036">NO</span></div>
<div style="position:absolute;left:638.56px;top:320.24px" class="cls_036"><span class="cls_036">YES</span></div>
<div style="position:absolute;left:20.60px;top:334.37px" class="cls_040"><span class="cls_040">6</span></div>
<div style="position:absolute;left:50.85px;top:334.37px" class="cls_040"><span class="cls_040">No</span></div>
<div style="position:absolute;left:106.98px;top:334.37px" class="cls_040"><span class="cls_040">Married</span></div>
<div style="position:absolute;left:171.16px;top:334.37px" class="cls_040"><span class="cls_040">60K</span></div>
<div style="position:absolute;left:232.55px;top:334.45px" class="cls_041"><span class="cls_041">No</span></div>
<div style="position:absolute;left:20.60px;top:359.44px" class="cls_040"><span class="cls_040">7</span></div>
<div style="position:absolute;left:50.85px;top:359.44px" class="cls_040"><span class="cls_040">Yes</span></div>
<div style="position:absolute;left:106.98px;top:359.44px" class="cls_040"><span class="cls_040">Divorced</span></div>
<div style="position:absolute;left:171.16px;top:359.44px" class="cls_040"><span class="cls_040">220K</span></div>
<div style="position:absolute;left:232.55px;top:359.52px" class="cls_041"><span class="cls_041">No</span></div>
<div style="position:absolute;left:20.60px;top:384.52px" class="cls_040"><span class="cls_040">8</span></div>
<div style="position:absolute;left:50.85px;top:384.52px" class="cls_040"><span class="cls_040">No</span></div>
<div style="position:absolute;left:106.98px;top:384.52px" class="cls_040"><span class="cls_040">Single</span></div>
<div style="position:absolute;left:171.16px;top:384.52px" class="cls_040"><span class="cls_040">85K</span></div>
<div style="position:absolute;left:232.55px;top:384.59px" class="cls_041"><span class="cls_041">Yes</span></div>
<div style="position:absolute;left:349.20px;top:399.60px" class="cls_042"><span class="cls_042">There could be more than one tree that</span></div>
<div style="position:absolute;left:20.60px;top:409.68px" class="cls_040"><span class="cls_040">9</span></div>
<div style="position:absolute;left:50.85px;top:409.68px" class="cls_040"><span class="cls_040">No</span></div>
<div style="position:absolute;left:106.98px;top:409.68px" class="cls_040"><span class="cls_040">Married</span></div>
<div style="position:absolute;left:171.16px;top:409.68px" class="cls_040"><span class="cls_040">75K</span></div>
<div style="position:absolute;left:232.55px;top:409.76px" class="cls_041"><span class="cls_041">No</span></div>
<div style="position:absolute;left:349.20px;top:420.48px" class="cls_042"><span class="cls_042">fits the same data!</span></div>
<div style="position:absolute;left:20.60px;top:434.83px" class="cls_040"><span class="cls_040">10</span></div>
<div style="position:absolute;left:50.85px;top:434.83px" class="cls_040"><span class="cls_040">No</span></div>
<div style="position:absolute;left:106.98px;top:434.83px" class="cls_040"><span class="cls_040">Single</span></div>
<div style="position:absolute;left:171.16px;top:434.83px" class="cls_040"><span class="cls_040">90K</span></div>
<div style="position:absolute;left:232.55px;top:434.91px" class="cls_041"><span class="cls_041">Yes</span></div>
<div style="position:absolute;left:56.70px;top:507.60px" class="cls_006"><span class="cls_006">10/23/21</span></div>
<div style="position:absolute;left:248.06px;top:507.60px" class="cls_006"><span class="cls_006">Introduction to Data Mining, 2</span><span class="cls_007"><sup>nd</sup></span><span class="cls_006"> Edition</span></div>
<div style="position:absolute;left:656.58px;top:507.60px" class="cls_006"><span class="cls_006">7</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:3850px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background08.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:11.44px" class="cls_002"><span class="cls_002">Apply Model to Test Data</span></div>
<div style="position:absolute;left:396.74px;top:88.48px" class="cls_038"><span class="cls_038">Test Data</span></div>
<div style="position:absolute;left:85.20px;top:112.48px" class="cls_044"><span class="cls_044">Start from the root of tree.</span></div>
<div style="position:absolute;left:404.90px;top:131.56px" class="cls_045"><span class="cls_045">Home</span></div>
<div style="position:absolute;left:466.58px;top:131.56px" class="cls_045"><span class="cls_045">Marital</span></div>
<div style="position:absolute;left:531.98px;top:131.56px" class="cls_045"><span class="cls_045">Annual</span></div>
<div style="position:absolute;left:592.39px;top:131.56px" class="cls_045"><span class="cls_045">Defaulted</span></div>
<div style="position:absolute;left:403.47px;top:147.71px" class="cls_045"><span class="cls_045">Owner</span></div>
<div style="position:absolute;left:467.76px;top:147.71px" class="cls_045"><span class="cls_045">Status</span></div>
<div style="position:absolute;left:531.22px;top:147.71px" class="cls_045"><span class="cls_045">Income</span></div>
<div style="position:absolute;left:593.15px;top:147.71px" class="cls_045"><span class="cls_045">Borrower</span></div>
<div style="position:absolute;left:399.50px;top:174.55px" class="cls_046"><span class="cls_046">No</span></div>
<div style="position:absolute;left:459.06px;top:174.55px" class="cls_046"><span class="cls_046">Married</span></div>
<div style="position:absolute;left:527.16px;top:174.55px" class="cls_046"><span class="cls_046">80K</span></div>
<div style="position:absolute;left:592.30px;top:174.63px" class="cls_047"><span class="cls_047">?</span></div>
<div style="position:absolute;left:144.75px;top:189.68px" class="cls_035"><span class="cls_035">Home</span></div>
<div style="position:absolute;left:142.00px;top:208.64px" class="cls_035"><span class="cls_035">Owner</span></div>
<div style="position:absolute;left:76.42px;top:215.12px" class="cls_034"><span class="cls_034">Yes</span></div>
<div style="position:absolute;left:235.32px;top:215.12px" class="cls_034"><span class="cls_034">No</span></div>
<div style="position:absolute;left:71.38px;top:259.76px" class="cls_036"><span class="cls_036">NO</span></div>
<div style="position:absolute;left:232.38px;top:259.76px" class="cls_035"><span class="cls_035">MarSt</span></div>
<div style="position:absolute;left:138.07px;top:291.68px" class="cls_034"><span class="cls_034">Single, Divorced</span></div>
<div style="position:absolute;left:323.95px;top:289.04px" class="cls_034"><span class="cls_034">Married</span></div>
<div style="position:absolute;left:165.44px;top:336.32px" class="cls_035"><span class="cls_035">Income</span></div>
<div style="position:absolute;left:319.81px;top:338.96px" class="cls_036"><span class="cls_036">NO</span></div>
<div style="position:absolute;left:98.20px;top:368.24px" class="cls_034"><span class="cls_034">&lt; 80K</span></div>
<div style="position:absolute;left:251.45px;top:368.24px" class="cls_034"><span class="cls_034">> 80K</span></div>
<div style="position:absolute;left:120.25px;top:412.88px" class="cls_036"><span class="cls_036">NO</span></div>
<div style="position:absolute;left:238.75px;top:412.64px" class="cls_036"><span class="cls_036">YES</span></div>
<div style="position:absolute;left:56.70px;top:507.60px" class="cls_006"><span class="cls_006">10/23/21</span></div>
<div style="position:absolute;left:248.06px;top:507.60px" class="cls_006"><span class="cls_006">Introduction to Data Mining, 2</span><span class="cls_007"><sup>nd</sup></span><span class="cls_006"> Edition</span></div>
<div style="position:absolute;left:656.58px;top:507.60px" class="cls_006"><span class="cls_006">8</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:4400px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background09.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:11.44px" class="cls_002"><span class="cls_002">Apply Model to Test Data</span></div>
<div style="position:absolute;left:396.74px;top:88.48px" class="cls_038"><span class="cls_038">Test Data</span></div>
<div style="position:absolute;left:407.27px;top:131.57px" class="cls_049"><span class="cls_049">Home</span></div>
<div style="position:absolute;left:468.88px;top:131.57px" class="cls_049"><span class="cls_049">Marital</span></div>
<div style="position:absolute;left:534.20px;top:131.57px" class="cls_049"><span class="cls_049">Annual</span></div>
<div style="position:absolute;left:597.77px;top:131.57px" class="cls_049"><span class="cls_049">Defaulted</span></div>
<div style="position:absolute;left:405.83px;top:147.72px" class="cls_049"><span class="cls_049">Owner</span></div>
<div style="position:absolute;left:470.06px;top:147.72px" class="cls_049"><span class="cls_049">Status</span></div>
<div style="position:absolute;left:533.44px;top:147.72px" class="cls_049"><span class="cls_049">Income</span></div>
<div style="position:absolute;left:598.53px;top:147.72px" class="cls_049"><span class="cls_049">Borrower</span></div>
<div style="position:absolute;left:401.86px;top:174.56px" class="cls_050"><span class="cls_050">No</span></div>
<div style="position:absolute;left:461.38px;top:174.56px" class="cls_050"><span class="cls_050">Married</span></div>
<div style="position:absolute;left:529.39px;top:174.56px" class="cls_050"><span class="cls_050">80K</span></div>
<div style="position:absolute;left:594.48px;top:174.64px" class="cls_051"><span class="cls_051">?</span></div>
<div style="position:absolute;left:144.75px;top:189.68px" class="cls_035"><span class="cls_035">Home</span></div>
<div style="position:absolute;left:142.00px;top:208.64px" class="cls_035"><span class="cls_035">Owner</span></div>
<div style="position:absolute;left:76.42px;top:215.12px" class="cls_034"><span class="cls_034">Yes</span></div>
<div style="position:absolute;left:235.32px;top:215.12px" class="cls_034"><span class="cls_034">No</span></div>
<div style="position:absolute;left:71.38px;top:259.76px" class="cls_036"><span class="cls_036">NO</span></div>
<div style="position:absolute;left:232.38px;top:259.76px" class="cls_035"><span class="cls_035">MarSt</span></div>
<div style="position:absolute;left:138.07px;top:291.68px" class="cls_034"><span class="cls_034">Single, Divorced</span></div>
<div style="position:absolute;left:323.95px;top:289.04px" class="cls_034"><span class="cls_034">Married</span></div>
<div style="position:absolute;left:165.44px;top:336.32px" class="cls_035"><span class="cls_035">Income</span></div>
<div style="position:absolute;left:319.81px;top:338.96px" class="cls_036"><span class="cls_036">NO</span></div>
<div style="position:absolute;left:98.20px;top:368.24px" class="cls_034"><span class="cls_034">&lt; 80K</span></div>
<div style="position:absolute;left:251.45px;top:368.24px" class="cls_034"><span class="cls_034">> 80K</span></div>
<div style="position:absolute;left:120.25px;top:412.88px" class="cls_036"><span class="cls_036">NO</span></div>
<div style="position:absolute;left:238.75px;top:412.64px" class="cls_036"><span class="cls_036">YES</span></div>
<div style="position:absolute;left:56.70px;top:507.60px" class="cls_006"><span class="cls_006">10/23/21</span></div>
<div style="position:absolute;left:248.06px;top:507.60px" class="cls_006"><span class="cls_006">Introduction to Data Mining, 2</span><span class="cls_007"><sup>nd</sup></span><span class="cls_006"> Edition</span></div>
<div style="position:absolute;left:656.58px;top:507.60px" class="cls_006"><span class="cls_006">9</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:4950px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background10.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:11.44px" class="cls_002"><span class="cls_002">Apply Model to Test Data</span></div>
<div style="position:absolute;left:396.74px;top:88.48px" class="cls_038"><span class="cls_038">Test Data</span></div>
<div style="position:absolute;left:405.02px;top:131.51px" class="cls_053"><span class="cls_053">Home</span></div>
<div style="position:absolute;left:467.21px;top:131.51px" class="cls_053"><span class="cls_053">Marital</span></div>
<div style="position:absolute;left:533.15px;top:131.51px" class="cls_053"><span class="cls_053">Annual</span></div>
<div style="position:absolute;left:594.05px;top:131.51px" class="cls_053"><span class="cls_053">Defaulted</span></div>
<div style="position:absolute;left:403.57px;top:147.65px" class="cls_053"><span class="cls_053">Owner</span></div>
<div style="position:absolute;left:468.40px;top:147.65px" class="cls_053"><span class="cls_053">Status</span></div>
<div style="position:absolute;left:532.39px;top:147.65px" class="cls_053"><span class="cls_053">Income</span></div>
<div style="position:absolute;left:594.81px;top:147.65px" class="cls_053"><span class="cls_053">Borrower</span></div>
<div style="position:absolute;left:403.49px;top:174.48px" class="cls_054"><span class="cls_054">No</span></div>
<div style="position:absolute;left:459.63px;top:174.48px" class="cls_055"><span class="cls_055">Married</span></div>
<div style="position:absolute;left:528.30px;top:174.48px" class="cls_055"><span class="cls_055">80K</span></div>
<div style="position:absolute;left:593.96px;top:174.56px" class="cls_056"><span class="cls_056">?</span></div>
<div style="position:absolute;left:144.75px;top:189.68px" class="cls_035"><span class="cls_035">Home</span></div>
<div style="position:absolute;left:142.00px;top:208.64px" class="cls_035"><span class="cls_035">Owner</span></div>
<div style="position:absolute;left:76.42px;top:215.12px" class="cls_034"><span class="cls_034">Yes</span></div>
<div style="position:absolute;left:235.32px;top:215.12px" class="cls_058"><span class="cls_058">No</span></div>
<div style="position:absolute;left:71.38px;top:259.76px" class="cls_036"><span class="cls_036">NO</span></div>
<div style="position:absolute;left:232.38px;top:259.76px" class="cls_035"><span class="cls_035">MarSt</span></div>
<div style="position:absolute;left:138.07px;top:291.68px" class="cls_034"><span class="cls_034">Single, Divorced</span></div>
<div style="position:absolute;left:323.95px;top:289.04px" class="cls_034"><span class="cls_034">Married</span></div>
<div style="position:absolute;left:165.44px;top:336.32px" class="cls_035"><span class="cls_035">Income</span></div>
<div style="position:absolute;left:319.81px;top:338.96px" class="cls_036"><span class="cls_036">NO</span></div>
<div style="position:absolute;left:98.20px;top:368.24px" class="cls_034"><span class="cls_034">&lt; 80K</span></div>
<div style="position:absolute;left:251.45px;top:368.24px" class="cls_034"><span class="cls_034">> 80K</span></div>
<div style="position:absolute;left:120.25px;top:412.88px" class="cls_036"><span class="cls_036">NO</span></div>
<div style="position:absolute;left:238.75px;top:412.64px" class="cls_036"><span class="cls_036">YES</span></div>
<div style="position:absolute;left:56.70px;top:507.60px" class="cls_006"><span class="cls_006">10/23/21</span></div>
<div style="position:absolute;left:248.06px;top:507.60px" class="cls_006"><span class="cls_006">Introduction to Data Mining, 2</span><span class="cls_007"><sup>nd</sup></span><span class="cls_006"> Edition</span></div>
<div style="position:absolute;left:649.95px;top:507.60px" class="cls_006"><span class="cls_006">10</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:5500px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background11.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:11.44px" class="cls_002"><span class="cls_002">Apply Model to Test Data</span></div>
<div style="position:absolute;left:396.74px;top:88.48px" class="cls_038"><span class="cls_038">Test Data</span></div>
<div style="position:absolute;left:404.97px;top:131.54px" class="cls_059"><span class="cls_059">Home</span></div>
<div style="position:absolute;left:466.92px;top:131.54px" class="cls_059"><span class="cls_059">Marital</span></div>
<div style="position:absolute;left:532.59px;top:131.54px" class="cls_059"><span class="cls_059">Annual</span></div>
<div style="position:absolute;left:594.89px;top:131.54px" class="cls_059"><span class="cls_059">Defaulted</span></div>
<div style="position:absolute;left:403.52px;top:147.68px" class="cls_059"><span class="cls_059">Owner</span></div>
<div style="position:absolute;left:468.11px;top:147.68px" class="cls_059"><span class="cls_059">Status</span></div>
<div style="position:absolute;left:531.83px;top:147.68px" class="cls_059"><span class="cls_059">Income</span></div>
<div style="position:absolute;left:595.66px;top:147.68px" class="cls_059"><span class="cls_059">Borrower</span></div>
<div style="position:absolute;left:399.54px;top:174.50px" class="cls_060"><span class="cls_060">No</span></div>
<div style="position:absolute;left:459.37px;top:174.50px" class="cls_061"><span class="cls_061">Married</span></div>
<div style="position:absolute;left:527.76px;top:174.50px" class="cls_061"><span class="cls_061">80K</span></div>
<div style="position:absolute;left:593.20px;top:174.59px" class="cls_062"><span class="cls_062">?</span></div>
<div style="position:absolute;left:144.75px;top:189.68px" class="cls_035"><span class="cls_035">Home</span></div>
<div style="position:absolute;left:142.00px;top:208.64px" class="cls_035"><span class="cls_035">Owner</span></div>
<div style="position:absolute;left:76.42px;top:215.12px" class="cls_034"><span class="cls_034">Yes</span></div>
<div style="position:absolute;left:235.32px;top:215.12px" class="cls_058"><span class="cls_058">No</span></div>
<div style="position:absolute;left:71.38px;top:259.76px" class="cls_036"><span class="cls_036">NO</span></div>
<div style="position:absolute;left:232.38px;top:259.76px" class="cls_035"><span class="cls_035">MarSt</span></div>
<div style="position:absolute;left:138.07px;top:291.68px" class="cls_034"><span class="cls_034">Single, Divorced</span></div>
<div style="position:absolute;left:323.95px;top:289.04px" class="cls_034"><span class="cls_034">Married</span></div>
<div style="position:absolute;left:165.44px;top:336.32px" class="cls_035"><span class="cls_035">Income</span></div>
<div style="position:absolute;left:319.81px;top:338.96px" class="cls_036"><span class="cls_036">NO</span></div>
<div style="position:absolute;left:98.20px;top:368.24px" class="cls_034"><span class="cls_034">&lt; 80K</span></div>
<div style="position:absolute;left:251.45px;top:368.24px" class="cls_034"><span class="cls_034">> 80K</span></div>
<div style="position:absolute;left:120.25px;top:412.88px" class="cls_036"><span class="cls_036">NO</span></div>
<div style="position:absolute;left:238.75px;top:412.64px" class="cls_036"><span class="cls_036">YES</span></div>
<div style="position:absolute;left:56.70px;top:507.60px" class="cls_006"><span class="cls_006">10/23/21</span></div>
<div style="position:absolute;left:248.06px;top:507.60px" class="cls_006"><span class="cls_006">Introduction to Data Mining, 2</span><span class="cls_007"><sup>nd</sup></span><span class="cls_006"> Edition</span></div>
<div style="position:absolute;left:650.61px;top:507.60px" class="cls_006"><span class="cls_006">11</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:6050px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background12.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:11.44px" class="cls_002"><span class="cls_002">Apply Model to Test Data</span></div>
<div style="position:absolute;left:396.74px;top:88.48px" class="cls_038"><span class="cls_038">Test Data</span></div>
<div style="position:absolute;left:404.93px;top:131.55px" class="cls_064"><span class="cls_064">Home</span></div>
<div style="position:absolute;left:466.74px;top:131.55px" class="cls_064"><span class="cls_064">Marital</span></div>
<div style="position:absolute;left:532.25px;top:131.55px" class="cls_064"><span class="cls_064">Annual</span></div>
<div style="position:absolute;left:595.99px;top:131.55px" class="cls_064"><span class="cls_064">Defaulted</span></div>
<div style="position:absolute;left:403.49px;top:147.70px" class="cls_064"><span class="cls_064">Owner</span></div>
<div style="position:absolute;left:467.92px;top:147.70px" class="cls_064"><span class="cls_064">Status</span></div>
<div style="position:absolute;left:531.49px;top:147.70px" class="cls_064"><span class="cls_064">Income</span></div>
<div style="position:absolute;left:596.75px;top:147.70px" class="cls_064"><span class="cls_064">Borrower</span></div>
<div style="position:absolute;left:399.51px;top:174.54px" class="cls_065"><span class="cls_065">No</span></div>
<div style="position:absolute;left:459.19px;top:174.54px" class="cls_065"><span class="cls_065">Married</span></div>
<div style="position:absolute;left:527.43px;top:174.54px" class="cls_066"><span class="cls_066">80K</span></div>
<div style="position:absolute;left:592.69px;top:174.62px" class="cls_067"><span class="cls_067">?</span></div>
<div style="position:absolute;left:144.75px;top:189.68px" class="cls_035"><span class="cls_035">Home</span></div>
<div style="position:absolute;left:142.00px;top:208.64px" class="cls_035"><span class="cls_035">Owner</span></div>
<div style="position:absolute;left:76.42px;top:215.12px" class="cls_034"><span class="cls_034">Yes</span></div>
<div style="position:absolute;left:235.32px;top:215.12px" class="cls_058"><span class="cls_058">No</span></div>
<div style="position:absolute;left:71.38px;top:259.76px" class="cls_036"><span class="cls_036">NO</span></div>
<div style="position:absolute;left:232.38px;top:259.76px" class="cls_035"><span class="cls_035">MarSt</span></div>
<div style="position:absolute;left:138.07px;top:291.68px" class="cls_034"><span class="cls_034">Single, Divorced</span></div>
<div style="position:absolute;left:323.95px;top:289.04px" class="cls_058"><span class="cls_058">Married</span></div>
<div style="position:absolute;left:165.44px;top:336.32px" class="cls_035"><span class="cls_035">Income</span></div>
<div style="position:absolute;left:319.81px;top:338.96px" class="cls_036"><span class="cls_036">NO</span></div>
<div style="position:absolute;left:98.20px;top:368.24px" class="cls_034"><span class="cls_034">&lt; 80K</span></div>
<div style="position:absolute;left:251.45px;top:368.24px" class="cls_034"><span class="cls_034">> 80K</span></div>
<div style="position:absolute;left:120.25px;top:412.88px" class="cls_036"><span class="cls_036">NO</span></div>
<div style="position:absolute;left:238.75px;top:412.64px" class="cls_036"><span class="cls_036">YES</span></div>
<div style="position:absolute;left:56.70px;top:507.60px" class="cls_006"><span class="cls_006">10/23/21</span></div>
<div style="position:absolute;left:248.06px;top:507.60px" class="cls_006"><span class="cls_006">Introduction to Data Mining, 2</span><span class="cls_007"><sup>nd</sup></span><span class="cls_006"> Edition</span></div>
<div style="position:absolute;left:649.95px;top:507.60px" class="cls_006"><span class="cls_006">12</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:6600px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background13.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:11.44px" class="cls_002"><span class="cls_002">Apply Model to Test Data</span></div>
<div style="position:absolute;left:396.74px;top:88.48px" class="cls_038"><span class="cls_038">Test Data</span></div>
<div style="position:absolute;left:404.99px;top:131.65px" class="cls_069"><span class="cls_069">Home</span></div>
<div style="position:absolute;left:467.04px;top:131.65px" class="cls_069"><span class="cls_069">Marital</span></div>
<div style="position:absolute;left:532.81px;top:131.65px" class="cls_069"><span class="cls_069">Annual</span></div>
<div style="position:absolute;left:595.19px;top:131.65px" class="cls_069"><span class="cls_069">Defaulted</span></div>
<div style="position:absolute;left:403.54px;top:147.96px" class="cls_069"><span class="cls_069">Owner</span></div>
<div style="position:absolute;left:468.23px;top:147.96px" class="cls_069"><span class="cls_069">Status</span></div>
<div style="position:absolute;left:532.05px;top:147.96px" class="cls_069"><span class="cls_069">Income</span></div>
<div style="position:absolute;left:595.95px;top:147.96px" class="cls_069"><span class="cls_069">Borrower</span></div>
<div style="position:absolute;left:399.55px;top:175.07px" class="cls_070"><span class="cls_070">No</span></div>
<div style="position:absolute;left:459.46px;top:175.07px" class="cls_070"><span class="cls_070">Married</span></div>
<div style="position:absolute;left:527.97px;top:175.07px" class="cls_071"><span class="cls_071">80K</span></div>
<div style="position:absolute;left:593.49px;top:175.15px" class="cls_072"><span class="cls_072">?</span></div>
<div style="position:absolute;left:144.75px;top:189.68px" class="cls_035"><span class="cls_035">Home</span></div>
<div style="position:absolute;left:142.00px;top:208.64px" class="cls_035"><span class="cls_035">Owner</span></div>
<div style="position:absolute;left:76.42px;top:215.12px" class="cls_034"><span class="cls_034">Yes</span></div>
<div style="position:absolute;left:235.32px;top:215.12px" class="cls_058"><span class="cls_058">No</span></div>
<div style="position:absolute;left:71.38px;top:259.76px" class="cls_036"><span class="cls_036">NO</span></div>
<div style="position:absolute;left:232.38px;top:259.76px" class="cls_035"><span class="cls_035">MarSt</span></div>
<div style="position:absolute;left:481.20px;top:280.48px" class="cls_044"><span class="cls_044">Assign Defaulted to</span></div>
<div style="position:absolute;left:138.07px;top:291.68px" class="cls_034"><span class="cls_034">Single, Divorced</span></div>
<div style="position:absolute;left:323.95px;top:289.04px" class="cls_058"><span class="cls_058">Married</span></div>
<div style="position:absolute;left:508.20px;top:299.68px" class="cls_044"><span class="cls_044">“No”</span></div>
<div style="position:absolute;left:165.44px;top:336.32px" class="cls_035"><span class="cls_035">Income</span></div>
<div style="position:absolute;left:319.81px;top:338.96px" class="cls_036"><span class="cls_036">NO</span></div>
<div style="position:absolute;left:98.20px;top:368.24px" class="cls_034"><span class="cls_034">&lt; 80K</span></div>
<div style="position:absolute;left:251.45px;top:368.24px" class="cls_034"><span class="cls_034">> 80K</span></div>
<div style="position:absolute;left:120.25px;top:412.88px" class="cls_036"><span class="cls_036">NO</span></div>
<div style="position:absolute;left:238.75px;top:412.64px" class="cls_036"><span class="cls_036">YES</span></div>
<div style="position:absolute;left:56.70px;top:507.60px" class="cls_006"><span class="cls_006">10/23/21</span></div>
<div style="position:absolute;left:248.06px;top:507.60px" class="cls_006"><span class="cls_006">Introduction to Data Mining, 2</span><span class="cls_007"><sup>nd</sup></span><span class="cls_006"> Edition</span></div>
<div style="position:absolute;left:649.95px;top:507.60px" class="cls_006"><span class="cls_006">13</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:7150px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background14.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:11.44px" class="cls_002"><span class="cls_002">Decision Tree Classification Task</span></div>
<div style="position:absolute;left:428.16px;top:106.13px" class="cls_020"><span class="cls_020">Tree</span></div>
<div style="position:absolute;left:97.98px;top:118.92px" class="cls_074"><span class="cls_074">Tid</span></div>
<div style="position:absolute;left:122.61px;top:118.72px" class="cls_075"><span class="cls_075">Attrib1</span></div>
<div style="position:absolute;left:169.52px;top:118.72px" class="cls_075"><span class="cls_075">Attrib2</span></div>
<div style="position:absolute;left:217.10px;top:118.72px" class="cls_075"><span class="cls_075">Attrib3</span></div>
<div style="position:absolute;left:259.91px;top:118.72px" class="cls_075"><span class="cls_075">Class</span></div>
<div style="position:absolute;left:411.86px;top:125.25px" class="cls_020"><span class="cls_020">Induction</span></div>
<div style="position:absolute;left:98.18px;top:136.01px" class="cls_076"><span class="cls_076">1</span></div>
<div style="position:absolute;left:122.41px;top:136.01px" class="cls_076"><span class="cls_076">Yes</span></div>
<div style="position:absolute;left:162.68px;top:136.01px" class="cls_076"><span class="cls_076">Large</span></div>
<div style="position:absolute;left:213.99px;top:136.01px" class="cls_076"><span class="cls_076">125K</span></div>
<div style="position:absolute;left:257.96px;top:135.63px" class="cls_077"><span class="cls_077">No</span></div>
<div style="position:absolute;left:98.18px;top:153.21px" class="cls_076"><span class="cls_076">2</span></div>
<div style="position:absolute;left:122.41px;top:153.21px" class="cls_076"><span class="cls_076">No</span></div>
<div style="position:absolute;left:162.68px;top:153.21px" class="cls_076"><span class="cls_076">Medium</span></div>
<div style="position:absolute;left:213.99px;top:153.21px" class="cls_076"><span class="cls_076">100K</span></div>
<div style="position:absolute;left:257.96px;top:152.82px" class="cls_077"><span class="cls_077">No</span></div>
<div style="position:absolute;left:411.41px;top:144.36px" class="cls_020"><span class="cls_020">algorithm</span></div>
<div style="position:absolute;left:98.18px;top:170.41px" class="cls_076"><span class="cls_076">3</span></div>
<div style="position:absolute;left:122.41px;top:170.41px" class="cls_076"><span class="cls_076">No</span></div>
<div style="position:absolute;left:162.68px;top:170.41px" class="cls_076"><span class="cls_076">Small</span></div>
<div style="position:absolute;left:213.99px;top:170.41px" class="cls_076"><span class="cls_076">70K</span></div>
<div style="position:absolute;left:257.96px;top:170.03px" class="cls_077"><span class="cls_077">No</span></div>
<div style="position:absolute;left:98.18px;top:187.61px" class="cls_076"><span class="cls_076">4</span></div>
<div style="position:absolute;left:122.41px;top:187.61px" class="cls_076"><span class="cls_076">Yes</span></div>
<div style="position:absolute;left:162.68px;top:187.61px" class="cls_076"><span class="cls_076">Medium</span></div>
<div style="position:absolute;left:213.99px;top:187.61px" class="cls_076"><span class="cls_076">120K</span></div>
<div style="position:absolute;left:257.96px;top:187.23px" class="cls_077"><span class="cls_077">No</span></div>
<div style="position:absolute;left:345.92px;top:186.17px" class="cls_020"><span class="cls_020">Induction</span></div>
<div style="position:absolute;left:98.18px;top:204.81px" class="cls_076"><span class="cls_076">5</span></div>
<div style="position:absolute;left:122.41px;top:204.81px" class="cls_076"><span class="cls_076">No</span></div>
<div style="position:absolute;left:162.68px;top:204.81px" class="cls_076"><span class="cls_076">Large</span></div>
<div style="position:absolute;left:213.99px;top:204.81px" class="cls_076"><span class="cls_076">95K</span></div>
<div style="position:absolute;left:257.96px;top:204.42px" class="cls_077"><span class="cls_077">Yes</span></div>
<div style="position:absolute;left:98.18px;top:222.00px" class="cls_076"><span class="cls_076">6</span></div>
<div style="position:absolute;left:122.41px;top:222.00px" class="cls_076"><span class="cls_076">No</span></div>
<div style="position:absolute;left:162.68px;top:222.00px" class="cls_076"><span class="cls_076">Medium</span></div>
<div style="position:absolute;left:213.99px;top:222.00px" class="cls_076"><span class="cls_076">60K</span></div>
<div style="position:absolute;left:257.96px;top:221.62px" class="cls_077"><span class="cls_077">No</span></div>
<div style="position:absolute;left:98.18px;top:239.19px" class="cls_076"><span class="cls_076">7</span></div>
<div style="position:absolute;left:122.41px;top:239.19px" class="cls_076"><span class="cls_076">Yes</span></div>
<div style="position:absolute;left:162.68px;top:239.19px" class="cls_076"><span class="cls_076">Large</span></div>
<div style="position:absolute;left:213.99px;top:239.19px" class="cls_076"><span class="cls_076">220K</span></div>
<div style="position:absolute;left:257.96px;top:238.81px" class="cls_077"><span class="cls_077">No</span></div>
<div style="position:absolute;left:426.58px;top:230.79px" class="cls_021"><span class="cls_021">Learn</span></div>
<div style="position:absolute;left:98.18px;top:256.40px" class="cls_076"><span class="cls_076">8</span></div>
<div style="position:absolute;left:122.41px;top:256.40px" class="cls_076"><span class="cls_076">No</span></div>
<div style="position:absolute;left:162.68px;top:256.40px" class="cls_076"><span class="cls_076">Small</span></div>
<div style="position:absolute;left:213.99px;top:256.40px" class="cls_076"><span class="cls_076">85K</span></div>
<div style="position:absolute;left:257.96px;top:256.02px" class="cls_077"><span class="cls_077">Yes</span></div>
<div style="position:absolute;left:427.16px;top:251.39px" class="cls_021"><span class="cls_021">Model</span></div>
<div style="position:absolute;left:98.18px;top:273.60px" class="cls_076"><span class="cls_076">9</span></div>
<div style="position:absolute;left:122.41px;top:273.60px" class="cls_076"><span class="cls_076">No</span></div>
<div style="position:absolute;left:162.68px;top:273.60px" class="cls_076"><span class="cls_076">Medium</span></div>
<div style="position:absolute;left:213.99px;top:273.60px" class="cls_076"><span class="cls_076">75K</span></div>
<div style="position:absolute;left:257.96px;top:273.22px" class="cls_077"><span class="cls_077">No</span></div>
<div style="position:absolute;left:98.18px;top:290.79px" class="cls_076"><span class="cls_076">10</span></div>
<div style="position:absolute;left:122.41px;top:290.79px" class="cls_076"><span class="cls_076">No</span></div>
<div style="position:absolute;left:162.68px;top:290.79px" class="cls_076"><span class="cls_076">Small</span></div>
<div style="position:absolute;left:213.99px;top:290.79px" class="cls_076"><span class="cls_076">90K</span></div>
<div style="position:absolute;left:257.96px;top:290.41px" class="cls_077"><span class="cls_077">Yes</span></div>
<div style="position:absolute;left:565.83px;top:301.30px" class="cls_022"><span class="cls_022">Model</span></div>
<div style="position:absolute;left:134.73px;top:308.61px" class="cls_020"><span class="cls_020">Training Set</span></div>
<div style="position:absolute;left:426.01px;top:332.86px" class="cls_024"><span class="cls_024">Apply</span></div>
<div style="position:absolute;left:565.20px;top:340.96px" class="cls_079"><span class="cls_079">Decision</span></div>
<div style="position:absolute;left:427.16px;top:352.93px" class="cls_024"><span class="cls_024">Model</span></div>
<div style="position:absolute;left:565.20px;top:356.80px" class="cls_079"><span class="cls_079">Tree</span></div>
<div style="position:absolute;left:97.98px;top:365.99px" class="cls_025"><span class="cls_025">Tid</span></div>
<div style="position:absolute;left:122.61px;top:365.81px" class="cls_026"><span class="cls_026">Attrib1</span></div>
<div style="position:absolute;left:169.52px;top:365.81px" class="cls_026"><span class="cls_026">Attrib2</span></div>
<div style="position:absolute;left:217.10px;top:365.81px" class="cls_026"><span class="cls_026">Attrib3</span></div>
<div style="position:absolute;left:259.91px;top:365.81px" class="cls_026"><span class="cls_026">Class</span></div>
<div style="position:absolute;left:98.18px;top:383.12px" class="cls_027"><span class="cls_027">11</span></div>
<div style="position:absolute;left:122.41px;top:383.12px" class="cls_027"><span class="cls_027">No</span></div>
<div style="position:absolute;left:162.68px;top:383.12px" class="cls_027"><span class="cls_027">Small</span></div>
<div style="position:absolute;left:213.99px;top:383.12px" class="cls_027"><span class="cls_027">55K</span></div>
<div style="position:absolute;left:257.96px;top:382.74px" class="cls_028"><span class="cls_028">?</span></div>
<div style="position:absolute;left:98.18px;top:400.34px" class="cls_027"><span class="cls_027">12</span></div>
<div style="position:absolute;left:122.41px;top:400.34px" class="cls_027"><span class="cls_027">Yes</span></div>
<div style="position:absolute;left:162.68px;top:400.34px" class="cls_027"><span class="cls_027">Medium</span></div>
<div style="position:absolute;left:213.99px;top:400.34px" class="cls_027"><span class="cls_027">80K</span></div>
<div style="position:absolute;left:257.96px;top:399.96px" class="cls_028"><span class="cls_028">?</span></div>
<div style="position:absolute;left:342.30px;top:404.81px" class="cls_020"><span class="cls_020">Deduction</span></div>
<div style="position:absolute;left:98.18px;top:417.56px" class="cls_027"><span class="cls_027">13</span></div>
<div style="position:absolute;left:122.41px;top:417.56px" class="cls_027"><span class="cls_027">Yes</span></div>
<div style="position:absolute;left:162.68px;top:417.56px" class="cls_027"><span class="cls_027">Large</span></div>
<div style="position:absolute;left:213.99px;top:417.56px" class="cls_027"><span class="cls_027">110K</span></div>
<div style="position:absolute;left:257.96px;top:417.17px" class="cls_028"><span class="cls_028">?</span></div>
<div style="position:absolute;left:98.18px;top:434.77px" class="cls_027"><span class="cls_027">14</span></div>
<div style="position:absolute;left:122.41px;top:434.77px" class="cls_027"><span class="cls_027">No</span></div>
<div style="position:absolute;left:162.68px;top:434.77px" class="cls_027"><span class="cls_027">Small</span></div>
<div style="position:absolute;left:213.99px;top:434.77px" class="cls_027"><span class="cls_027">95K</span></div>
<div style="position:absolute;left:257.96px;top:434.39px" class="cls_028"><span class="cls_028">?</span></div>
<div style="position:absolute;left:98.18px;top:452.00px" class="cls_027"><span class="cls_027">15</span></div>
<div style="position:absolute;left:122.41px;top:452.00px" class="cls_027"><span class="cls_027">No</span></div>
<div style="position:absolute;left:162.68px;top:452.00px" class="cls_027"><span class="cls_027">Large</span></div>
<div style="position:absolute;left:213.99px;top:452.00px" class="cls_027"><span class="cls_027">67K</span></div>
<div style="position:absolute;left:257.96px;top:451.61px" class="cls_028"><span class="cls_028">?</span></div>
<div style="position:absolute;left:152.56px;top:470.72px" class="cls_020"><span class="cls_020">Test Set</span></div>
<div style="position:absolute;left:56.70px;top:507.60px" class="cls_006"><span class="cls_006">10/23/21</span></div>
<div style="position:absolute;left:248.06px;top:507.60px" class="cls_006"><span class="cls_006">Introduction to Data Mining, 2</span><span class="cls_007"><sup>nd</sup></span><span class="cls_006"> Edition</span></div>
<div style="position:absolute;left:649.95px;top:507.60px" class="cls_006"><span class="cls_006">14</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:7700px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background15.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:11.44px" class="cls_002"><span class="cls_002">Decision Tree Induction</span></div>
<div style="position:absolute;left:39.50px;top:92.48px" class="cls_008"><span class="cls_008">●</span><span class="cls_005"> Many Algorithms:</span></div>
<div style="position:absolute;left:75.50px;top:133.52px" class="cls_009"><span class="cls_009">-</span><span class="cls_005"> Hunt’s Algorithm (one of the earliest)</span></div>
<div style="position:absolute;left:75.50px;top:174.56px" class="cls_009"><span class="cls_009">-</span><span class="cls_005"> CART</span></div>
<div style="position:absolute;left:75.50px;top:215.60px" class="cls_009"><span class="cls_009">-</span><span class="cls_005"> ID3, C4.5</span></div>
<div style="position:absolute;left:75.50px;top:256.40px" class="cls_009"><span class="cls_009">-</span><span class="cls_005"> SLIQ,SPRINT</span></div>
<div style="position:absolute;left:56.70px;top:507.60px" class="cls_006"><span class="cls_006">10/23/21</span></div>
<div style="position:absolute;left:248.06px;top:507.60px" class="cls_006"><span class="cls_006">Introduction to Data Mining, 2</span><span class="cls_007"><sup>nd</sup></span><span class="cls_006"> Edition</span></div>
<div style="position:absolute;left:649.95px;top:507.60px" class="cls_006"><span class="cls_006">15</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:8250px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background16.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:11.44px" class="cls_002"><span class="cls_002">General Structure of Hunt’s Algorithm</span></div>
<div style="position:absolute;left:462.19px;top:94.06px" class="cls_087"><span class="cls_087">Home</span></div>
<div style="position:absolute;left:510.05px;top:94.06px" class="cls_087"><span class="cls_087">Marital</span></div>
<div style="position:absolute;left:560.81px;top:94.06px" class="cls_087"><span class="cls_087">Annual</span></div>
<div style="position:absolute;left:608.81px;top:94.06px" class="cls_087"><span class="cls_087">Defaulted</span></div>
<div style="position:absolute;left:39.50px;top:92.56px" class="cls_080"><span class="cls_080">●</span><span class="cls_044"> Let D</span><span class="cls_081"><sub>t</sub></span><span class="cls_044"> be the set of training</span></div>
<div style="position:absolute;left:436.36px;top:100.28px" class="cls_087"><span class="cls_087">ID</span></div>
<div style="position:absolute;left:461.07px;top:106.64px" class="cls_087"><span class="cls_087">Owner</span></div>
<div style="position:absolute;left:510.97px;top:106.64px" class="cls_087"><span class="cls_087">Status</span></div>
<div style="position:absolute;left:560.22px;top:106.64px" class="cls_087"><span class="cls_087">Income</span></div>
<div style="position:absolute;left:609.40px;top:106.64px" class="cls_087"><span class="cls_087">Borrower</span></div>
<div style="position:absolute;left:62.50px;top:116.56px" class="cls_044"><span class="cls_044">records that reach a node t</span></div>
<div style="position:absolute;left:433.08px;top:127.55px" class="cls_082"><span class="cls_082">1</span></div>
<div style="position:absolute;left:457.99px;top:127.55px" class="cls_082"><span class="cls_082">Yes</span></div>
<div style="position:absolute;left:504.22px;top:127.55px" class="cls_082"><span class="cls_082">Single</span></div>
<div style="position:absolute;left:557.08px;top:127.55px" class="cls_082"><span class="cls_082">125K</span></div>
<div style="position:absolute;left:607.63px;top:127.61px" class="cls_083"><span class="cls_083">No</span></div>
<div style="position:absolute;left:433.08px;top:148.19px" class="cls_082"><span class="cls_082">2</span></div>
<div style="position:absolute;left:457.99px;top:148.19px" class="cls_082"><span class="cls_082">No</span></div>
<div style="position:absolute;left:504.22px;top:148.19px" class="cls_082"><span class="cls_082">Married</span></div>
<div style="position:absolute;left:557.08px;top:148.19px" class="cls_082"><span class="cls_082">100K</span></div>
<div style="position:absolute;left:607.63px;top:148.25px" class="cls_083"><span class="cls_083">No</span></div>
<div style="position:absolute;left:433.08px;top:168.89px" class="cls_082"><span class="cls_082">3</span></div>
<div style="position:absolute;left:457.99px;top:168.89px" class="cls_082"><span class="cls_082">No</span></div>
<div style="position:absolute;left:504.22px;top:168.89px" class="cls_082"><span class="cls_082">Single</span></div>
<div style="position:absolute;left:557.08px;top:168.89px" class="cls_082"><span class="cls_082">70K</span></div>
<div style="position:absolute;left:607.63px;top:168.96px" class="cls_083"><span class="cls_083">No</span></div>
<div style="position:absolute;left:39.50px;top:170.56px" class="cls_080"><span class="cls_080">●</span><span class="cls_044"> General Procedure:</span></div>
<div style="position:absolute;left:433.08px;top:189.53px" class="cls_082"><span class="cls_082">4</span></div>
<div style="position:absolute;left:457.99px;top:189.53px" class="cls_082"><span class="cls_082">Yes</span></div>
<div style="position:absolute;left:504.22px;top:189.53px" class="cls_082"><span class="cls_082">Married</span></div>
<div style="position:absolute;left:557.08px;top:189.53px" class="cls_082"><span class="cls_082">120K</span></div>
<div style="position:absolute;left:607.63px;top:189.59px" class="cls_083"><span class="cls_083">No</span></div>
<div style="position:absolute;left:75.50px;top:200.56px" class="cls_084"><span class="cls_084">–</span></div>
<div style="position:absolute;left:102.50px;top:200.56px" class="cls_044"><span class="cls_044">If D</span><span class="cls_081"><sub>t</sub></span><span class="cls_044"> contains records that</span></div>
<div style="position:absolute;left:433.08px;top:210.31px" class="cls_082"><span class="cls_082">5</span></div>
<div style="position:absolute;left:457.99px;top:210.31px" class="cls_082"><span class="cls_082">No</span></div>
<div style="position:absolute;left:504.22px;top:210.31px" class="cls_082"><span class="cls_082">Divorced</span></div>
<div style="position:absolute;left:557.08px;top:210.31px" class="cls_082"><span class="cls_082">95K</span></div>
<div style="position:absolute;left:607.63px;top:210.38px" class="cls_083"><span class="cls_083">Yes</span></div>
<div style="position:absolute;left:433.08px;top:230.95px" class="cls_082"><span class="cls_082">6</span></div>
<div style="position:absolute;left:457.99px;top:230.95px" class="cls_082"><span class="cls_082">No</span></div>
<div style="position:absolute;left:504.22px;top:230.95px" class="cls_082"><span class="cls_082">Married</span></div>
<div style="position:absolute;left:557.08px;top:230.95px" class="cls_082"><span class="cls_082">60K</span></div>
<div style="position:absolute;left:607.63px;top:231.02px" class="cls_083"><span class="cls_083">No</span></div>
<div style="position:absolute;left:102.50px;top:224.56px" class="cls_044"><span class="cls_044">belong the same class y</span><span class="cls_081"><sub>t</sub></span><span class="cls_044">,</span></div>
<div style="position:absolute;left:433.08px;top:251.59px" class="cls_082"><span class="cls_082">7</span></div>
<div style="position:absolute;left:457.99px;top:251.59px" class="cls_082"><span class="cls_082">Yes</span></div>
<div style="position:absolute;left:504.22px;top:251.59px" class="cls_082"><span class="cls_082">Divorced</span></div>
<div style="position:absolute;left:557.08px;top:251.59px" class="cls_082"><span class="cls_082">220K</span></div>
<div style="position:absolute;left:607.63px;top:251.65px" class="cls_083"><span class="cls_083">No</span></div>
<div style="position:absolute;left:102.50px;top:248.56px" class="cls_044"><span class="cls_044">then t is a leaf node</span></div>
<div style="position:absolute;left:433.08px;top:272.23px" class="cls_082"><span class="cls_082">8</span></div>
<div style="position:absolute;left:457.99px;top:272.23px" class="cls_082"><span class="cls_082">No</span></div>
<div style="position:absolute;left:504.22px;top:272.23px" class="cls_082"><span class="cls_082">Single</span></div>
<div style="position:absolute;left:557.08px;top:272.23px" class="cls_082"><span class="cls_082">85K</span></div>
<div style="position:absolute;left:607.63px;top:272.29px" class="cls_083"><span class="cls_083">Yes</span></div>
<div style="position:absolute;left:102.50px;top:272.56px" class="cls_044"><span class="cls_044">labeled as y</span><span class="cls_081"><sub>t</sub></span></div>
<div style="position:absolute;left:433.08px;top:292.94px" class="cls_082"><span class="cls_082">9</span></div>
<div style="position:absolute;left:457.99px;top:292.94px" class="cls_082"><span class="cls_082">No</span></div>
<div style="position:absolute;left:504.22px;top:292.94px" class="cls_082"><span class="cls_082">Married</span></div>
<div style="position:absolute;left:557.08px;top:292.94px" class="cls_082"><span class="cls_082">75K</span></div>
<div style="position:absolute;left:607.63px;top:293.01px" class="cls_083"><span class="cls_083">No</span></div>
<div style="position:absolute;left:75.50px;top:303.52px" class="cls_084"><span class="cls_084">-</span></div>
<div style="position:absolute;left:102.50px;top:303.52px" class="cls_044"><span class="cls_044">If D</span><span class="cls_081"><sub>t</sub></span><span class="cls_044"> contains records that</span></div>
<div style="position:absolute;left:433.08px;top:313.65px" class="cls_082"><span class="cls_082">10</span></div>
<div style="position:absolute;left:457.99px;top:313.65px" class="cls_082"><span class="cls_082">No</span></div>
<div style="position:absolute;left:504.22px;top:313.65px" class="cls_082"><span class="cls_082">Single</span></div>
<div style="position:absolute;left:557.08px;top:313.65px" class="cls_082"><span class="cls_082">90K</span></div>
<div style="position:absolute;left:607.63px;top:313.71px" class="cls_083"><span class="cls_083">Yes</span></div>
<div style="position:absolute;left:102.50px;top:327.52px" class="cls_044"><span class="cls_044">belong to more than one</span></div>
<div style="position:absolute;left:553.20px;top:338.56px" class="cls_085"><span class="cls_085">D</span><span class="cls_086"><sub>t</sub></span></div>
<div style="position:absolute;left:102.50px;top:351.52px" class="cls_044"><span class="cls_044">class, use an attribute test</span></div>
<div style="position:absolute;left:102.50px;top:375.52px" class="cls_044"><span class="cls_044">to split the data into smaller</span></div>
<div style="position:absolute;left:523.20px;top:392.64px" class="cls_089"><span class="cls_089">?</span></div>
<div style="position:absolute;left:102.50px;top:399.52px" class="cls_044"><span class="cls_044">subsets. Recursively apply</span></div>
<div style="position:absolute;left:102.50px;top:423.52px" class="cls_044"><span class="cls_044">the procedure to each</span></div>
<div style="position:absolute;left:102.50px;top:447.52px" class="cls_044"><span class="cls_044">subset.</span></div>
<div style="position:absolute;left:56.70px;top:507.60px" class="cls_006"><span class="cls_006">10/23/21</span></div>
<div style="position:absolute;left:248.06px;top:507.60px" class="cls_006"><span class="cls_006">Introduction to Data Mining, 2</span><span class="cls_007"><sup>nd</sup></span><span class="cls_006"> Edition</span></div>
<div style="position:absolute;left:649.95px;top:507.60px" class="cls_006"><span class="cls_006">16</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:8800px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background17.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:11.44px" class="cls_002"><span class="cls_002">Hunt’s Algorithm</span></div>
<div style="position:absolute;left:509.77px;top:88.45px" class="cls_091"><span class="cls_091">Home</span></div>
<div style="position:absolute;left:557.08px;top:88.45px" class="cls_091"><span class="cls_091">Marital</span></div>
<div style="position:absolute;left:607.24px;top:88.45px" class="cls_091"><span class="cls_091">Annual</span></div>
<div style="position:absolute;left:654.68px;top:88.45px" class="cls_091"><span class="cls_091">Defaulted</span></div>
<div style="position:absolute;left:484.23px;top:94.62px" class="cls_091"><span class="cls_091">ID</span></div>
<div style="position:absolute;left:508.67px;top:100.84px" class="cls_091"><span class="cls_091">Owner</span></div>
<div style="position:absolute;left:557.99px;top:100.84px" class="cls_091"><span class="cls_091">Status</span></div>
<div style="position:absolute;left:606.65px;top:100.84px" class="cls_091"><span class="cls_091">Income</span></div>
<div style="position:absolute;left:655.26px;top:100.84px" class="cls_091"><span class="cls_091">Borrower</span></div>
<div style="position:absolute;left:481.00px;top:121.56px" class="cls_092"><span class="cls_092">1</span></div>
<div style="position:absolute;left:505.62px;top:121.56px" class="cls_092"><span class="cls_092">Yes</span></div>
<div style="position:absolute;left:551.31px;top:121.56px" class="cls_092"><span class="cls_092">Single</span></div>
<div style="position:absolute;left:603.54px;top:121.56px" class="cls_092"><span class="cls_092">125K</span></div>
<div style="position:absolute;left:653.51px;top:121.63px" class="cls_093"><span class="cls_093">No</span></div>
<div style="position:absolute;left:59.10px;top:138.20px" class="cls_094"><span class="cls_094">Defaulted = No</span></div>
<div style="position:absolute;left:481.00px;top:141.97px" class="cls_092"><span class="cls_092">2</span></div>
<div style="position:absolute;left:505.62px;top:141.97px" class="cls_092"><span class="cls_092">No</span></div>
<div style="position:absolute;left:551.31px;top:141.97px" class="cls_092"><span class="cls_092">Married</span></div>
<div style="position:absolute;left:603.54px;top:141.97px" class="cls_092"><span class="cls_092">100K</span></div>
<div style="position:absolute;left:653.51px;top:142.03px" class="cls_093"><span class="cls_093">No</span></div>
<div style="position:absolute;left:481.00px;top:162.39px" class="cls_092"><span class="cls_092">3</span></div>
<div style="position:absolute;left:505.62px;top:162.39px" class="cls_092"><span class="cls_092">No</span></div>
<div style="position:absolute;left:551.31px;top:162.39px" class="cls_092"><span class="cls_092">Single</span></div>
<div style="position:absolute;left:603.54px;top:162.39px" class="cls_092"><span class="cls_092">70K</span></div>
<div style="position:absolute;left:653.51px;top:162.45px" class="cls_093"><span class="cls_093">No</span></div>
<div style="position:absolute;left:85.20px;top:165.28px" class="cls_079"><span class="cls_079">(7,3)</span></div>
<div style="position:absolute;left:481.00px;top:182.86px" class="cls_092"><span class="cls_092">4</span></div>
<div style="position:absolute;left:505.62px;top:182.86px" class="cls_092"><span class="cls_092">Yes</span></div>
<div style="position:absolute;left:551.31px;top:182.86px" class="cls_092"><span class="cls_092">Married</span></div>
<div style="position:absolute;left:603.54px;top:182.86px" class="cls_092"><span class="cls_092">120K</span></div>
<div style="position:absolute;left:653.51px;top:182.92px" class="cls_093"><span class="cls_093">No</span></div>
<div style="position:absolute;left:481.00px;top:203.33px" class="cls_092"><span class="cls_092">5</span></div>
<div style="position:absolute;left:505.62px;top:203.33px" class="cls_092"><span class="cls_092">No</span></div>
<div style="position:absolute;left:551.31px;top:203.33px" class="cls_092"><span class="cls_092">Divorced</span></div>
<div style="position:absolute;left:603.54px;top:203.33px" class="cls_092"><span class="cls_092">95K</span></div>
<div style="position:absolute;left:653.51px;top:203.39px" class="cls_093"><span class="cls_093">Yes</span></div>
<div style="position:absolute;left:92.55px;top:198.98px" class="cls_098"><span class="cls_098">(a)</span></div>
<div style="position:absolute;left:481.00px;top:223.74px" class="cls_092"><span class="cls_092">6</span></div>
<div style="position:absolute;left:505.62px;top:223.74px" class="cls_092"><span class="cls_092">No</span></div>
<div style="position:absolute;left:551.31px;top:223.74px" class="cls_092"><span class="cls_092">Married</span></div>
<div style="position:absolute;left:603.54px;top:223.74px" class="cls_092"><span class="cls_092">60K</span></div>
<div style="position:absolute;left:653.51px;top:223.81px" class="cls_093"><span class="cls_093">No</span></div>
<div style="position:absolute;left:481.00px;top:244.21px" class="cls_092"><span class="cls_092">7</span></div>
<div style="position:absolute;left:505.62px;top:244.21px" class="cls_092"><span class="cls_092">Yes</span></div>
<div style="position:absolute;left:551.31px;top:244.21px" class="cls_092"><span class="cls_092">Divorced</span></div>
<div style="position:absolute;left:603.54px;top:244.21px" class="cls_092"><span class="cls_092">220K</span></div>
<div style="position:absolute;left:653.51px;top:244.28px" class="cls_093"><span class="cls_093">No</span></div>
<div style="position:absolute;left:481.00px;top:264.61px" class="cls_092"><span class="cls_092">8</span></div>
<div style="position:absolute;left:505.62px;top:264.61px" class="cls_092"><span class="cls_092">No</span></div>
<div style="position:absolute;left:551.31px;top:264.61px" class="cls_092"><span class="cls_092">Single</span></div>
<div style="position:absolute;left:603.54px;top:264.61px" class="cls_092"><span class="cls_092">85K</span></div>
<div style="position:absolute;left:653.51px;top:264.68px" class="cls_093"><span class="cls_093">Yes</span></div>
<div style="position:absolute;left:481.00px;top:285.03px" class="cls_092"><span class="cls_092">9</span></div>
<div style="position:absolute;left:505.62px;top:285.03px" class="cls_092"><span class="cls_092">No</span></div>
<div style="position:absolute;left:551.31px;top:285.03px" class="cls_092"><span class="cls_092">Married</span></div>
<div style="position:absolute;left:603.54px;top:285.03px" class="cls_092"><span class="cls_092">75K</span></div>
<div style="position:absolute;left:653.51px;top:285.10px" class="cls_093"><span class="cls_093">No</span></div>
<div style="position:absolute;left:481.00px;top:305.57px" class="cls_092"><span class="cls_092">10</span></div>
<div style="position:absolute;left:505.62px;top:305.57px" class="cls_092"><span class="cls_092">No</span></div>
<div style="position:absolute;left:551.31px;top:305.57px" class="cls_092"><span class="cls_092">Single</span></div>
<div style="position:absolute;left:603.54px;top:305.57px" class="cls_092"><span class="cls_092">90K</span></div>
<div style="position:absolute;left:653.51px;top:305.63px" class="cls_093"><span class="cls_093">Yes</span></div>
<div style="position:absolute;left:56.70px;top:507.60px" class="cls_006"><span class="cls_006">10/23/21</span></div>
<div style="position:absolute;left:248.06px;top:507.60px" class="cls_006"><span class="cls_006">Introduction to Data Mining, 2</span><span class="cls_007"><sup>nd</sup></span><span class="cls_006"> Edition</span></div>
<div style="position:absolute;left:649.95px;top:507.60px" class="cls_006"><span class="cls_006">17</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:9350px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background18.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:11.44px" class="cls_002"><span class="cls_002">Hunt’s Algorithm</span></div>
<div style="position:absolute;left:509.77px;top:88.45px" class="cls_091"><span class="cls_091">Home</span></div>
<div style="position:absolute;left:557.08px;top:88.45px" class="cls_091"><span class="cls_091">Marital</span></div>
<div style="position:absolute;left:607.24px;top:88.45px" class="cls_091"><span class="cls_091">Annual</span></div>
<div style="position:absolute;left:654.68px;top:88.45px" class="cls_091"><span class="cls_091">Defaulted</span></div>
<div style="position:absolute;left:357.92px;top:95.94px" class="cls_095"><span class="cls_095">Home</span></div>
<div style="position:absolute;left:484.23px;top:94.62px" class="cls_091"><span class="cls_091">ID</span></div>
<div style="position:absolute;left:508.67px;top:100.84px" class="cls_091"><span class="cls_091">Owner</span></div>
<div style="position:absolute;left:557.99px;top:100.84px" class="cls_091"><span class="cls_091">Status</span></div>
<div style="position:absolute;left:606.65px;top:100.84px" class="cls_091"><span class="cls_091">Income</span></div>
<div style="position:absolute;left:655.26px;top:100.84px" class="cls_091"><span class="cls_091">Borrower</span></div>
<div style="position:absolute;left:355.63px;top:109.78px" class="cls_095"><span class="cls_095">Owner</span></div>
<div style="position:absolute;left:481.00px;top:121.56px" class="cls_092"><span class="cls_092">1</span></div>
<div style="position:absolute;left:505.62px;top:121.56px" class="cls_092"><span class="cls_092">Yes</span></div>
<div style="position:absolute;left:551.31px;top:121.56px" class="cls_092"><span class="cls_092">Single</span></div>
<div style="position:absolute;left:603.54px;top:121.56px" class="cls_092"><span class="cls_092">125K</span></div>
<div style="position:absolute;left:653.51px;top:121.63px" class="cls_093"><span class="cls_093">No</span></div>
<div style="position:absolute;left:334.10px;top:127.83px" class="cls_096"><span class="cls_096">Yes</span></div>
<div style="position:absolute;left:403.51px;top:127.83px" class="cls_096"><span class="cls_096">No</span></div>
<div style="position:absolute;left:59.10px;top:138.20px" class="cls_094"><span class="cls_094">Defaulted = No</span></div>
<div style="position:absolute;left:481.00px;top:141.97px" class="cls_092"><span class="cls_092">2</span></div>
<div style="position:absolute;left:505.62px;top:141.97px" class="cls_092"><span class="cls_092">No</span></div>
<div style="position:absolute;left:551.31px;top:141.97px" class="cls_092"><span class="cls_092">Married</span></div>
<div style="position:absolute;left:603.54px;top:141.97px" class="cls_092"><span class="cls_092">100K</span></div>
<div style="position:absolute;left:653.51px;top:142.03px" class="cls_093"><span class="cls_093">No</span></div>
<div style="position:absolute;left:287.04px;top:158.58px" class="cls_097"><span class="cls_097">Defaulted = No</span></div>
<div style="position:absolute;left:385.61px;top:158.58px" class="cls_097"><span class="cls_097">Defaulted = No</span></div>
<div style="position:absolute;left:481.00px;top:162.39px" class="cls_092"><span class="cls_092">3</span></div>
<div style="position:absolute;left:505.62px;top:162.39px" class="cls_092"><span class="cls_092">No</span></div>
<div style="position:absolute;left:551.31px;top:162.39px" class="cls_092"><span class="cls_092">Single</span></div>
<div style="position:absolute;left:603.54px;top:162.39px" class="cls_092"><span class="cls_092">70K</span></div>
<div style="position:absolute;left:653.51px;top:162.45px" class="cls_093"><span class="cls_093">No</span></div>
<div style="position:absolute;left:85.20px;top:165.28px" class="cls_079"><span class="cls_079">(7,3)</span></div>
<div style="position:absolute;left:301.20px;top:177.52px" class="cls_079"><span class="cls_079">(3,0)</span></div>
<div style="position:absolute;left:401.70px;top:177.52px" class="cls_079"><span class="cls_079">(4,3)</span></div>
<div style="position:absolute;left:481.00px;top:182.86px" class="cls_092"><span class="cls_092">4</span></div>
<div style="position:absolute;left:505.62px;top:182.86px" class="cls_092"><span class="cls_092">Yes</span></div>
<div style="position:absolute;left:551.31px;top:182.86px" class="cls_092"><span class="cls_092">Married</span></div>
<div style="position:absolute;left:603.54px;top:182.86px" class="cls_092"><span class="cls_092">120K</span></div>
<div style="position:absolute;left:653.51px;top:182.92px" class="cls_093"><span class="cls_093">No</span></div>
<div style="position:absolute;left:481.00px;top:203.33px" class="cls_092"><span class="cls_092">5</span></div>
<div style="position:absolute;left:505.62px;top:203.33px" class="cls_092"><span class="cls_092">No</span></div>
<div style="position:absolute;left:551.31px;top:203.33px" class="cls_092"><span class="cls_092">Divorced</span></div>
<div style="position:absolute;left:603.54px;top:203.33px" class="cls_092"><span class="cls_092">95K</span></div>
<div style="position:absolute;left:653.51px;top:203.39px" class="cls_093"><span class="cls_093">Yes</span></div>
<div style="position:absolute;left:92.55px;top:198.98px" class="cls_098"><span class="cls_098">(a)</span></div>
<div style="position:absolute;left:358.28px;top:198.98px" class="cls_098"><span class="cls_098">(b)</span></div>
<div style="position:absolute;left:481.00px;top:223.74px" class="cls_092"><span class="cls_092">6</span></div>
<div style="position:absolute;left:505.62px;top:223.74px" class="cls_092"><span class="cls_092">No</span></div>
<div style="position:absolute;left:551.31px;top:223.74px" class="cls_092"><span class="cls_092">Married</span></div>
<div style="position:absolute;left:603.54px;top:223.74px" class="cls_092"><span class="cls_092">60K</span></div>
<div style="position:absolute;left:653.51px;top:223.81px" class="cls_093"><span class="cls_093">No</span></div>
<div style="position:absolute;left:481.00px;top:244.21px" class="cls_092"><span class="cls_092">7</span></div>
<div style="position:absolute;left:505.62px;top:244.21px" class="cls_092"><span class="cls_092">Yes</span></div>
<div style="position:absolute;left:551.31px;top:244.21px" class="cls_092"><span class="cls_092">Divorced</span></div>
<div style="position:absolute;left:603.54px;top:244.21px" class="cls_092"><span class="cls_092">220K</span></div>
<div style="position:absolute;left:653.51px;top:244.28px" class="cls_093"><span class="cls_093">No</span></div>
<div style="position:absolute;left:481.00px;top:264.61px" class="cls_092"><span class="cls_092">8</span></div>
<div style="position:absolute;left:505.62px;top:264.61px" class="cls_092"><span class="cls_092">No</span></div>
<div style="position:absolute;left:551.31px;top:264.61px" class="cls_092"><span class="cls_092">Single</span></div>
<div style="position:absolute;left:603.54px;top:264.61px" class="cls_092"><span class="cls_092">85K</span></div>
<div style="position:absolute;left:653.51px;top:264.68px" class="cls_093"><span class="cls_093">Yes</span></div>
<div style="position:absolute;left:481.00px;top:285.03px" class="cls_092"><span class="cls_092">9</span></div>
<div style="position:absolute;left:505.62px;top:285.03px" class="cls_092"><span class="cls_092">No</span></div>
<div style="position:absolute;left:551.31px;top:285.03px" class="cls_092"><span class="cls_092">Married</span></div>
<div style="position:absolute;left:603.54px;top:285.03px" class="cls_092"><span class="cls_092">75K</span></div>
<div style="position:absolute;left:653.51px;top:285.10px" class="cls_093"><span class="cls_093">No</span></div>
<div style="position:absolute;left:481.00px;top:305.57px" class="cls_092"><span class="cls_092">10</span></div>
<div style="position:absolute;left:505.62px;top:305.57px" class="cls_092"><span class="cls_092">No</span></div>
<div style="position:absolute;left:551.31px;top:305.57px" class="cls_092"><span class="cls_092">Single</span></div>
<div style="position:absolute;left:603.54px;top:305.57px" class="cls_092"><span class="cls_092">90K</span></div>
<div style="position:absolute;left:653.51px;top:305.63px" class="cls_093"><span class="cls_093">Yes</span></div>
<div style="position:absolute;left:56.70px;top:507.60px" class="cls_006"><span class="cls_006">10/23/21</span></div>
<div style="position:absolute;left:248.06px;top:507.60px" class="cls_006"><span class="cls_006">Introduction to Data Mining, 2</span><span class="cls_007"><sup>nd</sup></span><span class="cls_006"> Edition</span></div>
<div style="position:absolute;left:649.95px;top:507.60px" class="cls_006"><span class="cls_006">18</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:9900px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background19.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:11.44px" class="cls_002"><span class="cls_002">Hunt’s Algorithm</span></div>
<div style="position:absolute;left:509.77px;top:88.45px" class="cls_091"><span class="cls_091">Home</span></div>
<div style="position:absolute;left:557.08px;top:88.45px" class="cls_091"><span class="cls_091">Marital</span></div>
<div style="position:absolute;left:607.24px;top:88.45px" class="cls_091"><span class="cls_091">Annual</span></div>
<div style="position:absolute;left:654.68px;top:88.45px" class="cls_091"><span class="cls_091">Defaulted</span></div>
<div style="position:absolute;left:357.92px;top:95.94px" class="cls_095"><span class="cls_095">Home</span></div>
<div style="position:absolute;left:484.23px;top:94.62px" class="cls_091"><span class="cls_091">ID</span></div>
<div style="position:absolute;left:508.67px;top:100.84px" class="cls_091"><span class="cls_091">Owner</span></div>
<div style="position:absolute;left:557.99px;top:100.84px" class="cls_091"><span class="cls_091">Status</span></div>
<div style="position:absolute;left:606.65px;top:100.84px" class="cls_091"><span class="cls_091">Income</span></div>
<div style="position:absolute;left:655.26px;top:100.84px" class="cls_091"><span class="cls_091">Borrower</span></div>
<div style="position:absolute;left:355.63px;top:109.78px" class="cls_095"><span class="cls_095">Owner</span></div>
<div style="position:absolute;left:481.00px;top:121.56px" class="cls_092"><span class="cls_092">1</span></div>
<div style="position:absolute;left:505.62px;top:121.56px" class="cls_092"><span class="cls_092">Yes</span></div>
<div style="position:absolute;left:551.31px;top:121.56px" class="cls_092"><span class="cls_092">Single</span></div>
<div style="position:absolute;left:603.54px;top:121.56px" class="cls_092"><span class="cls_092">125K</span></div>
<div style="position:absolute;left:653.51px;top:121.63px" class="cls_093"><span class="cls_093">No</span></div>
<div style="position:absolute;left:334.10px;top:127.83px" class="cls_096"><span class="cls_096">Yes</span></div>
<div style="position:absolute;left:403.51px;top:127.83px" class="cls_096"><span class="cls_096">No</span></div>
<div style="position:absolute;left:59.10px;top:138.20px" class="cls_094"><span class="cls_094">Defaulted = No</span></div>
<div style="position:absolute;left:481.00px;top:141.97px" class="cls_092"><span class="cls_092">2</span></div>
<div style="position:absolute;left:505.62px;top:141.97px" class="cls_092"><span class="cls_092">No</span></div>
<div style="position:absolute;left:551.31px;top:141.97px" class="cls_092"><span class="cls_092">Married</span></div>
<div style="position:absolute;left:603.54px;top:141.97px" class="cls_092"><span class="cls_092">100K</span></div>
<div style="position:absolute;left:653.51px;top:142.03px" class="cls_093"><span class="cls_093">No</span></div>
<div style="position:absolute;left:287.04px;top:158.58px" class="cls_097"><span class="cls_097">Defaulted = No</span></div>
<div style="position:absolute;left:385.61px;top:158.58px" class="cls_097"><span class="cls_097">Defaulted = No</span></div>
<div style="position:absolute;left:481.00px;top:162.39px" class="cls_092"><span class="cls_092">3</span></div>
<div style="position:absolute;left:505.62px;top:162.39px" class="cls_092"><span class="cls_092">No</span></div>
<div style="position:absolute;left:551.31px;top:162.39px" class="cls_092"><span class="cls_092">Single</span></div>
<div style="position:absolute;left:603.54px;top:162.39px" class="cls_092"><span class="cls_092">70K</span></div>
<div style="position:absolute;left:653.51px;top:162.45px" class="cls_093"><span class="cls_093">No</span></div>
<div style="position:absolute;left:85.20px;top:165.28px" class="cls_079"><span class="cls_079">(7,3)</span></div>
<div style="position:absolute;left:301.20px;top:177.52px" class="cls_079"><span class="cls_079">(3,0)</span></div>
<div style="position:absolute;left:401.70px;top:177.52px" class="cls_079"><span class="cls_079">(4,3)</span></div>
<div style="position:absolute;left:481.00px;top:182.86px" class="cls_092"><span class="cls_092">4</span></div>
<div style="position:absolute;left:505.62px;top:182.86px" class="cls_092"><span class="cls_092">Yes</span></div>
<div style="position:absolute;left:551.31px;top:182.86px" class="cls_092"><span class="cls_092">Married</span></div>
<div style="position:absolute;left:603.54px;top:182.86px" class="cls_092"><span class="cls_092">120K</span></div>
<div style="position:absolute;left:653.51px;top:182.92px" class="cls_093"><span class="cls_093">No</span></div>
<div style="position:absolute;left:481.00px;top:203.33px" class="cls_092"><span class="cls_092">5</span></div>
<div style="position:absolute;left:505.62px;top:203.33px" class="cls_092"><span class="cls_092">No</span></div>
<div style="position:absolute;left:551.31px;top:203.33px" class="cls_092"><span class="cls_092">Divorced</span></div>
<div style="position:absolute;left:603.54px;top:203.33px" class="cls_092"><span class="cls_092">95K</span></div>
<div style="position:absolute;left:653.51px;top:203.39px" class="cls_093"><span class="cls_093">Yes</span></div>
<div style="position:absolute;left:92.55px;top:198.98px" class="cls_098"><span class="cls_098">(a)</span></div>
<div style="position:absolute;left:358.28px;top:198.98px" class="cls_098"><span class="cls_098">(b)</span></div>
<div style="position:absolute;left:481.00px;top:223.74px" class="cls_092"><span class="cls_092">6</span></div>
<div style="position:absolute;left:505.62px;top:223.74px" class="cls_092"><span class="cls_092">No</span></div>
<div style="position:absolute;left:551.31px;top:223.74px" class="cls_092"><span class="cls_092">Married</span></div>
<div style="position:absolute;left:603.54px;top:223.74px" class="cls_092"><span class="cls_092">60K</span></div>
<div style="position:absolute;left:653.51px;top:223.81px" class="cls_093"><span class="cls_093">No</span></div>
<div style="position:absolute;left:481.00px;top:244.21px" class="cls_092"><span class="cls_092">7</span></div>
<div style="position:absolute;left:505.62px;top:244.21px" class="cls_092"><span class="cls_092">Yes</span></div>
<div style="position:absolute;left:551.31px;top:244.21px" class="cls_092"><span class="cls_092">Divorced</span></div>
<div style="position:absolute;left:603.54px;top:244.21px" class="cls_092"><span class="cls_092">220K</span></div>
<div style="position:absolute;left:653.51px;top:244.28px" class="cls_093"><span class="cls_093">No</span></div>
<div style="position:absolute;left:481.00px;top:264.61px" class="cls_092"><span class="cls_092">8</span></div>
<div style="position:absolute;left:505.62px;top:264.61px" class="cls_092"><span class="cls_092">No</span></div>
<div style="position:absolute;left:551.31px;top:264.61px" class="cls_092"><span class="cls_092">Single</span></div>
<div style="position:absolute;left:603.54px;top:264.61px" class="cls_092"><span class="cls_092">85K</span></div>
<div style="position:absolute;left:653.51px;top:264.68px" class="cls_093"><span class="cls_093">Yes</span></div>
<div style="position:absolute;left:481.00px;top:285.03px" class="cls_092"><span class="cls_092">9</span></div>
<div style="position:absolute;left:505.62px;top:285.03px" class="cls_092"><span class="cls_092">No</span></div>
<div style="position:absolute;left:551.31px;top:285.03px" class="cls_092"><span class="cls_092">Married</span></div>
<div style="position:absolute;left:603.54px;top:285.03px" class="cls_092"><span class="cls_092">75K</span></div>
<div style="position:absolute;left:653.51px;top:285.10px" class="cls_093"><span class="cls_093">No</span></div>
<div style="position:absolute;left:74.77px;top:288.76px" class="cls_095"><span class="cls_095">Home</span></div>
<div style="position:absolute;left:72.48px;top:302.59px" class="cls_095"><span class="cls_095">Owner</span></div>
<div style="position:absolute;left:481.00px;top:305.57px" class="cls_092"><span class="cls_092">10</span></div>
<div style="position:absolute;left:505.62px;top:305.57px" class="cls_092"><span class="cls_092">No</span></div>
<div style="position:absolute;left:551.31px;top:305.57px" class="cls_092"><span class="cls_092">Single</span></div>
<div style="position:absolute;left:603.54px;top:305.57px" class="cls_092"><span class="cls_092">90K</span></div>
<div style="position:absolute;left:653.51px;top:305.63px" class="cls_093"><span class="cls_093">Yes</span></div>
<div style="position:absolute;left:50.60px;top:320.63px" class="cls_101"><span class="cls_101">Yes</span></div>
<div style="position:absolute;left:119.63px;top:320.63px" class="cls_101"><span class="cls_101">No</span></div>
<div style="position:absolute;left:107.73px;top:345.70px" class="cls_102"><span class="cls_102">Marital</span></div>
<div style="position:absolute;left:12.94px;top:349.01px" class="cls_097"><span class="cls_097">Defaulted = No</span></div>
<div style="position:absolute;left:111.30px;top:359.33px" class="cls_102"><span class="cls_102">Status</span></div>
<div style="position:absolute;left:19.20px;top:369.52px" class="cls_079"><span class="cls_079">(3,0)</span><span class="cls_101"> Single,</span></div>
<div style="position:absolute;left:156.53px;top:377.33px" class="cls_101"><span class="cls_101">Married</span></div>
<div style="position:absolute;left:50.00px;top:383.33px" class="cls_101"><span class="cls_101">Divorced</span></div>
<div style="position:absolute;left:38.80px;top:406.04px" class="cls_097"><span class="cls_097">Defaulted = Yes</span></div>
<div style="position:absolute;left:147.31px;top:405.14px" class="cls_097"><span class="cls_097">Defaulted = No</span></div>
<div style="position:absolute;left:55.20px;top:429.28px" class="cls_079"><span class="cls_079">(1,3)</span></div>
<div style="position:absolute;left:167.70px;top:429.28px" class="cls_079"><span class="cls_079">(3,0)</span></div>
<div style="position:absolute;left:91.35px;top:467.38px" class="cls_098"><span class="cls_098">(c)</span></div>
<div style="position:absolute;left:56.70px;top:507.60px" class="cls_006"><span class="cls_006">10/23/21</span></div>
<div style="position:absolute;left:248.06px;top:507.60px" class="cls_006"><span class="cls_006">Introduction to Data Mining, 2</span><span class="cls_007"><sup>nd</sup></span><span class="cls_006"> Edition</span></div>
<div style="position:absolute;left:649.95px;top:507.60px" class="cls_006"><span class="cls_006">19</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:10450px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background20.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:11.44px" class="cls_002"><span class="cls_002">Hunt’s Algorithm</span></div>
<div style="position:absolute;left:509.77px;top:88.45px" class="cls_091"><span class="cls_091">Home</span></div>
<div style="position:absolute;left:557.08px;top:88.45px" class="cls_091"><span class="cls_091">Marital</span></div>
<div style="position:absolute;left:607.24px;top:88.45px" class="cls_091"><span class="cls_091">Annual</span></div>
<div style="position:absolute;left:654.68px;top:88.45px" class="cls_091"><span class="cls_091">Defaulted</span></div>
<div style="position:absolute;left:357.92px;top:95.94px" class="cls_095"><span class="cls_095">Home</span></div>
<div style="position:absolute;left:484.23px;top:94.62px" class="cls_091"><span class="cls_091">ID</span></div>
<div style="position:absolute;left:508.67px;top:100.84px" class="cls_091"><span class="cls_091">Owner</span></div>
<div style="position:absolute;left:557.99px;top:100.84px" class="cls_091"><span class="cls_091">Status</span></div>
<div style="position:absolute;left:606.65px;top:100.84px" class="cls_091"><span class="cls_091">Income</span></div>
<div style="position:absolute;left:655.26px;top:100.84px" class="cls_091"><span class="cls_091">Borrower</span></div>
<div style="position:absolute;left:355.63px;top:109.78px" class="cls_095"><span class="cls_095">Owner</span></div>
<div style="position:absolute;left:481.00px;top:121.56px" class="cls_092"><span class="cls_092">1</span></div>
<div style="position:absolute;left:505.62px;top:121.56px" class="cls_092"><span class="cls_092">Yes</span></div>
<div style="position:absolute;left:551.31px;top:121.56px" class="cls_092"><span class="cls_092">Single</span></div>
<div style="position:absolute;left:603.54px;top:121.56px" class="cls_092"><span class="cls_092">125K</span></div>
<div style="position:absolute;left:653.51px;top:121.63px" class="cls_093"><span class="cls_093">No</span></div>
<div style="position:absolute;left:334.10px;top:127.83px" class="cls_096"><span class="cls_096">Yes</span></div>
<div style="position:absolute;left:403.51px;top:127.83px" class="cls_096"><span class="cls_096">No</span></div>
<div style="position:absolute;left:59.10px;top:138.20px" class="cls_094"><span class="cls_094">Defaulted = No</span></div>
<div style="position:absolute;left:481.00px;top:141.97px" class="cls_092"><span class="cls_092">2</span></div>
<div style="position:absolute;left:505.62px;top:141.97px" class="cls_092"><span class="cls_092">No</span></div>
<div style="position:absolute;left:551.31px;top:141.97px" class="cls_092"><span class="cls_092">Married</span></div>
<div style="position:absolute;left:603.54px;top:141.97px" class="cls_092"><span class="cls_092">100K</span></div>
<div style="position:absolute;left:653.51px;top:142.03px" class="cls_093"><span class="cls_093">No</span></div>
<div style="position:absolute;left:287.04px;top:158.58px" class="cls_097"><span class="cls_097">Defaulted = No</span></div>
<div style="position:absolute;left:385.61px;top:158.58px" class="cls_097"><span class="cls_097">Defaulted = No</span></div>
<div style="position:absolute;left:481.00px;top:162.39px" class="cls_092"><span class="cls_092">3</span></div>
<div style="position:absolute;left:505.62px;top:162.39px" class="cls_092"><span class="cls_092">No</span></div>
<div style="position:absolute;left:551.31px;top:162.39px" class="cls_092"><span class="cls_092">Single</span></div>
<div style="position:absolute;left:603.54px;top:162.39px" class="cls_092"><span class="cls_092">70K</span></div>
<div style="position:absolute;left:653.51px;top:162.45px" class="cls_093"><span class="cls_093">No</span></div>
<div style="position:absolute;left:85.20px;top:165.28px" class="cls_079"><span class="cls_079">(7,3)</span></div>
<div style="position:absolute;left:301.20px;top:177.52px" class="cls_079"><span class="cls_079">(3,0)</span></div>
<div style="position:absolute;left:401.70px;top:177.52px" class="cls_079"><span class="cls_079">(4,3)</span></div>
<div style="position:absolute;left:481.00px;top:182.86px" class="cls_092"><span class="cls_092">4</span></div>
<div style="position:absolute;left:505.62px;top:182.86px" class="cls_092"><span class="cls_092">Yes</span></div>
<div style="position:absolute;left:551.31px;top:182.86px" class="cls_092"><span class="cls_092">Married</span></div>
<div style="position:absolute;left:603.54px;top:182.86px" class="cls_092"><span class="cls_092">120K</span></div>
<div style="position:absolute;left:653.51px;top:182.92px" class="cls_093"><span class="cls_093">No</span></div>
<div style="position:absolute;left:481.00px;top:203.33px" class="cls_092"><span class="cls_092">5</span></div>
<div style="position:absolute;left:505.62px;top:203.33px" class="cls_092"><span class="cls_092">No</span></div>
<div style="position:absolute;left:551.31px;top:203.33px" class="cls_092"><span class="cls_092">Divorced</span></div>
<div style="position:absolute;left:603.54px;top:203.33px" class="cls_092"><span class="cls_092">95K</span></div>
<div style="position:absolute;left:653.51px;top:203.39px" class="cls_093"><span class="cls_093">Yes</span></div>
<div style="position:absolute;left:92.55px;top:198.98px" class="cls_098"><span class="cls_098">(a)</span></div>
<div style="position:absolute;left:358.28px;top:198.98px" class="cls_098"><span class="cls_098">(b)</span></div>
<div style="position:absolute;left:481.00px;top:223.74px" class="cls_092"><span class="cls_092">6</span></div>
<div style="position:absolute;left:505.62px;top:223.74px" class="cls_092"><span class="cls_092">No</span></div>
<div style="position:absolute;left:551.31px;top:223.74px" class="cls_092"><span class="cls_092">Married</span></div>
<div style="position:absolute;left:603.54px;top:223.74px" class="cls_092"><span class="cls_092">60K</span></div>
<div style="position:absolute;left:653.51px;top:223.81px" class="cls_093"><span class="cls_093">No</span></div>
<div style="position:absolute;left:481.00px;top:244.21px" class="cls_092"><span class="cls_092">7</span></div>
<div style="position:absolute;left:505.62px;top:244.21px" class="cls_092"><span class="cls_092">Yes</span></div>
<div style="position:absolute;left:551.31px;top:244.21px" class="cls_092"><span class="cls_092">Divorced</span></div>
<div style="position:absolute;left:603.54px;top:244.21px" class="cls_092"><span class="cls_092">220K</span></div>
<div style="position:absolute;left:653.51px;top:244.28px" class="cls_093"><span class="cls_093">No</span></div>
<div style="position:absolute;left:347.64px;top:255.40px" class="cls_095"><span class="cls_095">Home</span></div>
<div style="position:absolute;left:481.00px;top:264.61px" class="cls_092"><span class="cls_092">8</span></div>
<div style="position:absolute;left:505.62px;top:264.61px" class="cls_092"><span class="cls_092">No</span></div>
<div style="position:absolute;left:551.31px;top:264.61px" class="cls_092"><span class="cls_092">Single</span></div>
<div style="position:absolute;left:603.54px;top:264.61px" class="cls_092"><span class="cls_092">85K</span></div>
<div style="position:absolute;left:653.51px;top:264.68px" class="cls_093"><span class="cls_093">Yes</span></div>
<div style="position:absolute;left:345.35px;top:269.24px" class="cls_095"><span class="cls_095">Owner</span></div>
<div style="position:absolute;left:324.90px;top:286.58px" class="cls_099"><span class="cls_099">Yes</span></div>
<div style="position:absolute;left:392.71px;top:286.58px" class="cls_099"><span class="cls_099">No</span></div>
<div style="position:absolute;left:481.00px;top:285.03px" class="cls_092"><span class="cls_092">9</span></div>
<div style="position:absolute;left:505.62px;top:285.03px" class="cls_092"><span class="cls_092">No</span></div>
<div style="position:absolute;left:551.31px;top:285.03px" class="cls_092"><span class="cls_092">Married</span></div>
<div style="position:absolute;left:603.54px;top:285.03px" class="cls_092"><span class="cls_092">75K</span></div>
<div style="position:absolute;left:653.51px;top:285.10px" class="cls_093"><span class="cls_093">No</span></div>
<div style="position:absolute;left:74.77px;top:288.76px" class="cls_095"><span class="cls_095">Home</span></div>
<div style="position:absolute;left:72.48px;top:302.59px" class="cls_095"><span class="cls_095">Owner</span></div>
<div style="position:absolute;left:481.00px;top:305.57px" class="cls_092"><span class="cls_092">10</span></div>
<div style="position:absolute;left:505.62px;top:305.57px" class="cls_092"><span class="cls_092">No</span></div>
<div style="position:absolute;left:551.31px;top:305.57px" class="cls_092"><span class="cls_092">Single</span></div>
<div style="position:absolute;left:603.54px;top:305.57px" class="cls_092"><span class="cls_092">90K</span></div>
<div style="position:absolute;left:653.51px;top:305.63px" class="cls_093"><span class="cls_093">Yes</span></div>
<div style="position:absolute;left:280.38px;top:311.20px" class="cls_097"><span class="cls_097">Defaulted = No</span></div>
<div style="position:absolute;left:380.43px;top:311.24px" class="cls_100"><span class="cls_100">Marital</span></div>
<div style="position:absolute;left:50.60px;top:320.63px" class="cls_101"><span class="cls_101">Yes</span></div>
<div style="position:absolute;left:119.63px;top:320.63px" class="cls_101"><span class="cls_101">No</span></div>
<div style="position:absolute;left:384.52px;top:325.18px" class="cls_100"><span class="cls_100">Status</span></div>
<div style="position:absolute;left:295.20px;top:333.28px" class="cls_079"><span class="cls_079">(3,0)</span><span class="cls_099"> Single,</span></div>
<div style="position:absolute;left:427.78px;top:342.88px" class="cls_099"><span class="cls_099">Married</span></div>
<div style="position:absolute;left:12.94px;top:349.01px" class="cls_097"><span class="cls_097">Defaulted = No</span></div>
<div style="position:absolute;left:107.73px;top:345.70px" class="cls_102"><span class="cls_102">Marital</span></div>
<div style="position:absolute;left:324.32px;top:348.77px" class="cls_099"><span class="cls_099">Divorced</span></div>
<div style="position:absolute;left:111.30px;top:359.33px" class="cls_102"><span class="cls_102">Status</span></div>
<div style="position:absolute;left:426.55px;top:367.45px" class="cls_097"><span class="cls_097">Defaulted = No</span></div>
<div style="position:absolute;left:19.20px;top:369.52px" class="cls_079"><span class="cls_079">(3,0)</span><span class="cls_101"> Single,</span></div>
<div style="position:absolute;left:337.60px;top:369.68px" class="cls_100"><span class="cls_100">Annual</span></div>
<div style="position:absolute;left:156.53px;top:377.33px" class="cls_101"><span class="cls_101">Married</span></div>
<div style="position:absolute;left:50.00px;top:383.33px" class="cls_101"><span class="cls_101">Divorced</span></div>
<div style="position:absolute;left:337.76px;top:383.08px" class="cls_100"><span class="cls_100">Income</span></div>
<div style="position:absolute;left:443.70px;top:387.28px" class="cls_079"><span class="cls_079">(3,0)</span></div>
<div style="position:absolute;left:302.69px;top:402.93px" class="cls_099"><span class="cls_099">&lt; 80K</span></div>
<div style="position:absolute;left:386.86px;top:402.93px" class="cls_099"><span class="cls_099">>= 80K</span></div>
<div style="position:absolute;left:38.80px;top:406.04px" class="cls_097"><span class="cls_097">Defaulted = Yes</span></div>
<div style="position:absolute;left:147.31px;top:405.14px" class="cls_097"><span class="cls_097">Defaulted = No</span></div>
<div style="position:absolute;left:273.32px;top:426.94px" class="cls_097"><span class="cls_097">Defaulted = No</span></div>
<div style="position:absolute;left:370.98px;top:426.94px" class="cls_097"><span class="cls_097">Defaulted = Yes</span></div>
<div style="position:absolute;left:55.20px;top:429.28px" class="cls_079"><span class="cls_079">(1,3)</span></div>
<div style="position:absolute;left:167.70px;top:429.28px" class="cls_079"><span class="cls_079">(3,0)</span></div>
<div style="position:absolute;left:295.20px;top:447.28px" class="cls_079"><span class="cls_079">(1,0)</span></div>
<div style="position:absolute;left:389.70px;top:447.28px" class="cls_079"><span class="cls_079">(0,3)</span></div>
<div style="position:absolute;left:91.35px;top:467.38px" class="cls_098"><span class="cls_098">(c)</span></div>
<div style="position:absolute;left:356.57px;top:467.38px" class="cls_098"><span class="cls_098">(d)</span></div>
<div style="position:absolute;left:56.70px;top:507.60px" class="cls_006"><span class="cls_006">10/23/21</span></div>
<div style="position:absolute;left:248.06px;top:507.60px" class="cls_006"><span class="cls_006">Introduction to Data Mining, 2</span><span class="cls_007"><sup>nd</sup></span><span class="cls_006"> Edition</span></div>
<div style="position:absolute;left:649.95px;top:507.60px" class="cls_006"><span class="cls_006">20</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:11000px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background21.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:11.44px" class="cls_002"><span class="cls_002">Design Issues of Decision Tree Induction</span></div>
<div style="position:absolute;left:39.50px;top:92.48px" class="cls_008"><span class="cls_008">●</span><span class="cls_005"> How should training records be split?</span></div>
<div style="position:absolute;left:75.50px;top:133.52px" class="cls_009"><span class="cls_009">-</span><span class="cls_005"> Method for specifying test condition</span></div>
<div style="position:absolute;left:111.50px;top:174.48px" class="cls_012"><span class="cls_012">u</span><span class="cls_014"> depending on attribute types</span></div>
<div style="position:absolute;left:75.50px;top:210.56px" class="cls_009"><span class="cls_009">-</span><span class="cls_005"> Measure for evaluating the goodness of a test</span></div>
<div style="position:absolute;left:102.50px;top:243.44px" class="cls_005"><span class="cls_005">condition</span></div>
<div style="position:absolute;left:39.50px;top:325.52px" class="cls_008"><span class="cls_008">●</span><span class="cls_005"> How should the splitting procedure stop?</span></div>
<div style="position:absolute;left:75.50px;top:366.56px" class="cls_009"><span class="cls_009">-</span><span class="cls_005"> Stop splitting if all the records belong to the</span></div>
<div style="position:absolute;left:102.50px;top:400.40px" class="cls_005"><span class="cls_005">same class or have identical attribute values</span></div>
<div style="position:absolute;left:75.50px;top:441.44px" class="cls_009"><span class="cls_009">-</span><span class="cls_005"> Early termination</span></div>
<div style="position:absolute;left:56.70px;top:507.60px" class="cls_006"><span class="cls_006">10/23/21</span></div>
<div style="position:absolute;left:248.06px;top:507.60px" class="cls_006"><span class="cls_006">Introduction to Data Mining, 2</span><span class="cls_007"><sup>nd</sup></span><span class="cls_006"> Edition</span></div>
<div style="position:absolute;left:649.95px;top:507.60px" class="cls_006"><span class="cls_006">21</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:11550px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background22.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:11.44px" class="cls_002"><span class="cls_002">Methods for Expressing Test Conditions</span></div>
<div style="position:absolute;left:39.50px;top:92.48px" class="cls_008"><span class="cls_008">●</span><span class="cls_005"> Depends on attribute types</span></div>
<div style="position:absolute;left:75.50px;top:133.52px" class="cls_009"><span class="cls_009">-</span><span class="cls_005"> Binary</span></div>
<div style="position:absolute;left:75.50px;top:174.56px" class="cls_009"><span class="cls_009">-</span><span class="cls_005"> Nominal</span></div>
<div style="position:absolute;left:75.50px;top:215.60px" class="cls_009"><span class="cls_009">-</span><span class="cls_005"> Ordinal</span></div>
<div style="position:absolute;left:75.50px;top:256.40px" class="cls_009"><span class="cls_009">-</span><span class="cls_005"> Continuous</span></div>
<div style="position:absolute;left:39.50px;top:338.48px" class="cls_008"><span class="cls_008">●</span><span class="cls_005"> Depends on number of ways to split</span></div>
<div style="position:absolute;left:75.50px;top:379.52px" class="cls_009"><span class="cls_009">-</span><span class="cls_005"> 2-way split</span></div>
<div style="position:absolute;left:75.50px;top:420.56px" class="cls_009"><span class="cls_009">-</span><span class="cls_005"> Multi-way split</span></div>
<div style="position:absolute;left:56.70px;top:507.60px" class="cls_006"><span class="cls_006">10/23/21</span></div>
<div style="position:absolute;left:248.06px;top:507.60px" class="cls_006"><span class="cls_006">Introduction to Data Mining, 2</span><span class="cls_007"><sup>nd</sup></span><span class="cls_006"> Edition</span></div>
<div style="position:absolute;left:649.95px;top:507.60px" class="cls_006"><span class="cls_006">22</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:12100px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background23.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:11.44px" class="cls_002"><span class="cls_002">Test Condition for Nominal Attributes</span></div>
<div style="position:absolute;left:39.50px;top:92.40px" class="cls_103"><span class="cls_103">●</span><span class="cls_104"> Multi-way split:</span></div>
<div style="position:absolute;left:528.98px;top:121.43px" class="cls_106"><span class="cls_106">Marital</span></div>
<div style="position:absolute;left:75.50px;top:128.40px" class="cls_105"><span class="cls_105">-</span><span class="cls_014"> Use as many partitions as</span></div>
<div style="position:absolute;left:530.21px;top:138.87px" class="cls_106"><span class="cls_106">Status</span></div>
<div style="position:absolute;left:98.00px;top:157.44px" class="cls_014"><span class="cls_014">distinct values.</span></div>
<div style="position:absolute;left:420.93px;top:229.46px" class="cls_106"><span class="cls_106">Single</span></div>
<div style="position:absolute;left:521.94px;top:229.46px" class="cls_106"><span class="cls_106">Divorced</span></div>
<div style="position:absolute;left:632.05px;top:231.27px" class="cls_106"><span class="cls_106">Married</span></div>
<div style="position:absolute;left:39.50px;top:264.48px" class="cls_103"><span class="cls_103">●</span><span class="cls_104"> Binary split:</span></div>
<div style="position:absolute;left:75.50px;top:300.48px" class="cls_105"><span class="cls_105">–</span><span class="cls_014"> Divides values into two subsets</span></div>
<div style="position:absolute;left:342.33px;top:365.55px" class="cls_107"><span class="cls_107">Marital</span></div>
<div style="position:absolute;left:485.33px;top:365.55px" class="cls_107"><span class="cls_107">Marital</span></div>
<div style="position:absolute;left:626.72px;top:365.93px" class="cls_110"><span class="cls_110">Marital</span></div>
<div style="position:absolute;left:343.40px;top:380.56px" class="cls_107"><span class="cls_107">Status</span></div>
<div style="position:absolute;left:486.39px;top:380.56px" class="cls_107"><span class="cls_107">Status</span></div>
<div style="position:absolute;left:627.82px;top:381.44px" class="cls_110"><span class="cls_110">Status</span></div>
<div style="position:absolute;left:423.13px;top:404.37px" class="cls_108"><span class="cls_108">OR</span></div>
<div style="position:absolute;left:558.83px;top:402.83px" class="cls_109"><span class="cls_109">OR</span></div>
<div style="position:absolute;left:300.80px;top:461.65px" class="cls_107"><span class="cls_107">{Married}</span></div>
<div style="position:absolute;left:372.86px;top:463.52px" class="cls_107"><span class="cls_107">{Single,</span></div>
<div style="position:absolute;left:452.01px;top:461.65px" class="cls_107"><span class="cls_107">{Single}</span></div>
<div style="position:absolute;left:518.97px;top:461.96px" class="cls_107"><span class="cls_107">{Married,</span></div>
<div style="position:absolute;left:593.53px;top:462.35px" class="cls_110"><span class="cls_110">{Single,</span></div>
<div style="position:absolute;left:655.03px;top:463.64px" class="cls_110"><span class="cls_110">{Divorced}</span></div>
<div style="position:absolute;left:367.17px;top:478.53px" class="cls_107"><span class="cls_107">Divorced}</span></div>
<div style="position:absolute;left:517.19px;top:476.97px" class="cls_107"><span class="cls_107">Divorced}</span></div>
<div style="position:absolute;left:591.33px;top:477.86px" class="cls_110"><span class="cls_110">Married}</span></div>
<div style="position:absolute;left:56.70px;top:507.60px" class="cls_006"><span class="cls_006">10/23/21</span></div>
<div style="position:absolute;left:248.06px;top:507.60px" class="cls_006"><span class="cls_006">Introduction to Data Mining, 2</span><span class="cls_007"><sup>nd</sup></span><span class="cls_006"> Edition</span></div>
<div style="position:absolute;left:649.95px;top:507.60px" class="cls_006"><span class="cls_006">23</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:12650px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background24.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:11.44px" class="cls_002"><span class="cls_002">Test Condition for Ordinal Attributes</span></div>
<div style="position:absolute;left:506.90px;top:99.28px" class="cls_111"><span class="cls_111">Shirt</span></div>
<div style="position:absolute;left:39.50px;top:92.40px" class="cls_103"><span class="cls_103">●</span><span class="cls_104"> Multi-way split:</span></div>
<div style="position:absolute;left:507.63px;top:114.81px" class="cls_111"><span class="cls_111">Size</span></div>
<div style="position:absolute;left:75.50px;top:128.40px" class="cls_105"><span class="cls_105">-</span><span class="cls_014"> Use as many partitions</span></div>
<div style="position:absolute;left:98.00px;top:157.44px" class="cls_014"><span class="cls_014">as distinct values</span></div>
<div style="position:absolute;left:420.83px;top:188.94px" class="cls_112"><span class="cls_112">Small</span></div>
<div style="position:absolute;left:597.95px;top:195.42px" class="cls_112"><span class="cls_112">Extra Large</span></div>
<div style="position:absolute;left:475.71px;top:200.27px" class="cls_112"><span class="cls_112">Medium</span></div>
<div style="position:absolute;left:536.70px;top:200.27px" class="cls_112"><span class="cls_112">Large</span></div>
<div style="position:absolute;left:433.41px;top:231.34px" class="cls_114"><span class="cls_114">Shirt</span></div>
<div style="position:absolute;left:575.29px;top:231.34px" class="cls_114"><span class="cls_114">Shirt</span></div>
<div style="position:absolute;left:39.50px;top:228.48px" class="cls_103"><span class="cls_103">●</span><span class="cls_104"> Binary split:</span></div>
<div style="position:absolute;left:434.15px;top:247.01px" class="cls_114"><span class="cls_114">Size</span></div>
<div style="position:absolute;left:576.03px;top:247.01px" class="cls_114"><span class="cls_114">Size</span></div>
<div style="position:absolute;left:75.50px;top:264.48px" class="cls_105"><span class="cls_105">-</span><span class="cls_014"> Divides values into two</span></div>
<div style="position:absolute;left:98.00px;top:293.52px" class="cls_014"><span class="cls_014">subsets</span></div>
<div style="position:absolute;left:400.88px;top:332.53px" class="cls_113"><span class="cls_113">{Small,</span></div>
<div style="position:absolute;left:456.82px;top:332.53px" class="cls_113"><span class="cls_113">{Large,</span></div>
<div style="position:absolute;left:542.68px;top:333.19px" class="cls_113"><span class="cls_113">{Small}</span></div>
<div style="position:absolute;left:582.93px;top:332.53px" class="cls_113"><span class="cls_113">{Medium, Large,</span></div>
<div style="position:absolute;left:75.50px;top:328.56px" class="cls_105"><span class="cls_105">-</span><span class="cls_014"> Preserve order</span></div>
<div style="position:absolute;left:396.84px;top:344.72px" class="cls_113"><span class="cls_113">Medium}</span></div>
<div style="position:absolute;left:456.82px;top:344.72px" class="cls_113"><span class="cls_113">Extra Large}</span></div>
<div style="position:absolute;left:592.18px;top:344.72px" class="cls_113"><span class="cls_113">Extra Large}</span></div>
<div style="position:absolute;left:98.00px;top:357.60px" class="cls_014"><span class="cls_014">property among</span></div>
<div style="position:absolute;left:427.12px;top:375.49px" class="cls_115"><span class="cls_115">Shirt</span></div>
<div style="position:absolute;left:427.87px;top:391.37px" class="cls_115"><span class="cls_115">Size</span></div>
<div style="position:absolute;left:98.00px;top:386.40px" class="cls_014"><span class="cls_014">attribute values</span></div>
<div style="position:absolute;left:565.20px;top:405.52px" class="cls_079"><span class="cls_079">This grouping</span></div>
<div style="position:absolute;left:565.20px;top:421.60px" class="cls_079"><span class="cls_079">violates order</span></div>
<div style="position:absolute;left:565.20px;top:438.64px" class="cls_079"><span class="cls_079">property</span></div>
<div style="position:absolute;left:390.85px;top:478.05px" class="cls_116"><span class="cls_116">{Small,</span></div>
<div style="position:absolute;left:445.72px;top:478.05px" class="cls_116"><span class="cls_116">{Medium,</span></div>
<div style="position:absolute;left:392.01px;top:490.41px" class="cls_116"><span class="cls_116">Large}</span></div>
<div style="position:absolute;left:445.72px;top:490.41px" class="cls_116"><span class="cls_116">Extra Large}</span></div>
<div style="position:absolute;left:56.70px;top:507.60px" class="cls_006"><span class="cls_006">10/23/21</span></div>
<div style="position:absolute;left:248.06px;top:507.60px" class="cls_006"><span class="cls_006">Introduction to Data Mining, 2</span><span class="cls_007"><sup>nd</sup></span><span class="cls_006"> Edition</span></div>
<div style="position:absolute;left:649.95px;top:507.60px" class="cls_006"><span class="cls_006">24</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:13200px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background25.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:11.44px" class="cls_002"><span class="cls_002">Test Condition for Continuous Attributes</span></div>
<div style="position:absolute;left:96.13px;top:148.21px" class="cls_117"><span class="cls_117">Annual</span></div>
<div style="position:absolute;left:467.98px;top:157.33px" class="cls_117"><span class="cls_117">Annual</span></div>
<div style="position:absolute;left:94.38px;top:172.94px" class="cls_117"><span class="cls_117">Income</span></div>
<div style="position:absolute;left:460.37px;top:182.05px" class="cls_117"><span class="cls_117">Income?</span></div>
<div style="position:absolute;left:95.25px;top:197.66px" class="cls_117"><span class="cls_117">> 80K?</span></div>
<div style="position:absolute;left:373.13px;top:236.45px" class="cls_118"><span class="cls_118">&lt; 10K</span></div>
<div style="position:absolute;left:576.85px;top:236.45px" class="cls_118"><span class="cls_118">> 80K</span></div>
<div style="position:absolute;left:81.72px;top:266.73px" class="cls_118"><span class="cls_118">Yes</span></div>
<div style="position:absolute;left:156.44px;top:266.73px" class="cls_118"><span class="cls_118">No</span></div>
<div style="position:absolute;left:378.61px;top:305.97px" class="cls_118"><span class="cls_118">[10K,25K)</span></div>
<div style="position:absolute;left:465.64px;top:305.97px" class="cls_118"><span class="cls_118">[25K,50K)</span></div>
<div style="position:absolute;left:556.63px;top:305.97px" class="cls_118"><span class="cls_118">[50K,80K)</span></div>
<div style="position:absolute;left:92.14px;top:355.77px" class="cls_117"><span class="cls_117">(i) Binary split</span></div>
<div style="position:absolute;left:420.63px;top:355.77px" class="cls_117"><span class="cls_117">(ii) Multi-way split</span></div>
<div style="position:absolute;left:56.70px;top:507.60px" class="cls_006"><span class="cls_006">10/23/21</span></div>
<div style="position:absolute;left:248.06px;top:507.60px" class="cls_006"><span class="cls_006">Introduction to Data Mining, 2</span><span class="cls_007"><sup>nd</sup></span><span class="cls_006"> Edition</span></div>
<div style="position:absolute;left:649.95px;top:507.60px" class="cls_006"><span class="cls_006">25</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:13750px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background26.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:11.44px" class="cls_002"><span class="cls_002">Splitting Based on Continuous Attributes</span></div>
<div style="position:absolute;left:39.50px;top:92.48px" class="cls_008"><span class="cls_008">●</span><span class="cls_005"> Different ways of handling</span></div>
<div style="position:absolute;left:75.50px;top:133.52px" class="cls_009"><span class="cls_009">-</span><span class="cls_119"> Discretization</span><span class="cls_005"> to form an ordinal categorical</span></div>
<div style="position:absolute;left:102.50px;top:167.60px" class="cls_005"><span class="cls_005">attribute</span></div>
<div style="position:absolute;left:118.13px;top:207.60px" class="cls_014"><span class="cls_014">Ranges can be found by equal interval bucketing,</span></div>
<div style="position:absolute;left:111.50px;top:236.40px" class="cls_014"><span class="cls_014">equal frequency bucketing (percentiles), or</span></div>
<div style="position:absolute;left:111.50px;top:265.44px" class="cls_014"><span class="cls_014">clustering.</span></div>
<div style="position:absolute;left:111.50px;top:301.44px" class="cls_012"><span class="cls_012">u</span><span class="cls_014"> Static - discretize once at the beginning</span></div>
<div style="position:absolute;left:111.50px;top:336.48px" class="cls_012"><span class="cls_012">u</span><span class="cls_014"> Dynamic - repeat at each node</span></div>
<div style="position:absolute;left:75.50px;top:401.60px" class="cls_009"><span class="cls_009">-</span><span class="cls_119"> Binary Decision</span><span class="cls_005">: (A &lt; v) or (A ³ v)</span></div>
<div style="position:absolute;left:111.50px;top:442.56px" class="cls_012"><span class="cls_012">u</span><span class="cls_014"> consider all possible splits and finds the best cut</span></div>
<div style="position:absolute;left:111.50px;top:477.60px" class="cls_012"><span class="cls_012">u</span><span class="cls_014"> can be more compute intensive</span></div>
<div style="position:absolute;left:56.70px;top:507.60px" class="cls_006"><span class="cls_006">10/23/21</span></div>
<div style="position:absolute;left:248.06px;top:507.60px" class="cls_006"><span class="cls_006">Introduction to Data Mining, 2</span><span class="cls_007"><sup>nd</sup></span><span class="cls_006"> Edition</span></div>
<div style="position:absolute;left:649.95px;top:507.60px" class="cls_006"><span class="cls_006">26</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:14300px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background27.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:11.44px" class="cls_002"><span class="cls_002">How to determine the Best Split</span></div>
<div style="position:absolute;left:49.20px;top:183.60px" class="cls_120"><span class="cls_120">Before Splitting: 10 records of class 0,</span></div>
<div style="position:absolute;left:193.20px;top:204.48px" class="cls_120"><span class="cls_120">10 records of class 1</span></div>
<div style="position:absolute;left:307.47px;top:332.18px" class="cls_121"><span class="cls_121">Car</span></div>
<div style="position:absolute;left:530.04px;top:332.18px" class="cls_121"><span class="cls_121">Customer</span></div>
<div style="position:absolute;left:107.44px;top:336.98px" class="cls_121"><span class="cls_121">Gender</span></div>
<div style="position:absolute;left:303.19px;top:348.62px" class="cls_121"><span class="cls_121">Type</span></div>
<div style="position:absolute;left:553.37px;top:348.62px" class="cls_121"><span class="cls_121">ID</span></div>
<div style="position:absolute;left:249.19px;top:378.06px" class="cls_121"><span class="cls_121">Family</span></div>
<div style="position:absolute;left:344.96px;top:378.06px" class="cls_121"><span class="cls_121">Luxury</span></div>
<div style="position:absolute;left:494.68px;top:378.06px" class="cls_121"><span class="cls_121">c</span><span class="cls_122"><sub>1</sub></span></div>
<div style="position:absolute;left:614.48px;top:378.06px" class="cls_121"><span class="cls_121">c</span><span class="cls_122"><sub>20</sub></span></div>
<div style="position:absolute;left:87.24px;top:381.63px" class="cls_121"><span class="cls_121">Yes</span></div>
<div style="position:absolute;left:149.11px;top:381.34px" class="cls_121"><span class="cls_121">No</span></div>
<div style="position:absolute;left:520.76px;top:391.90px" class="cls_121"><span class="cls_121">c</span><span class="cls_122"><sub>10</sub></span></div>
<div style="position:absolute;left:555.73px;top:391.90px" class="cls_121"><span class="cls_121">c</span><span class="cls_122"><sub>11</sub></span></div>
<div style="position:absolute;left:281.04px;top:402.17px" class="cls_121"><span class="cls_121">Sports</span></div>
<div style="position:absolute;left:93.18px;top:430.05px" class="cls_123"><span class="cls_123">C0: 6</span></div>
<div style="position:absolute;left:138.64px;top:430.05px" class="cls_123"><span class="cls_123">C0: 4</span></div>
<div style="position:absolute;left:244.43px;top:430.05px" class="cls_123"><span class="cls_123">C0: 1</span></div>
<div style="position:absolute;left:303.88px;top:430.05px" class="cls_123"><span class="cls_123">C0: 8</span></div>
<div style="position:absolute;left:366.83px;top:430.05px" class="cls_123"><span class="cls_123">C0: 1</span></div>
<div style="position:absolute;left:455.13px;top:430.05px" class="cls_123"><span class="cls_123">C0: 1</span></div>
<div style="position:absolute;left:521.58px;top:430.05px" class="cls_123"><span class="cls_123">C0: 1</span></div>
<div style="position:absolute;left:571.42px;top:430.05px" class="cls_123"><span class="cls_123">C0: 0</span></div>
<div style="position:absolute;left:638.73px;top:430.05px" class="cls_123"><span class="cls_123">C0: 0</span></div>
<div style="position:absolute;left:93.18px;top:444.66px" class="cls_123"><span class="cls_123">C1: 4</span></div>
<div style="position:absolute;left:138.64px;top:444.66px" class="cls_123"><span class="cls_123">C1: 6</span></div>
<div style="position:absolute;left:244.43px;top:444.66px" class="cls_123"><span class="cls_123">C1: 3</span></div>
<div style="position:absolute;left:303.88px;top:444.66px" class="cls_123"><span class="cls_123">C1: 0</span></div>
<div style="position:absolute;left:366.83px;top:444.66px" class="cls_123"><span class="cls_123">C1: 7</span></div>
<div style="position:absolute;left:455.13px;top:444.66px" class="cls_123"><span class="cls_123">C1: 0</span></div>
<div style="position:absolute;left:521.58px;top:444.66px" class="cls_123"><span class="cls_123">C1: 0</span></div>
<div style="position:absolute;left:571.42px;top:444.66px" class="cls_123"><span class="cls_123">C1: 1</span></div>
<div style="position:absolute;left:638.73px;top:444.66px" class="cls_123"><span class="cls_123">C1: 1</span></div>
<div style="position:absolute;left:163.20px;top:472.80px" class="cls_120"><span class="cls_120">Which test condition is the best?</span></div>
<div style="position:absolute;left:56.70px;top:507.60px" class="cls_006"><span class="cls_006">10/23/21</span></div>
<div style="position:absolute;left:248.06px;top:507.60px" class="cls_006"><span class="cls_006">Introduction to Data Mining, 2</span><span class="cls_007"><sup>nd</sup></span><span class="cls_006"> Edition</span></div>
<div style="position:absolute;left:649.95px;top:507.60px" class="cls_006"><span class="cls_006">27</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:14850px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background28.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:11.44px" class="cls_002"><span class="cls_002">How to determine the Best Split</span></div>
<div style="position:absolute;left:39.50px;top:92.48px" class="cls_008"><span class="cls_008">●</span><span class="cls_005"> Greedy approach:</span></div>
<div style="position:absolute;left:75.50px;top:133.52px" class="cls_009"><span class="cls_009">-</span><span class="cls_005"> Nodes with </span><span class="cls_125">purer</span><span class="cls_005"> class distribution are</span></div>
<div style="position:absolute;left:102.50px;top:167.60px" class="cls_005"><span class="cls_005">preferred</span></div>
<div style="position:absolute;left:39.50px;top:237.44px" class="cls_008"><span class="cls_008">●</span><span class="cls_005"> Need a measure of node impurity:</span></div>
<div style="position:absolute;left:183.51px;top:321.80px" class="cls_126"><span class="cls_126">C0: 5</span></div>
<div style="position:absolute;left:459.51px;top:321.80px" class="cls_126"><span class="cls_126">C0: 9</span></div>
<div style="position:absolute;left:183.51px;top:347.27px" class="cls_126"><span class="cls_126">C1: 5</span></div>
<div style="position:absolute;left:459.51px;top:347.27px" class="cls_126"><span class="cls_126">C1: 1</span></div>
<div style="position:absolute;left:115.20px;top:399.60px" class="cls_120"><span class="cls_120">High degree of impurity</span></div>
<div style="position:absolute;left:415.20px;top:399.60px" class="cls_120"><span class="cls_120">Low degree of impurity</span></div>
<div style="position:absolute;left:56.70px;top:507.60px" class="cls_006"><span class="cls_006">10/23/21</span></div>
<div style="position:absolute;left:248.06px;top:507.60px" class="cls_006"><span class="cls_006">Introduction to Data Mining, 2</span><span class="cls_007"><sup>nd</sup></span><span class="cls_006"> Edition</span></div>
<div style="position:absolute;left:649.95px;top:507.60px" class="cls_006"><span class="cls_006">28</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:15400px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background29.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:11.44px" class="cls_002"><span class="cls_002">Measures of Node Impurity</span></div>
<div style="position:absolute;left:39.50px;top:92.48px" class="cls_008"><span class="cls_008">●</span><span class="cls_005"> Gini Index</span></div>
<div style="position:absolute;left:500.59px;top:127.46px" class="cls_131"><span class="cls_131">2</span></div>
<div style="position:absolute;left:225.33px;top:128.33px" class="cls_130"><span class="cls_130">GINI</span><span class="cls_132">(</span><span class="cls_130">t</span><span class="cls_132">)</span></div>
<div style="position:absolute;left:319.98px;top:128.33px" class="cls_128"><span class="cls_128">=</span><span class="cls_132">1</span><span class="cls_128">-</span></div>
<div style="position:absolute;left:405.72px;top:128.33px" class="cls_132"><span class="cls_132">[</span><span class="cls_130">p</span><span class="cls_132">(</span><span class="cls_130">j</span><span class="cls_132">|</span><span class="cls_130">t</span><span class="cls_132">)]</span></div>
<div style="position:absolute;left:374.46px;top:120.87px" class="cls_127"><span class="cls_127">å</span></div>
<div style="position:absolute;left:388.21px;top:166.53px" class="cls_129"><span class="cls_129">j</span></div>
<div style="position:absolute;left:39.50px;top:215.60px" class="cls_008"><span class="cls_008">●</span><span class="cls_005"> Entropy</span></div>
<div style="position:absolute;left:379.64px;top:247.67px" class="cls_133"><span class="cls_133">=</span><span class="cls_134"> </span><span class="cls_133">-</span><span class="cls_134">å</span></div>
<div style="position:absolute;left:225.39px;top:247.67px" class="cls_135"><span class="cls_135">Entropy</span><span class="cls_136">(</span><span class="cls_135">t</span><span class="cls_136">)</span></div>
<div style="position:absolute;left:453.66px;top:247.67px" class="cls_135"><span class="cls_135">p</span><span class="cls_136">(</span><span class="cls_135"> j</span><span class="cls_136"> |</span><span class="cls_135">t</span><span class="cls_136">)log</span><span class="cls_135"> p</span><span class="cls_136">(</span></div>
<div style="position:absolute;left:635.82px;top:247.67px" class="cls_135"><span class="cls_135">j</span></div>
<div style="position:absolute;left:652.41px;top:247.67px" class="cls_136"><span class="cls_136">|</span><span class="cls_135">t</span></div>
<div style="position:absolute;left:676.53px;top:247.67px" class="cls_136"><span class="cls_136">)</span></div>
<div style="position:absolute;left:434.75px;top:283.94px" class="cls_137"><span class="cls_137">j</span></div>
<div style="position:absolute;left:39.50px;top:338.48px" class="cls_008"><span class="cls_008">●</span><span class="cls_005"> Misclassification error</span></div>
<div style="position:absolute;left:225.47px;top:397.52px" class="cls_138"><span class="cls_138">Error</span><span class="cls_139">(</span><span class="cls_138">t</span><span class="cls_139">)</span></div>
<div style="position:absolute;left:351.25px;top:397.52px" class="cls_140"><span class="cls_140">=</span></div>
<div style="position:absolute;left:374.84px;top:397.52px" class="cls_139"><span class="cls_139">1</span></div>
<div style="position:absolute;left:395.14px;top:397.52px" class="cls_140"><span class="cls_140">-</span></div>
<div style="position:absolute;left:420.37px;top:397.52px" class="cls_139"><span class="cls_139">max</span><span class="cls_138"> P</span><span class="cls_139">(</span><span class="cls_138">i</span></div>
<div style="position:absolute;left:537.32px;top:397.52px" class="cls_139"><span class="cls_139">|</span><span class="cls_138">t</span></div>
<div style="position:absolute;left:562.05px;top:397.52px" class="cls_139"><span class="cls_139">)</span></div>
<div style="position:absolute;left:448.72px;top:433.05px" class="cls_141"><span class="cls_141">i</span></div>
<div style="position:absolute;left:56.70px;top:507.60px" class="cls_006"><span class="cls_006">10/23/21</span></div>
<div style="position:absolute;left:248.06px;top:507.60px" class="cls_006"><span class="cls_006">Introduction to Data Mining, 2</span><span class="cls_007"><sup>nd</sup></span><span class="cls_006"> Edition</span></div>
<div style="position:absolute;left:649.95px;top:507.60px" class="cls_006"><span class="cls_006">29</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:15950px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background30.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:11.44px" class="cls_002"><span class="cls_002">Finding the Best Split</span></div>
<div style="position:absolute;left:39.50px;top:99.48px" class="cls_008"><span class="cls_008">1.</span></div>
<div style="position:absolute;left:81.50px;top:92.48px" class="cls_005"><span class="cls_005">Compute impurity measure (P) before splitting</span></div>
<div style="position:absolute;left:39.50px;top:140.52px" class="cls_008"><span class="cls_008">2.</span></div>
<div style="position:absolute;left:81.50px;top:133.52px" class="cls_005"><span class="cls_005">Compute impurity measure (M) after splitting</span></div>
<div style="position:absolute;left:111.50px;top:174.48px" class="cls_012"><span class="cls_012">●</span><span class="cls_014">   Compute impurity measure of each child node</span></div>
<div style="position:absolute;left:111.50px;top:209.52px" class="cls_012"><span class="cls_012">●</span><span class="cls_014">   M is the weighted impurity of children</span></div>
<div style="position:absolute;left:39.50px;top:253.56px" class="cls_008"><span class="cls_008">3.</span></div>
<div style="position:absolute;left:81.50px;top:246.56px" class="cls_005"><span class="cls_005">Choose the attribute test condition that</span></div>
<div style="position:absolute;left:81.50px;top:279.44px" class="cls_005"><span class="cls_005">produces the highest gain</span></div>
<div style="position:absolute;left:193.20px;top:326.64px" class="cls_089"><span class="cls_089">Gain = P - M</span></div>
<div style="position:absolute;left:81.50px;top:380.48px" class="cls_005"><span class="cls_005">or equivalently, lowest impurity measure after</span></div>
<div style="position:absolute;left:81.50px;top:414.56px" class="cls_005"><span class="cls_005">splitting (M)</span></div>
<div style="position:absolute;left:56.70px;top:507.60px" class="cls_006"><span class="cls_006">10/23/21</span></div>
<div style="position:absolute;left:248.06px;top:507.60px" class="cls_006"><span class="cls_006">Introduction to Data Mining, 2</span><span class="cls_007"><sup>nd</sup></span><span class="cls_006"> Edition</span></div>
<div style="position:absolute;left:649.95px;top:507.60px" class="cls_006"><span class="cls_006">30</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:16500px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background31.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:11.44px" class="cls_002"><span class="cls_002">Finding the Best Split</span></div>
<div style="position:absolute;left:336.47px;top:84.08px" class="cls_142"><span class="cls_142">C0</span></div>
<div style="position:absolute;left:388.41px;top:84.08px" class="cls_143"><span class="cls_143">N00</span></div>
<div style="position:absolute;left:157.20px;top:87.60px" class="cls_120"><span class="cls_120">Before Splitting:</span></div>
<div style="position:absolute;left:505.20px;top:86.56px" class="cls_085"><span class="cls_085">P</span></div>
<div style="position:absolute;left:336.47px;top:105.62px" class="cls_142"><span class="cls_142">C1</span></div>
<div style="position:absolute;left:388.41px;top:105.62px" class="cls_143"><span class="cls_143">N01</span></div>
<div style="position:absolute;left:142.06px;top:142.96px" class="cls_145"><span class="cls_145">A?</span></div>
<div style="position:absolute;left:538.63px;top:148.96px" class="cls_145"><span class="cls_145">B?</span></div>
<div style="position:absolute;left:55.40px;top:185.76px" class="cls_146"><span class="cls_146">Yes</span></div>
<div style="position:absolute;left:250.50px;top:185.76px" class="cls_146"><span class="cls_146">No</span></div>
<div style="position:absolute;left:451.40px;top:191.76px" class="cls_146"><span class="cls_146">Yes</span></div>
<div style="position:absolute;left:646.50px;top:191.76px" class="cls_146"><span class="cls_146">No</span></div>
<div style="position:absolute;left:40.13px;top:232.80px" class="cls_146"><span class="cls_146">Node N1</span></div>
<div style="position:absolute;left:212.38px;top:232.80px" class="cls_146"><span class="cls_146">Node N2</span></div>
<div style="position:absolute;left:436.13px;top:238.80px" class="cls_146"><span class="cls_146">Node N3</span></div>
<div style="position:absolute;left:608.38px;top:238.80px" class="cls_146"><span class="cls_146">Node N4</span></div>
<div style="position:absolute;left:31.98px;top:282.18px" class="cls_147"><span class="cls_147">C0</span></div>
<div style="position:absolute;left:86.31px;top:282.18px" class="cls_148"><span class="cls_148">N10</span></div>
<div style="position:absolute;left:211.54px;top:282.48px" class="cls_150"><span class="cls_150">C0</span></div>
<div style="position:absolute;left:264.94px;top:282.48px" class="cls_151"><span class="cls_151">N20</span></div>
<div style="position:absolute;left:427.42px;top:282.52px" class="cls_153"><span class="cls_153">C0</span></div>
<div style="position:absolute;left:480.58px;top:282.52px" class="cls_154"><span class="cls_154">N30</span></div>
<div style="position:absolute;left:606.73px;top:282.49px" class="cls_156"><span class="cls_156">C0</span></div>
<div style="position:absolute;left:658.42px;top:282.49px" class="cls_157"><span class="cls_157">N40</span></div>
<div style="position:absolute;left:31.98px;top:304.96px" class="cls_147"><span class="cls_147">C1</span></div>
<div style="position:absolute;left:86.31px;top:304.96px" class="cls_148"><span class="cls_148">N11</span></div>
<div style="position:absolute;left:211.54px;top:304.70px" class="cls_150"><span class="cls_150">C1</span></div>
<div style="position:absolute;left:264.94px;top:304.70px" class="cls_151"><span class="cls_151">N21</span></div>
<div style="position:absolute;left:427.42px;top:304.73px" class="cls_153"><span class="cls_153">C1</span></div>
<div style="position:absolute;left:480.58px;top:304.73px" class="cls_154"><span class="cls_154">N31</span></div>
<div style="position:absolute;left:606.73px;top:304.03px" class="cls_156"><span class="cls_156">C1</span></div>
<div style="position:absolute;left:658.42px;top:304.03px" class="cls_157"><span class="cls_157">N41</span></div>
<div style="position:absolute;left:55.20px;top:380.56px" class="cls_085"><span class="cls_085">M11</span></div>
<div style="position:absolute;left:235.20px;top:379.36px" class="cls_085"><span class="cls_085">M12</span></div>
<div style="position:absolute;left:457.20px;top:379.36px" class="cls_085"><span class="cls_085">M21</span></div>
<div style="position:absolute;left:631.20px;top:379.36px" class="cls_085"><span class="cls_085">M22</span></div>
<div style="position:absolute;left:139.20px;top:445.36px" class="cls_085"><span class="cls_085">M1</span></div>
<div style="position:absolute;left:547.20px;top:446.56px" class="cls_085"><span class="cls_085">M2</span></div>
<div style="position:absolute;left:229.20px;top:469.36px" class="cls_085"><span class="cls_085">Gain = P - M1    vs      P - M2</span></div>
<div style="position:absolute;left:56.70px;top:507.60px" class="cls_006"><span class="cls_006">10/23/21</span></div>
<div style="position:absolute;left:248.06px;top:507.60px" class="cls_006"><span class="cls_006">Introduction to Data Mining, 2</span><span class="cls_007"><sup>nd</sup></span><span class="cls_006"> Edition</span></div>
<div style="position:absolute;left:649.95px;top:507.60px" class="cls_006"><span class="cls_006">31</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:17050px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background32.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:11.44px" class="cls_002"><span class="cls_002">Measure of Impurity: GINI</span></div>
<div style="position:absolute;left:39.50px;top:89.52px" class="cls_103"><span class="cls_103">●</span><span class="cls_014"> Gini Index for a given node t :</span></div>
<div style="position:absolute;left:466.16px;top:141.27px" class="cls_164"><span class="cls_164">2</span></div>
<div style="position:absolute;left:218.99px;top:142.03px" class="cls_163"><span class="cls_163">GINI</span><span class="cls_165">(</span><span class="cls_163">t</span><span class="cls_165">)</span></div>
<div style="position:absolute;left:303.98px;top:142.03px" class="cls_160"><span class="cls_160">=</span><span class="cls_165">1</span><span class="cls_160">-</span></div>
<div style="position:absolute;left:380.98px;top:142.03px" class="cls_165"><span class="cls_165">[</span><span class="cls_163">p</span><span class="cls_165">(</span><span class="cls_163">j</span><span class="cls_165">|</span><span class="cls_163">t</span><span class="cls_165">)]</span></div>
<div style="position:absolute;left:352.91px;top:135.33px" class="cls_159"><span class="cls_159">å</span></div>
<div style="position:absolute;left:365.25px;top:176.20px" class="cls_161"><span class="cls_161">j</span></div>
<div style="position:absolute;left:111.50px;top:213.52px" class="cls_044"><span class="cls_044">(NOTE: </span><span class="cls_162">p( j | t) </span><span class="cls_044">is the relative frequency of class j at node t).</span></div>
<div style="position:absolute;left:75.50px;top:255.60px" class="cls_105"><span class="cls_105">-</span><span class="cls_014"> Maximum (1 - 1/n</span><span class="cls_034"><sub>c</sub></span><span class="cls_014">) when records are equally</span></div>
<div style="position:absolute;left:102.50px;top:281.52px" class="cls_014"><span class="cls_014">distributed among all classes, implying least</span></div>
<div style="position:absolute;left:102.50px;top:307.44px" class="cls_014"><span class="cls_014">interesting information</span></div>
<div style="position:absolute;left:75.50px;top:340.56px" class="cls_105"><span class="cls_105">-</span><span class="cls_014"> Minimum (0.0) when all records belong to one class,</span></div>
<div style="position:absolute;left:102.50px;top:366.48px" class="cls_014"><span class="cls_014">implying most interesting information</span></div>
<div style="position:absolute;left:56.70px;top:507.60px" class="cls_006"><span class="cls_006">10/23/21</span></div>
<div style="position:absolute;left:248.06px;top:507.60px" class="cls_006"><span class="cls_006">Introduction to Data Mining, 2</span><span class="cls_007"><sup>nd</sup></span><span class="cls_006"> Edition</span></div>
<div style="position:absolute;left:649.95px;top:507.60px" class="cls_006"><span class="cls_006">32</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:17600px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background33.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:11.44px" class="cls_002"><span class="cls_002">Measure of Impurity: GINI</span></div>
<div style="position:absolute;left:39.50px;top:89.52px" class="cls_103"><span class="cls_103">●</span><span class="cls_014"> Gini Index for a given node t :</span></div>
<div style="position:absolute;left:466.16px;top:141.27px" class="cls_164"><span class="cls_164">2</span></div>
<div style="position:absolute;left:218.99px;top:142.03px" class="cls_163"><span class="cls_163">GINI</span><span class="cls_165">(</span><span class="cls_163">t</span><span class="cls_165">)</span></div>
<div style="position:absolute;left:303.98px;top:142.03px" class="cls_160"><span class="cls_160">=</span><span class="cls_165">1</span><span class="cls_160">-</span></div>
<div style="position:absolute;left:380.98px;top:142.03px" class="cls_165"><span class="cls_165">[</span><span class="cls_163">p</span><span class="cls_165">(</span><span class="cls_163">j</span><span class="cls_165">|</span><span class="cls_163">t</span><span class="cls_165">)]</span></div>
<div style="position:absolute;left:352.91px;top:135.33px" class="cls_159"><span class="cls_159">å</span></div>
<div style="position:absolute;left:365.25px;top:176.20px" class="cls_161"><span class="cls_161">j</span></div>
<div style="position:absolute;left:111.50px;top:213.52px" class="cls_044"><span class="cls_044">(NOTE: </span><span class="cls_162">p( j | t) </span><span class="cls_044">is the relative frequency of class j at node t).</span></div>
<div style="position:absolute;left:75.50px;top:255.60px" class="cls_105"><span class="cls_105">-</span><span class="cls_014"> For 2-class problem (p, 1 - p):</span></div>
<div style="position:absolute;left:111.50px;top:288.40px" class="cls_166"><span class="cls_166">u</span><span class="cls_044"> GINI = 1 - p</span><span class="cls_081"><sup>2</sup></span><span class="cls_044"> - (1 - p)</span><span class="cls_081"><sup>2</sup></span><span class="cls_044"> = 2p (1-p)</span></div>
<div style="position:absolute;left:123.36px;top:360.08px" class="cls_167"><span class="cls_167">C1</span></div>
<div style="position:absolute;left:178.11px;top:360.08px" class="cls_168"><span class="cls_168">0</span></div>
<div style="position:absolute;left:255.36px;top:360.08px" class="cls_167"><span class="cls_167">C1</span></div>
<div style="position:absolute;left:310.11px;top:360.08px" class="cls_168"><span class="cls_168">1</span></div>
<div style="position:absolute;left:381.36px;top:360.08px" class="cls_167"><span class="cls_167">C1</span></div>
<div style="position:absolute;left:436.11px;top:360.08px" class="cls_168"><span class="cls_168">2</span></div>
<div style="position:absolute;left:513.36px;top:360.08px" class="cls_167"><span class="cls_167">C1</span></div>
<div style="position:absolute;left:568.11px;top:360.08px" class="cls_168"><span class="cls_168">3</span></div>
<div style="position:absolute;left:123.36px;top:378.74px" class="cls_167"><span class="cls_167">C2</span></div>
<div style="position:absolute;left:178.11px;top:378.74px" class="cls_168"><span class="cls_168">6</span></div>
<div style="position:absolute;left:255.36px;top:378.74px" class="cls_167"><span class="cls_167">C2</span></div>
<div style="position:absolute;left:310.11px;top:378.74px" class="cls_168"><span class="cls_168">5</span></div>
<div style="position:absolute;left:381.36px;top:378.74px" class="cls_167"><span class="cls_167">C2</span></div>
<div style="position:absolute;left:436.11px;top:378.74px" class="cls_168"><span class="cls_168">4</span></div>
<div style="position:absolute;left:513.36px;top:378.74px" class="cls_167"><span class="cls_167">C2</span></div>
<div style="position:absolute;left:568.11px;top:378.74px" class="cls_168"><span class="cls_168">3</span></div>
<div style="position:absolute;left:118.50px;top:398.54px" class="cls_168"><span class="cls_168">Gini=0.000</span></div>
<div style="position:absolute;left:250.50px;top:398.54px" class="cls_168"><span class="cls_168">Gini=0.278</span></div>
<div style="position:absolute;left:376.50px;top:398.54px" class="cls_168"><span class="cls_168">Gini=0.444</span></div>
<div style="position:absolute;left:508.50px;top:398.54px" class="cls_168"><span class="cls_168">Gini=0.500</span></div>
<div style="position:absolute;left:56.70px;top:507.60px" class="cls_006"><span class="cls_006">10/23/21</span></div>
<div style="position:absolute;left:248.06px;top:507.60px" class="cls_006"><span class="cls_006">Introduction to Data Mining, 2</span><span class="cls_007"><sup>nd</sup></span><span class="cls_006"> Edition</span></div>
<div style="position:absolute;left:649.95px;top:507.60px" class="cls_006"><span class="cls_006">33</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:18150px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background34.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:11.44px" class="cls_002"><span class="cls_002">Computing Gini Index of a Single Node</span></div>
<div style="position:absolute;left:454.16px;top:97.27px" class="cls_164"><span class="cls_164">2</span></div>
<div style="position:absolute;left:206.99px;top:98.03px" class="cls_163"><span class="cls_163">GINI</span><span class="cls_165">(</span><span class="cls_163">t</span><span class="cls_165">)</span></div>
<div style="position:absolute;left:291.98px;top:98.03px" class="cls_160"><span class="cls_160">=</span><span class="cls_165">1</span><span class="cls_160">-</span></div>
<div style="position:absolute;left:368.98px;top:98.03px" class="cls_165"><span class="cls_165">[</span><span class="cls_163">p</span><span class="cls_165">(</span><span class="cls_163">j</span><span class="cls_165">|</span><span class="cls_163">t</span><span class="cls_165">)]</span></div>
<div style="position:absolute;left:340.91px;top:91.33px" class="cls_159"><span class="cls_159">å</span></div>
<div style="position:absolute;left:353.25px;top:132.20px" class="cls_161"><span class="cls_161">j</span></div>
<div style="position:absolute;left:73.31px;top:183.87px" class="cls_169"><span class="cls_169">C1</span></div>
<div style="position:absolute;left:168.93px;top:183.87px" class="cls_170"><span class="cls_170">0</span></div>
<div style="position:absolute;left:247.20px;top:186.88px" class="cls_085"><span class="cls_085">P(C1) = 0/6 = 0     P(C2) = 6/6 = 1</span></div>
<div style="position:absolute;left:73.31px;top:215.28px" class="cls_169"><span class="cls_169">C2</span></div>
<div style="position:absolute;left:168.93px;top:215.28px" class="cls_170"><span class="cls_170">6</span></div>
<div style="position:absolute;left:247.20px;top:222.88px" class="cls_085"><span class="cls_085">Gini = 1 - P(C1)</span><span class="cls_086"><sup>2 </sup></span><span class="cls_085">- P(C2)</span><span class="cls_086"><sup>2</sup></span><span class="cls_085"> = 1 - 0 - 1 = 0</span></div>
<div style="position:absolute;left:78.10px;top:300.26px" class="cls_172"><span class="cls_172">C1</span></div>
<div style="position:absolute;left:170.64px;top:300.26px" class="cls_173"><span class="cls_173">1</span></div>
<div style="position:absolute;left:253.20px;top:303.28px" class="cls_085"><span class="cls_085">P(C1) = 1/6</span></div>
<div style="position:absolute;left:410.72px;top:303.28px" class="cls_085"><span class="cls_085">P(C2) = 5/6</span></div>
<div style="position:absolute;left:78.10px;top:330.65px" class="cls_172"><span class="cls_172">C2</span></div>
<div style="position:absolute;left:170.64px;top:330.65px" class="cls_173"><span class="cls_173">5</span></div>
<div style="position:absolute;left:253.20px;top:339.28px" class="cls_085"><span class="cls_085">Gini = 1 - (1/6)</span><span class="cls_086"><sup>2 </sup></span><span class="cls_085">- (5/6)</span><span class="cls_086"><sup>2</sup></span><span class="cls_085"> = 0.278</span></div>
<div style="position:absolute;left:253.20px;top:404.56px" class="cls_085"><span class="cls_085">P(C1) = 2/6</span></div>
<div style="position:absolute;left:410.72px;top:404.56px" class="cls_085"><span class="cls_085">P(C2) = 4/6</span></div>
<div style="position:absolute;left:78.10px;top:407.81px" class="cls_175"><span class="cls_175">C1</span></div>
<div style="position:absolute;left:170.64px;top:407.81px" class="cls_176"><span class="cls_176">2</span></div>
<div style="position:absolute;left:78.10px;top:438.72px" class="cls_175"><span class="cls_175">C2</span></div>
<div style="position:absolute;left:170.64px;top:438.72px" class="cls_176"><span class="cls_176">4</span></div>
<div style="position:absolute;left:253.20px;top:440.56px" class="cls_085"><span class="cls_085">Gini = 1 - (2/6)</span><span class="cls_086"><sup>2 </sup></span><span class="cls_085">- (4/6)</span><span class="cls_086"><sup>2</sup></span><span class="cls_085"> = 0.444</span></div>
<div style="position:absolute;left:56.70px;top:507.60px" class="cls_006"><span class="cls_006">10/23/21</span></div>
<div style="position:absolute;left:248.06px;top:507.60px" class="cls_006"><span class="cls_006">Introduction to Data Mining, 2</span><span class="cls_007"><sup>nd</sup></span><span class="cls_006"> Edition</span></div>
<div style="position:absolute;left:649.95px;top:507.60px" class="cls_006"><span class="cls_006">34</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:18700px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background35.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:0.00px" class="cls_002"><span class="cls_002">Computing Gini Index for a Collection of</span></div>
<div style="position:absolute;left:37.13px;top:29.44px" class="cls_002"><span class="cls_002">Nodes</span></div>
<div style="position:absolute;left:37.13px;top:92.40px" class="cls_103"><span class="cls_103">●</span><span class="cls_014"> When a node p is split into k partitions (children)</span></div>
<div style="position:absolute;left:351.73px;top:134.04px" class="cls_178"><span class="cls_178">k</span></div>
<div style="position:absolute;left:379.62px;top:132.97px" class="cls_183"><span class="cls_183">n</span></div>
<div style="position:absolute;left:394.42px;top:153.49px" class="cls_178"><span class="cls_178">i</span></div>
<div style="position:absolute;left:207.70px;top:152.41px" class="cls_183"><span class="cls_183">GINI</span></div>
<div style="position:absolute;left:316.05px;top:152.41px" class="cls_181"><span class="cls_181">=</span></div>
<div style="position:absolute;left:408.96px;top:152.41px" class="cls_183"><span class="cls_183">GINI</span><span class="cls_184">(</span><span class="cls_183">i</span><span class="cls_184">)</span></div>
<div style="position:absolute;left:273.53px;top:172.94px" class="cls_178"><span class="cls_178">split</span></div>
<div style="position:absolute;left:339.92px;top:144.13px" class="cls_179"><span class="cls_179">å</span></div>
<div style="position:absolute;left:383.56px;top:176.43px" class="cls_183"><span class="cls_183">n</span></div>
<div style="position:absolute;left:344.74px;top:194.62px" class="cls_178"><span class="cls_178">i</span></div>
<div style="position:absolute;left:351.17px;top:194.62px" class="cls_182"><span class="cls_182">=</span><span class="cls_180">1</span></div>
<div style="position:absolute;left:64.13px;top:235.44px" class="cls_014"><span class="cls_014">where,</span></div>
<div style="position:absolute;left:181.13px;top:235.44px" class="cls_014"><span class="cls_014">n</span><span class="cls_034"><sub>i</sub></span><span class="cls_014"> = number of records at child i,</span></div>
<div style="position:absolute;left:181.13px;top:271.44px" class="cls_014"><span class="cls_014">n = number of records at parent node p.</span></div>
<div style="position:absolute;left:37.13px;top:342.48px" class="cls_103"><span class="cls_103">●</span><span class="cls_014"> Choose the attribute that minimizes weighted average</span></div>
<div style="position:absolute;left:64.13px;top:371.52px" class="cls_014"><span class="cls_014">Gini index of the children</span></div>
<div style="position:absolute;left:37.13px;top:433.44px" class="cls_103"><span class="cls_103">●</span><span class="cls_014"> Gini index is used in decision tree algorithms such as</span></div>
<div style="position:absolute;left:64.13px;top:461.52px" class="cls_014"><span class="cls_014">CART, SLIQ, SPRINT</span></div>
<div style="position:absolute;left:56.70px;top:507.60px" class="cls_006"><span class="cls_006">10/23/21</span></div>
<div style="position:absolute;left:248.06px;top:507.60px" class="cls_006"><span class="cls_006">Introduction to Data Mining, 2</span><span class="cls_007"><sup>nd</sup></span><span class="cls_006"> Edition</span></div>
<div style="position:absolute;left:649.95px;top:507.60px" class="cls_006"><span class="cls_006">35</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:19250px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background36.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.13px;top:16.64px" class="cls_185"><span class="cls_185">Binary Attributes: Computing GINI Index</span></div>
<div style="position:absolute;left:31.20px;top:92.64px" class="cls_103"><span class="cls_103">●</span><span class="cls_014"> Splits into two partitions</span></div>
<div style="position:absolute;left:31.20px;top:128.64px" class="cls_103"><span class="cls_103">●</span><span class="cls_014"> Effect of Weighing partitions:</span></div>
<div style="position:absolute;left:67.20px;top:164.64px" class="cls_105"><span class="cls_105">-</span><span class="cls_014"> Larger and Purer Partitions are sought for.</span></div>
<div style="position:absolute;left:588.41px;top:207.96px" class="cls_187"><span class="cls_187">Parent</span></div>
<div style="position:absolute;left:316.63px;top:230.32px" class="cls_145"><span class="cls_145">B?</span></div>
<div style="position:absolute;left:537.95px;top:239.55px" class="cls_186"><span class="cls_186">C1</span></div>
<div style="position:absolute;left:615.31px;top:239.55px" class="cls_187"><span class="cls_187">7</span></div>
<div style="position:absolute;left:229.40px;top:273.12px" class="cls_146"><span class="cls_146">Yes</span></div>
<div style="position:absolute;left:424.50px;top:273.12px" class="cls_146"><span class="cls_146">No</span></div>
<div style="position:absolute;left:537.95px;top:272.10px" class="cls_186"><span class="cls_186">C2</span></div>
<div style="position:absolute;left:615.31px;top:272.10px" class="cls_187"><span class="cls_187">5</span></div>
<div style="position:absolute;left:530.61px;top:303.94px" class="cls_187"><span class="cls_187">Gini = 0.486</span></div>
<div style="position:absolute;left:214.13px;top:320.16px" class="cls_146"><span class="cls_146">Node N1</span></div>
<div style="position:absolute;left:386.38px;top:320.16px" class="cls_146"><span class="cls_146">Node N2</span></div>
<div style="position:absolute;left:37.20px;top:332.56px" class="cls_085"><span class="cls_085">Gini(N1)</span></div>
<div style="position:absolute;left:37.20px;top:356.56px" class="cls_085"><span class="cls_085">= 1 - (5/6)</span><span class="cls_086"><sup>2 </sup></span><span class="cls_085">- (1/6)</span><span class="cls_086"><sup>2</sup></span></div>
<div style="position:absolute;left:324.25px;top:367.92px" class="cls_190"><span class="cls_190">N1</span></div>
<div style="position:absolute;left:370.19px;top:367.92px" class="cls_190"><span class="cls_190">N2</span></div>
<div style="position:absolute;left:463.20px;top:373.44px" class="cls_120"><span class="cls_120">Weighted Gini of N1 N2</span></div>
<div style="position:absolute;left:37.20px;top:380.56px" class="cls_085"><span class="cls_085">= 0.278</span></div>
<div style="position:absolute;left:277.33px;top:394.20px" class="cls_189"><span class="cls_189">C1</span></div>
<div style="position:absolute;left:331.46px;top:394.20px" class="cls_190"><span class="cls_190">5</span></div>
<div style="position:absolute;left:377.40px;top:394.20px" class="cls_190"><span class="cls_190">2</span></div>
<div style="position:absolute;left:463.20px;top:394.32px" class="cls_120"><span class="cls_120">= 6/12 * 0.278 +</span></div>
<div style="position:absolute;left:37.20px;top:416.56px" class="cls_085"><span class="cls_085">Gini(N2)</span></div>
<div style="position:absolute;left:478.20px;top:416.40px" class="cls_120"><span class="cls_120">6/12 * 0.444</span></div>
<div style="position:absolute;left:277.33px;top:421.31px" class="cls_189"><span class="cls_189">C2</span></div>
<div style="position:absolute;left:331.46px;top:421.31px" class="cls_190"><span class="cls_190">1</span></div>
<div style="position:absolute;left:377.40px;top:421.31px" class="cls_190"><span class="cls_190">4</span></div>
<div style="position:absolute;left:463.20px;top:437.28px" class="cls_120"><span class="cls_120">= 0.361</span></div>
<div style="position:absolute;left:37.20px;top:440.56px" class="cls_085"><span class="cls_085">= 1 - (2/6)</span><span class="cls_086"><sup>2 </sup></span><span class="cls_085">- (4/6)</span><span class="cls_086"><sup>2</sup></span></div>
<div style="position:absolute;left:281.04px;top:449.19px" class="cls_190"><span class="cls_190">Gini=0.361</span></div>
<div style="position:absolute;left:37.20px;top:464.56px" class="cls_085"><span class="cls_085">= 0.444</span></div>
<div style="position:absolute;left:444.33px;top:465.60px" class="cls_192"><span class="cls_192">Gain = 0.486 - 0.361 = 0.125</span></div>
<div style="position:absolute;left:56.70px;top:507.60px" class="cls_006"><span class="cls_006">10/23/21</span></div>
<div style="position:absolute;left:248.06px;top:507.60px" class="cls_006"><span class="cls_006">Introduction to Data Mining, 2</span><span class="cls_007"><sup>nd</sup></span><span class="cls_006"> Edition</span></div>
<div style="position:absolute;left:649.95px;top:507.60px" class="cls_006"><span class="cls_006">36</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:19800px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background37.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:16.64px" class="cls_185"><span class="cls_185">Categorical Attributes: Computing Gini Index</span></div>
<div style="position:absolute;left:39.50px;top:92.40px" class="cls_103"><span class="cls_103">●</span><span class="cls_014"> For each distinct value, gather counts for each class in</span></div>
<div style="position:absolute;left:62.50px;top:121.44px" class="cls_014"><span class="cls_014">the dataset</span></div>
<div style="position:absolute;left:39.50px;top:157.44px" class="cls_103"><span class="cls_103">●</span><span class="cls_014"> Use the count matrix to make decisions</span></div>
<div style="position:absolute;left:79.33px;top:228.40px" class="cls_145"><span class="cls_145">Multi-way split</span></div>
<div style="position:absolute;left:435.39px;top:228.40px" class="cls_145"><span class="cls_145">Two-way split</span></div>
<div style="position:absolute;left:378.88px;top:252.40px" class="cls_145"><span class="cls_145">(find best partition of values)</span></div>
<div style="position:absolute;left:140.88px;top:302.21px" class="cls_205"><span class="cls_205">CarType</span></div>
<div style="position:absolute;left:394.51px;top:301.95px" class="cls_194"><span class="cls_194">CarType</span></div>
<div style="position:absolute;left:593.76px;top:301.58px" class="cls_202"><span class="cls_202">CarType</span></div>
<div style="position:absolute;left:365.08px;top:320.97px" class="cls_195"><span class="cls_195">{Sports,</span></div>
<div style="position:absolute;left:628.46px;top:320.59px" class="cls_203"><span class="cls_203">{Family,</span></div>
<div style="position:absolute;left:88.61px;top:326.08px" class="cls_196"><span class="cls_196">Family</span></div>
<div style="position:absolute;left:149.23px;top:326.08px" class="cls_196"><span class="cls_196">Sports</span></div>
<div style="position:absolute;left:206.78px;top:326.08px" class="cls_196"><span class="cls_196">Luxury</span></div>
<div style="position:absolute;left:428.42px;top:329.08px" class="cls_195"><span class="cls_195">{Family}</span></div>
<div style="position:absolute;left:563.54px;top:328.70px" class="cls_203"><span class="cls_203">{Sports}</span></div>
<div style="position:absolute;left:365.87px;top:337.13px" class="cls_195"><span class="cls_195">Luxury}</span></div>
<div style="position:absolute;left:629.25px;top:336.76px" class="cls_203"><span class="cls_203">Luxury}</span></div>
<div style="position:absolute;left:44.95px;top:349.34px" class="cls_196"><span class="cls_196">C1</span></div>
<div style="position:absolute;left:109.15px;top:349.93px" class="cls_197"><span class="cls_197">1</span></div>
<div style="position:absolute;left:169.82px;top:349.93px" class="cls_197"><span class="cls_197">8</span></div>
<div style="position:absolute;left:228.67px;top:349.93px" class="cls_197"><span class="cls_197">1</span></div>
<div style="position:absolute;left:325.12px;top:355.89px" class="cls_195"><span class="cls_195">C1</span></div>
<div style="position:absolute;left:388.15px;top:356.36px" class="cls_193"><span class="cls_193">9</span></div>
<div style="position:absolute;left:452.33px;top:356.36px" class="cls_193"><span class="cls_193">1</span></div>
<div style="position:absolute;left:524.37px;top:355.52px" class="cls_203"><span class="cls_203">C1</span></div>
<div style="position:absolute;left:587.40px;top:355.99px" class="cls_201"><span class="cls_201">8</span></div>
<div style="position:absolute;left:651.58px;top:355.99px" class="cls_201"><span class="cls_201">2</span></div>
<div style="position:absolute;left:44.95px;top:371.84px" class="cls_196"><span class="cls_196">C2</span></div>
<div style="position:absolute;left:109.15px;top:372.37px" class="cls_197"><span class="cls_197">3</span></div>
<div style="position:absolute;left:169.82px;top:372.37px" class="cls_197"><span class="cls_197">0</span></div>
<div style="position:absolute;left:228.67px;top:372.37px" class="cls_197"><span class="cls_197">7</span></div>
<div style="position:absolute;left:325.12px;top:376.43px" class="cls_195"><span class="cls_195">C2</span></div>
<div style="position:absolute;left:388.15px;top:376.90px" class="cls_193"><span class="cls_193">7</span></div>
<div style="position:absolute;left:452.33px;top:376.90px" class="cls_193"><span class="cls_193">3</span></div>
<div style="position:absolute;left:524.37px;top:376.05px" class="cls_203"><span class="cls_203">C2</span></div>
<div style="position:absolute;left:587.40px;top:376.52px" class="cls_201"><span class="cls_201">0</span></div>
<div style="position:absolute;left:647.73px;top:376.52px" class="cls_201"><span class="cls_201">10</span></div>
<div style="position:absolute;left:39.36px;top:395.53px" class="cls_198"><span class="cls_198">Gini</span></div>
<div style="position:absolute;left:149.82px;top:395.53px" class="cls_198"><span class="cls_198">0.163</span></div>
<div style="position:absolute;left:320.11px;top:397.66px" class="cls_199"><span class="cls_199">Gini</span></div>
<div style="position:absolute;left:402.51px;top:397.66px" class="cls_199"><span class="cls_199">0.468</span></div>
<div style="position:absolute;left:519.36px;top:397.28px" class="cls_204"><span class="cls_204">Gini</span></div>
<div style="position:absolute;left:601.76px;top:397.28px" class="cls_204"><span class="cls_204">0.167</span></div>
<div style="position:absolute;left:205.20px;top:454.56px" class="cls_208"><span class="cls_208">Which of these is the best?</span></div>
<div style="position:absolute;left:56.70px;top:507.60px" class="cls_006"><span class="cls_006">10/23/21</span></div>
<div style="position:absolute;left:248.06px;top:507.60px" class="cls_006"><span class="cls_006">Introduction to Data Mining, 2</span><span class="cls_007"><sup>nd</sup></span><span class="cls_006"> Edition</span></div>
<div style="position:absolute;left:649.95px;top:507.60px" class="cls_006"><span class="cls_006">37</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:20350px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background38.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:16.64px" class="cls_185"><span class="cls_185">Continuous Attributes: Computing Gini Index</span></div>
<div style="position:absolute;left:482.02px;top:94.84px" class="cls_213"><span class="cls_213">Home</span></div>
<div style="position:absolute;left:535.43px;top:94.84px" class="cls_213"><span class="cls_213">Marital</span></div>
<div style="position:absolute;left:592.07px;top:94.84px" class="cls_213"><span class="cls_213">Annual</span></div>
<div style="position:absolute;left:39.50px;top:90.40px" class="cls_080"><span class="cls_080">●</span><span class="cls_044"> Use Binary Decisions based on one</span></div>
<div style="position:absolute;left:452.52px;top:101.49px" class="cls_212"><span class="cls_212">ID</span></div>
<div style="position:absolute;left:645.63px;top:101.49px" class="cls_213"><span class="cls_213">Defaulted</span></div>
<div style="position:absolute;left:480.77px;top:108.28px" class="cls_213"><span class="cls_213">Owner</span></div>
<div style="position:absolute;left:536.45px;top:108.28px" class="cls_213"><span class="cls_213">Status</span></div>
<div style="position:absolute;left:591.41px;top:108.28px" class="cls_213"><span class="cls_213">Income</span></div>
<div style="position:absolute;left:62.50px;top:112.48px" class="cls_044"><span class="cls_044">value</span></div>
<div style="position:absolute;left:449.52px;top:130.63px" class="cls_209"><span class="cls_209">1</span></div>
<div style="position:absolute;left:477.33px;top:130.63px" class="cls_209"><span class="cls_209">Yes</span></div>
<div style="position:absolute;left:528.92px;top:130.63px" class="cls_209"><span class="cls_209">Single</span></div>
<div style="position:absolute;left:587.90px;top:130.63px" class="cls_210"><span class="cls_210">125K</span></div>
<div style="position:absolute;left:644.31px;top:130.70px" class="cls_211"><span class="cls_211">No</span></div>
<div style="position:absolute;left:39.50px;top:140.56px" class="cls_080"><span class="cls_080">●</span><span class="cls_044"> Several Choices for the splitting value</span></div>
<div style="position:absolute;left:449.52px;top:152.70px" class="cls_209"><span class="cls_209">2</span></div>
<div style="position:absolute;left:477.33px;top:152.70px" class="cls_209"><span class="cls_209">No</span></div>
<div style="position:absolute;left:528.92px;top:152.70px" class="cls_209"><span class="cls_209">Married</span></div>
<div style="position:absolute;left:587.90px;top:152.70px" class="cls_210"><span class="cls_210">100K</span></div>
<div style="position:absolute;left:644.31px;top:152.77px" class="cls_211"><span class="cls_211">No</span></div>
<div style="position:absolute;left:75.50px;top:168.40px" class="cls_084"><span class="cls_084">-</span><span class="cls_044"> Number of possible splitting values</span></div>
<div style="position:absolute;left:449.52px;top:174.83px" class="cls_209"><span class="cls_209">3</span></div>
<div style="position:absolute;left:477.33px;top:174.83px" class="cls_209"><span class="cls_209">No</span></div>
<div style="position:absolute;left:528.92px;top:174.83px" class="cls_209"><span class="cls_209">Single</span></div>
<div style="position:absolute;left:102.50px;top:189.52px" class="cls_044"><span class="cls_044">= Number of distinct values</span></div>
<div style="position:absolute;left:449.52px;top:196.89px" class="cls_209"><span class="cls_209">4</span></div>
<div style="position:absolute;left:477.33px;top:196.89px" class="cls_209"><span class="cls_209">Yes</span></div>
<div style="position:absolute;left:528.92px;top:196.89px" class="cls_209"><span class="cls_209">Married</span></div>
<div style="position:absolute;left:587.90px;top:196.89px" class="cls_210"><span class="cls_210">120K</span></div>
<div style="position:absolute;left:644.31px;top:196.96px" class="cls_211"><span class="cls_211">No</span></div>
<div style="position:absolute;left:449.52px;top:219.11px" class="cls_209"><span class="cls_209">5</span></div>
<div style="position:absolute;left:477.33px;top:219.11px" class="cls_209"><span class="cls_209">No</span></div>
<div style="position:absolute;left:528.92px;top:219.11px" class="cls_209"><span class="cls_209">Divorced</span></div>
<div style="position:absolute;left:39.50px;top:217.60px" class="cls_080"><span class="cls_080">●</span><span class="cls_044"> Each splitting value has a count matrix</span></div>
<div style="position:absolute;left:449.52px;top:241.17px" class="cls_209"><span class="cls_209">6</span></div>
<div style="position:absolute;left:477.33px;top:241.17px" class="cls_209"><span class="cls_209">No</span></div>
<div style="position:absolute;left:528.92px;top:241.17px" class="cls_209"><span class="cls_209">Married</span></div>
<div style="position:absolute;left:62.50px;top:239.44px" class="cls_044"><span class="cls_044">associated with it</span></div>
<div style="position:absolute;left:449.52px;top:263.23px" class="cls_209"><span class="cls_209">7</span></div>
<div style="position:absolute;left:477.33px;top:263.23px" class="cls_209"><span class="cls_209">Yes</span></div>
<div style="position:absolute;left:528.92px;top:263.23px" class="cls_209"><span class="cls_209">Divorced</span></div>
<div style="position:absolute;left:587.90px;top:263.23px" class="cls_210"><span class="cls_210">220K</span></div>
<div style="position:absolute;left:644.31px;top:263.30px" class="cls_211"><span class="cls_211">No</span></div>
<div style="position:absolute;left:75.50px;top:267.52px" class="cls_084"><span class="cls_084">-</span><span class="cls_044"> Class counts in each of the</span></div>
<div style="position:absolute;left:449.52px;top:285.29px" class="cls_209"><span class="cls_209">8</span></div>
<div style="position:absolute;left:477.33px;top:285.29px" class="cls_209"><span class="cls_209">No</span></div>
<div style="position:absolute;left:528.92px;top:285.29px" class="cls_209"><span class="cls_209">Single</span></div>
<div style="position:absolute;left:102.50px;top:288.40px" class="cls_044"><span class="cls_044">partitions, A &lt; v and A ³ v</span></div>
<div style="position:absolute;left:449.52px;top:307.43px" class="cls_209"><span class="cls_209">9</span></div>
<div style="position:absolute;left:477.33px;top:307.43px" class="cls_209"><span class="cls_209">No</span></div>
<div style="position:absolute;left:528.92px;top:307.43px" class="cls_209"><span class="cls_209">Married</span></div>
<div style="position:absolute;left:39.50px;top:316.48px" class="cls_080"><span class="cls_080">●</span><span class="cls_044"> Simple method to choose best v</span></div>
<div style="position:absolute;left:449.52px;top:329.56px" class="cls_209"><span class="cls_209">10</span></div>
<div style="position:absolute;left:477.33px;top:329.56px" class="cls_209"><span class="cls_209">No</span></div>
<div style="position:absolute;left:528.92px;top:329.56px" class="cls_209"><span class="cls_209">Single</span></div>
<div style="position:absolute;left:75.50px;top:344.56px" class="cls_084"><span class="cls_084">-</span><span class="cls_044"> For each v, scan the database to</span></div>
<div style="position:absolute;left:523.20px;top:357.52px" class="cls_079"><span class="cls_079">Annual Income ?</span></div>
<div style="position:absolute;left:102.50px;top:366.40px" class="cls_044"><span class="cls_044">gather count matrix and compute</span></div>
<div style="position:absolute;left:102.50px;top:388.48px" class="cls_044"><span class="cls_044">its Gini index</span></div>
<div style="position:absolute;left:537.38px;top:404.96px" class="cls_034"><span class="cls_034">≤ 80</span></div>
<div style="position:absolute;left:584.50px;top:404.96px" class="cls_034"><span class="cls_034">> 80</span></div>
<div style="position:absolute;left:75.50px;top:416.56px" class="cls_084"><span class="cls_084">–</span><span class="cls_044"> Computationally Inefficient!</span></div>
<div style="position:absolute;left:419.26px;top:434.24px" class="cls_034"><span class="cls_034">Defaulted Yes</span></div>
<div style="position:absolute;left:548.44px;top:434.24px" class="cls_034"><span class="cls_034">0</span></div>
<div style="position:absolute;left:595.88px;top:434.24px" class="cls_034"><span class="cls_034">3</span></div>
<div style="position:absolute;left:102.50px;top:437.44px" class="cls_044"><span class="cls_044">Repetition of work.</span></div>
<div style="position:absolute;left:421.94px;top:463.52px" class="cls_034"><span class="cls_034">Defaulted No</span></div>
<div style="position:absolute;left:548.44px;top:463.52px" class="cls_034"><span class="cls_034">3</span></div>
<div style="position:absolute;left:595.88px;top:463.52px" class="cls_034"><span class="cls_034">4</span></div>
<div style="position:absolute;left:56.70px;top:507.60px" class="cls_006"><span class="cls_006">10/23/21</span></div>
<div style="position:absolute;left:248.06px;top:507.60px" class="cls_006"><span class="cls_006">Introduction to Data Mining, 2</span><span class="cls_007"><sup>nd</sup></span><span class="cls_006"> Edition</span></div>
<div style="position:absolute;left:649.95px;top:507.60px" class="cls_006"><span class="cls_006">38</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:20900px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background39.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.13px;top:16.64px" class="cls_185"><span class="cls_185">Continuous Attributes: Computing Gini Index...</span></div>
<div style="position:absolute;left:37.13px;top:96.40px" class="cls_080"><span class="cls_080">●</span><span class="cls_044">  For efficient computation: for each attribute,</span></div>
<div style="position:absolute;left:73.13px;top:124.48px" class="cls_084"><span class="cls_084">-</span><span class="cls_044"> Sort the attribute on values</span></div>
<div style="position:absolute;left:73.13px;top:152.56px" class="cls_084"><span class="cls_084">-</span><span class="cls_044"> Linearly scan these values, each time updating the count matrix</span></div>
<div style="position:absolute;left:95.63px;top:174.40px" class="cls_044"><span class="cls_044">and computing gini index</span></div>
<div style="position:absolute;left:73.13px;top:202.48px" class="cls_084"><span class="cls_084">-</span><span class="cls_044"> Choose the split position that has the least gini index</span></div>
<div style="position:absolute;left:125.18px;top:278.50px" class="cls_215"><span class="cls_215">Cheat</span></div>
<div style="position:absolute;left:184.03px;top:278.50px" class="cls_216"><span class="cls_216">No</span></div>
<div style="position:absolute;left:240.99px;top:278.50px" class="cls_216"><span class="cls_216">No</span></div>
<div style="position:absolute;left:290.01px;top:278.50px" class="cls_216"><span class="cls_216">No</span></div>
<div style="position:absolute;left:330.83px;top:278.50px" class="cls_216"><span class="cls_216">Yes</span></div>
<div style="position:absolute;left:377.44px;top:278.50px" class="cls_216"><span class="cls_216">Yes</span></div>
<div style="position:absolute;left:424.31px;top:278.50px" class="cls_216"><span class="cls_216">Yes</span></div>
<div style="position:absolute;left:473.88px;top:278.50px" class="cls_216"><span class="cls_216">No</span></div>
<div style="position:absolute;left:520.75px;top:278.50px" class="cls_216"><span class="cls_216">No</span></div>
<div style="position:absolute;left:570.93px;top:278.50px" class="cls_216"><span class="cls_216">No</span></div>
<div style="position:absolute;left:634.61px;top:278.50px" class="cls_216"><span class="cls_216">No</span></div>
<div style="position:absolute;left:378.69px;top:300.61px" class="cls_216"><span class="cls_216">Annual Income</span></div>
<div style="position:absolute;left:13.20px;top:312.08px" class="cls_218"><span class="cls_218">Sorted Values</span></div>
<div style="position:absolute;left:185.28px;top:322.21px" class="cls_216"><span class="cls_216">60</span></div>
<div style="position:absolute;left:242.33px;top:322.21px" class="cls_216"><span class="cls_216">70</span></div>
<div style="position:absolute;left:290.99px;top:322.21px" class="cls_216"><span class="cls_216">75</span></div>
<div style="position:absolute;left:334.49px;top:322.21px" class="cls_216"><span class="cls_216">85</span></div>
<div style="position:absolute;left:381.37px;top:322.21px" class="cls_216"><span class="cls_216">90</span></div>
<div style="position:absolute;left:428.24px;top:322.21px" class="cls_216"><span class="cls_216">95</span></div>
<div style="position:absolute;left:471.83px;top:322.21px" class="cls_216"><span class="cls_216">100</span></div>
<div style="position:absolute;left:518.70px;top:322.21px" class="cls_216"><span class="cls_216">120</span></div>
<div style="position:absolute;left:568.97px;top:322.21px" class="cls_216"><span class="cls_216">125</span></div>
<div style="position:absolute;left:632.56px;top:322.21px" class="cls_216"><span class="cls_216">220</span></div>
<div style="position:absolute;left:56.70px;top:507.60px" class="cls_006"><span class="cls_006">10/23/21</span></div>
<div style="position:absolute;left:248.06px;top:507.60px" class="cls_006"><span class="cls_006">Introduction to Data Mining, 2</span><span class="cls_007"><sup>nd</sup></span><span class="cls_006"> Edition</span></div>
<div style="position:absolute;left:649.95px;top:507.60px" class="cls_006"><span class="cls_006">39</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:21450px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background40.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.13px;top:16.64px" class="cls_185"><span class="cls_185">Continuous Attributes: Computing Gini Index...</span></div>
<div style="position:absolute;left:37.13px;top:96.40px" class="cls_080"><span class="cls_080">●</span><span class="cls_044">  For efficient computation: for each attribute,</span></div>
<div style="position:absolute;left:73.13px;top:124.48px" class="cls_084"><span class="cls_084">-</span><span class="cls_044"> Sort the attribute on values</span></div>
<div style="position:absolute;left:73.13px;top:152.56px" class="cls_084"><span class="cls_084">-</span><span class="cls_044"> Linearly scan these values, each time updating the count matrix</span></div>
<div style="position:absolute;left:95.63px;top:174.40px" class="cls_044"><span class="cls_044">and computing gini index</span></div>
<div style="position:absolute;left:73.13px;top:202.48px" class="cls_084"><span class="cls_084">-</span><span class="cls_044"> Choose the split position that has the least gini index</span></div>
<div style="position:absolute;left:125.18px;top:278.50px" class="cls_215"><span class="cls_215">Cheat</span></div>
<div style="position:absolute;left:184.03px;top:278.50px" class="cls_216"><span class="cls_216">No</span></div>
<div style="position:absolute;left:240.99px;top:278.50px" class="cls_216"><span class="cls_216">No</span></div>
<div style="position:absolute;left:290.01px;top:278.50px" class="cls_216"><span class="cls_216">No</span></div>
<div style="position:absolute;left:330.83px;top:278.50px" class="cls_216"><span class="cls_216">Yes</span></div>
<div style="position:absolute;left:377.44px;top:278.50px" class="cls_216"><span class="cls_216">Yes</span></div>
<div style="position:absolute;left:424.31px;top:278.50px" class="cls_216"><span class="cls_216">Yes</span></div>
<div style="position:absolute;left:473.88px;top:278.50px" class="cls_216"><span class="cls_216">No</span></div>
<div style="position:absolute;left:520.75px;top:278.50px" class="cls_216"><span class="cls_216">No</span></div>
<div style="position:absolute;left:570.93px;top:278.50px" class="cls_216"><span class="cls_216">No</span></div>
<div style="position:absolute;left:634.61px;top:278.50px" class="cls_216"><span class="cls_216">No</span></div>
<div style="position:absolute;left:378.69px;top:300.61px" class="cls_216"><span class="cls_216">Annual Income</span></div>
<div style="position:absolute;left:13.20px;top:312.08px" class="cls_218"><span class="cls_218">Sorted Values</span></div>
<div style="position:absolute;left:185.28px;top:322.21px" class="cls_216"><span class="cls_216">60</span></div>
<div style="position:absolute;left:242.33px;top:322.21px" class="cls_216"><span class="cls_216">70</span></div>
<div style="position:absolute;left:290.99px;top:322.21px" class="cls_216"><span class="cls_216">75</span></div>
<div style="position:absolute;left:334.49px;top:322.21px" class="cls_216"><span class="cls_216">85</span></div>
<div style="position:absolute;left:381.37px;top:322.21px" class="cls_216"><span class="cls_216">90</span></div>
<div style="position:absolute;left:428.24px;top:322.21px" class="cls_216"><span class="cls_216">95</span></div>
<div style="position:absolute;left:471.83px;top:322.21px" class="cls_216"><span class="cls_216">100</span></div>
<div style="position:absolute;left:518.70px;top:322.21px" class="cls_216"><span class="cls_216">120</span></div>
<div style="position:absolute;left:568.97px;top:322.21px" class="cls_216"><span class="cls_216">125</span></div>
<div style="position:absolute;left:632.56px;top:322.21px" class="cls_216"><span class="cls_216">220</span></div>
<div style="position:absolute;left:13.20px;top:336.08px" class="cls_218"><span class="cls_218">Split Positions</span></div>
<div style="position:absolute;left:179.66px;top:345.48px" class="cls_215"><span class="cls_215">55</span></div>
<div style="position:absolute;left:225.19px;top:345.48px" class="cls_215"><span class="cls_215">65</span></div>
<div style="position:absolute;left:270.81px;top:345.48px" class="cls_215"><span class="cls_215">72</span></div>
<div style="position:absolute;left:316.44px;top:345.48px" class="cls_215"><span class="cls_215">80</span></div>
<div style="position:absolute;left:362.08px;top:345.48px" class="cls_215"><span class="cls_215">87</span></div>
<div style="position:absolute;left:407.71px;top:345.48px" class="cls_215"><span class="cls_215">92</span></div>
<div style="position:absolute;left:453.33px;top:345.48px" class="cls_215"><span class="cls_215">97</span></div>
<div style="position:absolute;left:495.67px;top:345.48px" class="cls_215"><span class="cls_215">110</span></div>
<div style="position:absolute;left:543.61px;top:345.48px" class="cls_215"><span class="cls_215">122</span></div>
<div style="position:absolute;left:593.88px;top:345.48px" class="cls_215"><span class="cls_215">172</span></div>
<div style="position:absolute;left:642.20px;top:345.48px" class="cls_215"><span class="cls_215">230</span></div>
<div style="position:absolute;left:167.86px;top:365.38px" class="cls_215"><span class="cls_215">&lt;=</span></div>
<div style="position:absolute;left:194.12px;top:365.38px" class="cls_215"><span class="cls_215">></span></div>
<div style="position:absolute;left:213.40px;top:365.38px" class="cls_215"><span class="cls_215">&lt;=</span></div>
<div style="position:absolute;left:239.74px;top:365.38px" class="cls_215"><span class="cls_215">></span></div>
<div style="position:absolute;left:259.03px;top:365.38px" class="cls_215"><span class="cls_215">&lt;=</span></div>
<div style="position:absolute;left:285.37px;top:365.38px" class="cls_215"><span class="cls_215">></span></div>
<div style="position:absolute;left:304.65px;top:365.38px" class="cls_215"><span class="cls_215">&lt;=</span></div>
<div style="position:absolute;left:331.01px;top:365.38px" class="cls_215"><span class="cls_215">></span></div>
<div style="position:absolute;left:350.30px;top:365.38px" class="cls_215"><span class="cls_215">&lt;=</span></div>
<div style="position:absolute;left:376.64px;top:365.38px" class="cls_215"><span class="cls_215">></span></div>
<div style="position:absolute;left:395.92px;top:365.38px" class="cls_215"><span class="cls_215">&lt;=</span></div>
<div style="position:absolute;left:422.26px;top:365.38px" class="cls_215"><span class="cls_215">></span></div>
<div style="position:absolute;left:441.55px;top:365.38px" class="cls_215"><span class="cls_215">&lt;=</span></div>
<div style="position:absolute;left:467.81px;top:365.38px" class="cls_215"><span class="cls_215">></span></div>
<div style="position:absolute;left:487.18px;top:365.38px" class="cls_215"><span class="cls_215">&lt;=</span></div>
<div style="position:absolute;left:513.52px;top:365.38px" class="cls_215"><span class="cls_215">></span></div>
<div style="position:absolute;left:533.97px;top:365.38px" class="cls_215"><span class="cls_215">&lt;=</span></div>
<div style="position:absolute;left:562.54px;top:365.38px" class="cls_215"><span class="cls_215">></span></div>
<div style="position:absolute;left:584.23px;top:365.38px" class="cls_215"><span class="cls_215">&lt;=</span></div>
<div style="position:absolute;left:612.83px;top:365.38px" class="cls_215"><span class="cls_215">></span></div>
<div style="position:absolute;left:634.43px;top:365.38px" class="cls_215"><span class="cls_215">&lt;=</span></div>
<div style="position:absolute;left:663.09px;top:365.38px" class="cls_220"><span class="cls_220">></span></div>
<div style="position:absolute;left:56.70px;top:507.60px" class="cls_006"><span class="cls_006">10/23/21</span></div>
<div style="position:absolute;left:248.06px;top:507.60px" class="cls_006"><span class="cls_006">Introduction to Data Mining, 2</span><span class="cls_007"><sup>nd</sup></span><span class="cls_006"> Edition</span></div>
<div style="position:absolute;left:649.95px;top:507.60px" class="cls_006"><span class="cls_006">40</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:22000px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background41.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.13px;top:16.64px" class="cls_185"><span class="cls_185">Continuous Attributes: Computing Gini Index...</span></div>
<div style="position:absolute;left:37.13px;top:96.40px" class="cls_080"><span class="cls_080">●</span><span class="cls_044">  For efficient computation: for each attribute,</span></div>
<div style="position:absolute;left:73.13px;top:124.48px" class="cls_084"><span class="cls_084">-</span><span class="cls_044"> Sort the attribute on values</span></div>
<div style="position:absolute;left:73.13px;top:152.56px" class="cls_084"><span class="cls_084">-</span><span class="cls_044"> Linearly scan these values, each time updating the count matrix</span></div>
<div style="position:absolute;left:95.63px;top:174.40px" class="cls_044"><span class="cls_044">and computing gini index</span></div>
<div style="position:absolute;left:73.13px;top:202.48px" class="cls_084"><span class="cls_084">-</span><span class="cls_044"> Choose the split position that has the least gini index</span></div>
<div style="position:absolute;left:125.18px;top:278.50px" class="cls_215"><span class="cls_215">Cheat</span></div>
<div style="position:absolute;left:184.03px;top:278.50px" class="cls_216"><span class="cls_216">No</span></div>
<div style="position:absolute;left:240.99px;top:278.50px" class="cls_216"><span class="cls_216">No</span></div>
<div style="position:absolute;left:290.01px;top:278.50px" class="cls_216"><span class="cls_216">No</span></div>
<div style="position:absolute;left:330.83px;top:278.50px" class="cls_216"><span class="cls_216">Yes</span></div>
<div style="position:absolute;left:377.44px;top:278.50px" class="cls_216"><span class="cls_216">Yes</span></div>
<div style="position:absolute;left:424.31px;top:278.50px" class="cls_216"><span class="cls_216">Yes</span></div>
<div style="position:absolute;left:473.88px;top:278.50px" class="cls_216"><span class="cls_216">No</span></div>
<div style="position:absolute;left:520.75px;top:278.50px" class="cls_216"><span class="cls_216">No</span></div>
<div style="position:absolute;left:570.93px;top:278.50px" class="cls_216"><span class="cls_216">No</span></div>
<div style="position:absolute;left:634.61px;top:278.50px" class="cls_216"><span class="cls_216">No</span></div>
<div style="position:absolute;left:378.69px;top:300.61px" class="cls_216"><span class="cls_216">Annual Income</span></div>
<div style="position:absolute;left:13.20px;top:312.08px" class="cls_218"><span class="cls_218">Sorted Values</span></div>
<div style="position:absolute;left:185.28px;top:322.21px" class="cls_216"><span class="cls_216">60</span></div>
<div style="position:absolute;left:242.33px;top:322.21px" class="cls_216"><span class="cls_216">70</span></div>
<div style="position:absolute;left:290.99px;top:322.21px" class="cls_216"><span class="cls_216">75</span></div>
<div style="position:absolute;left:334.49px;top:322.21px" class="cls_216"><span class="cls_216">85</span></div>
<div style="position:absolute;left:381.37px;top:322.21px" class="cls_216"><span class="cls_216">90</span></div>
<div style="position:absolute;left:428.24px;top:322.21px" class="cls_216"><span class="cls_216">95</span></div>
<div style="position:absolute;left:471.83px;top:322.21px" class="cls_216"><span class="cls_216">100</span></div>
<div style="position:absolute;left:518.70px;top:322.21px" class="cls_216"><span class="cls_216">120</span></div>
<div style="position:absolute;left:568.97px;top:322.21px" class="cls_216"><span class="cls_216">125</span></div>
<div style="position:absolute;left:632.56px;top:322.21px" class="cls_216"><span class="cls_216">220</span></div>
<div style="position:absolute;left:13.20px;top:336.08px" class="cls_218"><span class="cls_218">Split Positions</span></div>
<div style="position:absolute;left:179.66px;top:345.48px" class="cls_215"><span class="cls_215">55</span></div>
<div style="position:absolute;left:225.19px;top:345.48px" class="cls_215"><span class="cls_215">65</span></div>
<div style="position:absolute;left:270.81px;top:345.48px" class="cls_215"><span class="cls_215">72</span></div>
<div style="position:absolute;left:316.44px;top:345.48px" class="cls_215"><span class="cls_215">80</span></div>
<div style="position:absolute;left:362.08px;top:345.48px" class="cls_215"><span class="cls_215">87</span></div>
<div style="position:absolute;left:407.71px;top:345.48px" class="cls_215"><span class="cls_215">92</span></div>
<div style="position:absolute;left:453.33px;top:345.48px" class="cls_215"><span class="cls_215">97</span></div>
<div style="position:absolute;left:495.67px;top:345.48px" class="cls_215"><span class="cls_215">110</span></div>
<div style="position:absolute;left:543.61px;top:345.48px" class="cls_215"><span class="cls_215">122</span></div>
<div style="position:absolute;left:593.88px;top:345.48px" class="cls_215"><span class="cls_215">172</span></div>
<div style="position:absolute;left:642.20px;top:345.48px" class="cls_215"><span class="cls_215">230</span></div>
<div style="position:absolute;left:167.86px;top:365.38px" class="cls_215"><span class="cls_215">&lt;=</span></div>
<div style="position:absolute;left:194.12px;top:365.38px" class="cls_215"><span class="cls_215">></span></div>
<div style="position:absolute;left:213.40px;top:365.38px" class="cls_215"><span class="cls_215">&lt;=</span></div>
<div style="position:absolute;left:239.74px;top:365.38px" class="cls_215"><span class="cls_215">></span></div>
<div style="position:absolute;left:259.03px;top:365.38px" class="cls_215"><span class="cls_215">&lt;=</span></div>
<div style="position:absolute;left:285.37px;top:365.38px" class="cls_215"><span class="cls_215">></span></div>
<div style="position:absolute;left:304.65px;top:365.38px" class="cls_215"><span class="cls_215">&lt;=</span></div>
<div style="position:absolute;left:331.01px;top:365.38px" class="cls_215"><span class="cls_215">></span></div>
<div style="position:absolute;left:350.30px;top:365.38px" class="cls_215"><span class="cls_215">&lt;=</span></div>
<div style="position:absolute;left:376.64px;top:365.38px" class="cls_215"><span class="cls_215">></span></div>
<div style="position:absolute;left:395.92px;top:365.38px" class="cls_215"><span class="cls_215">&lt;=</span></div>
<div style="position:absolute;left:422.26px;top:365.38px" class="cls_215"><span class="cls_215">></span></div>
<div style="position:absolute;left:441.55px;top:365.38px" class="cls_215"><span class="cls_215">&lt;=</span></div>
<div style="position:absolute;left:467.81px;top:365.38px" class="cls_215"><span class="cls_215">></span></div>
<div style="position:absolute;left:487.18px;top:365.38px" class="cls_215"><span class="cls_215">&lt;=</span></div>
<div style="position:absolute;left:513.52px;top:365.38px" class="cls_215"><span class="cls_215">></span></div>
<div style="position:absolute;left:533.97px;top:365.38px" class="cls_215"><span class="cls_215">&lt;=</span></div>
<div style="position:absolute;left:562.54px;top:365.38px" class="cls_215"><span class="cls_215">></span></div>
<div style="position:absolute;left:584.23px;top:365.38px" class="cls_215"><span class="cls_215">&lt;=</span></div>
<div style="position:absolute;left:612.83px;top:365.38px" class="cls_215"><span class="cls_215">></span></div>
<div style="position:absolute;left:634.43px;top:365.38px" class="cls_215"><span class="cls_215">&lt;=</span></div>
<div style="position:absolute;left:663.09px;top:365.38px" class="cls_220"><span class="cls_220">></span></div>
<div style="position:absolute;left:131.07px;top:388.57px" class="cls_215"><span class="cls_215">Yes</span></div>
<div style="position:absolute;left:133.75px;top:417.95px" class="cls_215"><span class="cls_215">No</span></div>
<div style="position:absolute;left:123.93px;top:447.86px" class="cls_222"><span class="cls_222">Gini</span></div>
<div style="position:absolute;left:308.22px;top:447.86px" class="cls_222"><span class="cls_222">0.343</span></div>
<div style="position:absolute;left:56.70px;top:507.60px" class="cls_006"><span class="cls_006">10/23/21</span></div>
<div style="position:absolute;left:248.06px;top:507.60px" class="cls_006"><span class="cls_006">Introduction to Data Mining, 2</span><span class="cls_007"><sup>nd</sup></span><span class="cls_006"> Edition</span></div>
<div style="position:absolute;left:649.95px;top:507.60px" class="cls_006"><span class="cls_006">41</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:22550px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background42.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.13px;top:16.64px" class="cls_185"><span class="cls_185">Continuous Attributes: Computing Gini Index...</span></div>
<div style="position:absolute;left:37.13px;top:96.40px" class="cls_080"><span class="cls_080">●</span><span class="cls_044">  For efficient computation: for each attribute,</span></div>
<div style="position:absolute;left:73.13px;top:124.48px" class="cls_084"><span class="cls_084">-</span><span class="cls_044"> Sort the attribute on values</span></div>
<div style="position:absolute;left:73.13px;top:152.56px" class="cls_084"><span class="cls_084">-</span><span class="cls_044"> Linearly scan these values, each time updating the count matrix</span></div>
<div style="position:absolute;left:95.63px;top:174.40px" class="cls_044"><span class="cls_044">and computing gini index</span></div>
<div style="position:absolute;left:73.13px;top:202.48px" class="cls_084"><span class="cls_084">-</span><span class="cls_044"> Choose the split position that has the least gini index</span></div>
<div style="position:absolute;left:125.18px;top:278.50px" class="cls_215"><span class="cls_215">Cheat</span></div>
<div style="position:absolute;left:184.03px;top:278.50px" class="cls_216"><span class="cls_216">No</span></div>
<div style="position:absolute;left:240.99px;top:278.50px" class="cls_216"><span class="cls_216">No</span></div>
<div style="position:absolute;left:290.01px;top:278.50px" class="cls_216"><span class="cls_216">No</span></div>
<div style="position:absolute;left:330.83px;top:278.50px" class="cls_216"><span class="cls_216">Yes</span></div>
<div style="position:absolute;left:377.44px;top:278.50px" class="cls_216"><span class="cls_216">Yes</span></div>
<div style="position:absolute;left:424.31px;top:278.50px" class="cls_216"><span class="cls_216">Yes</span></div>
<div style="position:absolute;left:473.88px;top:278.50px" class="cls_216"><span class="cls_216">No</span></div>
<div style="position:absolute;left:520.75px;top:278.50px" class="cls_216"><span class="cls_216">No</span></div>
<div style="position:absolute;left:570.93px;top:278.50px" class="cls_216"><span class="cls_216">No</span></div>
<div style="position:absolute;left:634.61px;top:278.50px" class="cls_216"><span class="cls_216">No</span></div>
<div style="position:absolute;left:378.69px;top:300.61px" class="cls_216"><span class="cls_216">Annual Income</span></div>
<div style="position:absolute;left:13.20px;top:312.08px" class="cls_218"><span class="cls_218">Sorted Values</span></div>
<div style="position:absolute;left:185.28px;top:322.21px" class="cls_216"><span class="cls_216">60</span></div>
<div style="position:absolute;left:242.33px;top:322.21px" class="cls_216"><span class="cls_216">70</span></div>
<div style="position:absolute;left:290.99px;top:322.21px" class="cls_216"><span class="cls_216">75</span></div>
<div style="position:absolute;left:334.49px;top:322.21px" class="cls_216"><span class="cls_216">85</span></div>
<div style="position:absolute;left:381.37px;top:322.21px" class="cls_216"><span class="cls_216">90</span></div>
<div style="position:absolute;left:428.24px;top:322.21px" class="cls_216"><span class="cls_216">95</span></div>
<div style="position:absolute;left:471.83px;top:322.21px" class="cls_216"><span class="cls_216">100</span></div>
<div style="position:absolute;left:518.70px;top:322.21px" class="cls_216"><span class="cls_216">120</span></div>
<div style="position:absolute;left:568.97px;top:322.21px" class="cls_216"><span class="cls_216">125</span></div>
<div style="position:absolute;left:632.56px;top:322.21px" class="cls_216"><span class="cls_216">220</span></div>
<div style="position:absolute;left:13.20px;top:336.08px" class="cls_218"><span class="cls_218">Split Positions</span></div>
<div style="position:absolute;left:179.66px;top:345.48px" class="cls_215"><span class="cls_215">55</span></div>
<div style="position:absolute;left:225.19px;top:345.48px" class="cls_215"><span class="cls_215">65</span></div>
<div style="position:absolute;left:270.81px;top:345.48px" class="cls_215"><span class="cls_215">72</span></div>
<div style="position:absolute;left:316.44px;top:345.48px" class="cls_215"><span class="cls_215">80</span></div>
<div style="position:absolute;left:362.08px;top:345.48px" class="cls_215"><span class="cls_215">87</span></div>
<div style="position:absolute;left:407.71px;top:345.48px" class="cls_215"><span class="cls_215">92</span></div>
<div style="position:absolute;left:453.33px;top:345.48px" class="cls_215"><span class="cls_215">97</span></div>
<div style="position:absolute;left:495.67px;top:345.48px" class="cls_215"><span class="cls_215">110</span></div>
<div style="position:absolute;left:543.61px;top:345.48px" class="cls_215"><span class="cls_215">122</span></div>
<div style="position:absolute;left:593.88px;top:345.48px" class="cls_215"><span class="cls_215">172</span></div>
<div style="position:absolute;left:642.20px;top:345.48px" class="cls_215"><span class="cls_215">230</span></div>
<div style="position:absolute;left:167.86px;top:365.38px" class="cls_215"><span class="cls_215">&lt;=</span></div>
<div style="position:absolute;left:194.12px;top:365.38px" class="cls_215"><span class="cls_215">></span></div>
<div style="position:absolute;left:213.40px;top:365.38px" class="cls_215"><span class="cls_215">&lt;=</span></div>
<div style="position:absolute;left:239.74px;top:365.38px" class="cls_215"><span class="cls_215">></span></div>
<div style="position:absolute;left:259.03px;top:365.38px" class="cls_215"><span class="cls_215">&lt;=</span></div>
<div style="position:absolute;left:285.37px;top:365.38px" class="cls_215"><span class="cls_215">></span></div>
<div style="position:absolute;left:304.65px;top:365.38px" class="cls_215"><span class="cls_215">&lt;=</span></div>
<div style="position:absolute;left:331.01px;top:365.38px" class="cls_215"><span class="cls_215">></span></div>
<div style="position:absolute;left:350.30px;top:365.38px" class="cls_215"><span class="cls_215">&lt;=</span></div>
<div style="position:absolute;left:376.64px;top:365.38px" class="cls_215"><span class="cls_215">></span></div>
<div style="position:absolute;left:395.92px;top:365.38px" class="cls_215"><span class="cls_215">&lt;=</span></div>
<div style="position:absolute;left:422.26px;top:365.38px" class="cls_215"><span class="cls_215">></span></div>
<div style="position:absolute;left:441.55px;top:365.38px" class="cls_215"><span class="cls_215">&lt;=</span></div>
<div style="position:absolute;left:467.81px;top:365.38px" class="cls_215"><span class="cls_215">></span></div>
<div style="position:absolute;left:487.18px;top:365.38px" class="cls_215"><span class="cls_215">&lt;=</span></div>
<div style="position:absolute;left:513.52px;top:365.38px" class="cls_215"><span class="cls_215">></span></div>
<div style="position:absolute;left:533.97px;top:365.38px" class="cls_215"><span class="cls_215">&lt;=</span></div>
<div style="position:absolute;left:562.54px;top:365.38px" class="cls_215"><span class="cls_215">></span></div>
<div style="position:absolute;left:584.23px;top:365.38px" class="cls_215"><span class="cls_215">&lt;=</span></div>
<div style="position:absolute;left:612.83px;top:365.38px" class="cls_215"><span class="cls_215">></span></div>
<div style="position:absolute;left:634.43px;top:365.38px" class="cls_215"><span class="cls_215">&lt;=</span></div>
<div style="position:absolute;left:663.09px;top:365.38px" class="cls_220"><span class="cls_220">></span></div>
<div style="position:absolute;left:131.07px;top:388.57px" class="cls_215"><span class="cls_215">Yes</span></div>
<div style="position:absolute;left:308.31px;top:388.57px" class="cls_221"><span class="cls_221">0</span></div>
<div style="position:absolute;left:331.19px;top:388.57px" class="cls_221"><span class="cls_221">3</span></div>
<div style="position:absolute;left:133.75px;top:417.95px" class="cls_215"><span class="cls_215">No</span></div>
<div style="position:absolute;left:308.31px;top:417.95px" class="cls_221"><span class="cls_221">3</span></div>
<div style="position:absolute;left:331.19px;top:417.95px" class="cls_221"><span class="cls_221">4</span></div>
<div style="position:absolute;left:123.93px;top:447.86px" class="cls_222"><span class="cls_222">Gini</span></div>
<div style="position:absolute;left:308.22px;top:447.86px" class="cls_222"><span class="cls_222">0.343</span></div>
<div style="position:absolute;left:353.87px;top:447.86px" class="cls_222"><span class="cls_222">0.417</span></div>
<div style="position:absolute;left:56.70px;top:507.60px" class="cls_006"><span class="cls_006">10/23/21</span></div>
<div style="position:absolute;left:248.06px;top:507.60px" class="cls_006"><span class="cls_006">Introduction to Data Mining, 2</span><span class="cls_007"><sup>nd</sup></span><span class="cls_006"> Edition</span></div>
<div style="position:absolute;left:649.95px;top:507.60px" class="cls_006"><span class="cls_006">42</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:23100px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background43.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.13px;top:16.64px" class="cls_185"><span class="cls_185">Continuous Attributes: Computing Gini Index...</span></div>
<div style="position:absolute;left:37.13px;top:96.40px" class="cls_080"><span class="cls_080">●</span><span class="cls_044">  For efficient computation: for each attribute,</span></div>
<div style="position:absolute;left:73.13px;top:124.48px" class="cls_084"><span class="cls_084">-</span><span class="cls_044"> Sort the attribute on values</span></div>
<div style="position:absolute;left:73.13px;top:152.56px" class="cls_084"><span class="cls_084">-</span><span class="cls_044"> Linearly scan these values, each time updating the count matrix</span></div>
<div style="position:absolute;left:95.63px;top:174.40px" class="cls_044"><span class="cls_044">and computing gini index</span></div>
<div style="position:absolute;left:73.13px;top:202.48px" class="cls_084"><span class="cls_084">-</span><span class="cls_044"> Choose the split position that has the least gini index</span></div>
<div style="position:absolute;left:125.18px;top:278.50px" class="cls_215"><span class="cls_215">Cheat</span></div>
<div style="position:absolute;left:184.03px;top:278.50px" class="cls_216"><span class="cls_216">No</span></div>
<div style="position:absolute;left:240.99px;top:278.50px" class="cls_216"><span class="cls_216">No</span></div>
<div style="position:absolute;left:290.01px;top:278.50px" class="cls_216"><span class="cls_216">No</span></div>
<div style="position:absolute;left:330.83px;top:278.50px" class="cls_216"><span class="cls_216">Yes</span></div>
<div style="position:absolute;left:377.44px;top:278.50px" class="cls_216"><span class="cls_216">Yes</span></div>
<div style="position:absolute;left:424.31px;top:278.50px" class="cls_216"><span class="cls_216">Yes</span></div>
<div style="position:absolute;left:473.88px;top:278.50px" class="cls_216"><span class="cls_216">No</span></div>
<div style="position:absolute;left:520.75px;top:278.50px" class="cls_216"><span class="cls_216">No</span></div>
<div style="position:absolute;left:570.93px;top:278.50px" class="cls_216"><span class="cls_216">No</span></div>
<div style="position:absolute;left:634.61px;top:278.50px" class="cls_216"><span class="cls_216">No</span></div>
<div style="position:absolute;left:378.69px;top:300.61px" class="cls_216"><span class="cls_216">Annual Income</span></div>
<div style="position:absolute;left:13.20px;top:312.08px" class="cls_218"><span class="cls_218">Sorted Values</span></div>
<div style="position:absolute;left:185.28px;top:322.21px" class="cls_216"><span class="cls_216">60</span></div>
<div style="position:absolute;left:242.33px;top:322.21px" class="cls_216"><span class="cls_216">70</span></div>
<div style="position:absolute;left:290.99px;top:322.21px" class="cls_216"><span class="cls_216">75</span></div>
<div style="position:absolute;left:334.49px;top:322.21px" class="cls_216"><span class="cls_216">85</span></div>
<div style="position:absolute;left:381.37px;top:322.21px" class="cls_216"><span class="cls_216">90</span></div>
<div style="position:absolute;left:428.24px;top:322.21px" class="cls_216"><span class="cls_216">95</span></div>
<div style="position:absolute;left:471.83px;top:322.21px" class="cls_216"><span class="cls_216">100</span></div>
<div style="position:absolute;left:518.70px;top:322.21px" class="cls_216"><span class="cls_216">120</span></div>
<div style="position:absolute;left:568.97px;top:322.21px" class="cls_216"><span class="cls_216">125</span></div>
<div style="position:absolute;left:632.56px;top:322.21px" class="cls_216"><span class="cls_216">220</span></div>
<div style="position:absolute;left:13.20px;top:336.08px" class="cls_218"><span class="cls_218">Split Positions</span></div>
<div style="position:absolute;left:179.66px;top:345.48px" class="cls_215"><span class="cls_215">55</span></div>
<div style="position:absolute;left:225.19px;top:345.48px" class="cls_215"><span class="cls_215">65</span></div>
<div style="position:absolute;left:270.81px;top:345.48px" class="cls_215"><span class="cls_215">72</span></div>
<div style="position:absolute;left:316.44px;top:345.48px" class="cls_215"><span class="cls_215">80</span></div>
<div style="position:absolute;left:362.08px;top:345.48px" class="cls_215"><span class="cls_215">87</span></div>
<div style="position:absolute;left:407.71px;top:345.48px" class="cls_215"><span class="cls_215">92</span></div>
<div style="position:absolute;left:453.33px;top:345.48px" class="cls_215"><span class="cls_215">97</span></div>
<div style="position:absolute;left:495.67px;top:345.48px" class="cls_215"><span class="cls_215">110</span></div>
<div style="position:absolute;left:543.61px;top:345.48px" class="cls_215"><span class="cls_215">122</span></div>
<div style="position:absolute;left:593.88px;top:345.48px" class="cls_215"><span class="cls_215">172</span></div>
<div style="position:absolute;left:642.20px;top:345.48px" class="cls_215"><span class="cls_215">230</span></div>
<div style="position:absolute;left:167.86px;top:365.38px" class="cls_215"><span class="cls_215">&lt;=</span></div>
<div style="position:absolute;left:194.12px;top:365.38px" class="cls_215"><span class="cls_215">></span></div>
<div style="position:absolute;left:213.40px;top:365.38px" class="cls_215"><span class="cls_215">&lt;=</span></div>
<div style="position:absolute;left:239.74px;top:365.38px" class="cls_215"><span class="cls_215">></span></div>
<div style="position:absolute;left:259.03px;top:365.38px" class="cls_215"><span class="cls_215">&lt;=</span></div>
<div style="position:absolute;left:285.37px;top:365.38px" class="cls_215"><span class="cls_215">></span></div>
<div style="position:absolute;left:304.65px;top:365.38px" class="cls_215"><span class="cls_215">&lt;=</span></div>
<div style="position:absolute;left:331.01px;top:365.38px" class="cls_215"><span class="cls_215">></span></div>
<div style="position:absolute;left:350.30px;top:365.38px" class="cls_215"><span class="cls_215">&lt;=</span></div>
<div style="position:absolute;left:376.64px;top:365.38px" class="cls_215"><span class="cls_215">></span></div>
<div style="position:absolute;left:395.92px;top:365.38px" class="cls_215"><span class="cls_215">&lt;=</span></div>
<div style="position:absolute;left:422.26px;top:365.38px" class="cls_215"><span class="cls_215">></span></div>
<div style="position:absolute;left:441.55px;top:365.38px" class="cls_215"><span class="cls_215">&lt;=</span></div>
<div style="position:absolute;left:467.81px;top:365.38px" class="cls_215"><span class="cls_215">></span></div>
<div style="position:absolute;left:487.18px;top:365.38px" class="cls_215"><span class="cls_215">&lt;=</span></div>
<div style="position:absolute;left:513.52px;top:365.38px" class="cls_215"><span class="cls_215">></span></div>
<div style="position:absolute;left:533.97px;top:365.38px" class="cls_215"><span class="cls_215">&lt;=</span></div>
<div style="position:absolute;left:562.54px;top:365.38px" class="cls_215"><span class="cls_215">></span></div>
<div style="position:absolute;left:584.23px;top:365.38px" class="cls_215"><span class="cls_215">&lt;=</span></div>
<div style="position:absolute;left:612.83px;top:365.38px" class="cls_215"><span class="cls_215">></span></div>
<div style="position:absolute;left:634.43px;top:365.38px" class="cls_215"><span class="cls_215">&lt;=</span></div>
<div style="position:absolute;left:663.09px;top:365.38px" class="cls_220"><span class="cls_220">></span></div>
<div style="position:absolute;left:131.07px;top:388.57px" class="cls_215"><span class="cls_215">Yes</span></div>
<div style="position:absolute;left:171.52px;top:388.57px" class="cls_221"><span class="cls_221">0</span></div>
<div style="position:absolute;left:194.30px;top:388.57px" class="cls_221"><span class="cls_221">3</span></div>
<div style="position:absolute;left:217.07px;top:388.57px" class="cls_221"><span class="cls_221">0</span></div>
<div style="position:absolute;left:239.92px;top:388.57px" class="cls_221"><span class="cls_221">3</span></div>
<div style="position:absolute;left:262.69px;top:388.57px" class="cls_221"><span class="cls_221">0</span></div>
<div style="position:absolute;left:285.55px;top:388.57px" class="cls_221"><span class="cls_221">3</span></div>
<div style="position:absolute;left:308.31px;top:388.57px" class="cls_221"><span class="cls_221">0</span></div>
<div style="position:absolute;left:331.19px;top:388.57px" class="cls_221"><span class="cls_221">3</span></div>
<div style="position:absolute;left:353.96px;top:388.57px" class="cls_221"><span class="cls_221">1</span></div>
<div style="position:absolute;left:376.82px;top:388.57px" class="cls_221"><span class="cls_221">2</span></div>
<div style="position:absolute;left:399.58px;top:388.57px" class="cls_221"><span class="cls_221">2</span></div>
<div style="position:absolute;left:422.44px;top:388.57px" class="cls_221"><span class="cls_221">1</span></div>
<div style="position:absolute;left:445.21px;top:388.57px" class="cls_221"><span class="cls_221">3</span></div>
<div style="position:absolute;left:467.99px;top:388.57px" class="cls_221"><span class="cls_221">0</span></div>
<div style="position:absolute;left:490.84px;top:388.57px" class="cls_221"><span class="cls_221">3</span></div>
<div style="position:absolute;left:513.70px;top:388.57px" class="cls_221"><span class="cls_221">0</span></div>
<div style="position:absolute;left:537.63px;top:388.57px" class="cls_221"><span class="cls_221">3</span></div>
<div style="position:absolute;left:562.72px;top:388.57px" class="cls_221"><span class="cls_221">0</span></div>
<div style="position:absolute;left:587.90px;top:388.57px" class="cls_221"><span class="cls_221">3</span></div>
<div style="position:absolute;left:613.01px;top:388.57px" class="cls_221"><span class="cls_221">0</span></div>
<div style="position:absolute;left:638.09px;top:388.57px" class="cls_221"><span class="cls_221">3</span></div>
<div style="position:absolute;left:663.27px;top:388.57px" class="cls_221"><span class="cls_221">0</span></div>
<div style="position:absolute;left:133.75px;top:417.95px" class="cls_215"><span class="cls_215">No</span></div>
<div style="position:absolute;left:171.52px;top:417.95px" class="cls_221"><span class="cls_221">0</span></div>
<div style="position:absolute;left:194.30px;top:417.95px" class="cls_221"><span class="cls_221">7</span></div>
<div style="position:absolute;left:217.07px;top:417.95px" class="cls_221"><span class="cls_221">1</span></div>
<div style="position:absolute;left:239.92px;top:417.95px" class="cls_221"><span class="cls_221">6</span></div>
<div style="position:absolute;left:262.69px;top:417.95px" class="cls_221"><span class="cls_221">2</span></div>
<div style="position:absolute;left:285.55px;top:417.95px" class="cls_221"><span class="cls_221">5</span></div>
<div style="position:absolute;left:308.31px;top:417.95px" class="cls_221"><span class="cls_221">3</span></div>
<div style="position:absolute;left:331.19px;top:417.95px" class="cls_221"><span class="cls_221">4</span></div>
<div style="position:absolute;left:353.96px;top:417.95px" class="cls_221"><span class="cls_221">3</span></div>
<div style="position:absolute;left:376.82px;top:417.95px" class="cls_221"><span class="cls_221">4</span></div>
<div style="position:absolute;left:399.58px;top:417.95px" class="cls_221"><span class="cls_221">3</span></div>
<div style="position:absolute;left:422.44px;top:417.95px" class="cls_221"><span class="cls_221">4</span></div>
<div style="position:absolute;left:445.21px;top:417.95px" class="cls_221"><span class="cls_221">3</span></div>
<div style="position:absolute;left:467.99px;top:417.95px" class="cls_221"><span class="cls_221">4</span></div>
<div style="position:absolute;left:490.84px;top:417.95px" class="cls_221"><span class="cls_221">4</span></div>
<div style="position:absolute;left:513.70px;top:417.95px" class="cls_221"><span class="cls_221">3</span></div>
<div style="position:absolute;left:537.63px;top:417.95px" class="cls_221"><span class="cls_221">5</span></div>
<div style="position:absolute;left:562.72px;top:417.95px" class="cls_221"><span class="cls_221">2</span></div>
<div style="position:absolute;left:587.90px;top:417.95px" class="cls_221"><span class="cls_221">6</span></div>
<div style="position:absolute;left:613.01px;top:417.95px" class="cls_221"><span class="cls_221">1</span></div>
<div style="position:absolute;left:638.09px;top:417.95px" class="cls_221"><span class="cls_221">7</span></div>
<div style="position:absolute;left:663.27px;top:417.95px" class="cls_221"><span class="cls_221">0</span></div>
<div style="position:absolute;left:123.93px;top:447.86px" class="cls_222"><span class="cls_222">Gini</span></div>
<div style="position:absolute;left:171.43px;top:447.86px" class="cls_222"><span class="cls_222">0.420</span></div>
<div style="position:absolute;left:216.98px;top:447.86px" class="cls_222"><span class="cls_222">0.400</span></div>
<div style="position:absolute;left:262.60px;top:447.86px" class="cls_222"><span class="cls_222">0.375</span></div>
<div style="position:absolute;left:308.22px;top:447.86px" class="cls_222"><span class="cls_222">0.343</span></div>
<div style="position:absolute;left:353.87px;top:447.86px" class="cls_222"><span class="cls_222">0.417</span></div>
<div style="position:absolute;left:399.49px;top:447.86px" class="cls_222"><span class="cls_222">0.400</span></div>
<div style="position:absolute;left:445.12px;top:447.86px" class="cls_290"><span class="cls_290">0.300</span></div>
<div style="position:absolute;left:490.76px;top:447.86px" class="cls_222"><span class="cls_222">0.343</span></div>
<div style="position:absolute;left:538.61px;top:447.86px" class="cls_222"><span class="cls_222">0.375</span></div>
<div style="position:absolute;left:588.88px;top:447.86px" class="cls_222"><span class="cls_222">0.400</span></div>
<div style="position:absolute;left:639.26px;top:447.86px" class="cls_222"><span class="cls_222">0.420</span></div>
<div style="position:absolute;left:56.70px;top:507.60px" class="cls_006"><span class="cls_006">10/23/21</span></div>
<div style="position:absolute;left:248.06px;top:507.60px" class="cls_006"><span class="cls_006">Introduction to Data Mining, 2</span><span class="cls_007"><sup>nd</sup></span><span class="cls_006"> Edition</span></div>
<div style="position:absolute;left:649.95px;top:507.60px" class="cls_006"><span class="cls_006">43</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:23650px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background44.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:16.64px" class="cls_185"><span class="cls_185">Measure of Impurity: Entropy</span></div>
<div style="position:absolute;left:19.13px;top:89.60px" class="cls_008"><span class="cls_008">●</span><span class="cls_005"> Entropy at a given node t:</span></div>
<div style="position:absolute;left:315.93px;top:133.75px" class="cls_225"><span class="cls_225">=</span><span class="cls_226"> </span><span class="cls_225">-</span><span class="cls_226">å</span></div>
<div style="position:absolute;left:165.31px;top:133.75px" class="cls_228"><span class="cls_228">Entropy</span><span class="cls_229">(</span><span class="cls_228">t</span><span class="cls_229">)</span></div>
<div style="position:absolute;left:388.21px;top:133.75px" class="cls_228"><span class="cls_228">p</span><span class="cls_229">(</span><span class="cls_228"> j</span><span class="cls_229"> |</span><span class="cls_228">t</span><span class="cls_229">)log</span><span class="cls_228"> p</span><span class="cls_229">(</span></div>
<div style="position:absolute;left:566.09px;top:133.75px" class="cls_228"><span class="cls_228">j</span></div>
<div style="position:absolute;left:582.29px;top:133.75px" class="cls_229"><span class="cls_229">|</span><span class="cls_228">t</span></div>
<div style="position:absolute;left:605.85px;top:133.75px" class="cls_229"><span class="cls_229">)</span></div>
<div style="position:absolute;left:369.75px;top:169.13px" class="cls_227"><span class="cls_227">j</span></div>
<div style="position:absolute;left:86.63px;top:217.60px" class="cls_044"><span class="cls_044">(NOTE: </span><span class="cls_162">p( j | t) </span><span class="cls_044">is the relative frequency of class j at node t).</span></div>
<div style="position:absolute;left:86.63px;top:271.44px" class="cls_012"><span class="cls_012">u</span><span class="cls_014">Maximum (log n</span><span class="cls_034"><sub>c</sub></span><span class="cls_014">) when records are equally distributed</span></div>
<div style="position:absolute;left:104.63px;top:297.60px" class="cls_014"><span class="cls_014">among all classes implying least information</span></div>
<div style="position:absolute;left:86.63px;top:330.48px" class="cls_012"><span class="cls_012">u</span><span class="cls_014">Minimum (0.0) when all records belong to one class,</span></div>
<div style="position:absolute;left:104.63px;top:356.40px" class="cls_014"><span class="cls_014">implying most information</span></div>
<div style="position:absolute;left:55.13px;top:426.56px" class="cls_009"><span class="cls_009">-</span><span class="cls_005"> Entropy based computations are quite similar to</span></div>
<div style="position:absolute;left:77.63px;top:457.52px" class="cls_005"><span class="cls_005">the GINI index computations</span></div>
<div style="position:absolute;left:56.70px;top:507.60px" class="cls_006"><span class="cls_006">10/23/21</span></div>
<div style="position:absolute;left:248.06px;top:507.60px" class="cls_006"><span class="cls_006">Introduction to Data Mining, 2</span><span class="cls_007"><sup>nd</sup></span><span class="cls_006"> Edition</span></div>
<div style="position:absolute;left:649.95px;top:507.60px" class="cls_006"><span class="cls_006">44</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:24200px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background45.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:11.44px" class="cls_002"><span class="cls_002">Computing Entropy of a Single Node</span></div>
<div style="position:absolute;left:292.42px;top:91.75px" class="cls_230"><span class="cls_230">=</span><span class="cls_231"> </span><span class="cls_230">-</span><span class="cls_231">å</span></div>
<div style="position:absolute;left:141.81px;top:91.75px" class="cls_234"><span class="cls_234">Entropy</span><span class="cls_235">(</span><span class="cls_234">t</span><span class="cls_235">)</span></div>
<div style="position:absolute;left:364.70px;top:91.75px" class="cls_234"><span class="cls_234">p</span><span class="cls_235">(</span><span class="cls_234"> j</span><span class="cls_235"> |</span><span class="cls_234">t</span><span class="cls_235">)log</span><span class="cls_234"> p</span><span class="cls_235">(</span></div>
<div style="position:absolute;left:553.72px;top:91.75px" class="cls_234"><span class="cls_234">j</span></div>
<div style="position:absolute;left:569.92px;top:91.75px" class="cls_235"><span class="cls_235">|</span><span class="cls_234">t</span></div>
<div style="position:absolute;left:593.48px;top:91.75px" class="cls_235"><span class="cls_235">)</span></div>
<div style="position:absolute;left:499.99px;top:122.32px" class="cls_232"><span class="cls_232">2</span></div>
<div style="position:absolute;left:346.24px;top:127.13px" class="cls_233"><span class="cls_233">j</span></div>
<div style="position:absolute;left:61.31px;top:183.87px" class="cls_169"><span class="cls_169">C1</span></div>
<div style="position:absolute;left:156.93px;top:183.87px" class="cls_170"><span class="cls_170">0</span></div>
<div style="position:absolute;left:235.20px;top:186.88px" class="cls_085"><span class="cls_085">P(C1) = 0/6 = 0     P(C2) = 6/6 = 1</span></div>
<div style="position:absolute;left:61.31px;top:215.28px" class="cls_169"><span class="cls_169">C2</span></div>
<div style="position:absolute;left:156.93px;top:215.28px" class="cls_170"><span class="cls_170">6</span></div>
<div style="position:absolute;left:235.20px;top:222.88px" class="cls_085"><span class="cls_085">Entropy = - 0 log 0 - 1 log 1 = - 0 - 0 = 0</span></div>
<div style="position:absolute;left:241.20px;top:296.56px" class="cls_085"><span class="cls_085">P(C1) = 1/6</span></div>
<div style="position:absolute;left:398.72px;top:296.56px" class="cls_085"><span class="cls_085">P(C2) = 5/6</span></div>
<div style="position:absolute;left:66.10px;top:300.26px" class="cls_172"><span class="cls_172">C1</span></div>
<div style="position:absolute;left:158.64px;top:300.26px" class="cls_173"><span class="cls_173">1</span></div>
<div style="position:absolute;left:66.10px;top:330.65px" class="cls_172"><span class="cls_172">C2</span></div>
<div style="position:absolute;left:158.64px;top:330.65px" class="cls_173"><span class="cls_173">5</span></div>
<div style="position:absolute;left:241.20px;top:332.56px" class="cls_085"><span class="cls_085">Entropy = - (1/6) log</span><span class="cls_086"><sub>2</sub></span><span class="cls_085"> (1/6) - (5/6) log</span><span class="cls_086"><sub>2</sub></span><span class="cls_085"> (1/6) = 0.65</span></div>
<div style="position:absolute;left:241.20px;top:404.56px" class="cls_085"><span class="cls_085">P(C1) = 2/6</span></div>
<div style="position:absolute;left:398.72px;top:404.56px" class="cls_085"><span class="cls_085">P(C2) = 4/6</span></div>
<div style="position:absolute;left:66.10px;top:407.81px" class="cls_175"><span class="cls_175">C1</span></div>
<div style="position:absolute;left:158.64px;top:407.81px" class="cls_176"><span class="cls_176">2</span></div>
<div style="position:absolute;left:66.10px;top:438.72px" class="cls_175"><span class="cls_175">C2</span></div>
<div style="position:absolute;left:158.64px;top:438.72px" class="cls_176"><span class="cls_176">4</span></div>
<div style="position:absolute;left:241.20px;top:440.56px" class="cls_085"><span class="cls_085">Entropy = - (2/6) log</span><span class="cls_086"><sub>2</sub></span><span class="cls_085"> (2/6) - (4/6) log</span><span class="cls_086"><sub>2</sub></span><span class="cls_085"> (4/6) = 0.92</span></div>
<div style="position:absolute;left:56.70px;top:507.60px" class="cls_006"><span class="cls_006">10/23/21</span></div>
<div style="position:absolute;left:248.06px;top:507.60px" class="cls_006"><span class="cls_006">Introduction to Data Mining, 2</span><span class="cls_007"><sup>nd</sup></span><span class="cls_006"> Edition</span></div>
<div style="position:absolute;left:649.95px;top:507.60px" class="cls_006"><span class="cls_006">45</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:24750px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background46.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:16.64px" class="cls_185"><span class="cls_185">Computing Information Gain After Splitting</span></div>
<div style="position:absolute;left:37.13px;top:92.40px" class="cls_103"><span class="cls_103">●</span><span class="cls_014"> Information Gain:</span></div>
<div style="position:absolute;left:442.90px;top:148.66px" class="cls_239"><span class="cls_239">k</span></div>
<div style="position:absolute;left:458.63px;top:129.85px" class="cls_240"><span class="cls_240">n</span></div>
<div style="position:absolute;left:422.72px;top:133.23px" class="cls_236"><span class="cls_236">æ</span></div>
<div style="position:absolute;left:612.41px;top:133.23px" class="cls_236"><span class="cls_236">ö</span></div>
<div style="position:absolute;left:473.06px;top:156.88px" class="cls_239"><span class="cls_239">i</span></div>
<div style="position:absolute;left:139.07px;top:147.97px" class="cls_240"><span class="cls_240">GAIN</span></div>
<div style="position:absolute;left:237.52px;top:147.97px" class="cls_236"><span class="cls_236">=</span></div>
<div style="position:absolute;left:261.46px;top:147.97px" class="cls_240"><span class="cls_240">Entropy</span><span class="cls_242">(</span><span class="cls_240">p</span><span class="cls_242">)</span><span class="cls_236">-</span></div>
<div style="position:absolute;left:437.61px;top:155.07px" class="cls_237"><span class="cls_237">å</span></div>
<div style="position:absolute;left:485.18px;top:147.97px" class="cls_240"><span class="cls_240">Entropy</span><span class="cls_242">(</span><span class="cls_240">i</span><span class="cls_242">)</span></div>
<div style="position:absolute;left:422.72px;top:152.12px" class="cls_236"><span class="cls_236">ç</span></div>
<div style="position:absolute;left:612.41px;top:152.12px" class="cls_236"><span class="cls_236">÷</span></div>
<div style="position:absolute;left:209.75px;top:175.00px" class="cls_239"><span class="cls_239">split</span></div>
<div style="position:absolute;left:439.22px;top:179.26px" class="cls_239"><span class="cls_239">i</span><span class="cls_238">=</span><span class="cls_241">1</span></div>
<div style="position:absolute;left:422.72px;top:172.85px" class="cls_236"><span class="cls_236">è</span></div>
<div style="position:absolute;left:461.62px;top:170.54px" class="cls_240"><span class="cls_240">n</span></div>
<div style="position:absolute;left:612.41px;top:172.85px" class="cls_236"><span class="cls_236">ø</span></div>
<div style="position:absolute;left:181.13px;top:224.56px" class="cls_044"><span class="cls_044">Parent Node, p is split into k partitions;</span></div>
<div style="position:absolute;left:181.13px;top:255.52px" class="cls_044"><span class="cls_044">n</span><span class="cls_081"><sub>i</sub></span><span class="cls_044"> is number of records in partition i</span></div>
<div style="position:absolute;left:73.13px;top:311.52px" class="cls_105"><span class="cls_105">-</span><span class="cls_014"> Choose the split that achieves most reduction</span></div>
<div style="position:absolute;left:95.63px;top:340.56px" class="cls_014"><span class="cls_014">(maximizes GAIN)</span></div>
<div style="position:absolute;left:73.13px;top:402.48px" class="cls_105"><span class="cls_105">-</span><span class="cls_014"> Used in the ID3 and C4.5 decision tree algorithms</span></div>
<div style="position:absolute;left:56.70px;top:507.60px" class="cls_006"><span class="cls_006">10/23/21</span></div>
<div style="position:absolute;left:248.06px;top:507.60px" class="cls_006"><span class="cls_006">Introduction to Data Mining, 2</span><span class="cls_007"><sup>nd</sup></span><span class="cls_006"> Edition</span></div>
<div style="position:absolute;left:649.95px;top:507.60px" class="cls_006"><span class="cls_006">46</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:25300px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background47.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:14.40px" class="cls_243"><span class="cls_243">Problem with large number of partitions</span></div>
<div style="position:absolute;left:39.50px;top:92.48px" class="cls_008"><span class="cls_008">●</span><span class="cls_005"> Node impurity measures tend to prefer splits that</span></div>
<div style="position:absolute;left:62.50px;top:126.56px" class="cls_005"><span class="cls_005">result in large number of partitions, each being</span></div>
<div style="position:absolute;left:62.50px;top:159.44px" class="cls_005"><span class="cls_005">small but pure</span></div>
<div style="position:absolute;left:281.40px;top:223.45px" class="cls_244"><span class="cls_244">Car</span></div>
<div style="position:absolute;left:516.03px;top:223.45px" class="cls_244"><span class="cls_244">Customer</span></div>
<div style="position:absolute;left:70.53px;top:228.50px" class="cls_244"><span class="cls_244">Gender</span></div>
<div style="position:absolute;left:276.89px;top:240.78px" class="cls_244"><span class="cls_244">Type</span></div>
<div style="position:absolute;left:540.62px;top:240.78px" class="cls_244"><span class="cls_244">ID</span></div>
<div style="position:absolute;left:219.96px;top:271.84px" class="cls_244"><span class="cls_244">Family</span></div>
<div style="position:absolute;left:320.92px;top:271.84px" class="cls_244"><span class="cls_244">Luxury</span></div>
<div style="position:absolute;left:478.75px;top:271.84px" class="cls_244"><span class="cls_244">c</span><span class="cls_245"><sub>1</sub></span></div>
<div style="position:absolute;left:605.04px;top:271.84px" class="cls_244"><span class="cls_244">c</span><span class="cls_245"><sub>20</sub></span></div>
<div style="position:absolute;left:49.24px;top:275.60px" class="cls_244"><span class="cls_244">Yes</span></div>
<div style="position:absolute;left:114.45px;top:275.30px" class="cls_244"><span class="cls_244">No</span></div>
<div style="position:absolute;left:506.24px;top:286.43px" class="cls_244"><span class="cls_244">c</span><span class="cls_245"><sub>10</sub></span></div>
<div style="position:absolute;left:543.11px;top:286.43px" class="cls_244"><span class="cls_244">c</span><span class="cls_245"><sub>11</sub></span></div>
<div style="position:absolute;left:253.53px;top:297.27px" class="cls_244"><span class="cls_244">Sports</span></div>
<div style="position:absolute;left:55.50px;top:326.67px" class="cls_246"><span class="cls_246">C0: 6</span></div>
<div style="position:absolute;left:103.42px;top:326.67px" class="cls_246"><span class="cls_246">C0: 4</span></div>
<div style="position:absolute;left:214.94px;top:326.67px" class="cls_246"><span class="cls_246">C0: 1</span></div>
<div style="position:absolute;left:277.61px;top:326.67px" class="cls_246"><span class="cls_246">C0: 8</span></div>
<div style="position:absolute;left:343.97px;top:326.67px" class="cls_246"><span class="cls_246">C0: 1</span></div>
<div style="position:absolute;left:437.06px;top:326.67px" class="cls_246"><span class="cls_246">C0: 1</span></div>
<div style="position:absolute;left:507.11px;top:326.67px" class="cls_246"><span class="cls_246">C0: 1</span></div>
<div style="position:absolute;left:559.64px;top:326.67px" class="cls_246"><span class="cls_246">C0: 0</span></div>
<div style="position:absolute;left:630.61px;top:326.67px" class="cls_246"><span class="cls_246">C0: 0</span></div>
<div style="position:absolute;left:55.50px;top:342.08px" class="cls_246"><span class="cls_246">C1: 4</span></div>
<div style="position:absolute;left:103.42px;top:342.08px" class="cls_246"><span class="cls_246">C1: 6</span></div>
<div style="position:absolute;left:214.94px;top:342.08px" class="cls_246"><span class="cls_246">C1: 3</span></div>
<div style="position:absolute;left:277.61px;top:342.08px" class="cls_246"><span class="cls_246">C1: 0</span></div>
<div style="position:absolute;left:343.97px;top:342.08px" class="cls_246"><span class="cls_246">C1: 7</span></div>
<div style="position:absolute;left:437.06px;top:342.08px" class="cls_246"><span class="cls_246">C1: 0</span></div>
<div style="position:absolute;left:507.11px;top:342.08px" class="cls_246"><span class="cls_246">C1: 0</span></div>
<div style="position:absolute;left:559.64px;top:342.08px" class="cls_246"><span class="cls_246">C1: 1</span></div>
<div style="position:absolute;left:630.61px;top:342.08px" class="cls_246"><span class="cls_246">C1: 1</span></div>
<div style="position:absolute;left:75.50px;top:405.44px" class="cls_009"><span class="cls_009">-</span><span class="cls_005"> Customer ID has highest information gain</span></div>
<div style="position:absolute;left:102.50px;top:439.52px" class="cls_005"><span class="cls_005">because entropy for all the children is zero</span></div>
<div style="position:absolute;left:56.70px;top:507.60px" class="cls_006"><span class="cls_006">10/23/21</span></div>
<div style="position:absolute;left:248.06px;top:507.60px" class="cls_006"><span class="cls_006">Introduction to Data Mining, 2</span><span class="cls_007"><sup>nd</sup></span><span class="cls_006"> Edition</span></div>
<div style="position:absolute;left:649.95px;top:507.60px" class="cls_006"><span class="cls_006">47</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:25850px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background48.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:16.64px" class="cls_185"><span class="cls_185">Gain Ratio</span></div>
<div style="position:absolute;left:43.13px;top:92.56px" class="cls_080"><span class="cls_080">●</span><span class="cls_044">  Gain Ratio:</span></div>
<div style="position:absolute;left:258.99px;top:133.22px" class="cls_248"><span class="cls_248">GAIN</span></div>
<div style="position:absolute;left:583.81px;top:153.74px" class="cls_254"><span class="cls_254">k</span></div>
<div style="position:absolute;left:601.98px;top:132.97px" class="cls_255"><span class="cls_255">n</span></div>
<div style="position:absolute;left:680.30px;top:132.97px" class="cls_255"><span class="cls_255">n</span></div>
<div style="position:absolute;left:329.92px;top:159.30px" class="cls_249"><span class="cls_249">Split</span></div>
<div style="position:absolute;left:618.63px;top:162.39px" class="cls_254"><span class="cls_254">i</span></div>
<div style="position:absolute;left:696.95px;top:162.39px" class="cls_254"><span class="cls_254">i</span></div>
<div style="position:absolute;left:49.08px;top:152.21px" class="cls_248"><span class="cls_248">GainRATIO</span></div>
<div style="position:absolute;left:219.45px;top:152.21px" class="cls_250"><span class="cls_250">=</span></div>
<div style="position:absolute;left:380.83px;top:152.04px" class="cls_255"><span class="cls_255">SplitINFO</span></div>
<div style="position:absolute;left:531.28px;top:152.04px" class="cls_253"><span class="cls_253">=</span></div>
<div style="position:absolute;left:557.85px;top:152.04px" class="cls_253"><span class="cls_253">-</span></div>
<div style="position:absolute;left:577.70px;top:159.88px" class="cls_251"><span class="cls_251">å</span></div>
<div style="position:absolute;left:631.04px;top:152.04px" class="cls_257"><span class="cls_257">log</span></div>
<div style="position:absolute;left:191.58px;top:178.29px" class="cls_249"><span class="cls_249">split</span></div>
<div style="position:absolute;left:579.56px;top:185.93px" class="cls_254"><span class="cls_254">i</span><span class="cls_252">=</span><span class="cls_256">1</span></div>
<div style="position:absolute;left:244.25px;top:173.51px" class="cls_248"><span class="cls_248">SplitINFO</span></div>
<div style="position:absolute;left:605.43px;top:175.78px" class="cls_255"><span class="cls_255">n</span></div>
<div style="position:absolute;left:683.75px;top:175.78px" class="cls_255"><span class="cls_255">n</span></div>
<div style="position:absolute;left:115.38px;top:239.52px" class="cls_258"><span class="cls_258">Parent Node, p is split into k partitions</span></div>
<div style="position:absolute;left:115.38px;top:267.60px" class="cls_258"><span class="cls_258">n</span><span class="cls_259"><sub>i</sub></span><span class="cls_258"> is the number of records in partition i</span></div>
<div style="position:absolute;left:79.13px;top:308.56px" class="cls_084"><span class="cls_084">-</span><span class="cls_044"> Adjusts Information Gain by the entropy of the partitioning</span></div>
<div style="position:absolute;left:101.63px;top:332.56px" class="cls_044"><span class="cls_044">(SplitINFO).</span></div>
<div style="position:absolute;left:115.38px;top:362.40px" class="cls_260"><span class="cls_260">u</span><span class="cls_258"> Higher entropy partitioning (large number of small partitions) is</span></div>
<div style="position:absolute;left:133.38px;top:384.48px" class="cls_258"><span class="cls_258">penalized!</span></div>
<div style="position:absolute;left:79.13px;top:412.48px" class="cls_084"><span class="cls_084">-</span><span class="cls_044"> Used in C4.5 algorithm</span></div>
<div style="position:absolute;left:79.13px;top:442.48px" class="cls_084"><span class="cls_084">-</span><span class="cls_044"> Designed to overcome the disadvantage of Information Gain</span></div>
<div style="position:absolute;left:56.70px;top:507.60px" class="cls_006"><span class="cls_006">10/23/21</span></div>
<div style="position:absolute;left:248.06px;top:507.60px" class="cls_006"><span class="cls_006">Introduction to Data Mining, 2</span><span class="cls_007"><sup>nd</sup></span><span class="cls_006"> Edition</span></div>
<div style="position:absolute;left:649.95px;top:507.60px" class="cls_006"><span class="cls_006">48</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:26400px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background49.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:16.64px" class="cls_185"><span class="cls_185">Gain Ratio</span></div>
<div style="position:absolute;left:43.13px;top:92.56px" class="cls_080"><span class="cls_080">●</span><span class="cls_044">  Gain Ratio:</span></div>
<div style="position:absolute;left:258.99px;top:133.22px" class="cls_248"><span class="cls_248">GAIN</span></div>
<div style="position:absolute;left:583.81px;top:153.74px" class="cls_254"><span class="cls_254">k</span></div>
<div style="position:absolute;left:601.98px;top:132.97px" class="cls_255"><span class="cls_255">n</span></div>
<div style="position:absolute;left:680.30px;top:132.97px" class="cls_255"><span class="cls_255">n</span></div>
<div style="position:absolute;left:329.92px;top:159.30px" class="cls_249"><span class="cls_249">Split</span></div>
<div style="position:absolute;left:618.63px;top:162.39px" class="cls_254"><span class="cls_254">i</span></div>
<div style="position:absolute;left:696.95px;top:162.39px" class="cls_254"><span class="cls_254">i</span></div>
<div style="position:absolute;left:49.08px;top:152.21px" class="cls_248"><span class="cls_248">GainRATIO</span></div>
<div style="position:absolute;left:219.45px;top:152.21px" class="cls_250"><span class="cls_250">=</span></div>
<div style="position:absolute;left:380.83px;top:152.04px" class="cls_255"><span class="cls_255">SplitINFO</span></div>
<div style="position:absolute;left:531.28px;top:152.04px" class="cls_253"><span class="cls_253">=</span></div>
<div style="position:absolute;left:557.85px;top:152.04px" class="cls_253"><span class="cls_253">-</span></div>
<div style="position:absolute;left:577.70px;top:159.88px" class="cls_251"><span class="cls_251">å</span></div>
<div style="position:absolute;left:631.04px;top:152.04px" class="cls_257"><span class="cls_257">log</span></div>
<div style="position:absolute;left:191.58px;top:178.29px" class="cls_249"><span class="cls_249">split</span></div>
<div style="position:absolute;left:579.56px;top:185.93px" class="cls_254"><span class="cls_254">i</span><span class="cls_252">=</span><span class="cls_256">1</span></div>
<div style="position:absolute;left:244.25px;top:173.51px" class="cls_248"><span class="cls_248">SplitINFO</span></div>
<div style="position:absolute;left:605.43px;top:175.78px" class="cls_255"><span class="cls_255">n</span></div>
<div style="position:absolute;left:683.75px;top:175.78px" class="cls_255"><span class="cls_255">n</span></div>
<div style="position:absolute;left:115.38px;top:239.52px" class="cls_258"><span class="cls_258">Parent Node, p is split into k partitions</span></div>
<div style="position:absolute;left:115.38px;top:267.60px" class="cls_258"><span class="cls_258">n</span><span class="cls_259"><sub>i</sub></span><span class="cls_258"> is the number of records in partition i</span></div>
<div style="position:absolute;left:140.88px;top:325.83px" class="cls_269"><span class="cls_269">CarType</span></div>
<div style="position:absolute;left:376.14px;top:325.58px" class="cls_262"><span class="cls_262">CarType</span></div>
<div style="position:absolute;left:593.76px;top:325.20px" class="cls_194"><span class="cls_194">CarType</span></div>
<div style="position:absolute;left:346.71px;top:344.59px" class="cls_263"><span class="cls_263">{Sports,</span></div>
<div style="position:absolute;left:628.46px;top:344.22px" class="cls_195"><span class="cls_195">{Family,</span></div>
<div style="position:absolute;left:88.61px;top:349.70px" class="cls_264"><span class="cls_264">Family</span></div>
<div style="position:absolute;left:149.23px;top:349.70px" class="cls_264"><span class="cls_264">Sports</span></div>
<div style="position:absolute;left:206.78px;top:349.70px" class="cls_264"><span class="cls_264">Luxury</span></div>
<div style="position:absolute;left:410.05px;top:352.70px" class="cls_263"><span class="cls_263">{Family}</span></div>
<div style="position:absolute;left:563.54px;top:352.33px" class="cls_195"><span class="cls_195">{Sports}</span></div>
<div style="position:absolute;left:347.50px;top:360.76px" class="cls_263"><span class="cls_263">Luxury}</span></div>
<div style="position:absolute;left:629.25px;top:360.38px" class="cls_195"><span class="cls_195">Luxury}</span></div>
<div style="position:absolute;left:44.95px;top:372.96px" class="cls_264"><span class="cls_264">C1</span></div>
<div style="position:absolute;left:109.15px;top:373.55px" class="cls_265"><span class="cls_265">1</span></div>
<div style="position:absolute;left:169.82px;top:373.55px" class="cls_265"><span class="cls_265">8</span></div>
<div style="position:absolute;left:228.67px;top:373.55px" class="cls_265"><span class="cls_265">1</span></div>
<div style="position:absolute;left:306.75px;top:379.52px" class="cls_263"><span class="cls_263">C1</span></div>
<div style="position:absolute;left:369.77px;top:379.99px" class="cls_261"><span class="cls_261">9</span></div>
<div style="position:absolute;left:433.95px;top:379.99px" class="cls_261"><span class="cls_261">1</span></div>
<div style="position:absolute;left:524.37px;top:379.14px" class="cls_195"><span class="cls_195">C1</span></div>
<div style="position:absolute;left:587.40px;top:379.61px" class="cls_193"><span class="cls_193">8</span></div>
<div style="position:absolute;left:651.58px;top:379.61px" class="cls_193"><span class="cls_193">2</span></div>
<div style="position:absolute;left:44.95px;top:395.47px" class="cls_264"><span class="cls_264">C2</span></div>
<div style="position:absolute;left:109.15px;top:396.00px" class="cls_265"><span class="cls_265">3</span></div>
<div style="position:absolute;left:169.82px;top:396.00px" class="cls_265"><span class="cls_265">0</span></div>
<div style="position:absolute;left:228.67px;top:396.00px" class="cls_265"><span class="cls_265">7</span></div>
<div style="position:absolute;left:306.75px;top:400.05px" class="cls_263"><span class="cls_263">C2</span></div>
<div style="position:absolute;left:369.77px;top:400.52px" class="cls_261"><span class="cls_261">7</span></div>
<div style="position:absolute;left:433.95px;top:400.52px" class="cls_261"><span class="cls_261">3</span></div>
<div style="position:absolute;left:524.37px;top:399.68px" class="cls_195"><span class="cls_195">C2</span></div>
<div style="position:absolute;left:587.40px;top:400.15px" class="cls_193"><span class="cls_193">0</span></div>
<div style="position:absolute;left:647.73px;top:400.15px" class="cls_193"><span class="cls_193">10</span></div>
<div style="position:absolute;left:39.36px;top:419.15px" class="cls_266"><span class="cls_266">Gini</span></div>
<div style="position:absolute;left:149.82px;top:419.15px" class="cls_266"><span class="cls_266">0.163</span></div>
<div style="position:absolute;left:301.74px;top:421.28px" class="cls_267"><span class="cls_267">Gini</span></div>
<div style="position:absolute;left:384.14px;top:421.28px" class="cls_267"><span class="cls_267">0.468</span></div>
<div style="position:absolute;left:519.36px;top:420.91px" class="cls_199"><span class="cls_199">Gini</span></div>
<div style="position:absolute;left:601.76px;top:420.91px" class="cls_199"><span class="cls_199">0.167</span></div>
<div style="position:absolute;left:529.20px;top:447.68px" class="cls_218"><span class="cls_218">SplitINFO = 0.97</span></div>
<div style="position:absolute;left:79.20px;top:451.04px" class="cls_218"><span class="cls_218">SplitINFO = 1.52</span></div>
<div style="position:absolute;left:313.20px;top:451.04px" class="cls_218"><span class="cls_218">SplitINFO = 0.72</span></div>
<div style="position:absolute;left:56.70px;top:507.60px" class="cls_006"><span class="cls_006">10/23/21</span></div>
<div style="position:absolute;left:248.06px;top:507.60px" class="cls_006"><span class="cls_006">Introduction to Data Mining, 2</span><span class="cls_007"><sup>nd</sup></span><span class="cls_006"> Edition</span></div>
<div style="position:absolute;left:649.95px;top:507.60px" class="cls_006"><span class="cls_006">49</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:26950px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background50.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:16.64px" class="cls_185"><span class="cls_185">Measure of Impurity: Classification Error</span></div>
<div style="position:absolute;left:39.50px;top:92.48px" class="cls_008"><span class="cls_008">●</span><span class="cls_005"> Classification error at a node t :</span></div>
<div style="position:absolute;left:141.83px;top:151.07px" class="cls_271"><span class="cls_271">Error</span><span class="cls_272">(</span><span class="cls_271">t</span><span class="cls_272">)</span></div>
<div style="position:absolute;left:306.38px;top:151.07px" class="cls_272"><span class="cls_272">1</span></div>
<div style="position:absolute;left:356.55px;top:151.07px" class="cls_272"><span class="cls_272">max</span><span class="cls_271"> P</span><span class="cls_272">(</span><span class="cls_271">i</span></div>
<div style="position:absolute;left:485.39px;top:151.07px" class="cls_272"><span class="cls_272">|</span><span class="cls_271">t</span></div>
<div style="position:absolute;left:512.64px;top:151.07px" class="cls_272"><span class="cls_272">)</span></div>
<div style="position:absolute;left:280.39px;top:151.07px" class="cls_274"><span class="cls_274">=</span></div>
<div style="position:absolute;left:328.74px;top:151.07px" class="cls_274"><span class="cls_274">-</span></div>
<div style="position:absolute;left:387.77px;top:190.23px" class="cls_273"><span class="cls_273">i</span></div>
<div style="position:absolute;left:75.50px;top:250.56px" class="cls_105"><span class="cls_105">-</span><span class="cls_014"> Maximum (1 - 1/n</span><span class="cls_034"><sub>c</sub></span><span class="cls_014">) when records are equally</span></div>
<div style="position:absolute;left:98.00px;top:279.60px" class="cls_014"><span class="cls_014">distributed among all classes, implying least</span></div>
<div style="position:absolute;left:98.00px;top:308.40px" class="cls_014"><span class="cls_014">interesting information</span></div>
<div style="position:absolute;left:75.50px;top:344.40px" class="cls_105"><span class="cls_105">-</span><span class="cls_014"> Minimum (0) when all records belong to one class,</span></div>
<div style="position:absolute;left:98.00px;top:373.44px" class="cls_014"><span class="cls_014">implying most interesting information</span></div>
<div style="position:absolute;left:56.70px;top:507.60px" class="cls_006"><span class="cls_006">10/23/21</span></div>
<div style="position:absolute;left:248.06px;top:507.60px" class="cls_006"><span class="cls_006">Introduction to Data Mining, 2</span><span class="cls_007"><sup>nd</sup></span><span class="cls_006"> Edition</span></div>
<div style="position:absolute;left:649.95px;top:507.60px" class="cls_006"><span class="cls_006">50</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:27500px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background51.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:11.44px" class="cls_002"><span class="cls_002">Computing Error of a Single Node</span></div>
<div style="position:absolute;left:147.83px;top:91.07px" class="cls_271"><span class="cls_271">Error</span><span class="cls_272">(</span><span class="cls_271">t</span><span class="cls_272">)</span></div>
<div style="position:absolute;left:312.38px;top:91.07px" class="cls_272"><span class="cls_272">1</span></div>
<div style="position:absolute;left:362.55px;top:91.07px" class="cls_272"><span class="cls_272">max</span><span class="cls_271"> P</span><span class="cls_272">(</span><span class="cls_271">i</span></div>
<div style="position:absolute;left:491.39px;top:91.07px" class="cls_272"><span class="cls_272">|</span><span class="cls_271">t</span></div>
<div style="position:absolute;left:518.64px;top:91.07px" class="cls_272"><span class="cls_272">)</span></div>
<div style="position:absolute;left:286.39px;top:91.07px" class="cls_274"><span class="cls_274">=</span></div>
<div style="position:absolute;left:334.74px;top:91.07px" class="cls_274"><span class="cls_274">-</span></div>
<div style="position:absolute;left:393.77px;top:130.23px" class="cls_273"><span class="cls_273">i</span></div>
<div style="position:absolute;left:61.31px;top:183.87px" class="cls_169"><span class="cls_169">C1</span></div>
<div style="position:absolute;left:156.93px;top:183.87px" class="cls_170"><span class="cls_170">0</span></div>
<div style="position:absolute;left:235.20px;top:186.88px" class="cls_085"><span class="cls_085">P(C1) = 0/6 = 0     P(C2) = 6/6 = 1</span></div>
<div style="position:absolute;left:61.31px;top:215.28px" class="cls_169"><span class="cls_169">C2</span></div>
<div style="position:absolute;left:156.93px;top:215.28px" class="cls_170"><span class="cls_170">6</span></div>
<div style="position:absolute;left:235.20px;top:222.88px" class="cls_085"><span class="cls_085">Error = 1 - max (0, 1) = 1 - 1 = 0</span></div>
<div style="position:absolute;left:241.20px;top:296.56px" class="cls_085"><span class="cls_085">P(C1) = 1/6</span></div>
<div style="position:absolute;left:398.72px;top:296.56px" class="cls_085"><span class="cls_085">P(C2) = 5/6</span></div>
<div style="position:absolute;left:66.10px;top:300.26px" class="cls_172"><span class="cls_172">C1</span></div>
<div style="position:absolute;left:158.64px;top:300.26px" class="cls_173"><span class="cls_173">1</span></div>
<div style="position:absolute;left:66.10px;top:330.65px" class="cls_172"><span class="cls_172">C2</span></div>
<div style="position:absolute;left:158.64px;top:330.65px" class="cls_173"><span class="cls_173">5</span></div>
<div style="position:absolute;left:241.20px;top:332.56px" class="cls_085"><span class="cls_085">Error = 1 - max (1/6, 5/6) = 1 - 5/6 = 1/6</span></div>
<div style="position:absolute;left:241.20px;top:404.56px" class="cls_085"><span class="cls_085">P(C1) = 2/6</span></div>
<div style="position:absolute;left:398.72px;top:404.56px" class="cls_085"><span class="cls_085">P(C2) = 4/6</span></div>
<div style="position:absolute;left:66.10px;top:407.81px" class="cls_175"><span class="cls_175">C1</span></div>
<div style="position:absolute;left:158.64px;top:407.81px" class="cls_176"><span class="cls_176">2</span></div>
<div style="position:absolute;left:66.10px;top:438.72px" class="cls_175"><span class="cls_175">C2</span></div>
<div style="position:absolute;left:158.64px;top:438.72px" class="cls_176"><span class="cls_176">4</span></div>
<div style="position:absolute;left:241.20px;top:440.56px" class="cls_085"><span class="cls_085">Error = 1 - max (2/6, 4/6) = 1 - 4/6 = 1/3</span></div>
<div style="position:absolute;left:56.70px;top:507.60px" class="cls_006"><span class="cls_006">10/23/21</span></div>
<div style="position:absolute;left:248.06px;top:507.60px" class="cls_006"><span class="cls_006">Introduction to Data Mining, 2</span><span class="cls_007"><sup>nd</sup></span><span class="cls_006"> Edition</span></div>
<div style="position:absolute;left:649.95px;top:507.60px" class="cls_006"><span class="cls_006">51</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:28050px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background52.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:11.44px" class="cls_002"><span class="cls_002">Comparison among Impurity Measures</span></div>
<div style="position:absolute;left:37.20px;top:98.64px" class="cls_089"><span class="cls_089">For a 2-class problem:</span></div>
<div style="position:absolute;left:56.70px;top:507.60px" class="cls_006"><span class="cls_006">10/23/21</span></div>
<div style="position:absolute;left:248.06px;top:507.60px" class="cls_006"><span class="cls_006">Introduction to Data Mining, 2</span><span class="cls_007"><sup>nd</sup></span><span class="cls_006"> Edition</span></div>
<div style="position:absolute;left:649.95px;top:507.60px" class="cls_006"><span class="cls_006">52</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:28600px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background53.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:11.44px" class="cls_002"><span class="cls_002">Misclassification Error vs Gini Index</span></div>
<div style="position:absolute;left:563.69px;top:99.68px" class="cls_276"><span class="cls_276">Parent</span></div>
<div style="position:absolute;left:274.06px;top:106.96px" class="cls_145"><span class="cls_145">A?</span></div>
<div style="position:absolute;left:513.42px;top:131.10px" class="cls_275"><span class="cls_275">C1</span></div>
<div style="position:absolute;left:590.47px;top:131.10px" class="cls_276"><span class="cls_276">7</span></div>
<div style="position:absolute;left:187.40px;top:149.76px" class="cls_146"><span class="cls_146">Yes</span></div>
<div style="position:absolute;left:382.50px;top:149.76px" class="cls_146"><span class="cls_146">No</span></div>
<div style="position:absolute;left:513.42px;top:163.41px" class="cls_275"><span class="cls_275">C2</span></div>
<div style="position:absolute;left:590.47px;top:163.41px" class="cls_276"><span class="cls_276">3</span></div>
<div style="position:absolute;left:172.13px;top:196.80px" class="cls_146"><span class="cls_146">Node N1</span></div>
<div style="position:absolute;left:344.38px;top:196.80px" class="cls_146"><span class="cls_146">Node N2</span></div>
<div style="position:absolute;left:512.45px;top:195.05px" class="cls_276"><span class="cls_276">Gini = 0.42</span></div>
<div style="position:absolute;left:31.20px;top:284.56px" class="cls_085"><span class="cls_085">Gini(N1)</span></div>
<div style="position:absolute;left:300.21px;top:296.07px" class="cls_279"><span class="cls_279">N1</span></div>
<div style="position:absolute;left:346.05px;top:296.07px" class="cls_279"><span class="cls_279">N2</span></div>
<div style="position:absolute;left:451.20px;top:302.56px" class="cls_085"><span class="cls_085">Gini(Children)</span></div>
<div style="position:absolute;left:31.20px;top:308.56px" class="cls_085"><span class="cls_085">= 1 - (3/3)</span><span class="cls_086"><sup>2 </sup></span><span class="cls_085">- (0/3)</span><span class="cls_086"><sup>2</sup></span></div>
<div style="position:absolute;left:253.33px;top:322.39px" class="cls_278"><span class="cls_278">C1</span></div>
<div style="position:absolute;left:307.33px;top:322.39px" class="cls_279"><span class="cls_279">3</span></div>
<div style="position:absolute;left:353.24px;top:322.39px" class="cls_279"><span class="cls_279">4</span></div>
<div style="position:absolute;left:451.20px;top:326.56px" class="cls_085"><span class="cls_085">= 3/10 * 0</span></div>
<div style="position:absolute;left:31.20px;top:332.56px" class="cls_085"><span class="cls_085">= 0</span></div>
<div style="position:absolute;left:253.33px;top:349.41px" class="cls_278"><span class="cls_278">C2</span></div>
<div style="position:absolute;left:307.33px;top:349.41px" class="cls_279"><span class="cls_279">0</span></div>
<div style="position:absolute;left:353.24px;top:349.41px" class="cls_279"><span class="cls_279">3</span></div>
<div style="position:absolute;left:451.20px;top:350.56px" class="cls_085"><span class="cls_085">+ 7/10 * 0.489</span></div>
<div style="position:absolute;left:31.20px;top:368.56px" class="cls_085"><span class="cls_085">Gini(N2)</span></div>
<div style="position:absolute;left:257.08px;top:377.32px" class="cls_279"><span class="cls_279">Gini=0.342</span></div>
<div style="position:absolute;left:451.20px;top:374.56px" class="cls_085"><span class="cls_085">= 0.342</span></div>
<div style="position:absolute;left:31.20px;top:392.56px" class="cls_085"><span class="cls_085">= 1 - (4/7)</span><span class="cls_086"><sup>2 </sup></span><span class="cls_085">- (3/7)</span><span class="cls_086"><sup>2</sup></span></div>
<div style="position:absolute;left:451.20px;top:410.56px" class="cls_281"><span class="cls_281">Gini improves but</span></div>
<div style="position:absolute;left:31.20px;top:416.56px" class="cls_085"><span class="cls_085">= 0.489</span></div>
<div style="position:absolute;left:451.20px;top:434.56px" class="cls_281"><span class="cls_281">error remains the</span></div>
<div style="position:absolute;left:451.20px;top:458.56px" class="cls_281"><span class="cls_281">same!!</span></div>
<div style="position:absolute;left:56.70px;top:507.60px" class="cls_006"><span class="cls_006">10/23/21</span></div>
<div style="position:absolute;left:248.06px;top:507.60px" class="cls_006"><span class="cls_006">Introduction to Data Mining, 2</span><span class="cls_007"><sup>nd</sup></span><span class="cls_006"> Edition</span></div>
<div style="position:absolute;left:649.95px;top:507.60px" class="cls_006"><span class="cls_006">53</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:29150px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background54.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:11.44px" class="cls_002"><span class="cls_002">Misclassification Error vs Gini Index</span></div>
<div style="position:absolute;left:563.69px;top:99.68px" class="cls_276"><span class="cls_276">Parent</span></div>
<div style="position:absolute;left:274.06px;top:106.96px" class="cls_145"><span class="cls_145">A?</span></div>
<div style="position:absolute;left:513.42px;top:131.10px" class="cls_275"><span class="cls_275">C1</span></div>
<div style="position:absolute;left:590.47px;top:131.10px" class="cls_276"><span class="cls_276">7</span></div>
<div style="position:absolute;left:187.40px;top:149.76px" class="cls_146"><span class="cls_146">Yes</span></div>
<div style="position:absolute;left:382.50px;top:149.76px" class="cls_146"><span class="cls_146">No</span></div>
<div style="position:absolute;left:513.42px;top:163.41px" class="cls_275"><span class="cls_275">C2</span></div>
<div style="position:absolute;left:590.47px;top:163.41px" class="cls_276"><span class="cls_276">3</span></div>
<div style="position:absolute;left:172.13px;top:196.80px" class="cls_146"><span class="cls_146">Node N1</span></div>
<div style="position:absolute;left:344.38px;top:196.80px" class="cls_146"><span class="cls_146">Node N2</span></div>
<div style="position:absolute;left:512.45px;top:195.05px" class="cls_276"><span class="cls_276">Gini = 0.42</span></div>
<div style="position:absolute;left:132.21px;top:296.07px" class="cls_279"><span class="cls_279">N1</span></div>
<div style="position:absolute;left:178.05px;top:296.07px" class="cls_279"><span class="cls_279">N2</span></div>
<div style="position:absolute;left:376.88px;top:296.17px" class="cls_283"><span class="cls_283">N1</span></div>
<div style="position:absolute;left:421.81px;top:296.17px" class="cls_283"><span class="cls_283">N2</span></div>
<div style="position:absolute;left:85.33px;top:322.39px" class="cls_278"><span class="cls_278">C1</span></div>
<div style="position:absolute;left:139.33px;top:322.39px" class="cls_279"><span class="cls_279">3</span></div>
<div style="position:absolute;left:185.24px;top:322.39px" class="cls_279"><span class="cls_279">4</span></div>
<div style="position:absolute;left:330.95px;top:322.29px" class="cls_282"><span class="cls_282">C1</span></div>
<div style="position:absolute;left:383.86px;top:322.29px" class="cls_283"><span class="cls_283">3</span></div>
<div style="position:absolute;left:428.86px;top:322.29px" class="cls_283"><span class="cls_283">4</span></div>
<div style="position:absolute;left:85.33px;top:349.41px" class="cls_278"><span class="cls_278">C2</span></div>
<div style="position:absolute;left:139.33px;top:349.41px" class="cls_279"><span class="cls_279">0</span></div>
<div style="position:absolute;left:185.24px;top:349.41px" class="cls_279"><span class="cls_279">3</span></div>
<div style="position:absolute;left:330.95px;top:349.12px" class="cls_282"><span class="cls_282">C2</span></div>
<div style="position:absolute;left:383.86px;top:349.12px" class="cls_283"><span class="cls_283">1</span></div>
<div style="position:absolute;left:428.86px;top:349.12px" class="cls_283"><span class="cls_283">2</span></div>
<div style="position:absolute;left:89.08px;top:377.32px" class="cls_279"><span class="cls_279">Gini=0.342</span></div>
<div style="position:absolute;left:334.62px;top:376.82px" class="cls_283"><span class="cls_283">Gini=0.416</span></div>
<div style="position:absolute;left:97.70px;top:446.40px" class="cls_208"><span class="cls_208">Misclassification error for all three cases = 0.3 !</span></div>
<div style="position:absolute;left:56.70px;top:507.60px" class="cls_006"><span class="cls_006">10/23/21</span></div>
<div style="position:absolute;left:248.06px;top:507.60px" class="cls_006"><span class="cls_006">Introduction to Data Mining, 2</span><span class="cls_007"><sup>nd</sup></span><span class="cls_006"> Edition</span></div>
<div style="position:absolute;left:649.95px;top:507.60px" class="cls_006"><span class="cls_006">54</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:29700px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background55.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:11.44px" class="cls_002"><span class="cls_002">Decision Tree Based Classification</span></div>
<div style="position:absolute;left:39.50px;top:74.40px" class="cls_103"><span class="cls_103">●</span><span class="cls_014"> Advantages:</span></div>
<div style="position:absolute;left:75.50px;top:110.48px" class="cls_285"><span class="cls_285">-</span><span class="cls_286"> Inexpensive to construct</span></div>
<div style="position:absolute;left:75.50px;top:143.60px" class="cls_285"><span class="cls_285">-</span><span class="cls_286"> Extremely fast at classifying unknown records</span></div>
<div style="position:absolute;left:75.50px;top:176.48px" class="cls_285"><span class="cls_285">-</span><span class="cls_286"> Easy to interpret for small-sized trees</span></div>
<div style="position:absolute;left:75.50px;top:209.60px" class="cls_285"><span class="cls_285">-</span><span class="cls_286"> Robust to noise (especially when methods to avoid</span></div>
<div style="position:absolute;left:102.50px;top:235.52px" class="cls_286"><span class="cls_286">overfitting are employed)</span></div>
<div style="position:absolute;left:75.50px;top:268.40px" class="cls_285"><span class="cls_285">-</span><span class="cls_286"> Can easily handle redundant or irrelevant attributes (unless</span></div>
<div style="position:absolute;left:102.50px;top:295.52px" class="cls_286"><span class="cls_286">the attributes are interacting)</span></div>
<div style="position:absolute;left:39.50px;top:328.56px" class="cls_103"><span class="cls_103">●</span><span class="cls_014"> Disadvantages:</span></div>
<div style="position:absolute;left:75.50px;top:364.40px" class="cls_285"><span class="cls_285">-</span><span class="cls_286"> Space of possible decision trees is exponentially large.</span></div>
<div style="position:absolute;left:102.50px;top:390.56px" class="cls_286"><span class="cls_286">Greedy approaches are often unable to find the best tree.</span></div>
<div style="position:absolute;left:75.50px;top:423.44px" class="cls_285"><span class="cls_285">-</span><span class="cls_286"> Does not take into account interactions between attributes</span></div>
<div style="position:absolute;left:75.50px;top:456.56px" class="cls_285"><span class="cls_285">-</span><span class="cls_286"> Each decision boundary involves only a single attribute</span></div>
<div style="position:absolute;left:56.70px;top:507.60px" class="cls_006"><span class="cls_006">10/23/21</span></div>
<div style="position:absolute;left:248.06px;top:507.60px" class="cls_006"><span class="cls_006">Introduction to Data Mining, 2</span><span class="cls_007"><sup>nd</sup></span><span class="cls_006"> Edition</span></div>
<div style="position:absolute;left:649.95px;top:507.60px" class="cls_006"><span class="cls_006">55</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:30250px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background56.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:11.44px" class="cls_002"><span class="cls_002">Handling interactions</span></div>
<div style="position:absolute;left:327.20px;top:99.60px" class="cls_287"><span class="cls_287">+ : 1000 instances</span></div>
<div style="position:absolute;left:517.20px;top:99.60px" class="cls_120"><span class="cls_120">Entropy (X) : 0.99</span></div>
<div style="position:absolute;left:517.20px;top:120.48px" class="cls_120"><span class="cls_120">Entropy (Y) : 0.99</span></div>
<div style="position:absolute;left:327.20px;top:142.56px" class="cls_192"><span class="cls_192">o : 1000 instances</span></div>
<div style="position:absolute;left:34.83px;top:165.60px" class="cls_120"><span class="cls_120">Y</span></div>
<div style="position:absolute;left:181.20px;top:273.60px" class="cls_120"><span class="cls_120">X</span></div>
<div style="position:absolute;left:56.70px;top:507.60px" class="cls_006"><span class="cls_006">10/23/21</span></div>
<div style="position:absolute;left:248.06px;top:507.60px" class="cls_006"><span class="cls_006">Introduction to Data Mining, 2</span><span class="cls_007"><sup>nd</sup></span><span class="cls_006"> Edition</span></div>
<div style="position:absolute;left:649.95px;top:507.60px" class="cls_006"><span class="cls_006">56</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:30800px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background57.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.13px;top:11.44px" class="cls_002"><span class="cls_002">Handling interactions</span></div>
<div style="position:absolute;left:327.20px;top:99.60px" class="cls_287"><span class="cls_287">+ : 1000 instances</span></div>
<div style="position:absolute;left:526.20px;top:101.04px" class="cls_120"><span class="cls_120">Entropy (X) : 0.99</span></div>
<div style="position:absolute;left:526.20px;top:122.16px" class="cls_120"><span class="cls_120">Entropy (Y) : 0.99</span></div>
<div style="position:absolute;left:327.20px;top:142.56px" class="cls_192"><span class="cls_192">o : 1000 instances</span></div>
<div style="position:absolute;left:526.20px;top:144.00px" class="cls_120"><span class="cls_120">Entropy (Z) : 0.98</span></div>
<div style="position:absolute;left:34.83px;top:165.60px" class="cls_120"><span class="cls_120">Y</span></div>
<div style="position:absolute;left:327.20px;top:185.52px" class="cls_120"><span class="cls_120">Adding Z as a noisy</span></div>
<div style="position:absolute;left:526.20px;top:187.20px" class="cls_208"><span class="cls_208">Attribute Z will be</span></div>
<div style="position:absolute;left:327.20px;top:207.60px" class="cls_120"><span class="cls_120">attribute generated</span></div>
<div style="position:absolute;left:526.20px;top:209.04px" class="cls_208"><span class="cls_208">chosen for splitting!</span></div>
<div style="position:absolute;left:327.20px;top:228.48px" class="cls_120"><span class="cls_120">from a uniform</span></div>
<div style="position:absolute;left:327.20px;top:250.56px" class="cls_120"><span class="cls_120">distribution</span></div>
<div style="position:absolute;left:181.20px;top:273.60px" class="cls_120"><span class="cls_120">X</span></div>
<div style="position:absolute;left:31.20px;top:375.60px" class="cls_120"><span class="cls_120">Z</span></div>
<div style="position:absolute;left:391.20px;top:375.60px" class="cls_120"><span class="cls_120">Z</span></div>
<div style="position:absolute;left:169.20px;top:477.60px" class="cls_120"><span class="cls_120">X</span></div>
<div style="position:absolute;left:529.20px;top:477.60px" class="cls_120"><span class="cls_120">Y</span></div>
<div style="position:absolute;left:56.70px;top:507.60px" class="cls_006"><span class="cls_006">10/23/21</span></div>
<div style="position:absolute;left:248.06px;top:507.60px" class="cls_006"><span class="cls_006">Introduction to Data Mining, 2</span><span class="cls_007"><sup>nd</sup></span><span class="cls_006"> Edition</span></div>
<div style="position:absolute;left:649.95px;top:507.60px" class="cls_006"><span class="cls_006">57</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:31350px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background58.jpg" width=720 height=540></div>
<div style="position:absolute;left:13.13px;top:28.84px" class="cls_288"><span class="cls_288">Limitations of single attribute-based decision boundaries</span></div>
<div style="position:absolute;left:490.70px;top:146.56px" class="cls_085"><span class="cls_085">oth</span><span class="cls_289"> positive (+)</span><span class="cls_085"> and</span></div>
<div style="position:absolute;left:488.45px;top:170.56px" class="cls_281"><span class="cls_281">egative (o)</span><span class="cls_085"> classes</span></div>
<div style="position:absolute;left:488.45px;top:194.56px" class="cls_085"><span class="cls_085">enerated from</span></div>
<div style="position:absolute;left:487.33px;top:218.56px" class="cls_085"><span class="cls_085">kewed Gaussians</span></div>
<div style="position:absolute;left:491.70px;top:242.56px" class="cls_085"><span class="cls_085">ith centers at (8,8)</span></div>
<div style="position:absolute;left:487.33px;top:266.56px" class="cls_085"><span class="cls_085">nd (12,12)</span></div>
<div style="position:absolute;left:495.08px;top:290.56px" class="cls_085"><span class="cls_085">spectively.</span></div>
<div style="position:absolute;left:56.70px;top:507.60px" class="cls_006"><span class="cls_006">10/23/21</span></div>
<div style="position:absolute;left:248.06px;top:507.60px" class="cls_006"><span class="cls_006">Introduction to Data Mining, 2</span><span class="cls_007"><sup>nd</sup></span><span class="cls_006"> Edition</span></div>
<div style="position:absolute;left:649.95px;top:507.60px" class="cls_006"><span class="cls_006">58</span></div>
</div>

</body>
</html>
